package es.uji.apps.gdo.dao;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.model.TitulacionPersona;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TitulacionDAOTest extends BaseGDOTest
{
    @Test
    public void existenTitulacionesPorPersona()
    {
        List<TitulacionPersona> listaTitulaciones = titulacionDAO
                .getTitulacionesByPersona(9999L);

        Assert.assertNotNull(listaTitulaciones);
    }

//    @Test
//    public void existeTitulacionPorAsingaturaId() throws AsignaturaNoAsignadaATitulacion
//    {
//        Titulacion listaTitulaciones = titulacionDAO.getTitulacionByAsignaturaId(9999L);
//
//        Assert.assertNotNull(listaTitulaciones);
//    }
}