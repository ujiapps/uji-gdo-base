package es.uji.apps.gdo.dao;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Random;

import javax.sql.DataSource;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseGDOTest
{
    @Autowired
    protected ActividadDAO actividadDAO;

    @Autowired
    protected ApartadoDAO apartadoDAO;

    @Autowired
    protected AsignaturaDAO asignaturaDAO;

    @Autowired
    protected IdiomaAsignaturaDAO idiomaDAO;

    @Autowired
    protected DashboardDAO dashboardDAO;

    @Autowired
    protected TitulacionDAO titulacionDAO;

    @Autowired
    protected DataSource dataSource;

    protected Random random;

    public BaseGDOTest()
    {
        random = new Random();
    }

    @Before
    public void initDB() throws MalformedURLException, SQLException
    {
//        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File(
//                "src/main/resources/dataset.xml"));
//        IDatabaseConnection dbConn = new DatabaseDataSourceConnection(dataSource);
//
//        DatabaseConfig config = dbConn.getConfig();
//        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
//
//        DatabaseOperation.REFRESH.execute(dbConn, dataSet);
    }
}