package es.uji.apps.gdo.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.model.ActividadAsignatura;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class ActividadDAOTest extends BaseGDOTest
{
    @SuppressWarnings("unused")
    @Autowired
    private ApplicationContext applicationContext;
    
//    @Test
//    public void existenActividadesPorCodigoAsignatura()
//    {
//        List<ActividadAsignatura> actividadesList = actividadDAO.getActividadesByCodAsignatura(
//                "XXX001", 9999L);
//
//        Object object = applicationContext.getBean(ActividadDAODatabaseImpl.class);
//        
//        Assert.assertNotNull(actividadesList);
//        Assert.assertTrue(actividadesList.size() > 0);
//    }

//    @Test
//    public void existenActividadesPorAsignaturaId()
//    {
//        List<ActividadAsignatura> actividadesList = actividadDAO.getActividadesByAsignaturaId(9999L);
//
//        Assert.assertNotNull(actividadesList);
//        Assert.assertTrue(actividadesList.size() > 0);
//    }

    @Test
    public void existeActividadPorIdActividad()
    {
        ActividadAsignatura actividad = actividadDAO.getActividad(9999L);
        Assert.assertNotNull(actividad);
    }

//    @Test
//    public void updateCorrectoEnActividad()
//    {
//        ActividadAsignatura actividad = actividadDAO.getActividad(9999L);
//        Assert.assertNotNull(actividad);
//
//        Float valorHoras = new Float(random.nextInt());
//
//        actividad.setHorasNoPresenciales(valorHoras);
//        actividadDAO.update(actividad);
//
//        actividad = actividadDAO.getActividad(9999L);
//        Assert.assertNotNull(actividad);
//        Assert.assertEquals(valorHoras, actividad.getHorasNoPresenciales());
//    }
}
