package es.uji.apps.gdo.model;

import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Collections;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.EvaluacionDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public class EvaluacionTest
{
    private EvaluacionDAO evaluacionDAO;

    @Before
    public void initDAOMocks()
    {
        evaluacionDAO = Mockito.mock(EvaluacionDAO.class);
    }

    private Calendar getFechaActualAnyadiendoDias(int dias)
    {
        Calendar fecha = Calendar.getInstance();
        fecha.add(Calendar.DAY_OF_WEEK, dias);

        return fecha;
    }

    private Asignatura getAsignaturaModicable()
    {
        Asignatura asignatura = new Asignatura();
        CursoAcademico cursoAcademico = new CursoAcademico();
        cursoAcademico.setActivo(true);
        cursoAcademico.setFechaIni(getFechaActualAnyadiendoDias(-1).getTime());
        cursoAcademico.setFechaFin(getFechaActualAnyadiendoDias(1).getTime());
        asignatura.setGdoCursosAcademico(cursoAcademico);

        return asignatura;
    }

    private Asignatura getAsignaturaNoModificable()
    {
        Asignatura asignatura = new Asignatura();
        asignatura.setFechaIni(getFechaActualAnyadiendoDias(-2).getTime());
        asignatura.setFechaFin(getFechaActualAnyadiendoDias(-1).getTime());

        return asignatura;
    }

    @Test
    public void evaluacionNoEditableSiAsignaturaNoModificable()
    {
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setEditable(true);
        Asignatura asignatura = getAsignaturaNoModificable();
        evaluacion.setGdoAsignatura(asignatura);

        Assert.assertFalse(evaluacion.isEditable());
    }

    @Test
    public void evaluacionEditableSiAsignaturaModificable()
    {
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setEditable(true);

        Asignatura asignatura = getAsignaturaModicable();
        evaluacion.setGdoAsignatura(asignatura);

        Assert.assertTrue(evaluacion.isEditable());
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void deleteEvaluacionLanzaExcepcionSiAsignaturaNoModificable()
            throws AsignaturaNoModificableException, RegistroNoEncontradoException,
            AsignaturaNoAsignadaATitulacion
    {
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setId(1L);
        evaluacion.setEditable(true);
        Asignatura asignatura = getAsignaturaNoModificable();
        evaluacion.setGdoAsignatura(asignatura);
        evaluacion.setEvaluacionDAO(evaluacionDAO);

        when(evaluacionDAO.get(Evaluacion.class, 1L)).thenReturn(
                Collections.singletonList(evaluacion));

        Evaluacion.delete(evaluacion.getId());
    }
}
