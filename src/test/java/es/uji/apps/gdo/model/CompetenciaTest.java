package es.uji.apps.gdo.model;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.CompetenciaDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public class CompetenciaTest
{
    private CompetenciaDAO competenciaDAO;

    @Before
    public void initMocks()
    {
        competenciaDAO = mock(CompetenciaDAO.class);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void updateErrorSiLaCompetenciaNoEsEditable() throws AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion
    {
        Competencia competencia = getCompetenciaNoEditable();
        competencia.update();
    }

    @Test
    @Ignore
    public void updateOKCuandoCompetenciaEditableYAsignaturaModificableYEstudioMaster()
            throws Exception
    {
        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(true);
        when(asignatura.getTitulacion()).thenReturn(getTitulacionMaster());

        Competencia competencia = getCompetenciaEditable(asignatura);

        verify(competenciaDAO).update(competencia);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void updateErrorSiCompetenciaEditableYAsignaturaNoModificable() throws Exception
    {
        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(false);

        getCompetenciaEditable(asignatura);
    }

    private Competencia getCompetenciaEditable(Asignatura asignatura)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Competencia competencia = getCompetenciaEditable();
        competencia.setGdoAsignatura(asignatura);
        competencia.update();
        return competencia;
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void updateErrorSiCompetenciaEditableYAsignaturaModificableYEstudioGrado()
            throws Exception
    {
        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(true);
        when(asignatura.getTitulacion()).thenReturn(getTitulacionGrado());

        getCompetenciaEditable(asignatura);
    }

    private Titulacion getTitulacionMaster()
    {
        Titulacion titulacion = new Titulacion();
        titulacion.setTipo("M");

        return titulacion;
    }
    
    private Titulacion getTitulacionGrado()
    {
        Titulacion titulacion = new Titulacion();
        titulacion.setTipo("G");

        return titulacion;
    }

    private Competencia getCompetenciaEditable()
    {
        Competencia competencia = new Competencia();
        competencia.setCompetenciaDAO(competenciaDAO);
        competencia.setEditable("S");

        return competencia;
    }

    private Competencia getCompetenciaNoEditable()
    {
        Competencia competencia = getCompetenciaEditable();
        competencia.setEditable("N");

        return competencia;
    }

    @Test(expected = RegistroNoEncontradoException.class)
    public void deleteErrorSiCompetenciaABorrarNoEncontrada() throws Exception
    {
        new Competencia().setCompetenciaDAO(competenciaDAO);

        Competencia.delete(1L);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void deleteErrorSiCompetenciaABorrarNoEditable() throws Exception
    {
        Competencia competencia = getCompetenciaNoEditable();
        when(competenciaDAO.get(Competencia.class, 1L)).thenReturn(
                Collections.singletonList(competencia));

        Competencia.delete(1L);
    }

    @Test
    @Ignore
    public void deleteOKSiCompetenciaABorrarExisteYEditable() throws Exception
    {
        Competencia competencia = getCompetenciaEditable();
        when(competenciaDAO.get(Competencia.class, 1L)).thenReturn(
                Collections.singletonList(competencia));

        Titulacion titulacionMaster = getTitulacionMaster();

        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.getTitulacion()).thenReturn(titulacionMaster);

        when(asignatura.isModificable()).thenReturn(true);

        competencia.setGdoAsignatura(asignatura);

        Competencia.delete(1L);

        verify(competenciaDAO).delete(Competencia.class, 1L);
    }

    @After
    public void cleanMocks()
    {
        new Competencia().setCompetenciaDAO(null);
    }
}