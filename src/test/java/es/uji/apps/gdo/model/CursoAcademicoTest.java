package es.uji.apps.gdo.model;

import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import es.uji.apps.gdo.CursoAcademicoSoloUnoActivo;
import es.uji.apps.gdo.dao.CursoAcademicoDAO;

public class CursoAcademicoTest
{
    private CursoAcademico cursoAcademico;
    private CursoAcademicoDAO cursoAcademicoDAO;

    @Before
    public void initDAOMocks()
    {
        cursoAcademicoDAO = Mockito.mock(CursoAcademicoDAO.class);
        cursoAcademico = new CursoAcademico();
        cursoAcademico.setCursoAcademicoDAO(cursoAcademicoDAO);
    }

    @Test
    public void elCursoEsVisibleSiHoyEstaEntreFechas()
    {
        Calendar ayer = getAyer();
        Calendar manana = getManyana();

        cursoAcademico.setFechaIni(ayer.getTime());
        cursoAcademico.setFechaFin(manana.getTime());
        Assert.assertTrue(cursoAcademico.isVisible());
    }

    private Calendar getManyana()
    {
        Calendar manana = Calendar.getInstance();
        manana.add(Calendar.DAY_OF_WEEK, 1);

        return manana;
    }

    private Calendar getAyer()
    {
        Calendar ayer = Calendar.getInstance();
        ayer.add(Calendar.DAY_OF_WEEK, -1);
        return ayer;
    }

    @Test
    public void elCursoNoEsVisibleSiHoyNoEstaEntreFechas()
    {
        Calendar antesDeAyer = getAntesDeAyer();
        Calendar ayer = getAyer();

        cursoAcademico.setFechaIni(antesDeAyer.getTime());
        cursoAcademico.setFechaFin(ayer.getTime());
        Assert.assertFalse(cursoAcademico.isVisible());
    }

    private Calendar getAntesDeAyer()
    {
        Calendar ayer = Calendar.getInstance();
        ayer.add(Calendar.DAY_OF_WEEK, -2);
        return ayer;
    }

    @Test
    public void devuelveElCursoActivoSiLoHay()
    {
        cursoAcademico.setCursoAca(2011L);
        when(cursoAcademicoDAO.getCursoActivo()).thenReturn(cursoAcademico);

        Assert.assertTrue(CursoAcademico.getCursoActivo().equals(cursoAcademico));
    }

    @Test
    public void devuelveUnCursoActivoNuevoSiNoHayCursoActivo()
    {
        when(cursoAcademicoDAO.getCursoActivo()).thenReturn(new CursoAcademico());

        Assert.assertNull(CursoAcademico.getCursoActivo().getCursoAca());

    }

    @Test(expected = CursoAcademicoSoloUnoActivo.class)
    public void devuelveUnaExcepcionSiHayMasDeUnCursoActivo() throws CursoAcademicoSoloUnoActivo
    {
        cursoAcademico.setCursoAca(2011L);
        cursoAcademico.setActivo(true);

        CursoAcademico otroCursoActivo = new CursoAcademico();
        otroCursoActivo.setCursoAca(2012L);

        when(cursoAcademicoDAO.getCursoActivo()).thenReturn(otroCursoActivo);
        cursoAcademico.update();
    }

    @After
    public void cleanDAOMocks()
    {
        cursoAcademicoDAO = null;
    }
}
