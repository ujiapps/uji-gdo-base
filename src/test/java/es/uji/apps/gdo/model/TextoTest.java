package es.uji.apps.gdo.model;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

import es.uji.apps.gdo.AsignaturaNoModificableException;

public class TextoTest
{

    @Test(expected = AsignaturaNoModificableException.class)
    public void updateDevuelveExcepcionSiLaAsignaturaNoEsModificable()
            throws AsignaturaNoModificableException
    {
        Asignatura asignatura = Mockito.mock(Asignatura.class);
        Texto texto = new Texto();
        texto.setGdoAsignatura(asignatura);

        when(asignatura.isModificable()).thenReturn(false);
        texto.update();
    }
}
