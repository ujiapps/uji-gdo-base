package es.uji.apps.gdo.model;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.ActividadDAO;
import es.uji.apps.gdo.dao.AsignaturaDAO;
import es.uji.apps.gdo.dao.CursoAcademicoDAO;
import es.uji.apps.gdo.dao.TitulacionDAO;
import es.uji.apps.gdo.model.domains.EstadoAsignatura;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public class ActividadAsignaturaTest
{
    private Titulacion titulacion;
    private TitulacionDAO titulacionDAO;
    private ActividadDAO actividadDAO;
    private AsignaturaDAO asignaturaDAO;
    private CursoAcademicoDAO cursoAcademicoDAO;

    public ActividadAsignaturaTest()
    {
        titulacion = new Titulacion();
    }

    @Before
    public void initDAOMock()
    {
        titulacionDAO = mock(TitulacionDAO.class);
        actividadDAO = mock(ActividadDAO.class);
        asignaturaDAO = mock(AsignaturaDAO.class);
        cursoAcademicoDAO = mock(CursoAcademicoDAO.class);

        titulacion.setTitulacionDAO(titulacionDAO);
    }

    private Date getFecha(int dia, int mes, int anyo)
    {
        Calendar fecha = Calendar.getInstance();
        fecha.set(anyo, mes - 1, dia);

        return fecha.getTime();
    }

    private ActividadAsignatura getAsignaturaDeMasterNoActiva()
    {
        Asignatura asignatura = new Asignatura();
        asignatura.setId(1L);
        asignatura.setFechaIni(getFecha(1, 1, 2010));
        asignatura.setFechaFin(getFecha(1, 1, 2011));

        CursoAcademico cursoAcademicoActivo = new CursoAcademico();
        cursoAcademicoActivo.setActivo(true);

        CursoAcademicoDAO cursoAcademicoDAO = mock(CursoAcademicoDAO.class);
        when(cursoAcademicoDAO.getCursoAcademicoByAsignatura(anyLong())).thenReturn(
                cursoAcademicoActivo);

        CursoAcademico cursoAcademico = new CursoAcademico();
        cursoAcademico.setCursoAca(1L);
        cursoAcademico.setCursoAcademicoDAO(cursoAcademicoDAO);
        asignatura.setGdoCursosAcademico(cursoAcademico);

        ActividadAsignatura actividadAsignatura = new ActividadAsignatura();
        actividadAsignatura.setId(1L);
        actividadAsignatura.setGdoAsignatura(asignatura);
        return actividadAsignatura;
    }

    private Titulacion getGdoExtTituTodas()
    {
        Titulacion titulacionDB = new Titulacion();
        titulacionDB.setId(1L);
        titulacionDB.setNombre("Titulación test");
        titulacionDB.setTipo("M");
        return titulacionDB;
    }

    private CursoAcademico getGdoCursosAcademicos()
    {
        CursoAcademico cursoAcademicoDB = new CursoAcademico();
        cursoAcademicoDB.setCursoAca(2010L);
        cursoAcademicoDB.setActivo(true);
        return cursoAcademicoDB;
    }

    private AsignaturaPOD getGdoExtAsiTodas()
    {
        AsignaturaPOD extAsignatura = new AsignaturaPOD();

        extAsignatura.setId("XX001");
        extAsignatura.setNombreCa("Asignatura test");
        extAsignatura.setNombreEs("Asignatura test");
        extAsignatura.setNombreUk("Asignatura test");
        return extAsignatura;
    }

    private Asignatura getGdoAsignatura()
    {
        Asignatura asignaturaDB = new Asignatura();
        asignaturaDB.setId(1L);
        asignaturaDB.setAutorizaPas("S");
        asignaturaDB.setEstado("D");
        asignaturaDB.setFechaIni(getFecha(1, 1, 2011));
        asignaturaDB.setFechaFin(getFecha(1, 1, 2012));
        return asignaturaDB;
    }

    private ActividadAsignatura getAsignaturaDeMasterActiva()
            throws AsignaturaNoAsignadaATitulacion
    {
        Asignatura asignaturaDB = getGdoAsignatura();
        asignaturaDB.setGdoExtAsiToda(getGdoExtAsiTodas());

        CursoAcademico cursoAcademico = new CursoAcademico();
        cursoAcademico.setCursoAcademicoDAO(cursoAcademicoDAO);
        cursoAcademico.setActivo(true);

        when(cursoAcademicoDAO.getCursoAcademicoByAsignatura(anyLong())).thenReturn(
                getGdoCursosAcademicos());

        Asignatura asignaturaModel = new Asignatura();
        asignaturaModel.setEstado(EstadoAsignatura.D.toString());
        asignaturaModel.setGdoCursosAcademico(cursoAcademico);
        asignaturaModel.setAsignaturaDAO(asignaturaDAO);
        asignaturaModel.setFechaIni(getFecha(1, 1, 1900));
        asignaturaModel.setFechaFin(getFecha(1, 1, 9999));

        Titulacion titulacion = new Titulacion();
        titulacion.setTitulacionDAO(titulacionDAO);

        when(titulacionDAO.getTitulacionByAsignaturaId(anyLong())).thenReturn(getGdoExtTituTodas());

        ActividadAsignatura actividadAsignatura = new ActividadAsignatura();
        actividadAsignatura.setId(1L);

        when(actividadDAO.get(ActividadAsignatura.class, 1L)).thenReturn(
                (List<ActividadAsignatura>) Collections.singletonList(actividadAsignatura));

        actividadAsignatura.setActividadDAO(actividadDAO);
        actividadAsignatura.setGdoAsignatura(asignaturaModel);

        return actividadAsignatura;
    }
    

    private ActividadAsignatura getAsignaturaDeGrado() throws AsignaturaNoAsignadaATitulacion
    {
        titulacion.setTipo("G");
        when(titulacionDAO.getTitulacionByAsignaturaId(anyLong())).thenReturn(titulacion);

        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(true);
        when(asignatura.getTitulacion()).thenReturn(titulacion);

        ActividadAsignatura actividadAsignatura = new ActividadAsignatura();
        actividadAsignatura.setActividadDAO(actividadDAO);
        actividadAsignatura.setGdoAsignatura(asignatura);

        when(actividadDAO.get(ActividadAsignatura.class, 1L)).thenReturn(
                (List<ActividadAsignatura>) Collections.singletonList(actividadAsignatura));
        
        return actividadAsignatura;
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void actividadAsignaturaNoActualizableCuandoAsignaturaNoActiva()
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        ActividadAsignatura actividadAsignatura = getAsignaturaDeMasterNoActiva();

        actividadAsignatura.update();
    }

    @SuppressWarnings("unchecked")
    @Test(expected = RegistroNoEncontradoException.class)
    public void registroNoEcontradoExceptionParaCodigoDeAsignaturaIncorrecto() throws Exception
    {
        ActividadAsignatura actividadAsignatura = new ActividadAsignatura();
        actividadAsignatura.setActividadDAO(actividadDAO);

        when(actividadDAO.get(ActividadAsignatura.class, 1L)).thenReturn(Collections.EMPTY_LIST);

        ActividadAsignatura.delete(-1L);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void asignaturaNoModificableExceptionParaAsignaturaNoEditableBorrando() throws Exception
    {
        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(false);

        ActividadAsignatura actividadAsignatura = new ActividadAsignatura();
        actividadAsignatura.setActividadDAO(actividadDAO);
        actividadAsignatura.setGdoAsignatura(asignatura);

        when(actividadDAO.get(ActividadAsignatura.class, 1L)).thenReturn(
                (List<ActividadAsignatura>) Collections.singletonList(actividadAsignatura));

        ActividadAsignatura.delete(1L);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void asignaturaNoModificableParaAsignaturaDeGradoBorrando() throws Exception
    {
        getAsignaturaDeGrado();

        ActividadAsignatura.delete(1L);
    }

    @Test
    @Ignore
    public void actividadAsignaturaActualizableCuandoAsignaturaActivaYTitulacionMaster()
            throws Exception
    {
        ActividadAsignatura actividadAsignatura = getAsignaturaDeMasterActiva();
        actividadAsignatura.update();

        verify(actividadDAO).update(actividadAsignatura);
    }

    @Test
    @Ignore
    @SuppressWarnings("static-access")
    public void actividadAsignaturaBorrableCuandoAsignaturaActivaYTitulacionMaster()
            throws Exception
    {
        ActividadAsignatura asignaturaDeMasterActiva = getAsignaturaDeMasterActiva();
        asignaturaDeMasterActiva.delete(1L);

        verify(actividadDAO).delete(ActividadAsignatura.class, 1L);
    }

    @Test
    @Ignore
    public void actividadAsignaturaInsertableCuandoAsignaturaActivaYTitulacionMaster()
            throws Exception
    {
        ActividadAsignatura asignaturaDeMasterActiva = getAsignaturaDeMasterActiva();
        asignaturaDeMasterActiva.insert();

        verify(actividadDAO).insert(asignaturaDeMasterActiva);
    }
    
    @Test(expected = AsignaturaNoModificableException.class)
    public void asignaturaNoModificableParaAsignaturaDeGradoInsertando() throws Exception
    {
        ActividadAsignatura actividadAsignatura = getAsignaturaDeGrado();

        actividadAsignatura.insert();
    }

    @After
    public void cleanDAOMocks()
    {
        new Titulacion().setTitulacionDAO(null);
        new Asignatura().setAsignaturaDAO(null);
        new CursoAcademico().setCursoAcademicoDAO(null);
        new ActividadAsignatura().setActividadDAO(null);
    }
}