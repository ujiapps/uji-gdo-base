package es.uji.apps.gdo.model;

import es.uji.apps.gdo.AsignaturaBasadaEnIncorrecta;
import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.CamposObligatoriosNoRellenadosException;
import es.uji.apps.gdo.dao.AsignaturaDAO;
import es.uji.apps.gdo.dao.CursoAcademicoDAO;
import es.uji.apps.gdo.model.domains.EstadoAsignatura;
import es.uji.commons.messaging.client.MissingConfigurationFileException;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.sso.dao.ApaDAO;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AsignaturaTest
{
    static final long UMBRAL_COMPARACION_FECHAS = 5000L;

    private AsignaturaDAO asignaturaDAO;
    private ApaDAO apaDAO;
    private CursoAcademicoDAO cursoAcademicoDAO;
    private Asignatura asignatura;
    private CursoAcademico cursoAcademico;
    private Persona persona;
    private static Calendar ayer;
    private static Calendar manyana;

    @BeforeClass
    public static void init()
    {
        ayer = getAyer();
        manyana = getManyana();
    }

    @Before
    public void initDAOMocks()
    {
        asignaturaDAO = mock(AsignaturaDAO.class);
        cursoAcademicoDAO = mock(CursoAcademicoDAO.class);
        apaDAO = mock(ApaDAO.class);

        cursoAcademico = new CursoAcademico();
        cursoAcademico.setCursoAcademicoDAO(cursoAcademicoDAO);

        asignatura = new Asignatura();
        asignatura.setAsignaturaDAO(asignaturaDAO);

        persona = new Persona();
        persona.setApaDAO(apaDAO);
    }

    @Test
    public void asignaturaActivaCuandoFechaActualEntreFechaInicioYFechaFin()
            throws AsignaturaNoModificableException
    {
        cursoAcademico = buildCursoAcademicoModificable();
        asignatura = buildAsignaturaModificable();
        when(cursoAcademicoDAO.getCursoAcademicoByAsignatura(anyLong())).thenReturn(cursoAcademico);
        Assert.assertTrue(asignatura.isActiva());
    }

    @Test(expected = CamposObligatoriosNoRellenadosException.class)
    public void siLaPersonaNoEsAdminYLaAsignaturaEsModificableNoPuedePasarseAEstadoValidadoSiNoTieneLosApartadosCompletados()
            throws AsignaturaNoModificableException, CamposObligatoriosNoRellenadosException,
            MissingConfigurationFileException, AsignaturaNoAsignadaATitulacion,
            FieldValidationException, AsignaturaBasadaEnIncorrecta
    {
        // La asignatura es modificable
        asignatura = buildAsignaturaModificable();
        // La persona no es administrador
        when(apaDAO.hasPerfil(anyString(), anyString(), anyLong())).thenReturn(false);
        // Los apartados no están completos
        when(asignaturaDAO.apartadosObligatoriosLlenos(anyLong(), anyString())).thenReturn(5L);
        when(asignaturaDAO.apartadosObligatoriosTotales(anyLong())).thenReturn(10L);
        // La asignatura está en estado pendiente de validar
        asignatura.setEstado(EstadoAsignatura.V.name());

        asignatura.updateAsignaturaSiEsModificable(100L);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void siLaPersonaNoEsAdminYLaAsignaturaNoEsModificableNoPuedePasarseAEstadoValidado()
            throws AsignaturaNoModificableException, CamposObligatoriosNoRellenadosException,
            MissingConfigurationFileException, AsignaturaNoAsignadaATitulacion,
            FieldValidationException, AsignaturaBasadaEnIncorrecta
    {
        // La asignatura no es modificable
        asignatura = buildAsignaturaNoModificable();
        // La persona no es administrador
        when(apaDAO.hasPerfil(anyString(), anyString(), anyLong())).thenReturn(false);
        // Los apartados no están completos
        when(asignaturaDAO.apartadosObligatoriosLlenos(anyLong(), anyString())).thenReturn(5L);
        when(asignaturaDAO.apartadosObligatoriosTotales(anyLong())).thenReturn(10L);
        // La asignatura está en estado pendiente de validar
        asignatura.setEstado(EstadoAsignatura.V.name());

        asignatura.updateAsignaturaSiEsModificable(100L);
    }

    @Test
    public void siAsignaturaCambiaEstadoDeIntroduciendoDatosAPendienteDeRevisarYLaFechaDeFinalizacionEsNullSeDebeEstablecerConLaFechaActual()
    {
        asignatura = buildAsignaturaModificable();
        asignatura.setEstado(EstadoAsignatura.D.name());
        asignatura.setFechaFinalizacion(null);
        asignatura.setEstado(EstadoAsignatura.V.name());
        Date hoy = new Date();
        Long diferenciaFechasMilisegundos = Math.abs(asignatura.getFechaFinalizacion().getTime()
                - hoy.getTime());
        Assert.assertTrue(diferenciaFechasMilisegundos < UMBRAL_COMPARACION_FECHAS);
    }

    @Test
    public void siAsignaturaCambiaEstadoDeIntroduciendoDatosAPendienteDeRevisarYLaFechaDeFinalizacionNoEsNullNoSeDebeCambiar()
    {
        Calendar fechaHaceUnMes = Calendar.getInstance();
        fechaHaceUnMes.add(Calendar.MONTH, -1);
        asignatura = buildAsignaturaModificable();
        asignatura.setEstado(EstadoAsignatura.D.name());
        asignatura.setFechaFinalizacion(fechaHaceUnMes.getTime());
        asignatura.setEstado(EstadoAsignatura.V.name());
        Long diferenciaFechasMilisegundos = Math.abs(asignatura.getFechaFinalizacion().getTime()
                - fechaHaceUnMes.getTime().getTime());
        Assert.assertTrue(diferenciaFechasMilisegundos < UMBRAL_COMPARACION_FECHAS);
    }

    private Asignatura buildAsignaturaModificable()
    {
        return buildAsignaturaModificableEntreFechas(ayer, manyana);
    }

    private Asignatura buildAsignaturaNoModificable()
    {
        return buildAsignaturaModificableEntreFechas(ayer, ayer);
    }

    private Asignatura buildAsignaturaModificableEntreFechas(Calendar fechaInicio, Calendar fechaFin)
    {
        cursoAcademico = buildCursoAcademicoModificable();
        asignatura.setGdoCursosAcademico(cursoAcademico);
        asignatura.setFechaIni(fechaInicio.getTime());
        asignatura.setFechaFin(fechaFin.getTime());
        return asignatura;
    }

    private CursoAcademico buildCursoAcademicoModificable()
    {
        cursoAcademico.setFechaIni(ayer.getTime());
        cursoAcademico.setFechaFin(manyana.getTime());
        cursoAcademico.setActivo(true);
        return cursoAcademico;
    }

    private static Calendar getAyer()
    {
        ayer = Calendar.getInstance();
        ayer.add(Calendar.DAY_OF_WEEK, -1);
        return ayer;
    }

    private static Calendar getManyana()
    {
        manyana = Calendar.getInstance();
        manyana.add(Calendar.DAY_OF_WEEK, 1);
        return manyana;
    }

    @After
    public void cleanDAOMocks()
    {
        asignaturaDAO = null;
        cursoAcademicoDAO = null;
        apaDAO = null;

        cursoAcademico = null;
        persona = null;
    }
}