package es.uji.apps.gdo.model;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.ResultadoDAO;

public class ResultadoTest
{
    private ResultadoDAO resultadoDAO;

    @Before
    public void initMocks()
    {
        resultadoDAO = mock(ResultadoDAO.class);
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void updateErrorSiResultadoNoEsEditable() throws Exception
    {
        Resultado resultado = getResultadoNoEditable();
        resultado.update();
    }

    @Test(expected = AsignaturaNoModificableException.class)
    public void updateErrorSiAsignaturaNoEsModificable() throws Exception
    {
        Resultado resultado = getResultadoEditable();

        Asignatura asignatura = getAsignaturaNoModificable();
        resultado.setGdoAsignatura(asignatura);

        resultado.update();
    }

    @Test
    public void updateOkSiResultadoEsEditable() throws Exception
    {
        Resultado resultado = getResultadoEditable();

        Asignatura asignatura = getAsignaturaModificable();
        resultado.setGdoAsignatura(asignatura);

        resultado.update();
        verify(resultadoDAO).update(resultado);
    }

    @Test
    @Ignore
    public void operacionPermitidaOkSiTipoEstudioEsMaster() throws Exception
    {
        Titulacion titulacion = getTitulacionDeTipo("M");
        Resultado resultado = getResultadoParaUnaTitulacion(titulacion);

        Assert.assertTrue(resultado.operacionPermitida());
    }

    @Test
    public void operacionPermitidaErrorSiTipoEstudioEsGrado() throws Exception
    {
        Titulacion titulacion = getTitulacionDeTipo("G");
        Resultado resultado = getResultadoParaUnaTitulacion(titulacion);

        Assert.assertFalse(resultado.operacionPermitida());
    }

    private Resultado getResultadoParaUnaTitulacion(Titulacion titulacion)
            throws AsignaturaNoAsignadaATitulacion
    {
        Resultado resultado = new Resultado();
        Asignatura asignatura = getAsignaturaModificable();
        when(asignatura.getTitulacion()).thenReturn(titulacion);
        resultado.setGdoAsignatura(asignatura);
        return resultado;
    }

    private Titulacion getTitulacionDeTipo(String tipoEstudio)
    {
        Titulacion titulacion = new Titulacion();
        titulacion.setTipo(tipoEstudio);
        return titulacion;
    }

    private Resultado getResultadoEditable()
    {
        Resultado resultado = new Resultado();
        resultado.setResultadoDAO(resultadoDAO);
        resultado.setEditable("S");
        return resultado;
    }

    private Resultado getResultadoNoEditable()
    {
        Resultado resultado = getResultadoEditable();
        resultado.setEditable("N");
        return resultado;
    }

    private Asignatura getAsignaturaModificable()
    {
        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(true);
        return asignatura;
    }

    private Asignatura getAsignaturaNoModificable()
    {
        Asignatura asignatura = mock(Asignatura.class);
        when(asignatura.isModificable()).thenReturn(false);
        return asignatura;
    }

    @After
    public void cleanMocks()
    {
        new Resultado().setResultadoDAO(null);
    }
}
