package es.uji.apps.gdo.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.gdo.model.Idioma;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("idioma")
public class IdiomaService extends CoreBaseService
{

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIdiomas(@QueryParam("asignaturaId") String asignaturaId)
    {

        return UIEntity.toUI(Idioma.getIdiomas());

    }

    @GET
    @Path("noasignados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIdiomasNoAsignados(@QueryParam("asignaturaId") String asignaturaId)
    {

        List<Idioma> idiomas = Idioma.getIdiomasByAsignaturaIdNoAsignados(Long
                .parseLong(asignaturaId));
        
        return UIEntity.toUI(idiomas);
    }

}