package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.dao.DataIntegrityViolationException;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.model.Actividad;
import es.uji.apps.gdo.model.ActividadAsignatura;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("actividad")
public class ActividadService extends CoreBaseService
{
    @GET
    @Path("{actividadId}")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public List<UIEntity> getActividad(@PathParam("actividadId") String actividadId)
    {
        List<ActividadAsignatura> listaActividadesAsignatura = ActividadAsignatura
                .getActividad(Long.parseLong(actividadId));

        return UIEntity.toUI(listaActividadesAsignatura);
    }

    private List<ActividadAsignatura> buscaActividadesPorCodigoAsignatura(String codigoAsignatura,
            String cursoAcademico)
    {
        List<ActividadAsignatura> result = new ArrayList<ActividadAsignatura>();

        if (codigoAsignatura != null)
        {
            result = ActividadAsignatura.getActividadesByCodAsignatura(codigoAsignatura,
                    Long.parseLong(cursoAcademico));
        }

        return result;
    }

    private List<ActividadAsignatura> buscaActividadesPorAsignaturaId(String asignaturaId)
    {
        List<ActividadAsignatura> result = new ArrayList<ActividadAsignatura>();

        if (asignaturaId != null)
        {
            result = ActividadAsignatura.getActividadesByAsignaturaId(Long.parseLong(asignaturaId));
        }

        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActividades(@QueryParam("asignaturaId") String asignaturaId,
            @QueryParam("codigoAsignatura") String codigoAsignatura,
            @QueryParam("cursoAcademico") String cursoAcademico)
    {
        List<ActividadAsignatura> listaActividadesAsignatura = new ArrayList<ActividadAsignatura>();

        listaActividadesAsignatura.addAll(buscaActividadesPorAsignaturaId(asignaturaId));
        listaActividadesAsignatura.addAll(buscaActividadesPorCodigoAsignatura(codigoAsignatura,
                cursoAcademico));

        List<UIEntity> result = new ArrayList<UIEntity>();

        for (ActividadAsignatura actividadAsignatura : listaActividadesAsignatura)
        {
            UIEntity entity = UIEntity.toUI(actividadAsignatura);
            entity.put("actividadId", actividadAsignatura.getGdoTipoActividade().getId());
            entity.put("asignaturaId", actividadAsignatura.getGdoAsignatura().getId());

            result.add(entity);
        }

        return result;
    }

    @GET
    @Path("actividadesByAsignaturaId")
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getActividadesByAsignaturaId(
            @QueryParam("asignaturaId") String asignaturaId)
    {
        List<ActividadAsignatura> listaActividadesAsignatura = ActividadAsignatura
                .getActividadesByAsignaturaId(Long.parseLong(asignaturaId));

        return UIEntity.toUI(listaActividadesAsignatura);
    }

    @GET
    @Path("tiposactividad")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposActividad()
    {
        List<Actividad> listaActividades = Actividad.getActividades();

        return listaActividades.stream().map(actividad -> {
            UIEntity ui = UIEntity.toUI(actividad);
            ui.put("nombre",actividad.getNombreCa());
            return ui;
        }).collect(Collectors.toList());

    }

    @PUT
    @Path("{actividadId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("actividadId") String actividadId, UIEntity entity)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        ActividadAsignatura actividadAsignatura = entity.toModel(ActividadAsignatura.class);

        Actividad actividad = new Actividad();
        actividad.setId(Long.parseLong(entity.get("actividadId")));
        actividadAsignatura.setGdoTipoActividade(actividad);

        Asignatura asignatura = Asignatura
                .getAsignatura(Long.parseLong(entity.get("asignaturaId")));
        actividadAsignatura.setGdoAsignatura(asignatura);

        actividadAsignatura.update();

        return Response.ok(entity).build();
    }

    @GET
    @Path("estado/{asignaturaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOperacionesUIposibles(@PathParam("asignaturaId") String asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        UIEntity operaciones = new UIEntity("OperacionesUI");

        boolean operacionPermitida = ActividadAsignatura.operacionPermitidaByAsignaturaId(Long
                .valueOf(asignaturaId));

        operaciones.put("asignaturaId", Long.valueOf(asignaturaId));
        operaciones.put("allowDelete", operacionPermitida);
        operaciones.put("allowInsert", operacionPermitida);
        operaciones.put("allowUpdate", operacionPermitida);
        operaciones.put("allowUpdateMaestro", operacionPermitida);

        return Collections.singletonList(operaciones);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(@QueryParam("asignaturaId") @DefaultValue("0") Long asignaturaId,UIEntity entity) throws AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion, RegistroDuplicadoException
    {
        if (entity == null || entity.get("actividadId") == null
                || asignaturaId.equals(0L))
        {
            return new ArrayList<UIEntity>();
        }

        Asignatura asignatura = Asignatura
                .getAsignatura(asignaturaId);

        Actividad actividad = new Actividad();
        actividad.setId(new Long(entity.get("actividadId")));

        ActividadAsignatura actividadAsignatura = new ActividadAsignatura();
        actividadAsignatura.setGdoAsignatura(asignatura);
        actividadAsignatura.setGdoTipoActividade(actividad);
        actividadAsignatura.setHorasNoPresenciales(new Float(entity.get("horasNoPresenciales")));
        actividadAsignatura.setHorasPresenciales(new Float(entity.get("horasPresenciales")));

        try
        {
            ActividadAsignatura newActividadAsignatura = actividadAsignatura.insert();
            entity.put("id", newActividadAsignatura.getId());

            return Collections.singletonList(entity);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new RegistroDuplicadoException();
        }

    }

    @DELETE
    @Path("{id}")
    public ResponseMessage delete(@PathParam("id") String id) throws AsignaturaNoModificableException,
            RegistroNoEncontradoException, AsignaturaNoAsignadaATitulacion
    {
        ActividadAsignatura.delete(Long.parseLong(id));

        return new ResponseMessage(true);
    }
}