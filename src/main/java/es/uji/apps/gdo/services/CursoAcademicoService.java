package es.uji.apps.gdo.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.gdo.CursoAcademicoSoloUnoActivo;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.CursoAcademico;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("curso")
public class CursoAcademicoService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursos()
    {
        List<UIEntity> entidades =  modelToUI(CursoAcademico.getCursos());
        return entidades;
    }

    private List<UIEntity> modelToUI(List<CursoAcademico> listaCursos)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (CursoAcademico curso : listaCursos)
        {
            result.add(modelToUI(curso));
        }
        return result;
    }

    private UIEntity modelToUI(CursoAcademico curso)
    {
        UIEntity entity = UIEntity.toUI(curso);
        entity.put("fechaInicioVisibilidad", curso.getFechaIni());
        entity.put("fechaFinVisibilidad", curso.getFechaFin());

        return entity;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") String cursoAcademicoId, UIEntity entity)
            throws ParseException, CursoAcademicoSoloUnoActivo
    {
        CursoAcademico cursoAcademico = entity.toModel(CursoAcademico.class);

        if (entity.get("activo").equals("Actiu"))
        {
            cursoAcademico.setActivo(true);
        }
        else
        {
            cursoAcademico.setActivo(false);
        }

        Date fechaInicio = entity.getDate("fechaInicioVisibilidad");
        cursoAcademico.setFechaIni(fechaInicio);

        Date fechaFin = entity.getDate("fechaFinVisibilidad");
        cursoAcademico.setFechaFin(fechaFin);

        cursoAcademico.update();

        entity.put("asignaturasCurso", Asignatura.cuentaAsignaturas(cursoAcademico.getCursoAca()));

        return Response.ok(entity).build();
    }

    @PUT
    @Path("activar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity activaCurso(@PathParam("id") String cursoAcademicoId)
            throws ParseException, CursoAcademicoSoloUnoActivo
    {
        CursoAcademico.setCursoActivo(Long.parseLong(cursoAcademicoId));

        UIEntity newEntity = UIEntity.toUI(CursoAcademico.getCursoAcademicoById(Long
                .parseLong(cursoAcademicoId)));
        newEntity.put("asignaturasCurso",
                Asignatura.cuentaAsignaturas(Long.parseLong(cursoAcademicoId)));

        return newEntity;
    }
}
