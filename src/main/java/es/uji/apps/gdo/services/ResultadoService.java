package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Resultado;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("resultado")
public class ResultadoService
{
    private String abreviar(String cadena)
    {
        String breve = cadena;

        if (breve != null && breve.length() > 50)
        {
            breve = breve.substring(0, 50) + "...";
        }

        return breve;
    }

    private UIEntity modelToUI(Resultado resultado)
    {

        UIEntity entity = UIEntity.toUI(resultado);
        entity.put("resultadoCaBreve", abreviar(resultado.getResultadoCa()));
        entity.put("resultadoEsBreve", abreviar(resultado.getResultadoEs()));
        entity.put("resultadoUkBreve", abreviar(resultado.getResultadoUk()));

        return entity;
    }

    public List<UIEntity> modelToUI(List<Resultado> listaResultado)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        for (Resultado resultado : listaResultado)
        {
            result.add(modelToUI(resultado));
        }

        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getResultados(@QueryParam("asignaturaId") String asignaturaId)
    {
        return modelToUI(Resultado.getResultadosByAsignatura(Long.parseLong(asignaturaId)));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getResultado(@PathParam("id") String id)
    {
        return Collections.singletonList(modelToUI(Resultado.getResultado(Long.parseLong(id))));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity putResultado(@FormParam("id") Long id, @FormParam("resultadoCa") String resultadoCa, @FormParam("resultadoEs") String resultadoEs, @FormParam("resultadoUk") String resultadoUk)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {

        Resultado resultado = Resultado.getResultado(id);

        resultado.setResultadoCa(resultadoCa);
        resultado.setResultadoEs(resultadoEs);
        resultado.setResultadoUk(resultadoUk);

        resultado.update();

        UIEntity entity = modelToUI(resultado);

        return entity;
    }

    @GET
    @Path("estado/{asignaturaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_XML)
    public List<UIEntity> getOperacionesUIposibles(@PathParam("asignaturaId") String asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        UIEntity entity = new UIEntity();

//        entity.put("asignaturaId", Long.valueOf(asignaturaId));
//        entity.put("allowDelete",
//                Resultado.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
//        entity.put("allowInsert",
//                Resultado.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
        entity.put("modificable",
                Resultado.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
     

        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage delete(@PathParam("id") String id) throws AsignaturaNoModificableException,
            RegistroNoEncontradoException, AsignaturaNoAsignadaATitulacion
    {
        Resultado.delete(Long.parseLong(id));
        
        return new ResponseMessage(true);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insert(MultivaluedMap<String, String> params)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Resultado resultado = new Resultado();
        resultado.setResultadoCa(params.getFirst("resultadoCa"));
        resultado.setResultadoEs(params.getFirst("resultadoEs"));
        resultado.setResultadoUk(params.getFirst("resultadoUk"));
        resultado.setEditable("S");
        resultado.setOrden(1L);
        if (!params.getFirst("asignaturaId").isEmpty())
        {
            resultado.setGdoAsignatura(Asignatura.getAsignatura(Long.parseLong(params
                    .getFirst("asignaturaId"))));
        }

        resultado.insert();
        
        UIEntity entity = modelToUI(resultado);

        return entity;
    }
}
