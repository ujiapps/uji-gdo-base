package es.uji.apps.gdo.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.gdo.dao.ComentarioDAO;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Comentario;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

public class ComentarioService
{
    @PathParam("asignaturaId")
    Long asignaturaId;

    @InjectParam
    public ComentarioDAO comentarioDAO;

    public static Logger log = Logger.getLogger(ComentarioService.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getComentariosByAsignatura()
    {
        List<UIEntity> listaEntidades = UIEntity.toUI(comentarioDAO
                .getComentariosByAsignatura(this.asignaturaId));

        for (UIEntity uiEntity : listaEntidades)
        {
            uiEntity.put("asignaturaId", this.asignaturaId);
        }

        return listaEntidades;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertComentario( UIEntity entity)
    {
        Comentario comentario = new Comentario();
        comentario.setComentario(entity.get("comentario"));
        comentario.setGdoAsignatura(Asignatura.getAsignatura(asignaturaId));
        comentario.setIdioma(entity.get("idioma"));

        comentario.insertComentario();

        entity.put("id", comentario.getId());

        return entity;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteComentario(@PathParam("id") String id)
    {
        Comentario.delete(Long.parseLong(id));
        return new ResponseMessage(true);
    }
}
