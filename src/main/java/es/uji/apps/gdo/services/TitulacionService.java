package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.gdo.model.Persona;
import es.uji.apps.gdo.model.Titulacion;
import es.uji.apps.gdo.model.TitulacionPersona;
import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("titulacion")
public class TitulacionService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTitulacionesbyPersona()
    {
        long personaId = AccessManager.getConnectedUserId(request);
        List<UIEntity> titulacionesPorPersona = new ArrayList<UIEntity>();

        if (Persona.isAdministrador(personaId))
        {
            List<Titulacion> titulaciones = Titulacion.getTitulaciones();
            for (Titulacion titulacion : titulaciones)
            {
                UIEntity entity = UIEntity.toUI(titulacion);
                entity.put("id", titulacion.getId());
                entity.setBaseClass("TitulacionPersona");
                titulacionesPorPersona.add(entity);
            }
        }
        else
        {
            List<TitulacionPersona> titulacionesbyPersona = Titulacion
                    .getTitulacionesbyPersona(personaId);
            for (TitulacionPersona titulacionPersona : titulacionesbyPersona)
            {
                UIEntity entity = UIEntity.toUI(titulacionPersona);
                entity.put("id", titulacionPersona.getId().getEstudioId());
                titulacionesPorPersona.add(entity);
            }
        }
        return titulacionesPorPersona;
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTitulaciones()
    {
        List<Titulacion> titulaciones = Titulacion.getTitulaciones();
        return UIEntity.toUI(titulaciones);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursos(@PathParam("id") String id)
    {
        long personaId = AccessManager.getConnectedUserId(request);
        List<Long> cursos;
        List<UIEntity> listadoCursosPorTitulacion = new ArrayList<UIEntity>();
        HashMap<Long, String> textosPorCurso = new HashMap<Long, String>();

        completaDescripcionCursos(textosPorCurso);

        if (Persona.isAdministrador(personaId))
        {
            cursos = Titulacion.getCursosByTitulacionId(id);
        }
        else
        {
            cursos = Titulacion.getCursosByTitulacionId(id, personaId);
        }
        UIEntity entidadTodos = creaEntidad(textosPorCurso, 0L);
        listadoCursosPorTitulacion.add(entidadTodos);
        creaEntidades(cursos, listadoCursosPorTitulacion, textosPorCurso);
        return listadoCursosPorTitulacion;
    }

    private void completaDescripcionCursos(HashMap<Long, String> textosPorCurso)
    {
        textosPorCurso.put(0L, "Tots");
        textosPorCurso.put(1L, "Primer");
        textosPorCurso.put(2L, "Segon");
        textosPorCurso.put(3L, "Tercer");
        textosPorCurso.put(4L, "Quart");
        textosPorCurso.put(5L, "Quint");
        textosPorCurso.put(6L, "Sext");
    }

    private void creaEntidades(List<Long> cursosAsignados, List<UIEntity> listaCursosPorTitulacion,
            HashMap<Long, String> textosPorCurso)
    {
        for (Long curso : cursosAsignados)
        {
            UIEntity entity = creaEntidad(textosPorCurso, curso);
            listaCursosPorTitulacion.add(entity);
        }
    }

    private UIEntity creaEntidad(HashMap<Long, String> textosPorCurso, Long curso)
    {
        UIEntity entity = UIEntity.toUI(curso);
        entity.put("id", curso);
        entity.put("nombre", textosPorCurso.get(curso));
        entity.setBaseClass("TitulacionCurso");
        return entity;
    }

    @GET
    @Path("tipos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstados()
    {
        String[] valores = new String[TipoEstudio.values().length];
        int i = 0;

        for (TipoEstudio estado : TipoEstudio.values())
        {
            valores[i] = estado.name().toString();
            i++;
        }

        return TipoEstudio.toUI(valores);
    }
}