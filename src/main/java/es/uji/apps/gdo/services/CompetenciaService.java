package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Competencia;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("competencia")
public class CompetenciaService extends CoreBaseService
{
    private String abreviar(String cadena)
    {
        String breve = cadena;

        if (breve != null && breve.length() > 50)
        {
            breve = breve.substring(0, 50) + "...";
        }

        return breve;
    }

    private UIEntity modelToUI(Competencia competencia)
    {
        UIEntity entity = UIEntity.toUI(competencia);

        entity.put("competenciaCaBreve", abreviar(competencia.getCompetenciaCa()));
        entity.put("competenciaEsBreve", abreviar(competencia.getCompetenciaEs()));
        entity.put("competenciaUkBreve", abreviar(competencia.getCompetenciaUk()));

        return entity;
    }

    public List<UIEntity> modelToUI(List<Competencia> listaCompetencia)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        for (Competencia competencia : listaCompetencia)
        {
            result.add(modelToUI(competencia));
        }

        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCompetencias(@QueryParam("asignaturaId") String asignaturaId)
    {
        return modelToUI(Competencia.getCompetenciasByAsignatura(Long.parseLong(asignaturaId)));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCompetencia(@PathParam("id") String id)
    {
        return Collections.singletonList(modelToUI(Competencia.getCompetencia(Long.parseLong(id))));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity putCompetencia(@FormParam("id") Long id,
                                   @FormParam("competenciaCa") String competenciaCa,
                                   @FormParam("competenciaEs") String competenciaEs,
                                   @FormParam("competenciaUk") String competenciaUk)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Competencia competencia = Competencia.getCompetencia(id);

        competencia.setCompetenciaCa(competenciaCa);
        competencia.setCompetenciaEs(competenciaEs);
        competencia.setCompetenciaUk(competenciaUk);

        competencia.update();
        
        UIEntity entity = modelToUI(competencia);

        return entity;
    }

    @GET
    @Path("estado/{asignaturaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOperacionesUIposibles(@PathParam("asignaturaId") String asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        UIEntity entity = new UIEntity();

//        entity.put("asignaturaId", Long.valueOf(asignaturaId));
//        entity.put("allowDelete",
//                Competencia.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
//        entity.put("allowInsert",
//                Competencia.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
//        entity.put("allowUpdate",
//                Competencia.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
        entity.put("modificable",
                Competencia.operacionPermitidaByAsignaturaId(Long.valueOf(asignaturaId)));
        
        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") String id) throws NumberFormatException,
            AsignaturaNoModificableException, RegistroNoEncontradoException,
            AsignaturaNoAsignadaATitulacion
    {
        Competencia.delete(Long.parseLong(id));
        
        return new ResponseMessage(true);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insert(MultivaluedMap<String, String> params)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Competencia competencia = new Competencia();
        competencia.setCompetenciaCa(params.getFirst("competenciaCa"));
        competencia.setCompetenciaEs(params.getFirst("competenciaEs"));
        competencia.setCompetenciaUk(params.getFirst("competenciaUk"));
        competencia.setEditable("S");
        competencia.setOrden(1L);
        if (!params.getFirst("asignaturaId").isEmpty())
        {
            competencia.setGdoAsignatura(Asignatura.getAsignatura(Long.parseLong(params
                    .getFirst("asignaturaId"))));
        }

        competencia.insert();
        
        UIEntity entity = modelToUI(competencia);

        return entity;
    }
}
