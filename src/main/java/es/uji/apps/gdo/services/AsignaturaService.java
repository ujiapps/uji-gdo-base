package es.uji.apps.gdo.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.gdo.*;
import es.uji.apps.gdo.model.*;
import org.apache.log4j.Logger;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.gdo.model.domains.EstadoAsignatura;
import es.uji.apps.gdo.model.domains.TipoApartado;
import es.uji.apps.gdo.model.domains.TipoCompartida;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("asignatura")
public class AsignaturaService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(AsignaturaService.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturas(@DefaultValue("0") @QueryParam("start") String start,
            @DefaultValue("20") @QueryParam("limit") String limit,
            @QueryParam("tipoEstudio") String tipoEstudio,
            @QueryParam("codigoAsignatura") String codigoAsignatura,
            @QueryParam("estado") String estado, @QueryParam("titulacion") String titulacion,
            @QueryParam("curso") String curso,
            @DefaultValue("false") @QueryParam("conGuia") String conGuia,
            @DefaultValue("false") @QueryParam("inicial") String inicial,
            @QueryParam("pantalla") String pantalla) throws DemasiadasAsignaturasException
    {

        Long personaId = AccessManager.getConnectedUserId(request);

        List<AsignaturaInfo> listaAsignaturas = AsignaturaInfo.getAsignaturasConFiltros(personaId, tipoEstudio,
                codigoAsignatura, estado, titulacion, curso, conGuia.equals("true"), pantalla);

        return AsignaturaInfoToUI(listaAsignaturas);
    }

    private List<UIEntity> AsignaturaInfoToUI(List<AsignaturaInfo> listaAsignaturas)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (AsignaturaInfo asignatura : listaAsignaturas)
        {
            result.add(AsignaturaInfoToUI(asignatura));
        }
        return result;
    }

    private UIEntity AsignaturaInfoToUI(AsignaturaInfo asignatura)
    {
        UIEntity entity = UIEntity.toUI(asignatura);
        if (asignatura.getEstado() != null)
        {
            entity.put("estadoNombre", EstadoAsignatura.valueOf(asignatura.getEstado()));
        }
        entity.put("gdoExtAsiTodaId", asignatura.getCodAsignatura());
        entity.put("gdoExtPersonaId", asignatura.getPersonaId());
        entity.put("apartadosTotales", asignatura.getApartadosTextoTotales() + 4L);

        Long apartadosNoTexto = asignatura.getCompetencias() + asignatura.getActividades() + asignatura.getIdiomas() + asignatura.getEvaluaciones();
        entity.put("apartadosEsLlenos", asignatura.getApartadosEsLlenos() + apartadosNoTexto);
        entity.put("apartadosCaLlenos", asignatura.getApartadosCaLlenos() + apartadosNoTexto);
        entity.put("apartadosUkLlenos", asignatura.getApartadosUkLlenos() + apartadosNoTexto);

        entity.put("deficiencias", asignatura.getDeficiencias());

        if (asignatura.getAsignaturaRelacionada() == null)
        {
            entity.put("asignaturaRelacionada", "");
        }

        return entity;
    }

    public List<UIEntity> getAsignaturasOLD(@DefaultValue("0") @QueryParam("start") String start,
                                         @DefaultValue("20") @QueryParam("limit") String limit,
                                         @QueryParam("tipoEstudio") String tipoEstudio,
                                         @QueryParam("codigoAsignatura") String codigoAsignatura,
                                         @QueryParam("estado") String estado, @QueryParam("titulacion") String titulacion,
                                         @QueryParam("curso") String curso,
                                         @DefaultValue("false") @QueryParam("conGuia") String conGuia,
                                         @DefaultValue("false") @QueryParam("inicial") String inicial,
                                         @QueryParam("pantalla") String pantalla) throws DemasiadasAsignaturasException
    {

        long personaId = AccessManager.getConnectedUserId(request);

        List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();

        if (inicial.equals("false"))
        {
            listaAsignaturas = Asignatura.getAsignaturasConFiltros(personaId, tipoEstudio,
                    codigoAsignatura, estado, titulacion, curso, conGuia.equals("true"), pantalla);
        }
        else
        {
            if (!Persona.isAdministrador(personaId))
            {
                try
                {
                    Asignatura.checkDemasiadasAsignaturas(personaId, tipoEstudio, codigoAsignatura,
                            estado, titulacion, curso, Boolean.parseBoolean(conGuia));

                    listaAsignaturas = Asignatura.getAsignaturasConFiltros(personaId, tipoEstudio,
                            codigoAsignatura, estado, titulacion, curso, conGuia.equals("true"),
                            pantalla);
                }
                catch (DemasiadasAsignaturasException e)
                {
                    // No ejecuto la búsqueda inicial porque hay demasiadas asignaturas;
                }

            }
        }
        return modelToUI(listaAsignaturas);
    }

    @GET
    @Path("{id}/idiomas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIdiomas(@PathParam("id") String id)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        Long asignaturaId = Long.parseLong(id);

        for (IdiomaAsignatura idiomaAsignatura : IdiomaAsignatura
                .getIdiomasByAsignaturaId(asignaturaId))
        {
            UIEntity entity = UIEntity.toUI(idiomaAsignatura);
            entity.put("idiomaId", idiomaAsignatura.getGdoIdioma().getId());
            entity.put("nombreIdioma", idiomaAsignatura.getGdoIdioma().getNombreCa());
            entity.put("asignaturaId", idiomaAsignatura.getGdoAsignatura().getId());
            result.add(entity);
        }

        return result;
    }

    @GET
    @Path("ayuda/{asignaturaId}/{tipoApartado}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTextoAyuda(@PathParam("asignaturaId") String asignaturaId,
                                        @PathParam("tipoApartado") String tipoApartado)
    {
        Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(asignaturaId));
        String asignaturaNombre = asignatura.getNombreAsignaturaCompleto();

        Apartado apartado = Apartado.getApartadoByAsignaturaAndTipo(Long.parseLong(asignaturaId),
                TipoApartado.valueOf(tipoApartado));
        String textoAyuda = apartado.getTextoAyuda();

        UIEntity entity = new UIEntity("textoAyudaFormulariosUI");
        entity.put("asignatura", asignatura.getGdoExtAsiToda().getId() + " - " + asignaturaNombre);
        entity.put("texto", textoAyuda);

        return Collections.singletonList(entity);
    }

    @POST
    @Path("{id}/idiomas")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertIdioma(@PathParam("id") String asignaturaId, UIEntity entity)
            throws AsignaturaNoModificableException
    {
        Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(asignaturaId));

        Long idiomaId = Long.parseLong(entity.get("idiomaId"));
        asignatura.insertIdioma(idiomaId);

        entity.put("id", idiomaId);

        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{id}/idiomas/{idiomaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteIdioma(@PathParam("id") String asignaturaId,
                                 @PathParam("idiomaId") String idiomaId)
    {
        try
        {
            Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(asignaturaId));
            asignatura.deleteIdioma(Long.parseLong(idiomaId));
            return Response.ok().build();
        }
        catch (Exception e)
        {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignatura(@PathParam("id") String id)
    {

        Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(id));
        UIEntity asignaturaUI = modelToUI(asignatura);

        return Collections.singletonList(asignaturaUI);
    }

    @GET
    @Path("titulacion/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getAsignaturaByTitulacion(@PathParam("id") String titulacionId)
    {
        List<Asignatura> asignaturasByTitulacion = Asignatura.getAsignaturasByTitulacion(Long
                .parseLong(titulacionId));

        return modelToUI(asignaturasByTitulacion);
    }

    @PUT
    @Path("{origen}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateAsignatura(@PathParam("id") String id,
                                     @PathParam("origen") String origen, UIEntity entity) throws ParseException,
            CamposObligatoriosNoRellenadosException, AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion, AsignaturaBasadaEnIncorrecta
    {
        Asignatura asignatura = cargaDatosAsignatura(id, entity);

        Long personaId = AccessManager.getConnectedUserId(request);

        if (asignatura.getGdoAsignatura() == null)
        {
            asignatura.setTipoCompartida(null);
        }

        asignatura.checkAsignaturaNoEstaEnSlt(personaId);
        asignatura.checkEsResposableCambioAPendienteRevisarEnPeriodoOrdinario(personaId);

        // Pasar a SLT si la asignatura se ha modificado algún texto y se pasa a estado finalizado
        if ((asignatura.getEstado().equals(EstadoAsignatura.F.name()) || asignatura.getEstado().equals(EstadoAsignatura.FD.name())) && asignatura.getNumeroDeModificaciones() > 0)
        {
            asignatura.setEnSlt("S");
        }

        if (origen.equals("guia") && !Persona.isAdministrador(personaId))
        {
            asignatura.updateAsignaturaSiEsModificable(personaId);
        }
        else
        {
            asignatura.updateAsignaturaSinCheckeoPorAdmin();
        }

        UIEntity nuevaEntidad = modelToUI(asignatura);
        nuevaEntidad.put("notificaResponsable", asignatura.getNotificaResponsable());
        nuevaEntidad.put("notificaCoordinador", asignatura.getNotificaCoordinador());
        nuevaEntidad.put("notificaSLT", asignatura.getNotificaSLT());
        nuevaEntidad.put("notificaFinalizada", asignatura.getNotificaFinalizada());
        return nuevaEntidad;
    }

    @PUT
    @Path("{id}/modificaciones")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity putModificaciones(@FormParam("id") Long id,
                                      @FormParam("modificaciones") String modificaciones)
    {
        Asignatura asignatura = Asignatura.getAsignatura(id);
        asignatura.setModificaciones(modificaciones);
        asignatura.update();
        asignatura.updateAsignaturaParaCopiaDeAsignaturas();

        UIEntity entity = modelToUI(asignatura);

        return null;
    }

    @PUT
    @Path("{id}/deficiencias")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity putDeficiencias(@PathParam("id") Long id,
            @FormParam("deficiencias") String deficiencias)
    {
        Asignatura asignatura = Asignatura.getAsignatura(id);
        asignatura.setDeficiencias(deficiencias);
        asignatura.update();
        return null;
    }

    @PUT
    @Path("{id}/enviarSLT")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity enviarSLT(@PathParam("id") Long id)
    {
        Asignatura asignatura = Asignatura.getAsignatura(id);
        asignatura.setEnSlt("S");
        asignatura.update();
        return null;
    }

    private Asignatura cargaDatosAsignatura(String id, UIEntity entity) throws ParseException,
            AsignaturaNoAsignadaATitulacion
    {
        Asignatura asignatura = Asignatura.getAsignaturaConRelaciones(Long.parseLong(id));

        asignatura.setDeficiencias(entity.get("deficiencias"));

        if (asignatura.getEstado().equals(EstadoAsignatura.I.name())
                && !entity.get("estado").equals(EstadoAsignatura.I.name()))
        {
            copiaDelVerifica(asignatura, entity);
        }

        if (entity.get("fechaIni") != null)
        {
            Date fechaInicio = entity.getDate("fechaIni");
            asignatura.setFechaIni(fechaInicio);
        }
        else
        {
            asignatura.setFechaIni(null);
        }

        if (entity.get("fechaFin") != null)
        {
            Date fechaFin = entity.getDate("fechaFin");
            asignatura.setFechaFin(fechaFin);
        }
        else
        {
            asignatura.setFechaFin(null);
        }

        asignatura.setEstado(entity.get("estado"));

        if (ParamUtils.isNotNull(entity.get("asignaturaRelacionada")))
        {
            Asignatura asignaturaRelacionada = Asignatura
                    .getAsignaturaByCodigoDelCursoActivo(entity.get("asignaturaRelacionada"));

            asignatura.setGdoAsignatura(asignaturaRelacionada);
            asignatura.setEstado(asignaturaRelacionada.getEstado());
            asignatura.setTipoCompartida(entity.get("tipoCompartida"));
        }
        else
        {
            asignatura.setGdoAsignatura(null);
            asignatura.setTipoCompartida(null);
        }


        if (entity.get("fechaValidacionCa") == null
                || entity.get("fechaValidacionCa").equals("false"))
        {
            asignatura.setFechaValidacionCa(null);
        }
        else
        {
            if (asignatura.getFechaValidacionCa() == null)
            {
                asignatura.setFechaValidacionCa(new Date());
            }
        }

        if (entity.get("fechaValidacionEs") == null
                || entity.get("fechaValidacionEs").equals("false"))
        {
            asignatura.setFechaValidacionEs(null);
        }
        else
        {
            if (asignatura.getFechaValidacionEs() == null)
            {
                asignatura.setFechaValidacionEs(new Date());
            }
        }

        if (entity.get("fechaValidacionUk") == null
                || entity.get("fechaValidacionUk").equals("false"))
        {
            asignatura.setFechaValidacionUk(null);
        }
        else
        {
            if (asignatura.getFechaValidacionUk() == null)
            {
                asignatura.setFechaValidacionUk(new Date());
            }
        }

        if (entity.get("personaId") != null)
        {
            asignatura.setGdoExtPersona(Persona.getPersona(Long.parseLong(entity.get("personaId"))));
        }
        else
        {
            asignatura.setGdoExtPersona(null);
        }
        return asignatura;
    }

    private void copiaDelVerifica(Asignatura asignatura, UIEntity entity)
            throws AsignaturaNoAsignadaATitulacion
    {
        GeneracionGuia.copiaDelVerifica(asignatura.getId());
    }

    private List<UIEntity> getAllEstados()
    {
        String[] valores = new String[EstadoAsignatura.values().length];
        int i = 0;

        for (EstadoAsignatura estado : EstadoAsignatura.values())
        {
            valores[i] = estado.name().toString();
            i++;
        }

        return EstadoAsignatura.toUI(valores);
    }

    @GET
    @Path("{id}/estados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstadosAsignatura(@PathParam("id") String asignaturaId,
                                               @DefaultValue("false") @QueryParam("coordinador") String coordinador)
    {
        Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(asignaturaId));

        if (Persona.isAdministrador(AccessManager.getConnectedUserId(request)))
        {
            return getAllEstados();
        }

        if (coordinador.equals("true"))
        {
            switch (asignatura.getEstado().charAt(0))
            {
                case 'I':
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.I.name(), EstadoAsignatura.D.name()});
                case 'D':
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.D.name(), EstadoAsignatura.V.name()});
                default:
                    return getAllEstados();
            }
        }
        else
        {
            switch (asignatura.getEstado())
            {
                case "I":
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.I.name(), EstadoAsignatura.D.name()});
                case "D":
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.D.name(), EstadoAsignatura.V.name()});
                case "V":
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.V.name()});
                case "F":
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.F.name()});
                case "FD":
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.FD.name()});
                default:
                    return EstadoAsignatura.toUI(new String[]{EstadoAsignatura.I.name(), EstadoAsignatura.D.name(), EstadoAsignatura.V.name()});
            }
        }
    }

    @GET
    @Path("tiposcompartida")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposCompartida()
    {
        return TipoCompartida.toUI(new String[]{"T", "P"});
    }

    @GET
    @Path("{asignaturaId}/apartados/obligatoriosvacios")
    @Produces(MediaType.APPLICATION_XML)
    public String getApartadosObligatoriosVacios(@PathParam("asignaturaId") String asignaturaId)
    {
        String apartados = Apartado.getApartadosObligatoriosVaciosByAsignatura(Long
                .parseLong(asignaturaId));

        return apartados;
    }

    @GET
    @Path("estados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstados()
    {
        return getAllEstados();
    }

    @GET
    @Path("pendientes-traducir")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPendientesTraducir(@QueryParam("cursoAca") Long cursoAca, @QueryParam("tipoEstudio") String tipoEstudio)
    {
        List<PendientesTraducir> pendientesTraducir = PendientesTraducir.getListaPendientesTraducir(cursoAca, tipoEstudio);
        return pendientesTraducir.stream().map(a -> UIEntity.toUI(a)).collect(Collectors.toList());
    }

    @PUT
    @Path("{id}/finalizar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity finalizarAsignatura(@PathParam("id") Long id, UIEntity entity)
            throws AsignaturaNoAsignadaATitulacion, AsignaturaNoModificableException, AsignaturaBasadaEnIncorrecta, CamposObligatoriosNoRellenadosException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        Asignatura asignatura = Asignatura.getAsignatura(id);
        asignatura.setEstado(EstadoAsignatura.F.name());
        // Pasar a SLT si la asignatura se ha modificado algún texto
        if (asignatura.getNumeroDeModificaciones() > 0)
        {
            asignatura.setEnSlt("S");
        }

        asignatura.updateAsignaturaSiEsModificable(personaId);
        return modelToUI(asignatura);
    }

    @PUT
    @Path("{id}/finalizarSLT")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity finalizarAsignaturaSLT(@PathParam("id") Long id, UIEntity entity)
            throws GeneralGDOException, AsignaturaBasadaEnIncorrecta
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        if (!Persona.isSLT(personaId) && !Persona.isAdministrador(personaId))
        {
            throw new GeneralGDOException("Només poden finalitzar les assignatures els usuaris pertanyents al Servei de Llengües i Terminologia\n");
        }
        Asignatura asignatura = Asignatura.getAsignaturaConRelaciones(id);
        if (!asignatura.getEnSlt().equals("S"))
        {
            throw new GeneralGDOException("L'assignatura ha d'estar en estat SLT\n");
        }
        PendientesTraducir pendientesTraducir = PendientesTraducir.getPendientesTraducir(id);
        if (pendientesTraducir != null && pendientesTraducir.tieneApartadosPendientes())
        {
            throw new GeneralGDOException("L'assignatura té apartats pendents de traduir i/o validar\n");
        }
        asignatura.setEnSlt("N");
        asignatura.updateAsignaturaSinCheckeoPorAdmin();
        return modelToUI(asignatura);
    }

    @Path("{asignaturaId}/comentario")
    public ComentarioService getPlatform(@InjectParam ComentarioService comentarioService)
    {
        return comentarioService;
    }

    private UIEntity modelToUI(Asignatura asignatura)
    {
        UIEntity entity = UIEntity.toUI(asignatura);

        if (asignatura.getFechaIni() == null)
        {
            entity.put("fechaIni", "");
        }
        else
        {
            entity.put("fechaIni", asignatura.getFechaIni());
        }

        if (asignatura.getFechaFin() == null)
        {
            entity.put("fechaFin", "");
        }
        else
        {
            entity.put("fechaFin", asignatura.getFechaFin());
        }

        if (asignatura.getFechaFinalizacion() == null)
        {
            entity.put("fechaFinalizacion", "");
        }
        else
        {
            entity.put("fechaFinalizacion", asignatura.getFechaFinalizacion());
        }

        if (asignatura.getFechaValidacionCa() == null)
        {
            entity.put("fechaValidacionCa", "");
        }
        else
        {
            entity.put("fechaValidacionCa", asignatura.getFechaValidacionCa());
        }

        if (asignatura.getFechaValidacionEs() == null)
        {
            entity.put("fechaValidacionEs", "");
        }
        else
        {
            entity.put("fechaValidacionEs", asignatura.getFechaValidacionEs());
        }

        if (asignatura.getFechaValidacionUk() == null)
        {
            entity.put("fechaValidacionUk", "");
        }
        else
        {
            entity.put("fechaValidacionUk", asignatura.getFechaValidacionUk());
        }

        if (asignatura.getGdoExtPersona() != null)
        {
            entity.put("personaId", asignatura.getGdoExtPersona().getId());
            entity.put("nombreProfesor", asignatura.getGdoExtPersona().getNombreCompleto());
        }
        if (asignatura.getGdoAsignatura() != null)
        {
            entity.put("asignaturaRelacionada", asignatura.getGdoAsignatura().getGdoExtAsiToda()
                    .getId());
        }
        if (asignatura.getEstado() != null)
        {
            entity.put("estadoNombre", EstadoAsignatura.valueOf(asignatura.getEstado()));
        }
        entity.put("cursoAca", asignatura.getGdoCursosAcademico().getCursoAca());
        entity.put(
                "nombre",
                asignatura.getGdoExtAsiToda().getId() + " - "
                        + asignatura.getNombreAsignaturaCompleto());
        entity.put("activa", asignatura.isActiva());
        entity.put("autorizaPas", asignatura.getAutorizaPas());
        entity.put("tipoCompartida", asignatura.getTipoCompartida());

        Asignatura asignaturaOrigen = asignatura.getAsignaturaOriginalConRelaciones();
        entity.put("apartadosTotales", asignaturaOrigen.getNumeroSeccionesObligatorias());
        entity.put("apartadosCaLlenos", asignaturaOrigen.getNumeroSeccionesLlenasByIdioma("CA"));
        entity.put("apartadosEsLlenos", asignaturaOrigen.getNumeroSeccionesLlenasByIdioma("ES"));
        entity.put("apartadosUkLlenos", asignaturaOrigen.getNumeroSeccionesLlenasByIdioma("UK"));
        entity.put("numeroComentarios", asignatura.getNumeroComentarios());

        entity.put("curso", asignatura.getCursoAsignatura());
        entity.put("nuevaImplantacion", asignatura.isNuevaImplantacion());
        entity.put("modificaciones", asignatura.getModificaciones());
        entity.put("numeroModificaciones", asignatura.getNumeroDeModificaciones());

        entity.put("deficiencias", asignatura.getDeficiencias());

        return entity;
    }

    private List<UIEntity> modelToUI(List<Asignatura> listaAsignaturas)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (Asignatura asignatura : listaAsignaturas)
        {
            result.add(modelToUI(asignatura));
        }
        return result;
    }
}