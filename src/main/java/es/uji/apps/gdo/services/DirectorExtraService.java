package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.dao.DataIntegrityViolationException;

import es.uji.apps.gdo.CoordinadorSinCursoEspecificadoException;
import es.uji.apps.gdo.model.DirectorExtra;
import es.uji.apps.gdo.model.Persona;
import es.uji.apps.gdo.model.Titulacion;
import es.uji.apps.gdo.model.domains.TipoPermiso;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;

@Path("director")
public class DirectorExtraService extends CoreBaseService
{

    public UIEntity modelToUI(DirectorExtra DirectorExtra)
    {
        UIEntity entity = UIEntity.toUI(DirectorExtra);
        entity.put("personaId", DirectorExtra.getGdoExtPersona().getId());
        entity.put("usuario", DirectorExtra.getGdoExtPersona().getLogin());
        entity.put("nombre", DirectorExtra.getGdoExtPersona().getNombreCompleto());
        entity.put("titulacionId", DirectorExtra.getGdoExtTituToda().getId());
        return entity;
    }

    public List<UIEntity> modelToUI(List<DirectorExtra> listaDirectorExtra)
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        for (DirectorExtra DirectorExtra : listaDirectorExtra)
        {
            result.add(modelToUI(DirectorExtra));
        }

        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDirectoresExtra()
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        result = modelToUI(DirectorExtra.getDirectores());
        return result;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getDirectorExtra(@PathParam("id") String id)
    {

        DirectorExtra directorExtra = DirectorExtra.getDirectorExtra(Long.parseLong(id));
        UIEntity directorUI = modelToUI(directorExtra);

        return Collections.singletonList(directorUI);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity entity) throws RegistroDuplicadoException,
            CoordinadorSinCursoEspecificadoException
    {
        if (entity.get("personaId") == null || entity.get("titulacionId") == null)
        {
            return new ArrayList<UIEntity>();
        }

        DirectorExtra directorExtra = entity.toModel(DirectorExtra.class);

        Persona persona = Persona.getPersona(Long.parseLong(entity.get("personaId")));
        directorExtra.setGdoExtPersona(persona);

        Titulacion titulacion = new Titulacion();
        titulacion.setId(Long.parseLong(entity.get("titulacionId")));
        directorExtra.setGdoExtTituToda(titulacion);

        try
        {
            directorExtra = directorExtra.insert();
        }
        catch (DataIntegrityViolationException e)
        {
            throw new RegistroDuplicadoException();
        }

        UIEntity newEntity = new UIEntity("DirectorExtra");
        newEntity.put("id", directorExtra.getId());
        newEntity.put("usuario", persona.getLogin());
        newEntity.put("nombre", persona.getNombreCompleto());
        newEntity.putAll(entity);

        return Collections.singletonList(newEntity);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity entity)
            throws RegistroDuplicadoException, CoordinadorSinCursoEspecificadoException
    {
        DirectorExtra directorExtra = entity.toModel(DirectorExtra.class);

        Persona persona = Persona.getPersona(Long.parseLong(entity.get("personaId")));
        directorExtra.setGdoExtPersona(persona);

        Titulacion titulacion = new Titulacion();
        titulacion.setId(Long.parseLong(entity.get("titulacionId")));
        directorExtra.setGdoExtTituToda(titulacion);

        try
        {
            directorExtra.update();
        }
        catch (DataIntegrityViolationException e)
        {
            throw new RegistroDuplicadoException();
        }

        entity.put("usuario", persona.getLogin());
        entity.put("nombre", persona.getNombreCompleto());

        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") String id)
    {
        try
        {
            DirectorExtra directorExtra = DirectorExtra.getDirectorExtra(Long.parseLong(id));
            directorExtra.delete();
            return Response.ok().build();
        }
        catch (Exception e)
        {
            return Response.notModified().build();
        }
    }

//    @GET
//    @Path("tipospermiso")
//    @Produces(MediaType.APPLICATION_XML)
//    public String getTiposPermisos()
//    {
//        String[] valores = new String[TipoPermiso.values().length];
//        int i = 0;
//
//        for (TipoPermiso permiso : TipoPermiso.values())
//        {
//            valores[i] = permiso.name().toString();
//            i++;
//        }
//
//        return TipoPermiso.toXML(valores);
//    }

    @GET
    @Path("tipospermiso")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposPermisos()
    {
        return TipoPermiso.toUI();
    }
}