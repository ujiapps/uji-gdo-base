package es.uji.apps.gdo.services;

import es.uji.apps.gdo.ErrorAlLeerPlantilla;
import es.uji.apps.gdo.NotificacionSinDestinatarios;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Notificacion;
import es.uji.apps.gdo.model.PendientesTraducir;
import es.uji.apps.gdo.model.Persona;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Path("notifica") public class NotificacionService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(NotificacionService.class);

    private String destinatarios(List<Persona> destinatarios)
    {
        StringBuffer buffer = new StringBuffer();

        for (Persona persona : destinatarios)
        {
            buffer.append(persona.getNombre());
            buffer.append(" ");
            buffer.append(persona.getApellido1());
            buffer.append(", ");
        }

        return buffer.toString().substring(0, buffer.length() - 2);
    }


    @POST
    @Path("coordinador/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> notificaCoordinador(

            @PathParam("id") String asignaturaId)
            throws FieldValidationException, NotificacionSinDestinatarios, ErrorAlLeerPlantilla,
            MessageNotSentException
    {
        Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(asignaturaId));

        List<Persona> coordinadores = asignatura.getCoordinadorDeLaAsignatura();

        if (coordinadores.isEmpty())
        {
            throw new NotificacionSinDestinatarios("No es troba coordinador de l\'assignatura.\n"
                    + "No es pot enviar notificació.");
        }

        Notificacion.enviaNotificacion("COR", coordinadores, asignatura);

        UIEntity entity = new UIEntity();
        entity.put("nombres", destinatarios(coordinadores));

        return Collections.singletonList(entity);
    }


    @POST
    @Path("responsable/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> notificaResponsable(
            @PathParam("id") String asignaturaId)
            throws FieldValidationException, NotificacionSinDestinatarios, ErrorAlLeerPlantilla,
            MessageNotSentException
    {
        List<Persona> responsables = notificaResponsables(Long.parseLong(asignaturaId), "RES");

        UIEntity entity = new UIEntity();
        entity.put("nombres", destinatarios(responsables));

        return Collections.singletonList(entity);
    }


    @POST
    @Path("finalizada/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> notificaFinalizada(
            @PathParam("id") String asignaturaId)
            throws FieldValidationException, NotificacionSinDestinatarios, ErrorAlLeerPlantilla,
            MessageNotSentException
    {
        List<Persona> responsables = notificaResponsables(Long.parseLong(asignaturaId), "FIN");

        UIEntity entity = new UIEntity();
        entity.put("nombres", destinatarios(responsables));

        return Collections.singletonList(entity);
    }

    private List<Persona> notificaResponsables(Long asignaturaId, String tipoNotificacion)
            throws NumberFormatException, NotificacionSinDestinatarios, FieldValidationException,
            ErrorAlLeerPlantilla, MessageNotSentException
    {

        Asignatura asignatura = Asignatura.getAsignaturaConRelaciones(asignaturaId);

        if (asignatura.getFechaIni() == null && tipoNotificacion.equals("RES"))
        {
            tipoNotificacion = "RAC";
        }

        List<Persona> responsables = asignatura.getResponsablesAsignatura();

        if (responsables.isEmpty())
        {
            throw new NotificacionSinDestinatarios("No es troba responsable de l\'assignatura.\n"
                    + "No es pot enviar notificació.");
        }

        Date fechaActual = new Date();

        if (asignatura.getFechaIni() != null && asignatura.getFechaFin() != null
                && fechaActual.after(asignatura.getFechaIni()) && fechaActual.before(
                asignatura.getFechaFin()))
        {
            responsables.add(Persona.getPersonaByLogin("mcapdevi"));
        }

        Notificacion.enviaNotificacion(tipoNotificacion, responsables, asignatura);

        return responsables;
    }


    @POST
    @Path("slt/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> notificaSLT(
            @PathParam("id") String asignaturaId)
            throws NotificacionSinDestinatarios, ErrorAlLeerPlantilla, FieldValidationException,
            MessageNotSentException
    {
        Asignatura asignatura = Asignatura.getAsignatura(Long.parseLong(asignaturaId));

        PendientesTraducir pendientesTraducir = PendientesTraducir.getPendientesTraducir(
                Long.parseLong(asignaturaId));

        List<Persona> slt = new ArrayList<Persona>();

        if (pendientesTraducir.getPendientesUk().compareTo(0L) > 0)
        {
            slt.add(Persona.getPersonaByLogin("fherrero"));
            slt.add(Persona.getPersonaByLogin("andrews"));
        }

        if (pendientesTraducir.getPendientesCa().compareTo(0L) > 0
                || pendientesTraducir.getPendientesEs().compareTo(0L) > 0)
        {
            slt.add(Persona.getPersonaByLogin("andreu"));
            slt.add(Persona.getPersonaByLogin("tolos"));
            slt.add(Persona.getPersonaByLogin("montielj"));
            slt.add(Persona.getPersonaByLogin("boluda"));
            slt.add(Persona.getPersonaByLogin("chorda"));
        }

        if (slt.isEmpty())
        {
            throw new NotificacionSinDestinatarios(
                    "No es troba usuaris destinataris.\n" + "No es pot enviar notificació.");
        }

        Notificacion.enviaNotificacion("SLT", slt, asignatura);

        UIEntity entity = new UIEntity();
        entity.put("nombres", destinatarios(slt));

        return Collections.singletonList(entity);
    }
}
