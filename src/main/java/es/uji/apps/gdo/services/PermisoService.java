package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Transient;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Permiso;
import es.uji.apps.gdo.model.Persona;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;

@Path("permiso")
public class PermisoService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActividades(@QueryParam("asignaturaId") String asignaturaId)
    {
        if (asignaturaId != null)
        {
            List<Permiso> listaPermisos = Permiso.getPermisosPorAsignatura(Long
                    .parseLong(asignaturaId));
            List<UIEntity> result = new ArrayList<UIEntity>();

            for (Permiso permiso : listaPermisos)
            {
                UIEntity entity = UIEntity.toUI(permiso);
                entity.put("personaId", permiso.getGdoExtPersona().getId());
                entity.put("nombre", permiso.getGdoExtPersona().getNombreCompleto());
                entity.put("asignaturaId", permiso.getGdoAsignatura().getId());

                result.add(entity);
            }

            return result;
        }
        else
        {
            return new ArrayList<UIEntity>();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transient
    public List<UIEntity> insertPermiso(UIEntity entity) throws RegistroDuplicadoException
    {
        if (entity.get("personaId") == null || entity.get("asignaturaId") == null)
        {
            return new ArrayList<UIEntity>();
        }

        try
        {
            Permiso permiso = new Permiso();

            permiso.setGdoAsignatura(Asignatura.getAsignatura(Long.parseLong(entity
                    .get("asignaturaId"))));
            permiso.setGdoExtPersona(Persona.getPersona(Long.parseLong(entity.get("personaId"))));

            permiso.insert();
            
            entity.put("id", permiso.getId());

            return Collections.singletonList(entity);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new RegistroDuplicadoException();
        }

    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response delete(@PathParam("id") String id)
    {
        try
        {
            Permiso.delete(Long.parseLong(id));
            return Response.ok().build();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }
}