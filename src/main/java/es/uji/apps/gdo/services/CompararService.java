package es.uji.apps.gdo.services;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.outerj.daisy.diff.helper.NekoHtmlParser;
import org.outerj.daisy.diff.html.HTMLDiffer;
import org.outerj.daisy.diff.html.HtmlSaxDiffOutput;
import org.outerj.daisy.diff.html.TextNodeComparator;
import org.outerj.daisy.diff.html.dom.DomTreeBuilder;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.gdo.dao.TextoDAO;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Texto;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("comparar")
public class CompararService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(CompararService.class);

    @InjectParam
    private TextoDAO textoDAO;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity compararVersionAnterior(UIEntity entity) throws Exception
    {
        Long curso = entity.getLong("curso");
        Long apartadoId = entity.getLong("apartadoId");
        String codigoAsignatura = entity.get("codigoAsignatura");
        String idioma = entity.get("idioma");
        String textoNuevo = entity.get("nuevo");

        Asignatura asignatura = Asignatura.getAsignaturaByCodigoAndCurso(codigoAsignatura, curso);
        Texto texto = (asignatura != null) ? Texto.getTextoByAsignaturaApartadoOrDefault(asignatura.getId(), apartadoId) : new Texto();

        String diff = diff(texto.getTextoByIdioma(idioma), textoNuevo);
        entity.put("resultado", diff);
        return entity;
    }

    @POST
    @Path("texto")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity compararVersionAnteriorTexto(UIEntity entity) throws Exception
    {
        Long textoId = entity.getLong("textoId");
        String idioma = entity.get("idioma");

        Texto textoAsignaturaActual = textoDAO.getTexto(textoId);
        String textoActual = (textoAsignaturaActual != null) ? textoAsignaturaActual.getTextoByIdioma(idioma) : "";
        String textoAnterior = "";

        if (textoAsignaturaActual != null)
        {
            Asignatura asignatura = Asignatura.getAsignaturaByCodigoAndCurso(textoAsignaturaActual.getGdoAsignatura().getGdoExtAsiToda().getId(), textoAsignaturaActual.getGdoAsignatura().getGdoCursosAcademico().getCursoAca() - 1);
            if (asignatura != null)
            {
                Texto textoAsignaturaAnterior = Texto.getTextosByAsignaturaApartado(asignatura.getId(), textoAsignaturaActual.getGdoApartado().getId()).get(0);
                textoAnterior = (textoAsignaturaAnterior != null) ? textoAsignaturaAnterior.getTextoByIdioma(idioma) : "";
            }
        }

        String diff = diff(textoAnterior, textoActual);
        entity.put("resultado", diff);
        entity.put("apartado", textoAsignaturaActual.getGdoApartado().getApartadoByIdioma(idioma));
        return entity;
    }

    private String diff(String originalTexto, String nuevoTexto) throws Exception
    {
        StringWriter finalResult = new StringWriter();
        SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();

        TransformerHandler result = tf.newTransformerHandler();
        result.getTransformer().setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        result.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
        result.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
        result.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        result.setResult(new StreamResult(finalResult));

        ContentHandler postProcess = result;

        Locale locale = Locale.getDefault();
        String prefix = "diff";

        NekoHtmlParser cleaner = new NekoHtmlParser();

        InputSource oldSource = new InputSource(new StringReader(originalTexto != null ? originalTexto.replaceAll("(?=<!--)([\\s\\S]*?)-->", "") : ""));
        InputSource newSource = new InputSource(new StringReader(nuevoTexto != null ? nuevoTexto.replaceAll("(?=<!--)([\\s\\S]*?)-->", "") : ""));

        DomTreeBuilder oldHandler = new DomTreeBuilder();
        cleaner.parse(oldSource, oldHandler);
        TextNodeComparator leftComparator = new TextNodeComparator(oldHandler, locale);

        DomTreeBuilder newHandler = new DomTreeBuilder();
        cleaner.parse(newSource, newHandler);
        TextNodeComparator rightComparator = new TextNodeComparator(newHandler, locale);

        HtmlSaxDiffOutput output = new HtmlSaxDiffOutput(postProcess, prefix);

        HTMLDiffer differ = new HTMLDiffer(output);
        differ.diff(leftComparator, rightComparator);

        return finalResult.toString();
    }

}
