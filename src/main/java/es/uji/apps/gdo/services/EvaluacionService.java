package es.uji.apps.gdo.services;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Evaluacion;
import es.uji.apps.gdo.model.domains.Operacion;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("evaluacion")
public class EvaluacionService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEvaluaciones(
            @QueryParam("asignaturaId") @DefaultValue("0") String asignaturaId)
    {
        List<Evaluacion> listaEvaluaciones = Evaluacion.getEvaluacionesByAsignaturaId(Long
                .parseLong(asignaturaId));

        List<UIEntity> lista = UIEntity.toUI(listaEvaluaciones);
        return lista;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage update(@PathParam("id") String id, UIEntity entity)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Evaluacion evaluacion = Evaluacion.getEvaluacion(Long.parseLong(id));

        evaluacion.setNombreCa(entity.get("nombreCa"));
        evaluacion.setNombreEs(entity.get("nombreEs"));
        evaluacion.setNombreUk(entity.get("nombreUk"));
        evaluacion.setPonderacion(Float.parseFloat((entity.get("ponderacion"))));
        evaluacion.setOrden(Long.parseLong(entity.get("orden")));

        evaluacion.update();

        return new ResponseMessage(true);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert( @QueryParam("asignaturaId") @DefaultValue("0") Long asignaturaId,UIEntity entity) throws AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion
    {
        Evaluacion evaluacion = entity.toModel(Evaluacion.class);

        Asignatura asignatura = Asignatura
                .getAsignatura(asignaturaId);

        evaluacion.setGdoAsignatura(asignatura);
        Evaluacion newEvaluacion = evaluacion.insert();

        entity.put("id", newEvaluacion.getId());

        return Collections.singletonList(entity);
    }

    @GET
    @Path("estado/{asignaturaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOperacionesUIposibles(@PathParam("asignaturaId") String asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        UIEntity operaciones = new UIEntity();

        operaciones.put("asignaturaId", Long.valueOf(asignaturaId));
        operaciones.put("allowDelete", Evaluacion.operacionPermitidaByAsignaturaId(
                Long.valueOf(asignaturaId), Operacion.DELETE));
        operaciones.put("allowInsert", Evaluacion.operacionPermitidaByAsignaturaId(
                Long.valueOf(asignaturaId), Operacion.INSERT));
        operaciones.put("allowUpdate", Evaluacion.operacionPermitidaByAsignaturaId(
                Long.valueOf(asignaturaId), Operacion.UPDATE));
        operaciones.put("allowUpdateMaestro", Evaluacion.operacionPermitidaByAsignaturaId(
                Long.valueOf(asignaturaId), Operacion.UPDATEGENERAL));

        return Collections.singletonList(operaciones);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") String id) throws NumberFormatException,
            AsignaturaNoModificableException, RegistroNoEncontradoException,
            AsignaturaNoAsignadaATitulacion
    {
        Evaluacion.delete(Long.parseLong(id));

        return new ResponseMessage(true);
    }
}