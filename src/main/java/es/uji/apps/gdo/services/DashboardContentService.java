package es.uji.apps.gdo.services;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.QueryParam;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.gdo.dao.DashboardDAO;
import es.uji.apps.gdo.ui.ResumenEstadisticas;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("dashboard")
public class DashboardContentService extends CoreBaseService
{
    @InjectParam
    private DashboardDAO dashboardDAO;

    @GET
    @Path("estadisticas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ResumenEstadisticas> getStatistics(@QueryParam("centroId") Long centroId, @QueryParam("titulacionId") Long titulacionId, @QueryParam("cursoId") Long cursoId)
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        return Collections.singletonList(dashboardDAO.getEstadisticas(personaId, centroId, titulacionId, cursoId));
    }

}
