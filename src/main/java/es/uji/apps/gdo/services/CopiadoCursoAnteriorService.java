package es.uji.apps.gdo.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.model.CopiadoCursoAnterior;
import es.uji.commons.messaging.client.MissingConfigurationFileException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;

@Path("copiacurso")
public class CopiadoCursoAnteriorService extends CoreBaseService
{
    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage copiaCurso(@PathParam("id") String cursoId)
            throws AsignaturaNoModificableException, MissingConfigurationFileException
    {
        Long cursoDestinoId = Long.parseLong(cursoId);
        Long cursoOrigenId = cursoDestinoId - 1L;

        CopiadoCursoAnterior.copiaCursoAcademico(cursoOrigenId, cursoDestinoId);

        return new ResponseMessage(true);
    }

    @GET
    @Path("{cursoId}/{asignaturaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage copiaCurso(@PathParam("cursoId") String cursoId, @PathParam("asignaturaId") String asignaturaId)
            throws AsignaturaNoModificableException, MissingConfigurationFileException
    {
        Long cursoDestinoId = Long.parseLong(cursoId);
        Long cursoOrigenId = cursoDestinoId - 1L;

        CopiadoCursoAnterior.copiaCursoAcademico(cursoOrigenId, cursoDestinoId, asignaturaId);

        return new ResponseMessage(true);
    }
}
