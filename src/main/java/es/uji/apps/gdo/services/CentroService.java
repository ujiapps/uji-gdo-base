package es.uji.apps.gdo.services;

import es.uji.apps.gdo.model.Centro;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("centro")
public class CentroService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCentros()
    {
        List<Centro> centros = Centro.getCentros();
        return UIEntity.toUI(centros);
    }
}