package es.uji.apps.gdo.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.HtmlCleaner;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Texto;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("texto")
public class TextoService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(TextoService.class);

    private UIEntity modelToUI(Texto texto) throws AsignaturaNoAsignadaATitulacion
    {
        UIEntity entity = UIEntity.toUI(texto);

        Asignatura asignatura = texto.getGdoAsignatura();

        entity.put("apartadoId", texto.getGdoApartado().getId());
        entity.put("asignaturaId", asignatura.getId());
        entity.put("esModificable", texto.getGdoApartado().isModificable(asignatura));

        return entity;
    }

    private List<UIEntity> modelToUI(List<Texto> listaTextos) throws AsignaturaNoAsignadaATitulacion
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        for (Texto texto : listaTextos)
        {
            result.add(modelToUI(texto));
        }

        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTextos(@QueryParam("asignaturaId") String asignaturaId,
                                    @QueryParam("apartadoId") String apartadoId) throws AsignaturaNoAsignadaATitulacion
    {

        List<Texto> textos = Texto.getTextosByAsignaturaApartado(Long.parseLong(asignaturaId),
                Long.parseLong(apartadoId));

        List<UIEntity> result = modelToUI(textos);

        return result;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTexto(@PathParam("id") String id)
    {
        return UIEntity.toUI(Texto.getTexto(Long.parseLong(id)));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response putTexto(@FormParam("id") Long id, @FormParam("textoCa") String textoCa, @FormParam("textoEs") String textoEs, @FormParam("textoUk") String textoUk, @Context HttpServletRequest request0)
            throws AsignaturaNoModificableException
    {
        Texto texto = Texto.getTexto(id).get(0);

        texto.setTextoCa(textoCa);
        texto.setTextoEs(textoEs);
        texto.setTextoUk(textoUk);

        texto.update();

        return Response.ok().build();

    }

    @PUT
    @Path("{id}/validar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity validarTexto(@PathParam("id") Long id, UIEntity entity)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Texto texto = Texto.getTexto(id).get(0);
        texto.validarIdioma(entity.get("idioma"));

        return modelToUI(texto);
    }

    @PUT
    @Path("{id}/modificar-idioma")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity guardarTexto(@PathParam("id") Long id, UIEntity entity)
            throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        Texto texto = Texto.getTexto(id).get(0);
        texto.guardarIdioma(entity.get("idioma"), entity.get("texto"));

        return modelToUI(texto);
    }

    @POST
    @Path("limpiar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity limpiaTexto(@FormParam("texto") String texto)
    {
        HtmlCleaner cleaner = new HtmlCleaner();
        String resultText = cleaner.clean(texto);
        UIEntity entity = new UIEntity();
        entity.put("textoLimpio", resultText);
        return entity;
    }

}
