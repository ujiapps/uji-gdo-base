package es.uji.apps.gdo.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.gdo.model.RevisionApartadoSLT;
import org.apache.log4j.Logger;

import es.uji.apps.gdo.model.Apartado;
import es.uji.apps.gdo.model.Texto;
import es.uji.apps.gdo.model.domains.TipoApartado;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("apartado")
public class ApartadoService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(ApartadoService.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getApartadosByAsignaturaAndTipo(
            @QueryParam("asignaturaId") String asignaturaId,
            @QueryParam("tipoApartado") String tipoApartado)
    {
        List<Apartado> listaApartados = Apartado.getApartadosByAsignaturaAndTipo(
                Long.parseLong(asignaturaId), TipoApartado.valueOf(tipoApartado));

        List<RevisionApartadoSLT> revisionApartados = (TipoApartado.T.equals(TipoApartado.valueOf(tipoApartado))) ?
                RevisionApartadoSLT.getRevisionApartadoSLTByAsignatura(Long.parseLong(asignaturaId)) : null;

        return toUI(listaApartados, revisionApartados);
    }

    private List<UIEntity> toUI(List<Apartado> listaApartados, List<RevisionApartadoSLT> revisionApartados)
    {
        return listaApartados.stream().map(a -> toUI(a, revisionApartados)).collect(Collectors.toList());
    }

    private UIEntity toUI(Apartado apartado, List<RevisionApartadoSLT> revisionApartados)
    {
        UIEntity entity = UIEntity.toUI(apartado);

        String obligatorio = "";
        if (entity.get("obligatorio").equals("S"))
        {
            obligatorio = " (obligatori)";
        }

        entity.put("descripcion", entity.get("nombreCa") + obligatorio);

        Texto texto = apartado.getGdoAsiTextos().iterator().next();
        if (texto != null)
        {
            entity.put("fechaSltEs", texto.getFechaSltEs());
            entity.put("fechaSltCa", texto.getFechaSltCa());
            entity.put("fechaSltUk", texto.getFechaSltUk());

            entity.put("modificadoEs", texto.isModificadoEs());
            entity.put("modificadoCa", texto.isModificadoCa());
            entity.put("modificadoUk", texto.isModificadoUk());

            if (revisionApartados != null)
            {
                RevisionApartadoSLT revisionApartado = revisionApartados.stream().filter(r -> r.getApartadoId().equals(apartado.getId())).findFirst().orElse(null);
                if (revisionApartado != null)
                {

                    entity.put("ultimaRevisionSltEs", formatDate(revisionApartado.getUltimaRevisionSltEs()));
                    entity.put("ultimaRevisionSltCa", formatDate(revisionApartado.getUltimaRevisionSltCa()));
                    entity.put("ultimaRevisionSltUk", formatDate(revisionApartado.getUltimaRevisionSltUk()));
                }
            }
        }
        return entity;
    }

    private String formatDate(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return (date != null) ? dateFormat.format(date) : null;
    }
}