package es.uji.apps.gdo;

public class HtmlCleaner
{
    private String content;

    public String clean(String inputText)
    {
        this.content = inputText;

        cleanTitle();
        cleanComments();
        cleanClassesAndStyles();
        cleanXML();
        cleanUnnecessaryTags();
        cleanEmptyParagraphs();
        cleanExtraLines();
        fixEntities();

        return content.trim();
    }

    private void cleanTitle()
    {
        content = content.replaceAll("\\<title>(\\w|\\W)+?</title>", "");
    }

    private void cleanComments()
    {
        content = content.replaceAll("(?s)<!--.*?-->", "");
    }

    private void cleanClassesAndStyles()
    {
        content = content.replaceAll("\\s*class=\\w+", "");
        content = content.replaceAll("\\s*class=\"[^\"]+\"", "");
        content = content.replaceAll("\\s*class='[^']+'", "");
        content = content.replaceAll("\\s+style=\"[^\"]+\"", "");
        content = content.replaceAll("\\s+style='[^']+'", "");
    }

    private void cleanXML()
    {
        content = content.replaceAll("<\\?[^>]+", "");
    }

    private void cleanUnnecessaryTags()
    {
        content = content.replaceAll("</?meta[^>]*>", "");
        content = content.replaceAll("</?font[^>]*>", "");
        content = content.replaceAll("</?link[^>]*>", "");
        content = content.replaceAll("</?body[^>]*>", "");
        content = content.replaceAll("</?span[^>]*>", "");
        content = content.replaceAll("</?style[^>]*>", "");
        content = content.replaceAll("</?div[^>]*>", "");
        content = content.replaceAll("</?head[^>]*>", "");
        content = content.replaceAll("</?html[^>]*>", "");
    }

    private void cleanEmptyParagraphs()
    {
        content = content.replaceAll("\\s+v:\\w+=\"\"[^\"\"]+\"\"", "");
    }

    private void cleanExtraLines()
    {
        content = content.replaceAll("\"(\n\r){2,}", "");
    }

    private void fixEntities()
    {
        content = content.replaceAll("&ldquo;", "\"");
        content = content.replaceAll("&rdquo;", "\"");
        content = content.replaceAll("&mdash;", "–");
        content = content.replaceAll("\\s*style=\"\"\\s*", "");

    }
}
