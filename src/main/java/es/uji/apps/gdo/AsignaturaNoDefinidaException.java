package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class AsignaturaNoDefinidaException extends GeneralGDOException
{
    public AsignaturaNoDefinidaException(String message)
    {
        super(message);
    }
}