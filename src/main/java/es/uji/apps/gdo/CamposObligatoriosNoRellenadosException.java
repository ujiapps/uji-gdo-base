package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class CamposObligatoriosNoRellenadosException extends Exception
{
    public CamposObligatoriosNoRellenadosException()
    {
        super("No s'han completat tots els apartats obligatoris");
    }

    public CamposObligatoriosNoRellenadosException(String message)
    {
        super(message);
    }
}
