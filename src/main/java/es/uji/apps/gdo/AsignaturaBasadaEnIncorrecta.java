package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class AsignaturaBasadaEnIncorrecta extends Exception {
    public AsignaturaBasadaEnIncorrecta() {
        super("En les assignatures basades en una altra cal especificar si està basada totalment o parcial excepte apartats del verifica.");
    }

    public AsignaturaBasadaEnIncorrecta(String message) {
        super(message);
    }
}