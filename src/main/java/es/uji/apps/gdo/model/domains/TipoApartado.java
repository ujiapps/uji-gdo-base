package es.uji.apps.gdo.model.domains;

public enum TipoApartado
{
    T("Texte"), C("Competència"), A("Activitat"), E("Avaluació"), I("Idioma");

    private String descripcion;

    private TipoApartado(String descripcion)
    {
        this.descripcion = descripcion;
    }

    @Override
    public String toString()
    {
        return descripcion;
    }
}