package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.CompetenciaDAO;
import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the GDO_ASI_COMPETENCIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_COMPETENCIAS")
@Component
public class Competencia implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "COMPETENCIA_CA")
    private String competenciaCa;

    @Column(name = "COMPETENCIA_ES")
    private String competenciaEs;

    @Column(name = "COMPETENCIA_UK")
    private String competenciaUk;

    private String editable;

    private Long orden;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    private static CompetenciaDAO competenciaDAO;

    @Autowired
    public void setCompetenciaDAO(CompetenciaDAO competenciaDAO)
    {
        Competencia.competenciaDAO = competenciaDAO;
    }

    public Competencia()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCompetenciaCa()
    {
        return this.competenciaCa;
    }

    public void setCompetenciaCa(String competenciaCa)
    {
        this.competenciaCa = competenciaCa;
    }

    public String getCompetenciaEs()
    {
        return this.competenciaEs;
    }

    public void setCompetenciaEs(String competenciaEs)
    {
        this.competenciaEs = competenciaEs;
    }

    public String getCompetenciaUk()
    {
        return this.competenciaUk;
    }

    public void setCompetenciaUk(String competenciaUk)
    {
        this.competenciaUk = competenciaUk;
    }

    public String getEditable()
    {
        return this.editable;
    }

    public void setEditable(String editable)
    {
        this.editable = editable;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    @Transactional
    public void update() throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        if (isEditable())
        {
            competenciaDAO.update(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    public static Competencia getCompetencia(long idCompetencia)
    {
        return competenciaDAO.getCompetencia(idCompetencia);
    }

    public static List<Competencia> getCompetenciasByAsignatura(long idAsignatura)
    {

        return competenciaDAO.getCompetenciasByAsignatura(idAsignatura);
    }

    @Transient
    private boolean isEditable() throws AsignaturaNoAsignadaATitulacion
    {
        Asignatura asignatura = getGdoAsignatura();
        return ("S".equals(editable) && asignatura.isModificable() && operacionPermitida());
    }

    public boolean operacionPermitida() throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = getGdoAsignatura().getTitulacion();
        return isPermitidaOperationEnTitulacion(titulacion);
    }

    public static boolean operacionPermitidaByAsignaturaId(Long asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = Titulacion.getTitulacionByAsignaturaId(asignaturaId);
        return isPermitidaOperationEnTitulacion(titulacion);
    }

    private static boolean isPermitidaOperationEnTitulacion(Titulacion titulacion)
    {
        TipoEstudio tipoEstudio = TipoEstudio.valueOf(titulacion.getTipo());
        return isEstudioModificable(tipoEstudio);
    }

    private static boolean isEstudioModificable(TipoEstudio tipoEstudio)
    {
       /* if (TipoEstudio.M.equals(tipoEstudio))
        {
            return true;
        } */
        
        return false;
    }

    @Transactional
    public static void delete(long competenciaId) throws AsignaturaNoModificableException,
            RegistroNoEncontradoException, AsignaturaNoAsignadaATitulacion
    {
        List<Competencia> listaCompetencias = competenciaDAO.get(Competencia.class, competenciaId);

        if (listaCompetencias != null && listaCompetencias.size() > 0)
        {
            Competencia competencia = listaCompetencias.get(0);

            if (competencia.isEditable())
            {
                competenciaDAO.delete(Competencia.class, competenciaId);
            }
            else
            {
                throw new AsignaturaNoModificableException();
            }
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public void insertParaCopia()
    {
        competenciaDAO.insert(this);
    }

    @Transactional
    public Competencia insert() throws AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion
    {
        if (isEditable())
        {
            return competenciaDAO.insert(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }
}