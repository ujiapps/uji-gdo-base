package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.VerificaTextoDAO;


/**
 * The persistent class for the GDO_EXT_ASI_DETALLE database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name="GDO_EXT_ASI_DETALLE")
@Component
public class VerificaTexto implements Serializable {

    @Id
	@Column(name="COD_ASIGNATURA")
	private String codAsignatura;

    @Lob()
	@Column(name="CONTENIDOS_CA")
	private String contenidosCa;

    @Lob()
	@Column(name="CONTENIDOS_ES")
	private String contenidosEs;

	@Column(name="CURSO_ACA_FIN")
	private BigDecimal cursoAcaFin;

	@Id
	@Column(name="CURSO_ACA_INI")
	private BigDecimal cursoAcaIni;

	@Column(name="HORAS_AL")
	private BigDecimal horasAl;

	@Column(name="JUSTIFICACION_CA")
	private String justificacionCa;

	@Column(name="JUSTIFICACION_ES")
	private String justificacionEs;

    @Lob()
	@Column(name="PREREQUISITOS_CA")
	private String prerequisitosCa;

    @Lob()
	@Column(name="PREREQUISITOS_ES")
	private String prerequisitosEs;
    
    private static VerificaTextoDAO verificaTextoDAO;
    
    @Autowired
    public void setVerificaTextoDAO(VerificaTextoDAO verificaTextoDAO)
    {
        VerificaTexto.verificaTextoDAO = verificaTextoDAO;
    }

    public VerificaTexto() {
    }

	public String getCodAsignatura() {
		return this.codAsignatura;
	}

	public void setCodAsignatura(String codAsignatura) {
		this.codAsignatura = codAsignatura;
	}

	public String getContenidosCa() {
		return this.contenidosCa;
	}

	public void setContenidosCa(String contenidosCa) {
		this.contenidosCa = contenidosCa;
	}

	public String getContenidosEs() {
		return this.contenidosEs;
	}

	public void setContenidosEs(String contenidosEs) {
		this.contenidosEs = contenidosEs;
	}

	public BigDecimal getCursoAcaFin() {
		return this.cursoAcaFin;
	}

	public void setCursoAcaFin(BigDecimal cursoAcaFin) {
		this.cursoAcaFin = cursoAcaFin;
	}

	public BigDecimal getCursoAcaIni() {
		return this.cursoAcaIni;
	}

	public void setCursoAcaIni(BigDecimal cursoAcaIni) {
		this.cursoAcaIni = cursoAcaIni;
	}

	public BigDecimal getHorasAl() {
		return this.horasAl;
	}

	public void setHorasAl(BigDecimal horasAl) {
		this.horasAl = horasAl;
	}

	public String getJustificacionCa() {
		return this.justificacionCa;
	}

	public void setJustificacionCa(String justificacionCa) {
		this.justificacionCa = justificacionCa;
	}

	public String getJustificacionEs() {
		return this.justificacionEs;
	}

	public void setJustificacionEs(String justificacionEs) {
		this.justificacionEs = justificacionEs;
	}

	public String getPrerequisitosCa() {
		return this.prerequisitosCa;
	}

	public void setPrerequisitosCa(String prerequisitosCa) {
		this.prerequisitosCa = prerequisitosCa;
	}

	public String getPrerequisitosEs() {
		return this.prerequisitosEs;
	}

	public void setPrerequisitosEs(String prerequisitosEs) {
		this.prerequisitosEs = prerequisitosEs;
	}
	
	public static VerificaTexto getTextoByAsignatura(String codigoAsignatura)
	{
	   return verificaTextoDAO.getContenidosByAsignatura(codigoAsignatura).get(0);
	}
}