package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.dao.PermisoDAO;

/**
 * The persistent class for the GDO_ASI_PERMISOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_PERMISOS")
@Component
public class Permiso implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    // bi-directional many-to-one association to GdoExtPersonas
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona gdoExtPersona;

    private static PermisoDAO permisoDAO;

    @Autowired
    public void setPermisoDAO(PermisoDAO permisoDAO)
    {
        Permiso.permisoDAO = permisoDAO;
    }

    public Permiso()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    public Persona getGdoExtPersona()
    {
        return this.gdoExtPersona;
    }

    public void setGdoExtPersona(Persona gdoExtPersona)
    {
        this.gdoExtPersona = gdoExtPersona;
    }

    public static List<Permiso> getPermisosPorAsignatura(long asignaturaId)
    {
        return permisoDAO.getPermisosByAsignaturaId(asignaturaId);
    }

    @Transactional
    public Permiso insert()
    {
        return permisoDAO.insert(this);
    }

    public static void delete(long permisoId)
    {
        permisoDAO.delete(Permiso.class, permisoId);
    }
}