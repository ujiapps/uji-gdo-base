package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.TextoDAO;

/**
 * The persistent class for the GDO_ASI_TEXTOS database table.
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_TEXTOS")
@Component
public class Texto implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_SLT_CA")
    private Date fechaSltCa;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_SLT_ES")
    private Date fechaSltEs;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_SLT_UK")
    private Date fechaSltUk;

    @Column(name = "MODIFICADO_CA")
    private Boolean modificadoCa;

    @Column(name = "MODIFICADO_ES")
    private Boolean modificadoEs;

    @Column(name = "MODIFICADO_UK")
    private Boolean modificadoUk;

    @Lob()
    @Column(name = "TEXTO_CA")
    private String textoCa;

    @Lob()
    @Column(name = "TEXTO_ES")
    private String textoEs;

    @Lob()
    @Column(name = "TEXTO_UK")
    private String textoUk;

    // bi-directional many-to-one association to GdoApartados
    @ManyToOne
    @JoinColumn(name = "APARTADO_ID")
    private Apartado gdoApartado;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    private static TextoDAO textoDAO;

    @Autowired
    public void setTextoDAO(TextoDAO textoDAO)
    {
        Texto.textoDAO = textoDAO;
    }

    public Texto()
    {
        modificadoCa = false;
        modificadoEs = false;
        modificadoUk = false;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFechaSltCa()
    {
        return this.fechaSltCa;
    }

    public void setFechaSltCa(Date fechaSltCa)
    {
        this.fechaSltCa = fechaSltCa;
    }

    public Date getFechaSltEs()
    {
        return this.fechaSltEs;
    }

    public void setFechaSltEs(Date fechaSltEs)
    {
        this.fechaSltEs = fechaSltEs;
    }

    public Date getFechaSltUk()
    {
        return this.fechaSltUk;
    }

    public void setFechaSltUk(Date fechaSltUk)
    {
        this.fechaSltUk = fechaSltUk;
    }

    public String getTextoCa()
    {
        return this.textoCa;
    }

    public void setTextoCa(String textoCa)
    {
        this.textoCa = textoCa;
    }

    public String getTextoEs()
    {
        return this.textoEs;
    }

    public void setTextoEs(String textoEs)
    {
        this.textoEs = textoEs;
    }

    public String getTextoUk()
    {
        return this.textoUk;
    }

    public void setTextoUk(String textoUk)
    {
        this.textoUk = textoUk;
    }

    public Apartado getGdoApartado()
    {
        return this.gdoApartado;
    }

    public void setGdoApartado(Apartado gdoApartado)
    {
        this.gdoApartado = gdoApartado;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    public static List<Texto> getTexto(long textoId)
    {
        Texto textoDB = textoDAO.getTexto(textoId);

        return Collections.singletonList(textoDB);
    }

    public static List<Texto> getTextosByAsignaturaApartado(long asignaturaId, long apartadoId)
    {
        Texto textoDB = textoDAO.getTextosByAsignaturaApartado(asignaturaId, apartadoId);

        return Collections.singletonList(textoDB);
    }

    public static Texto getTextoByAsignaturaApartadoOrDefault(long asignaturaId, long apartadoId)
    {
        Texto textoDB = textoDAO.getTextosByAsignaturaApartado(asignaturaId, apartadoId);
        return (textoDB != null) ? textoDB : new Texto();
    }

    public Boolean isModificadoCa()
    {
        return modificadoCa;
    }

    public void setModificadoCa(Boolean modificadoCa)
    {
        this.modificadoCa = modificadoCa;
    }

    public Boolean isModificadoEs()
    {
        return modificadoEs;
    }

    public void setModificadoEs(Boolean modificadoEs)
    {
        this.modificadoEs = modificadoEs;
    }

    public Boolean isModificadoUk()
    {
        return modificadoUk;
    }

    public void setModificadoUk(Boolean modificadoUk)
    {
        this.modificadoUk = modificadoUk;
    }

    @Transactional
    public void update() throws AsignaturaNoModificableException
    {
        if (gdoAsignatura.isModificable())
        {
            textoDAO.updateTexto(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    public void updateParaCopia()
    {
        textoDAO.updateTexto(this);
    }

    public static List<Texto> getTextosByAsignatura(Long asignaturaId)
    {
        return textoDAO.getTextosByAsignatura(asignaturaId);
    }

    @Transactional
    public void insertTexto()
    {
        textoDAO.insert(this);
    }

    @Transactional
    public void validarIdioma(String idioma) throws AsignaturaNoModificableException
    {
        if (gdoAsignatura.getEnSlt().equals("N"))
        {
            throw new AsignaturaNoModificableException();
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                setFechaSltEs(new Date());
                break;
            case "CA":
                setFechaSltCa(new Date());
                break;
            case "UK":
                setFechaSltUk(new Date());
                break;
        }
        textoDAO.updateTexto(this);
    }

    @Transactional
    public void guardarIdioma(String idioma, String texto) throws AsignaturaNoModificableException
    {
        if (gdoAsignatura.getEnSlt().equals("N"))
        {
            throw new AsignaturaNoModificableException();
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                setTextoEs(texto);
                setFechaSltEs(null);
                break;
            case "CA":
                setTextoCa(texto);
                setFechaSltCa(null);
                break;
            case "UK":
                setTextoUk(texto);
                setFechaSltUk(null);
                break;
        }
        textoDAO.updateTexto(this);
    }

    public String getTextoByIdioma(String idioma)
    {
        switch (idioma.toUpperCase())
        {
            case "ES":
                return textoEs;
            case "CA":
                return textoCa;
            case "UK":
                return textoUk;
            default:
                return null;
        }
    }

    public Long getNumeroIdiomasModificados()
    {
        Long idiomasModificados = 0L;
        if (modificadoCa) idiomasModificados++;
        if (modificadoEs) idiomasModificados++;
        if (modificadoUk) idiomasModificados++;
        return idiomasModificados;
    }
}