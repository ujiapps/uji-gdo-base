package es.uji.apps.gdo.model;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.dao.CentroDAO;
import es.uji.apps.gdo.dao.TitulacionDAO;
import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * The persistent class for the GDO_EXT_TITU_TODAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_EXT_CENTROS")
@Component
public class Centro implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private static CentroDAO centroDAO;

    @Autowired
    public void setCentrosDAO(CentroDAO centroDAO)
    {
        Centro.centroDAO = centroDAO;
    }

    public Centro()
    {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public static List<Centro> getCentros()
    {
        return centroDAO.getCentros();
    }
}