package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.VerificaEvaluacionDAO;


/**
 * The persistent class for the GDO_VW_EXT_EVALUACIONES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name="GDO_VW_EXT_EVALUACIONES")
@Component
public class VerificaEvaluacion implements Serializable {

    @Id
	@Column(name="COD_ASIGNATURA")
	private String codAsignatura;

	@Column(name="CURSO_ACA_FIN")
	private Long cursoAcaFin;

	@Id
	@Column(name="CURSO_ACA_INI")
	private Long cursoAcaIni;

	@Id
	@Column(name="EVAL_ID")
	private Long evalId;

	@Column(name="NOMBRE_CA")
	private String nombreCa;

	@Column(name="NOMBRE_ES")
	private String nombreEs;

	@Column(name="NOMBRE_UK")
	private String nombreUk;

	private Float ponderacion;
	
	private static VerificaEvaluacionDAO verificaEvaluacionDAO;
	
	@Autowired
	public void setVerificaEvaluacionDAO(VerificaEvaluacionDAO verificaEvaluacionDAO)
	{
	    VerificaEvaluacion.verificaEvaluacionDAO = verificaEvaluacionDAO;
	}

    public VerificaEvaluacion() {
    }

	public String getCodAsignatura() {
		return this.codAsignatura;
	}

	public void setCodAsignatura(String codAsignatura) {
		this.codAsignatura = codAsignatura;
	}

	public Long getCursoAcaFin() {
		return this.cursoAcaFin;
	}

	public void setCursoAcaFin(Long cursoAcaFin) {
		this.cursoAcaFin = cursoAcaFin;
	}

	public Long getCursoAcaIni() {
		return this.cursoAcaIni;
	}

	public void setCursoAcaIni(Long cursoAcaIni) {
		this.cursoAcaIni = cursoAcaIni;
	}

	public Long getEvalId() {
		return this.evalId;
	}

	public void setEvalId(Long evalId) {
		this.evalId = evalId;
	}

	public String getNombreCa() {
		return this.nombreCa;
	}

	public void setNombreCa(String nombreCa) {
		this.nombreCa = nombreCa;
	}

	public String getNombreEs() {
		return this.nombreEs;
	}

	public void setNombreEs(String nombreEs) {
		this.nombreEs = nombreEs;
	}

	public String getNombreUk() {
		return this.nombreUk;
	}

	public void setNombreUk(String nombreUk) {
		this.nombreUk = nombreUk;
	}

	public Float getPonderacion() {
		return this.ponderacion;
	}

	public void setPonderacion(Float ponderacion) {
		this.ponderacion = ponderacion;
	}

    public static List<VerificaEvaluacion> getVerificaEvaluacionByAsignatura(Asignatura asignatura)
    {
        return verificaEvaluacionDAO.getEvaluacionesByAsignatura(asignatura.getGdoExtAsiToda()
                .getId(), asignatura.getGdoCursosAcademico().getCursoAca());
    }
}