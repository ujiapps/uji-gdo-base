package es.uji.apps.gdo.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.gdo.model.domains.EstadoAsignatura;

public class CopiadoCursoAnterior
{
    static Map<Long, Long> relacionAsignaturas = new HashMap<Long, Long>();

    public static void copiaCursoAcademico(Long cursoOrigenId, Long cursoDestinoid)
    {
        CursoAcademico cursoOrigen = CursoAcademico.getCursoAcademicoById(cursoOrigenId);
        CursoAcademico cursoDestino = CursoAcademico.getCursoAcademicoById(cursoDestinoid);

        copiaAsignaturas(cursoOrigen, cursoDestino);
    }

    public static void copiaCursoAcademico(Long cursoOrigenId, Long cursoDestinoId,
            String asignaturaId)
    {
        CursoAcademico cursoOrigen = CursoAcademico.getCursoAcademicoById(cursoOrigenId);
        CursoAcademico cursoDestino = CursoAcademico.getCursoAcademicoById(cursoDestinoId);

        List<Asignatura> listaAsignaturas = Asignatura
                .getAsignaturasByCursoAcademico(cursoOrigen.getCursoAca()).stream()
                .filter(a -> a.getGdoExtAsiToda().getId().equals(asignaturaId))
                .collect(Collectors.toList());

        copiaAsignaturasBase(cursoDestino, listaAsignaturas);
        copiaRelacionAsignaturas(cursoDestino, listaAsignaturas);
    }

    private static void copiaAsignaturas(CursoAcademico cursoOrigen, CursoAcademico cursoDestino)
    {
        List<Asignatura> listaAsignaturas = Asignatura
                .getAsignaturasByCursoAcademico(cursoOrigen.getCursoAca());

        copiaAsignaturasBase(cursoDestino, listaAsignaturas);
        copiaRelacionAsignaturas(cursoDestino, listaAsignaturas);
    }

    private static void copiaAsignaturasBase(CursoAcademico cursoDestino,
            List<Asignatura> listaAsignaturas)
    {
        if (listaAsignaturas != null && !listaAsignaturas.isEmpty())
        {
            for (Asignatura asignatura : listaAsignaturas)
            {
                Asignatura nuevaAsignatura = copiaAsignatura(cursoDestino, asignatura);

                if (nuevaAsignatura.getGdoAsignatura() == null)
                {
                    copiaTextos(asignatura, nuevaAsignatura);
                    copiaEvaluaciones(asignatura, nuevaAsignatura);
                    copiaActividades(asignatura, nuevaAsignatura);
                    copiaCompetencias(asignatura, nuevaAsignatura);
                    copiaResultados(asignatura, nuevaAsignatura);
                    copiaIdiomasVerifica(asignatura, nuevaAsignatura);
                }
            }
        }
    }

    private static Asignatura copiaAsignatura(CursoAcademico cursoDestino, Asignatura asignatura)
    {
        Asignatura nuevaAsignatura = new Asignatura();

        nuevaAsignatura.setAutorizaPas(asignatura.getAutorizaPas());

        if (asignatura.getEstado().equals(EstadoAsignatura.I.name()))
        {
            nuevaAsignatura.setEstado(EstadoAsignatura.I.name());
        }
        else
        {
            nuevaAsignatura.setEstado(EstadoAsignatura.D.name());
        }

        nuevaAsignatura.setGdoCursosAcademico(cursoDestino);
        nuevaAsignatura.setGdoExtAsiToda(asignatura.getGdoExtAsiToda());
        nuevaAsignatura.setGdoExtPersona(asignatura.getGdoExtPersona());
        nuevaAsignatura.setTipoCompartida(asignatura.getTipoCompartida());
        nuevaAsignatura.setEnSlt("N");
        nuevaAsignatura.setFechaUltimaModificacion(null);
        nuevaAsignatura.setDeficiencias(null);
        nuevaAsignatura = nuevaAsignatura.insertAsignatura();

        relacionAsignaturas.put(asignatura.getId(), nuevaAsignatura.getId());

        return nuevaAsignatura;
    }

    private static void copiaRelacionAsignaturas(CursoAcademico cursoDestino,
            List<Asignatura> listaAsignaturas)
    {
        if (listaAsignaturas != null && !listaAsignaturas.isEmpty())
        {
            for (Asignatura asignatura : listaAsignaturas)
            {
                Asignatura asignaturaBaseOrigen = asignatura.getGdoAsignatura();

                if (asignaturaBaseOrigen != null)
                {
                    Asignatura asignaturaDestino = Asignatura
                            .getAsignatura(relacionAsignaturas.get(asignatura.getId()));

                    asignaturaDestino.setGdoAsignatura(Asignatura.getAsignaturaByCodigoAndCurso(
                            asignaturaBaseOrigen.getGdoExtAsiToda().getId(),
                            cursoDestino.getCursoAca()));

                    asignaturaDestino.updateAsignaturaParaCopiaDeAsignaturas();
                }
            }
        }
    }

    private static void copiaTextos(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<Texto> listaTextos = Texto.getTextosByAsignatura(asignatura.getId());

        if (listaTextos != null && !listaTextos.isEmpty())
        {
            for (Texto texto : listaTextos)
            {
                Texto nuevoTexto = new Texto();

                nuevoTexto.setFechaSltCa(texto.getFechaSltCa());
                nuevoTexto.setFechaSltEs(texto.getFechaSltEs());
                nuevoTexto.setFechaSltUk(texto.getFechaSltUk());
                nuevoTexto.setTextoCa(texto.getTextoCa());
                nuevoTexto.setTextoEs(texto.getTextoEs());
                nuevoTexto.setTextoUk(texto.getTextoUk());
                nuevoTexto.setGdoApartado(texto.getGdoApartado());
                nuevoTexto.setGdoAsignatura(nuevaAsignatura);

                nuevoTexto.insertTexto();
            }
        }
    }

    private static void copiaEvaluaciones(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<Evaluacion> listaEvaluaciones = Evaluacion
                .getEvaluacionesByAsignaturaId(asignatura.getId());

        if (listaEvaluaciones != null && !listaEvaluaciones.isEmpty())
        {
            for (Evaluacion evaluacion : listaEvaluaciones)
            {
                Evaluacion nuevaEvaluacion = new Evaluacion();

                nuevaEvaluacion.setEditable(evaluacion.getEditable());
                nuevaEvaluacion.setEvaluacionId(evaluacion.getEvaluacionId());
                nuevaEvaluacion.setNombreCa(evaluacion.getNombreCa());
                nuevaEvaluacion.setNombreEs(evaluacion.getNombreEs());
                nuevaEvaluacion.setNombreUk(evaluacion.getNombreUk());
                nuevaEvaluacion.setOrden(evaluacion.getOrden());
                nuevaEvaluacion.setPonderacion(evaluacion.getPonderacion());
                nuevaEvaluacion.setGdoAsignatura(nuevaAsignatura);

                nuevaEvaluacion.insertParaCopia();
            }
        }
    }

    private static void copiaActividades(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<ActividadAsignatura> listaActividades = ActividadAsignatura
                .getActividadesByAsignaturaId(asignatura.getId());

        if (listaActividades != null && !listaActividades.isEmpty())
        {
            for (ActividadAsignatura actividadAsignatura : listaActividades)
            {
                ActividadAsignatura nuevaActividadAsignatura = new ActividadAsignatura();

                nuevaActividadAsignatura
                        .setHorasNoPresenciales(actividadAsignatura.getHorasNoPresenciales());
                nuevaActividadAsignatura
                        .setHorasPresenciales(actividadAsignatura.getHorasPresenciales());
                nuevaActividadAsignatura
                        .setGdoTipoActividade(actividadAsignatura.getGdoTipoActividade());
                nuevaActividadAsignatura.setGdoAsignatura(nuevaAsignatura);

                nuevaActividadAsignatura.insertParaCopia();
            }
        }
    }

    private static void copiaCompetencias(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<Competencia> listaCompetencias = Competencia
                .getCompetenciasByAsignatura(asignatura.getId());

        if (listaCompetencias != null && !listaCompetencias.isEmpty())
        {
            for (Competencia competencia : listaCompetencias)
            {
                Competencia nuevaCompetencia = new Competencia();

                nuevaCompetencia.setCompetenciaCa(competencia.getCompetenciaCa());
                nuevaCompetencia.setCompetenciaEs(competencia.getCompetenciaEs());
                nuevaCompetencia.setCompetenciaUk(competencia.getCompetenciaUk());
                nuevaCompetencia.setEditable(competencia.getEditable());
                nuevaCompetencia.setOrden(competencia.getOrden());
                nuevaCompetencia.setGdoAsignatura(nuevaAsignatura);

                nuevaCompetencia.insertParaCopia();
            }
        }
    }

    private static void copiaResultados(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<Resultado> listaResultados = Resultado.getResultadosByAsignatura(asignatura.getId());

        if (listaResultados != null && !listaResultados.isEmpty())
        {
            for (Resultado resultado : listaResultados)
            {
                Resultado nuevoResultado = new Resultado();

                nuevoResultado.setResultadoCa(resultado.getResultadoCa());
                nuevoResultado.setResultadoEs(resultado.getResultadoEs());
                nuevoResultado.setResultadoUk(resultado.getResultadoUk());
                nuevoResultado.setEditable(resultado.getEditable());
                nuevoResultado.setOrden(resultado.getOrden());
                nuevoResultado.setGdoAsignatura(nuevaAsignatura);

                nuevoResultado.insertParaCopia();
            }
        }
    }

    private static void copiaIdiomas(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<IdiomaAsignatura> listaIdiomas = IdiomaAsignatura
                .getIdiomasByAsignaturaId(asignatura.getId());

        if (listaIdiomas != null && !listaIdiomas.isEmpty())
        {
            for (IdiomaAsignatura idiomaAsignatura : listaIdiomas)
            {
                IdiomaAsignatura nuevoIdiomaAsignatura = new IdiomaAsignatura();

                nuevoIdiomaAsignatura.setGdoAsignatura(nuevaAsignatura);
                nuevoIdiomaAsignatura.setGdoIdioma(idiomaAsignatura.getGdoIdioma());

                nuevoIdiomaAsignatura.insert();
            }
        }
    }

    private static void copiaIdiomasVerifica(Asignatura asignatura, Asignatura nuevaAsignatura)
    {
        List<Idioma> listaIdiomas = IdiomaAsignaturaVerifica
                .getIdiomasByAsignatura(asignatura.getGdoExtAsiToda().getId());

        for (Idioma idioma : listaIdiomas)
        {
            IdiomaAsignatura nuevoIdiomaAsignatura = new IdiomaAsignatura();

            nuevoIdiomaAsignatura.setGdoAsignatura(nuevaAsignatura);
            nuevoIdiomaAsignatura.setGdoIdioma(idioma);

            nuevoIdiomaAsignatura.insert();
        }
    }
}