package es.uji.apps.gdo.model.domains;

public enum TipoEstudioQueModifica
{
    G("Grado"), M("Master"), T("Todos");

    private String descripcion;

    private TipoEstudioQueModifica(String descripcion)
    {
        this.descripcion = descripcion;
    }

    @Override
    public String toString()
    {
        return descripcion;
    }
}
