package es.uji.apps.gdo.model.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.commons.rest.UIEntity;

public enum TipoEstudio
{
    G("Grado"), M("Master");

    private String descripcion;

    private TipoEstudio(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public static String toXML(String... values)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<tipos>");

        for (TipoEstudio tipo : values())
        {
            if (Arrays.asList(values).contains(tipo.name()))
            {
                builder.append("<tipo><id>").append(tipo.name()).append("</id><nombre>")
                        .append(tipo.toString()).append("</nombre></tipo>");
            }
        }

        builder.append("</tipos>");

        return builder.toString();
    }

    @Override
    public String toString()
    {
        return descripcion;
    }

    public static List<UIEntity> toUI(String... values)
    {
        List<UIEntity> list = new ArrayList<>();
        for (TipoEstudio tipo : values())
        {
            if (Arrays.asList(values).contains(tipo.name()))
            {
                UIEntity ui = new UIEntity();
                ui.put("id", tipo.name());
                ui.put("nombre", tipo.toString());
                list.add(ui);
            }
        }
        return list;
    }
}
