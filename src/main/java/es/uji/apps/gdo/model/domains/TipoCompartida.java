package es.uji.apps.gdo.model.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.commons.rest.UIEntity;

public enum TipoCompartida
{
    T("Total"), P("Parcial excepte Verifica");

    private String descripcion;

    private TipoCompartida(String descripcion)
    {
        this.descripcion = descripcion;
    }

    @Override
    public String toString()
    {
        return descripcion;
    }
    
    public static String toXML(String... values)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<tiposCompartida>");

        for (TipoCompartida tipo : values())
        {
            if (Arrays.asList(values).contains(tipo.name()))
            {
                builder.append("<tipoCompartida><id>").append(tipo.name()).append("</id><nombre>")
                        .append(tipo.toString()).append("</nombre></tipoCompartida>");
            }
        }

        builder.append("</tiposCompartida>");

        return builder.toString();
    }

    public static List<UIEntity> toUI(String... values)
    {
        List<UIEntity> list = new ArrayList<>();
        for (TipoCompartida tipo : values())
        {
            if (Arrays.asList(values).contains(tipo.name()))
            {
                UIEntity ui = new UIEntity();
                ui.put("id", tipo.name());
                ui.put("nombre", tipo.toString());
                list.add(ui);
            }
        }
        return list;
    }
}
