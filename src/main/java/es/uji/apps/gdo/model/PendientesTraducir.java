package es.uji.apps.gdo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.PendientesTraducirDAO;

@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_VW_TRADUCIR_GUIAS")
@Component
public class PendientesTraducir
{
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "COD_ASIGNATURA")
    private String codigo;

    @Column(name = "CURSO_ACA")
    private Long cursoAca;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "TIPO")
    private String tipoTitulacion;

    @Column(name = "ESTADO")
    private String estadoAsignatura;

    @Column(name = "EN_SLT")
    private String enSlt;

    @Column(name = "BASADA_EN")
    private String codigoBasadaEn;

    @Convert(converter=SiNoToBooleanConverter.class)
    @Column(name = "UK")
    private Boolean asiEnIngles;

    @Column(name = "PEND_CA")
    private Long pendientesCa;

    @Column(name = "PEND_ES")
    private Long pendientesEs;

    @Column(name = "PEND_UK")
    private Long pendientesUk;

    private static PendientesTraducirDAO pendientesTraducirDAO;

    @Autowired
    public void setPendientesTraducirDAO(PendientesTraducirDAO pendientesTraducirDAO)
    {
        PendientesTraducir.pendientesTraducirDAO = pendientesTraducirDAO;
    }

    public PendientesTraducir()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Long getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipoTitulacion()
    {
        return tipoTitulacion;
    }

    public void setTipoTitulacion(String tipoTitulacion)
    {
        this.tipoTitulacion = tipoTitulacion;
    }

    public String getEstadoAsignatura()
    {
        return estadoAsignatura;
    }

    public void setEstadoAsignatura(String estadoAsignatura)
    {
        this.estadoAsignatura = estadoAsignatura;
    }

    public String getEnSlt()
    {
        return enSlt;
    }

    public void setEnSlt(String enSlt)
    {
        this.enSlt = enSlt;
    }

    public String getCodigoBasadaEn()
    {
        return codigoBasadaEn;
    }

    public void setCodigoBasadaEn(String codigoBasadaEn)
    {
        this.codigoBasadaEn = codigoBasadaEn;
    }

    public Boolean getAsiEnIngles()
    {
        return asiEnIngles;
    }

    @Transient
    public Boolean isAsiEnIngles()
    {
        return getAsiEnIngles();
    }

    public void setAsiEnIngles(Boolean asiEnIngles)
    {
        this.asiEnIngles = asiEnIngles;
    }

    public Long getPendientesCa()
    {
        return pendientesCa;
    }

    public void setPendientesCa(Long pendientesCa)
    {
        this.pendientesCa = pendientesCa;
    }

    public Long getPendientesEs()
    {
        return pendientesEs;
    }

    public void setPendientesEs(Long pendientesEs)
    {
        this.pendientesEs = pendientesEs;
    }

    public Long getPendientesUk()
    {
        return pendientesUk;
    }

    public void setPendientesUk(Long pendientesUk)
    {
        this.pendientesUk = pendientesUk;
    }

    public static PendientesTraducir getPendientesTraducir(Long asignaturaId)
    {
        PendientesTraducir pendientesTraducir = pendientesTraducirDAO.get(PendientesTraducir.class,
                asignaturaId).get(0);

        return pendientesTraducir;
    }

    public static List<PendientesTraducir> getListaPendientesTraducir(Long cursoAca, String tipoEstudio) {
        List<PendientesTraducir> pendientesTraducir = pendientesTraducirDAO.getPendientes(cursoAca, tipoEstudio);

        return pendientesTraducir;
    }

    public boolean tieneApartadosPendientes()
    {
        return (getPendientesEs() > 0L) || (getPendientesCa() > 0L) || (asiEnIngles && getPendientesUk() > 0L);
    }
}