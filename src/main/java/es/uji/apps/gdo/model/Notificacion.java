package es.uji.apps.gdo.model;

import java.io.FileInputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import es.uji.apps.gdo.ErrorAlLeerPlantilla;
import es.uji.apps.gdo.NotificacionSinDestinatarios;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.StreamUtils;

public class Notificacion
{
    public static void enviaNotificacion(String destino, List<Persona> destinatarios,
                                         Asignatura asignatura)
            throws FieldValidationException, MessageNotSentException,
            NotificacionSinDestinatarios, ErrorAlLeerPlantilla
    {
        String cuerpo = "";
        StringBuffer asunto = new StringBuffer();
        asunto.append("GDO - Canvi d\'estat en l\'assignatura "
                + asignatura.getGdoExtAsiToda().getId());

        if (!destinatarios.isEmpty())
        {
            if (destino.equals("RES"))
            {
                cuerpo = componeCuerpoResponsable(asignatura);
            }
            else if (destino.equals("RAC"))
            {
                cuerpo = componeCuerpoResponsableAct(asignatura);
            }
            else if (destino.equals("COR"))
            {
                cuerpo = componeCuerpoCoordinador(asignatura);
            }
            else if (destino.equals("SLT"))
            {
                cuerpo = componeCuerpoSLT(asignatura);
            }
            else if (destino.equals("FIN"))
            {
                cuerpo = componeCuerpoFinalizada(asignatura);
            }

            if (!cuerpo.equals(""))
            {
                enviar(destinatarios, cuerpo, asunto);
            }
        }
        else
        {
            throw new NotificacionSinDestinatarios();
        }
    }

    private static void enviar(List<Persona> destinatarios, String cuerpo, StringBuffer asunto)
            throws MessageNotSentException
    {
        MailMessage mensaje = new MailMessage("GDO");
        mensaje.setTitle(asunto.toString());
        mensaje.setContentType(MediaType.TEXT_PLAIN);
        mensaje.setSender("no_reply@uji.es");
        mensaje.setContent(cuerpo);

        for (Persona persona : destinatarios)
        {
            mensaje.addToRecipient(persona.getLogin() + "@uji.es");
        }

        MessagingClient client = new MessagingClient();
        client.send(mensaje);
    }

    private static String componeCuerpoResponsable(Asignatura asignatura)
            throws ErrorAlLeerPlantilla
    {
        String cuerpo;
        String comentarios = asignatura.getGdoAsiComentarios().stream().map(c -> c.getComentario()).collect(Collectors.joining("\n"));
        if (comentarios == null || comentarios.isEmpty())
        {
            comentarios = "Cap comentari";
        }
        String link = "https://ujiapps.uji.es/gdo/#editar-guia/" + asignatura.getGdoExtAsiToda().getId();
        Calendar fecha = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (asignatura.getFechaIni() != null)
        {
            fecha.setTime(asignatura.getFechaIni());
        }
        else
        {
            fecha.add(Calendar.DATE, 1);
        }

        try
        {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/gdo/templates/aviso-responsable.template")));
        }
        catch (Exception e)
        {
            throw new ErrorAlLeerPlantilla("Error al carregar la plantilla del responsable");
        }

        return MessageFormat.format(cuerpo, asignatura.getGdoExtAsiToda().getId(),
                dateFormat.format(fecha.getTime()), comentarios, link);
    }

    private static String componeCuerpoResponsableAct(Asignatura asignatura)
            throws ErrorAlLeerPlantilla
    {
        String cuerpo;

        try
        {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/gdo/templates/aviso-responsable-act.template")));
        }
        catch (Exception e)
        {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar la plantilla del responsable asignatura activa");
        }

        return MessageFormat.format(cuerpo, asignatura.getGdoExtAsiToda().getId());
    }

    private static String componeCuerpoCoordinador(Asignatura asignatura)
            throws ErrorAlLeerPlantilla
    {
        String cuerpo;

        try
        {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/gdo/templates/aviso-coordinador.template")));
        }
        catch (Exception e)
        {
            throw new ErrorAlLeerPlantilla("Error al carregar plantilla del coordinador.");
        }

        return MessageFormat.format(cuerpo, asignatura.getGdoExtAsiToda().getId());
    }

    private static String componeCuerpoSLT(Asignatura asignatura) throws ErrorAlLeerPlantilla
    {
        String cuerpo;

        try
        {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/gdo/templates/aviso-slt.template")));
        }
        catch (Exception e)
        {
            throw new ErrorAlLeerPlantilla("Error al carregar plantilla del slt.");
        }

        return MessageFormat.format(cuerpo, asignatura.getGdoExtAsiToda().getId());
    }

    private static String componeCuerpoFinalizada(Asignatura asignatura)
            throws ErrorAlLeerPlantilla
    {
        String cuerpo;

        try
        {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/gdo/templates/aviso-finalizada.template")));
        }
        catch (Exception e)
        {
            throw new ErrorAlLeerPlantilla(
                    "Error al carregar plantilla d\'avís d\'assignatura finalitzada.");
        }

        return MessageFormat.format(cuerpo, asignatura.getGdoExtAsiToda().getId());
    }

}
