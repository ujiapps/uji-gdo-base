package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.ActividadDAO;

/**
 * The persistent class for the GDO_TIPO_ACTIVIDADES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_TIPO_ACTIVIDADES")
@Component
public class Actividad implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    private String tipo;

    // bi-directional many-to-one association to GdoAsiActividades
    @OneToMany(mappedBy = "gdoTipoActividade")
    private Set<ActividadAsignatura> gdoAsiActividades;

    private static ActividadDAO actividadDAO;

    @Autowired
    public void setActividadDAO(ActividadDAO actividadDAO)
    {
        Actividad.actividadDAO = actividadDAO;
    }

    public Actividad()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    {
        this.nombreUk = nombreUk;
    }

    public String getTipo()
    {
        return this.tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Set<ActividadAsignatura> getGdoAsiActividades()
    {
        return this.gdoAsiActividades;
    }

    public void setGdoAsiActividades(Set<ActividadAsignatura> gdoAsiActividades)
    {
        this.gdoAsiActividades = gdoAsiActividades;
    }

    public static List<Actividad> getActividades()
    {
        return actividadDAO.getActividades();
    }
    
    public static Actividad getActividadById(Long actividadId)
    {
        return actividadDAO.get(Actividad.class, actividadId).get(0);
    }
}