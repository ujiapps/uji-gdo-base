package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.ResultadoDAO;
import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the GDO_ASI_RESULTADOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_RESULTADOS")
@Component
public class Resultado implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String editable;

    private Long orden;

    @Column(name = "RESULTADO_CA")
    private String resultadoCa;

    @Column(name = "RESULTADO_ES")
    private String resultadoEs;

    @Column(name = "RESULTADO_UK")
    private String resultadoUk;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    private static ResultadoDAO resultadoDAO;

    @Autowired
    public void setResultadoDAO(ResultadoDAO resultadoDAO)
    {
        Resultado.resultadoDAO = resultadoDAO;
    }

    public Resultado()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEditable()
    {
        return this.editable;
    }

    public void setEditable(String editable)
    {
        this.editable = editable;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getResultadoCa()
    {
        return this.resultadoCa;
    }

    public void setResultadoCa(String resultadoCa)
    {
        this.resultadoCa = resultadoCa;
    }

    public String getResultadoEs()
    {
        return this.resultadoEs;
    }

    public void setResultadoEs(String resultadoEs)
    {
        this.resultadoEs = resultadoEs;
    }

    public String getResultadoUk()
    {
        return this.resultadoUk;
    }

    public void setResultadoUk(String resultadoUk)
    {
        this.resultadoUk = resultadoUk;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    @Transactional
    public void update() throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        if (isEditable())
        {
            resultadoDAO.update(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    @Transient
    public boolean isEditable() throws AsignaturaNoAsignadaATitulacion
    {
        return "S".equals(editable) && gdoAsignatura.isModificable();
    }

    public static Resultado getResultado(long resultadoId)
    {
        return resultadoDAO.getResultado(resultadoId);
    }

    public static List<Resultado> getResultadosByAsignatura(long asignaturaId)
    {
        return resultadoDAO.getResultadosByAsignatura(asignaturaId);
    }

    public boolean operacionPermitida() throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = getGdoAsignatura().getTitulacion();
        return isPermitidaOperationEnTitulacion(titulacion);
    }

    public static boolean operacionPermitidaByAsignaturaId(Long asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = Titulacion.getTitulacionByAsignaturaId(asignaturaId);
        return isPermitidaOperationEnTitulacion(titulacion);
    }

    private static boolean isPermitidaOperationEnTitulacion(Titulacion titulacion)
    {
        TipoEstudio tipoEstudio = TipoEstudio.valueOf(titulacion.getTipo());
        return isEstudioModificable(tipoEstudio);
    }

    private static boolean isEstudioModificable(TipoEstudio tipoEstudio)
    {
      /*  if (TipoEstudio.M.equals(tipoEstudio))
        {
            return true;
        }   */
        return false;
    }

    @Transactional
    public static void delete(long resultadoId) throws AsignaturaNoModificableException,
            RegistroNoEncontradoException, AsignaturaNoAsignadaATitulacion
    {
        List<Resultado> listaResultados = resultadoDAO.get(Resultado.class, resultadoId);

        if (listaResultados != null && listaResultados.size() > 0)
        {
            Resultado resultado = listaResultados.get(0);

            if (resultado.isEditable())
            {
                resultadoDAO.delete(Resultado.class, resultadoId);
            }
            else
            {
                throw new AsignaturaNoModificableException();
            }
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Transactional
    public void insertParaCopia()
    {
        resultadoDAO.insert(this);
    }

    @Transactional
    public Resultado insert() throws AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion
    {
        if (isEditable())
        {
            return resultadoDAO.insert(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }
}