package es.uji.apps.gdo.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.BatchSize;


/**
 * The persistent class for the GDO_VW_ASIGNATURAS_PER database table.
 * 
 */
@Entity
@BatchSize(size = 100)
@Table(name="GDO_VW_ASIGNATURAS_PER")
public class PersonaAutorizada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ASIGNATURA_ID")
	private Long asignaturaId;

	@Column(name="CURSO_ACA")
	private Long cursoAca;

	@Column(name="PERSONA_ID")
	private Long personaId;

    public PersonaAutorizada() {
    }

	public Long getAsignaturaId() {
		return this.asignaturaId;
	}

	public void setAsignaturaId(Long asignaturaId) {
		this.asignaturaId = asignaturaId;
	}

	public Long getCursoAca() {
		return this.cursoAca;
	}

	public void setCursoAca(Long cursoAca) {
		this.cursoAca = cursoAca;
	}

	public Long getPersonaId() {
		return this.personaId;
	}

	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}

}