package es.uji.apps.gdo.model;

import es.uji.apps.gdo.DemasiadasAsignaturasException;
import es.uji.apps.gdo.dao.AsignaturaInfoDAO;
import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * The persistent class for the GDO_VW_ASIGNATURAS database table.
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_VW_ASIGNATURAS")
@Component
public class AsignaturaInfo implements Serializable
{
    @Transient
    public static final int maximoAsignaturas = 200;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CURSO_ACA")
    private Long cursoAca;

    @Column(name = "COD_ASIGNATURA")
    private String codAsignatura;

    @Column(name = "ASIGNATURA_RELACIONADA")
    private String asignaturaRelacionada;

    @Column(name = "ASIGNATURA_ID_ORIGEN")
    private Long asignaturaIdOrigen;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INI")
    private Date fechaIni;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "EN_SLT")
    private String enSlt;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "AUTORIZA_PAS")
    private String autorizaPas;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_VALIDACION_CA")
    private Date fechaValidacionCa;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_VALIDACION_ES")
    private Date fechaValidacionEs;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_VALIDACION_UK")
    private Date fechaValidacionUk;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FINALIZACION")
    private Date fechaFinalizacion;

    @Column(name = "MODIFICACIONES")
    private String modificaciones;

    @Column(name = "TIPO_COMPARTIDA")
    private String tipoCompartida;

    @Column(name = "EN_PLAZO")
    private String enPlazo;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "CURSO_ID")
    private Long curso;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "NOMBRE_PROFESOR")
    private String nombreProfesor;

    @Column(name = "ACTIVA")
    private Boolean activa;

    @Column(name = "APARTADOS_TEXTO_TOTALES")
    private Long apartadosTextoTotales;

    @Column(name = "APARTADOS_ES_LLENOS")
    private Long apartadosEsLlenos;

    @Column(name = "APARTADOS_CA_LLENOS")
    private Long apartadosCaLlenos;

    @Column(name = "APARTADOS_UK_LLENOS")
    private Long apartadosUkLlenos;

    @Column(name = "IDIOMAS")
    private Long idiomas;

    @Column(name = "EVALUACIONES")
    private Long evaluaciones;

    @Column(name = "ACTIVIDADES")
    private Long actividades;

    @Column(name = "COMPETENCIAS")
    private Long competencias;

    @Column(name = "NUMERO_MODIFICACIONES")
    private Long numeroModificaciones;

    @Column(name = "NUMERO_COMENTARIOS")
    private Long numeroComentarios;

    @Column(name = "DEFICIENCIAS")
    private String deficiencias;

    @Column(name = "NUEVA_IMPLANTACION")
    private Boolean nuevaImplantacion;

    private static AsignaturaInfoDAO asignaturaInfoDAO;

    @Autowired
    public void setAsignaturaDAO(AsignaturaInfoDAO asignaturaInfoDAO)
    {
        AsignaturaInfo.asignaturaInfoDAO = asignaturaInfoDAO;
    }

    public AsignaturaInfo()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getCodAsignatura()
    {
        return codAsignatura;
    }

    public void setCodAsignatura(String codAsignatura)
    {
        this.codAsignatura = codAsignatura;
    }

    public Long getAsignaturaIdOrigen()
    {
        return asignaturaIdOrigen;
    }

    public void setAsignaturaIdOrigen(Long asignaturaIdOrigen)
    {
        this.asignaturaIdOrigen = asignaturaIdOrigen;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni()
    {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni)
    {
        this.fechaIni = fechaIni;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getEnSlt()
    {
        return enSlt;
    }

    public void setEnSlt(String enSlt)
    {
        this.enSlt = enSlt;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getAutorizaPas()
    {
        return autorizaPas;
    }

    public void setAutorizaPas(String autorizaPas)
    {
        this.autorizaPas = autorizaPas;
    }

    public Date getFechaValidacionCa()
    {
        return fechaValidacionCa;
    }

    public void setFechaValidacionCa(Date fechaValidacionCa)
    {
        this.fechaValidacionCa = fechaValidacionCa;
    }

    public Date getFechaValidacionEs()
    {
        return fechaValidacionEs;
    }

    public void setFechaValidacionEs(Date fechaValidacionEs)
    {
        this.fechaValidacionEs = fechaValidacionEs;
    }

    public Date getFechaValidacionUk()
    {
        return fechaValidacionUk;
    }

    public void setFechaValidacionUk(Date fechaValidacionUk)
    {
        this.fechaValidacionUk = fechaValidacionUk;
    }

    public Date getFechaFinalizacion()
    {
        return fechaFinalizacion;
    }

    public void setFechaFinalizacion(Date fechaFinalizacion)
    {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    public String getModificaciones()
    {
        return modificaciones;
    }

    public void setModificaciones(String modificaciones)
    {
        this.modificaciones = modificaciones;
    }

    public String getTipoCompartida()
    {
        return tipoCompartida;
    }

    public void setTipoCompartida(String tipoCompartida)
    {
        this.tipoCompartida = tipoCompartida;
    }

    public String getEnPlazo()
    {
        return enPlazo;
    }

    public void setEnPlazo(String enPlazo)
    {
        this.enPlazo = enPlazo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getNombreProfesor()
    {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor)
    {
        this.nombreProfesor = nombreProfesor;
    }

    public Boolean isActiva()
    {
        return activa;
    }

    public void setActiva(Boolean activa)
    {
        this.activa = activa;
    }

    public Long getApartadosTextoTotales()
    {
        return apartadosTextoTotales;
    }

    public void setApartadosTextoTotales(Long apartadosTextoTotales)
    {
        this.apartadosTextoTotales = apartadosTextoTotales;
    }

    public Long getApartadosEsLlenos()
    {
        return apartadosEsLlenos;
    }

    public void setApartadosEsLlenos(Long apartadosEsLlenos)
    {
        this.apartadosEsLlenos = apartadosEsLlenos;
    }

    public Long getApartadosCaLlenos()
    {
        return apartadosCaLlenos;
    }

    public void setApartadosCaLlenos(Long apartadosCaLlenos)
    {
        this.apartadosCaLlenos = apartadosCaLlenos;
    }

    public Long getApartadosUkLlenos()
    {
        return apartadosUkLlenos;
    }

    public void setApartadosUkLlenos(Long apartadosUkLlenos)
    {
        this.apartadosUkLlenos = apartadosUkLlenos;
    }

    public Long getIdiomas()
    {
        return idiomas;
    }

    public void setIdiomas(Long idiomas)
    {
        this.idiomas = idiomas;
    }

    public Long getEvaluaciones()
    {
        return evaluaciones;
    }

    public void setEvaluaciones(Long evaluaciones)
    {
        this.evaluaciones = evaluaciones;
    }

    public Long getActividades()
    {
        return actividades;
    }

    public void setActividades(Long actividades)
    {
        this.actividades = actividades;
    }

    public Long getCompetencias()
    {
        return competencias;
    }

    public void setCompetencias(Long competencias)
    {
        this.competencias = competencias;
    }

    public Long getNumeroModificaciones()
    {
        return numeroModificaciones;
    }

    public void setNumeroModificaciones(Long numeroModificaciones)
    {
        this.numeroModificaciones = numeroModificaciones;
    }

    public Long getNumeroComentarios()
    {
        return numeroComentarios;
    }

    public void setNumeroComentarios(Long numeroComentarios)
    {
        this.numeroComentarios = numeroComentarios;
    }

    public String getAsignaturaRelacionada()
    {
        return asignaturaRelacionada;
    }

    public void setAsignaturaRelacionada(String asignaturaRelacionada)
    {
        this.asignaturaRelacionada = asignaturaRelacionada;
    }

    public Boolean isNuevaImplantacion()
    {
        return nuevaImplantacion;
    }

    public void setNuevaImplantacion(Boolean nuevaImplantacion)
    {
        this.nuevaImplantacion = nuevaImplantacion;
    }

    public String getDeficiencias()
    {
        return deficiencias;
    }

    public void setDeficiencias(String deficiencias)
    {
        this.deficiencias = deficiencias;
    }

    public static List<AsignaturaInfo> getAsignaturasConFiltros(long personaId, String tipoEstudio, String codigoAsignatura, String estado, String titulacion,
            String curso, boolean conGuia, String pantalla) throws DemasiadasAsignaturasException
    {
        Long personaIdBusqueda = Persona.isAdministrador(personaId) ? null : personaId;


        if (pantalla == null || !pantalla.equals("Sinlimite"))
        {
            checkDemasiadasAsignaturas(personaIdBusqueda, tipoEstudio, codigoAsignatura, estado,
                    titulacion, curso, conGuia);
        }

        return asignaturaInfoDAO.getAsignaturasConFiltros(personaIdBusqueda, tipoEstudio,
                    codigoAsignatura, estado, titulacion, curso, conGuia);
    }

    public static void checkDemasiadasAsignaturas(Long personaId, String tipoEstudio, String codigoAsignatura, String estado, String titulacion,
            String curso, boolean conGuia) throws DemasiadasAsignaturasException
    {
        Long numeroAsignaturas = asignaturaInfoDAO.contarAsignaturasConFiltros(personaId, tipoEstudio, codigoAsignatura,
                estado, titulacion, curso, conGuia);
        if (numeroAsignaturas > maximoAsignaturas)
        {
            throw new DemasiadasAsignaturasException();
        }
    }

}