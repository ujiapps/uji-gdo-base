package es.uji.apps.gdo.model.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.commons.rest.UIEntity;

public enum EstadoAsignatura
{
    I("Inactiva"), D("Introduint dades"), V("Pendent de revisar - validar"), F("Finalitzat"), FD("Finalitzat amb deficiències");

    private String descripcion;

    private EstadoAsignatura(String descripcion)
    {
        this.descripcion = descripcion;
    }

    @Override
    public String toString()
    {
        return descripcion;
    }

    public static String toXML(String... values)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<estados>");

        for (EstadoAsignatura estado : values())
        {
            if (Arrays.asList(values).contains(estado.name()))
            {
                builder.append("<estado><id>").append(estado.name()).append("</id><nombre>")
                        .append(estado.toString()).append("</nombre></estado>");
            }
        }

        builder.append("</estados>");

        return builder.toString();
    }

    public static List<UIEntity> toUI(String... values)
    {
        List<UIEntity> list = new ArrayList<>();
        for (EstadoAsignatura estado : values())
        {
            if (Arrays.asList(values).contains(estado.name()))
            {
                UIEntity ui = new UIEntity();
                ui.put("id", estado.name());
                ui.put("nombre", estado.toString());
                list.add(ui);
            }
        }
        return list;
    }
}
