package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.dao.TitulacionDAO;

/**
 * The persistent class for the GDO_EXT_TITU_TODAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_EXT_TITU_TODAS")
@Component
public class Titulacion implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private String tipo;

    private String interuniversitario;

    @Column(name = "COORDINA_UJI")
    private String coordinaUji;

    @Column(name = "ERASMUS_MUNDUS")
    private String erasmusMundus;

    @Column(name = "CENTRO_ID")
    private Long centroId;

    // bi-directional many-to-one association to GdoDirectoresExtras
    @OneToMany(mappedBy = "gdoExtTituToda")
    private Set<DirectorExtra> gdoDirectoresExtras;

    private static TitulacionDAO titulacionDAO;

    @Autowired
    public void setTitulacionDAO(TitulacionDAO titulacionDAO)
    {
        Titulacion.titulacionDAO = titulacionDAO;
    }

    public Titulacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipo()
    {
        return this.tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getInteruniversitario()
    {
        return this.interuniversitario;
    }

    public void setInteruniversitario(String interuniversitario)
    {
        this.interuniversitario = interuniversitario;
    }

    public String getCoordinaUji()
    {
        return this.coordinaUji;
    }

    public void setCoordinaUji(String coordinaUji)
    {
        this.coordinaUji = coordinaUji;
    }

    public Set<DirectorExtra> getGdoDirectoresExtras()
    {
        return this.gdoDirectoresExtras;
    }

    public void setGdoDirectoresExtras(Set<DirectorExtra> gdoDirectoresExtras)
    {
        this.gdoDirectoresExtras = gdoDirectoresExtras;
    }

    public static List<TitulacionPersona> getTitulacionesbyPersona(Long personaId)
    {
        return titulacionDAO.getTitulacionesByPersona(personaId);
    }

    public static Titulacion getTitulacion(Long titulacionId)
    {
        return titulacionDAO.get(Titulacion.class, titulacionId).get(0);
    }

    public static List<Titulacion> getTitulaciones()
    {
        return titulacionDAO.getTitulaciones();
    }

    public static Titulacion getTitulacionByAsignaturaId(Long asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        return titulacionDAO.getTitulacionByAsignaturaId(asignaturaId);
    }

    public static List<Long> getCursosByTitulacionId(String titulacionId)
    {
        return titulacionDAO.getCursosByTitulacionId(titulacionId);
    }

    public static List<Long> getCursosByTitulacionId(String titulacionId, Long personaId)
    {
        return titulacionDAO.getCursosByTitulacionId(titulacionId, personaId);
    }

    public Long getCentroId() {
        return centroId;
    }

    public void setCentroId(Long centroId) {
        this.centroId = centroId;
    }

    public String getErasmusMundus()
    {
        return erasmusMundus;
    }

    public void setErasmusMundus(String erasmusMundus)
    {
        this.erasmusMundus = erasmusMundus;
    }
}