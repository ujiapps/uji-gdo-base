package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.PersonaDAO;
import es.uji.commons.sso.dao.ApaDAO;

/**
 * The persistent class for the GDO_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_EXT_PERSONAS")
@Component
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ACTIVIDAD_ID")
    private String actividadId;

    private String apellido1;

    private String apellido2;

    @Column(name = "ES_AUXILIAR")
    private String esAuxiliar;

    private String login;

    private String nombre;

    @Column(name = "UBICACION_ID")
    private BigDecimal ubicacionId;

    // bi-directional many-to-one association to GdoAsignaturas
    @OneToMany(mappedBy = "gdoExtPersona")
    private Set<Asignatura> gdoAsignaturas;

    // bi-directional many-to-one association to GdoAsiPermisos
    @OneToMany(mappedBy = "gdoExtPersona")
    private Set<Permiso> gdoAsiPermisos;

    // bi-directional many-to-one association to GdoDirectoresExtras
    @OneToMany(mappedBy = "gdoExtPersona")
    private Set<DirectorExtra> gdoDirectoresExtras;

    protected static PersonaDAO personaDAO;
    protected static ApaDAO apaDAO;

    @Autowired
    protected void setPersonaDAO(PersonaDAO personaDAO) {
        Persona.personaDAO = personaDAO;
    }

    @Autowired
    protected void setApaDAO(ApaDAO apaDAO) {
        Persona.apaDAO = apaDAO;
    }

    public Persona() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividadId() {
        return this.actividadId;
    }

    public void setActividadId(String actividadId) {
        this.actividadId = actividadId;
    }

    public String getApellido1() {
        return this.apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return this.apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getEsAuxiliar() {
        return this.esAuxiliar;
    }

    public void setEsAuxiliar(String esAuxiliar) {
        this.esAuxiliar = esAuxiliar;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getUbicacionId() {
        return this.ubicacionId;
    }

    public void setUbicacionId(BigDecimal ubicacionId) {
        this.ubicacionId = ubicacionId;
    }

    public Set<Asignatura> getGdoAsignaturas() {
        return this.gdoAsignaturas;
    }

    public void setGdoAsignaturas(Set<Asignatura> gdoAsignaturas) {
        this.gdoAsignaturas = gdoAsignaturas;
    }

    public Set<Permiso> getGdoAsiPermisos() {
        return this.gdoAsiPermisos;
    }

    public void setGdoAsiPermisos(Set<Permiso> gdoAsiPermisos) {
        this.gdoAsiPermisos = gdoAsiPermisos;
    }

    public Set<DirectorExtra> getGdoDirectoresExtras() {
        return this.gdoDirectoresExtras;
    }

    public void setGdoDirectoresExtras(Set<DirectorExtra> gdoDirectoresExtras) {
        this.gdoDirectoresExtras = gdoDirectoresExtras;
    }

    public String getNombreCompleto() {
        String nombreCompleto = "";

        if (apellido1 != null) {
            nombreCompleto += apellido1;
        }

        if (apellido2 != null) {
            nombreCompleto += " " + apellido2;
        }

        nombreCompleto += ", " + nombre;

        return nombreCompleto;
    }

    public static boolean isAdministrador(Long id) {
        return apaDAO.hasPerfil("GDO", "Admin", id);
    }

    public static boolean hasPerfil(String perfil, Long id) {
        return apaDAO.hasPerfil("GDO", perfil, id);
    }

    public static Persona getPersona(Long personaId) {
        return personaDAO.get(Persona.class, personaId).get(0);
    }

    public static List<Persona> getPersonas() {
        return personaDAO.getPersonas();
    }

    public static Persona getPersonaByLogin(String login) {
        return personaDAO.getPersonaByLogin(login);
    }

    public static boolean isSLT(Long personaId) {
        List<Long> sltPerIds = new ArrayList<>(Arrays.asList(7955L, 11232L, 60611L, 111204L, 59985L, 128472L, 59925L, 60088L, 868070L, 149345L));
        return sltPerIds.contains(personaId);
    }
}