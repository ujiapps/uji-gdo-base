package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.VerificaActividadDAO;

/**
 * The persistent class for the GDO_VW_EXT_ACTIVIDADES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_VW_EXT_ACTIVIDADES")
@Component
public class VerificaActividad implements Serializable
{
    @Id
    @Column(name = "ACT_ID")
    private Long actId;

    @Id
    @Column(name = "COD_ASIGNATURA")
    private String codAsignatura;

    @Id
    @Column(name = "CURSO_ACA_INI")
    private Long cursoAcaIni;
    
    @Column(name = "CURSO_ACA_FIN")
    private Long cursoAcaFin;

    @Column(name = "H_NO_PRE")
    private Float hNoPre;

    @Column(name = "H_PRE")
    private Float hPre;
    
    private static VerificaActividadDAO verificaActividadDAO;
    
    @Autowired
    public void setVerificaActividadDAO(VerificaActividadDAO verificaActividadDAO)
    {
        VerificaActividad.verificaActividadDAO = verificaActividadDAO;
    }

    public VerificaActividad()
    {
    }

    public Long getActId()
    {
        return this.actId;
    }

    public void setActId(Long actId)
    {
        this.actId = actId;
    }

    public String getCodAsignatura()
    {
        return this.codAsignatura;
    }

    public void setCodAsignatura(String codAsignatura)
    {
        this.codAsignatura = codAsignatura;
    }

    public Long getCursoAcaFin()
    {
        return this.cursoAcaFin;
    }

    public void setCursoAcaFin(Long cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }

    public Long getCursoAcaIni()
    {
        return this.cursoAcaIni;
    }

    public void setCursoAcaIni(Long cursoAcaIni)
    {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Float getHNoPre()
    {
        return this.hNoPre;
    }

    public void setHNoPre(Float hNoPre)
    {
        this.hNoPre = hNoPre;
    }

    public Float getHPre()
    {
        return this.hPre;
    }

    public void setHPre(Float hPre)
    {
        this.hPre = hPre;
    }

    public static List<VerificaActividad> getVerificaActividadByAsignatura(Asignatura asignatura)
    {
        return verificaActividadDAO.getVerificaActividadByAsignatura(asignatura.getGdoExtAsiToda()
                .getId(), asignatura.getGdoCursosAcademico().getCursoAca());
    }
}