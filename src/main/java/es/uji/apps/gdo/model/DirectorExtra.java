package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.CoordinadorSinCursoEspecificadoException;
import es.uji.apps.gdo.dao.DirectorExtraDAO;
import es.uji.apps.gdo.model.domains.TipoPermiso;

/**
 * The persistent class for the GDO_DIRECTORES_EXTRA database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_DIRECTORES_EXTRA")
@Component
public class DirectorExtra implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CURSO_ID")
    private Long cursoId;

    @Column(name = "TIPO_PERMISO")
    private String tipoPermiso;

    // bi-directional many-to-one association to GdoExtPersona
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona gdoExtPersona;

    // bi-directional many-to-one association to GdoExtTituToda
    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private Titulacion gdoExtTituToda;

    private static DirectorExtraDAO directorExtraDAO;

    @Autowired
    public void setDirectorExtraDAO(DirectorExtraDAO directorExtraDAO)
    {
        DirectorExtra.directorExtraDAO = directorExtraDAO;
    }

    public DirectorExtra()
    {
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Long getCursoId()
    {
        return this.cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public String getTipoPermiso()
    {
        return this.tipoPermiso;
    }

    public void setTipoPermiso(String tipoPermiso)
    {
        this.tipoPermiso = tipoPermiso;
    }

    public Persona getGdoExtPersona()
    {
        return this.gdoExtPersona;
    }

    public void setGdoExtPersona(Persona gdoExtPersona)
    {
        this.gdoExtPersona = gdoExtPersona;
    }

    public Titulacion getGdoExtTituToda()
    {
        return this.gdoExtTituToda;
    }

    public void setGdoExtTituToda(Titulacion gdoExtTituToda)
    {
        this.gdoExtTituToda = gdoExtTituToda;
    }

    public static List<DirectorExtra> getDirectores()
    {
        return directorExtraDAO.getDirectoresExtra();
    }

    public static DirectorExtra getDirectorExtra(Long directorId)
    {
        return directorExtraDAO.get(DirectorExtra.class, directorId).get(0);
    }

    @Transactional
    public DirectorExtra update() throws CoordinadorSinCursoEspecificadoException
    {
        if (this.tipoPermiso.equals(TipoPermiso.COR.name()) && this.cursoId == null)
        {
            throw new CoordinadorSinCursoEspecificadoException();
        }
        else
        {
            return directorExtraDAO.update(this);
        }
    }

    @Transactional
    public DirectorExtra insert() throws CoordinadorSinCursoEspecificadoException
    {
        if (this.tipoPermiso.equals(TipoPermiso.COR.name()) && this.cursoId == null)
        {
            throw new CoordinadorSinCursoEspecificadoException();
        }
        else
        {
            return directorExtraDAO.insert(this);
        }
    }

    @Transactional
    public void delete()
    {
        directorExtraDAO.delete(DirectorExtra.class, id);
    }
}