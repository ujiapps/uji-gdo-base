package es.uji.apps.gdo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;


/**
 * The persistent class for the GDO_VW_ASIGNATURAS_PER database table.
 * 
 */
@Entity
@BatchSize(size = 100)
@Table(name="GDO_VW_ASIGNATURAS_PER")
public class AsignaturaPersona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ASIGNATURA_ID")
	private Long asignaturaId;

	@Id
	@Column(name="CURSO_ACA")
	private Long cursoAca;

	@Id
	@Column(name="PERSONA_ID")
	private Long personaId;

	@Id
	@Column(name="TIPO_ACCESO")
	private String tipoAcceso;

    public AsignaturaPersona() {
    }

	public Long getAsignaturaId() {
		return this.asignaturaId;
	}

	public void setAsignaturaId(Long asignaturaId) {
		this.asignaturaId = asignaturaId;
	}

	public Long getCursoAca() {
		return this.cursoAca;
	}

	public void setCursoAca(Long cursoAca) {
		this.cursoAca = cursoAca;
	}

	public Long getPersonaId() {
		return this.personaId;
	}

	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}

	public String getTipoAcceso() {
		return this.tipoAcceso;
	}

	public void setTipoAcceso(String tipoAcceso) {
		this.tipoAcceso = tipoAcceso;
	}

}