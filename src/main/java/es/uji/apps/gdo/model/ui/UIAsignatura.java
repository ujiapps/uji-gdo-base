package es.uji.apps.gdo.model.ui;

public class UIAsignatura
{
    Long asignaturaId;

    String estado;

    String enSlt;

    Long titulacionId;

    String titulacionTipo;

    Long personaId;

    String tipoAcceso;

    public Long getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(Long asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getEnSlt()
    {
        return enSlt;
    }

    public void setEnSlt(String enSlt)
    {
        this.enSlt = enSlt;
    }

    public Long getTitulacionId()
    {
        return titulacionId;
    }

    public void setTitulacionId(Long titulacionId)
    {
        this.titulacionId = titulacionId;
    }

    public String getTitulacionTipo()
    {
        return titulacionTipo;
    }

    public void setTitulacionTipo(String titulacionTipo)
    {
        this.titulacionTipo = titulacionTipo;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getTipoAcceso()
    {
        return tipoAcceso;
    }

    public void setTipoAcceso(String tipoAcceso)
    {
        this.tipoAcceso = tipoAcceso;
    }

    public boolean filtra(Boolean filtroAsignada, String filtroEstado, String filtroTipoEstudio, String filtroEnSlt)
    {
        if (filtroAsignada && !tipoAcceso.equals("AS")) {
            return false;
        }

        if (!filtroAsignada && tipoAcceso.equals("AS")) {
            return false;
        }

        if (filtroEstado != null && !estado.equals(filtroEstado)) {
            return false;
        }

        if (filtroEnSlt != null && !enSlt.equals(filtroEnSlt)) {
            return false;
        }

        if (filtroTipoEstudio != null && !titulacionTipo.equals(filtroTipoEstudio)) {
            return false;
        }

        return true;
    }
}
