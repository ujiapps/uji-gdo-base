package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.gdo.dao.AsignaturaDAO;
import es.uji.apps.gdo.dao.IdiomaAsignaturaDAO;
import es.uji.apps.gdo.dao.IdiomaAsignaturaVerificaDAO;
import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Entity
@BatchSize(size = 100)
@Table(name = "GDO_VW_IDIOMA_ASI_VERIFICA")
@Component
public class IdiomaAsignaturaVerifica implements Serializable
{
    private static final long serialVersionUID = 1L;

    private static IdiomaAsignaturaVerificaDAO idiomaAsignaturaVerificaDAO;

    @Id
    @Column(name = "CODIGO")
    private String codigoAsignatura;

    @Id
    @Column(name = "IDI_ID")
    private Long idiomaId;

    @Id
    @Column(name = "VERSION_ASIGNATURAS")
    private Long version;

    public String getCodigoAsignatura()
    {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura)
    {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Long getIdiomaId()
    {
        return idiomaId;
    }

    public void setIdiomaId(Long idiomaId)
    {
        this.idiomaId = idiomaId;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(Long version)
    {
        this.version = version;
    }

    public static List<Idioma> getIdiomasByAsignatura(String codigoAsignatura)
    {
        return idiomaAsignaturaVerificaDAO.getIdiomasByAsignatura(codigoAsignatura);
    }

    @Autowired
    public void setAsignaturaDAO(IdiomaAsignaturaVerificaDAO idiomaAsignaturaVerificaDAO)
    {
        IdiomaAsignaturaVerifica.idiomaAsignaturaVerificaDAO = idiomaAsignaturaVerificaDAO;
    }
}