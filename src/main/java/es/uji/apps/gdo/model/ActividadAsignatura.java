package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.ActividadDAO;
import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the GDO_ASI_ACTIVIDADES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_ACTIVIDADES")
@Component
public class ActividadAsignatura implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "HORAS_NO_PRESENCIALES")
    private Float horasNoPresenciales;

    @Column(name = "HORAS_PRESENCIALES")
    private Float horasPresenciales;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    // bi-directional many-to-one association to GdoTiposActividades
    @ManyToOne
    @JoinColumn(name = "ACTIVIDAD_ID")
    private Actividad gdoTipoActividade;

    private static ActividadDAO actividadAsignaturaDAO;

    @Autowired
    public void setActividadDAO(ActividadDAO actividadDAO)
    {
        ActividadAsignatura.actividadAsignaturaDAO = actividadDAO;
    }

    public ActividadAsignatura()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Float getHorasNoPresenciales()
    {
        return this.horasNoPresenciales;
    }

    public void setHorasNoPresenciales(Float horasNoPresenciales)
    {
        this.horasNoPresenciales = horasNoPresenciales;
    }

    public Float getHorasPresenciales()
    {
        return this.horasPresenciales;
    }

    public void setHorasPresenciales(Float horasPresenciales)
    {
        this.horasPresenciales = horasPresenciales;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    public Actividad getGdoTipoActividade()
    {
        return this.gdoTipoActividade;
    }

    public void setGdoTipoActividade(Actividad gdoTipoActividade)
    {
        this.gdoTipoActividade = gdoTipoActividade;
    }

    private boolean isEditable() throws AsignaturaNoAsignadaATitulacion
    {
        return getGdoAsignatura().isModificable() && operacionPermitida();
    }

    public boolean operacionPermitida() throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = this.getGdoAsignatura().getTitulacion();
        return isPermitidaOperationEnTitulacion(titulacion);
    }

    public static boolean operacionPermitidaByAsignaturaId(Long asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = Titulacion.getTitulacionByAsignaturaId(asignaturaId);
        return isPermitidaOperationEnTitulacion(titulacion);
    }

    private static boolean isPermitidaOperationEnTitulacion(Titulacion titulacion)
    {
        TipoEstudio tipoEstudio = TipoEstudio.valueOf(titulacion.getTipo());
        return isEstudioModificable(tipoEstudio);
    }

    public static boolean isEstudioModificable(TipoEstudio tipoEstudio)
    {
       /* if (TipoEstudio.M.equals(tipoEstudio))
        {
            return true;
        }*/
        
        return false;
    }

    @Transactional
    public void update() throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        if (isEditable())
        {
            actividadAsignaturaDAO.update(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    @Transactional
    public static void delete(Long actividadAsignaturaId) throws AsignaturaNoModificableException,
            RegistroNoEncontradoException, AsignaturaNoAsignadaATitulacion
    {
        List<ActividadAsignatura> listaActividades = actividadAsignaturaDAO.get(
                ActividadAsignatura.class, actividadAsignaturaId);

        if (listaActividades != null && listaActividades.size() > 0)
        {
            ActividadAsignatura asignaturasActividades = listaActividades.get(0);

            if (asignaturasActividades.isEditable())
            {
                actividadAsignaturaDAO.delete(ActividadAsignatura.class, actividadAsignaturaId);
            }
            else
            {
                throw new AsignaturaNoModificableException();
            }
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public void insertParaCopia()
    {
        actividadAsignaturaDAO.insert(this);
    }

    @Transactional
    public ActividadAsignatura insert() throws AsignaturaNoModificableException,
            AsignaturaNoAsignadaATitulacion
    {
        if (isEditable())
        {
            return actividadAsignaturaDAO.insert(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    public static List<ActividadAsignatura> getActividad(long actividadId)
    {
        ActividadAsignatura actividadDB = actividadAsignaturaDAO.getActividad(actividadId);

        return Collections.singletonList(actividadDB);
    }

    public static List<ActividadAsignatura> getActividadesByAsignaturaId(long asignaturaId)
    {
        List<ActividadAsignatura> listaActividadesDB = actividadAsignaturaDAO
                .getActividadesByAsignaturaId(asignaturaId);

        return listaActividadesDB;
    }

    public static List<ActividadAsignatura> getActividadesByCodAsignatura(String codigoAsignatura,
            long cursoAcademico)
    {
        List<ActividadAsignatura> listaActividadesDB = actividadAsignaturaDAO
                .getActividadesByCodAsignatura(codigoAsignatura, cursoAcademico);

        return listaActividadesDB;
    }
}