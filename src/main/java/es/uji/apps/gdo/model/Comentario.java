package es.uji.apps.gdo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.ComentarioDAO;


/**
 * The persistent class for the GDO_ASI_COMENTARIOS database table.
 * 
 */
@SuppressWarnings("serial")
@Component
@Entity
@BatchSize(size = 100)
@Table(name="GDO_ASI_COMENTARIOS")
public class Comentario implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	private String comentario;

	private String idioma;

	//bi-directional many-to-one association to GdoAsignatura
    @ManyToOne
	@JoinColumn(name="ASIGNATURA_ID")
	private Asignatura gdoAsignatura;
    
    private static ComentarioDAO comentarioDAO;

    @Autowired
    public void setComentarioDAO(ComentarioDAO comentarioDAO)
    {
        Comentario.comentarioDAO = comentarioDAO;
    }

    public Comentario() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getIdioma() {
		return this.idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public Asignatura getGdoAsignatura() {
		return this.gdoAsignatura;
	}

	public void setGdoAsignatura(Asignatura gdoAsignatura) {
		this.gdoAsignatura = gdoAsignatura;
	}
	
    public void insertComentario()
    {
        comentarioDAO.insertComentario(this);
    }

    public static void delete(long comentarioId)
    {
        comentarioDAO.delete(Comentario.class, comentarioId);
    }
}