package es.uji.apps.gdo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.stereotype.Component;


/**
 * The persistent class for the GDO_EXT_TITULACIONES_PER database table.
 * 
 */
@Entity
@BatchSize(size = 100)
@Table(name="GDO_EXT_TITULACIONES_PER")
@Component
public class TitulacionPersona implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TitulacionPersonaPK id;

	@Column(name="CARGO_ID")
	private String cargoId;

	@Column(name="CURSO_ID")
	private Long cursoId;

	private String nombre;

    public TitulacionPersona() {
    }

	public TitulacionPersonaPK getId() {
		return this.id;
	}

	public void setId(TitulacionPersonaPK id) {
		this.id = id;
	}
	
	public String getCargoId() {
		return this.cargoId;
	}

	public void setCargoId(String cargoId) {
		this.cargoId = cargoId;
	}

	public Long getCursoId() {
		return this.cursoId;
	}

	public void setCursoId(Long cursoId) {
		this.cursoId = cursoId;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}