package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.CursoAcademicoSoloUnoActivo;
import es.uji.apps.gdo.dao.CursoAcademicoDAO;

/**
 * The persistent class for the GDO_CURSOS_ACADEMICOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_CURSOS_ACADEMICOS")
@Component
public class CursoAcademico implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    private Long cursoAca;

    @Convert(converter=SiNoToBooleanConverter.class)
    private Boolean activo;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INI")
    private Date fechaIni;

    // bi-directional many-to-one association to GdoApartados
    @OneToMany(mappedBy = "gdoCursosAcademico")
    private Set<Apartado> gdoApartados;

    // bi-directional many-to-one association to GdoAsignaturas
    @OneToMany(mappedBy = "gdoCursosAcademico")
    private Set<Asignatura> gdoAsignaturas;

    private static CursoAcademicoDAO cursoAcademicoDAO;

    @Autowired
    public void setCursoAcademicoDAO(CursoAcademicoDAO cursoAcademicoDAO)
    {
        CursoAcademico.cursoAcademicoDAO = cursoAcademicoDAO;
    }

    public CursoAcademico()
    {
    }

    public Long getCursoAca()
    {
        return this.cursoAca;
    }

    public void setCursoAca(Long cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Boolean getActivo()
    {
        return this.activo;
    }

    @Transient
    public Boolean isActivo()
    {
        return getActivo();
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }

    public Date getFechaFin()
    {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni()
    {
        return this.fechaIni;
    }

    public void setFechaIni(Date fechaIni)
    {
        this.fechaIni = fechaIni;
    }

    public Set<Apartado> getGdoApartados()
    {
        return this.gdoApartados;
    }

    public void setGdoApartados(Set<Apartado> gdoApartados)
    {
        this.gdoApartados = gdoApartados;
    }

    public Set<Asignatura> getGdoAsignaturas()
    {
        return this.gdoAsignaturas;
    }

    public void setGdoAsignaturas(Set<Asignatura> gdoAsignaturas)
    {
        this.gdoAsignaturas = gdoAsignaturas;
    }

    @Transactional
    public void update() throws CursoAcademicoSoloUnoActivo
    {
        checkSoloPuedeHaberUnCursoActivo();

        cursoAcademicoDAO.update(this);
    }

    private Long checkSoloPuedeHaberUnCursoActivo() throws CursoAcademicoSoloUnoActivo
    {
        if (isActivo())
        {
            CursoAcademico cursoActivo = CursoAcademico.getCursoActivo();

            if (cursoActivo.getCursoAca() != null
                    && cursoActivo.getCursoAca().longValue() != cursoAca.longValue())
            {
                throw new CursoAcademicoSoloUnoActivo();
            }
            else
            {
                return Asignatura.cuentaAsignaturas(cursoAca.longValue() - 1);
            }
        }

        return -1L;
    }

    public Boolean isVisible()
    {
        Date date = new Date();
        return (getFechaIni().before(date) && this.getFechaFin().after(date));
    }

    public static List<CursoAcademico> getCursos()
    {
        return cursoAcademicoDAO.getCursos();
    }

    public static CursoAcademico getCursoActivo()
    {
        CursoAcademico cursoAcademico = cursoAcademicoDAO.getCursoActivo();

        if (cursoAcademico.getCursoAca() == null)
        {
            return new CursoAcademico();
        }
        else
        {
            return cursoAcademico;
        }
    }

    public static CursoAcademico getCursoAcademicoById(Long cursoAcaId)
    {
        return cursoAcademicoDAO.getCursoAcademicoById(cursoAcaId);
    }

    public static CursoAcademico setCursoActivo(Long cursoAcaId) throws CursoAcademicoSoloUnoActivo
    {
        return cursoAcademicoDAO.setCursoActivo(cursoAcaId);
    }
}