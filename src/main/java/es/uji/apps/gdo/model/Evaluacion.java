package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.dao.EvaluacionDAO;
import es.uji.apps.gdo.model.domains.Operacion;
import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the GDO_ASI_EVALUACION database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_EVALUACION")
@Component
public class Evaluacion implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Convert(converter=SiNoToBooleanConverter.class)
    private Boolean editable;

    @Column(name = "EVALUACION_ID")
    private Long evaluacionId;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    private Long orden;

    private Float ponderacion;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    private static EvaluacionDAO evaluacionDAO;

    @Autowired
    public void setEvaluacionDAO(EvaluacionDAO evaluacionDAO)
    {
        Evaluacion.evaluacionDAO = evaluacionDAO;
    }

    public Evaluacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean getEditable()
    {
        return this.editable;
    }

    public void setEditable(Boolean editable)
    {
        this.editable = editable;
    }

    public Long getEvaluacionId()
    {
        return this.evaluacionId;
    }

    public void setEvaluacionId(Long evaluacionId)
    {
        this.evaluacionId = evaluacionId;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    {
        this.nombreUk = nombreUk;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Float getPonderacion()
    {
        return this.ponderacion;
    }

    public void setPonderacion(Float ponderacion)
    {
        this.ponderacion = ponderacion;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    @Transient
    public boolean isEditable()
    {
        return editable && gdoAsignatura.isModificable();
    }

    public boolean isEditable(Operacion operacion) throws AsignaturaNoAsignadaATitulacion
    {
        return isEditable() && gdoAsignatura.isModificable() && operacionPermitida(operacion);
    }

    @Transactional
    public static void delete(Long evaluacionId) throws AsignaturaNoModificableException,
            RegistroNoEncontradoException, AsignaturaNoAsignadaATitulacion
    {
        List<Evaluacion> listaEvaluaciones = evaluacionDAO.get(Evaluacion.class,
                evaluacionId);

        if (listaEvaluaciones != null && listaEvaluaciones.size() > 0)
        {
            Evaluacion evaluacion = listaEvaluaciones.get(0);

            if (evaluacion.isEditable())
            {
                evaluacionDAO.delete(Evaluacion.class, evaluacionId);
            }
            else
            {
                throw new AsignaturaNoModificableException();
            }
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Transactional
    public void insertParaCopia() 
    {
       evaluacionDAO.insert(this);
    }

    @Transactional
    public Evaluacion insert() throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {
        if (isEditable(Operacion.INSERT))
        {
            return evaluacionDAO.insert(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    public boolean operacionPermitida(Operacion operacion) throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = getGdoAsignatura().getTitulacion();
        return isPermitidaOperationEnTitulacion(titulacion, operacion);
    }

    public static boolean operacionPermitidaByAsignaturaId(Long asignaturaId, Operacion operacion) throws AsignaturaNoAsignadaATitulacion
    {
        Titulacion titulacion = Titulacion.getTitulacionByAsignaturaId(asignaturaId);
        return isPermitidaOperationEnTitulacion(titulacion, operacion);
    }

    private static boolean isPermitidaOperationEnTitulacion(Titulacion titulacion,
            Operacion operacion)
    {
        TipoEstudio tipoEstudio = TipoEstudio.valueOf(titulacion.getTipo());
        return isEstudioModificable(tipoEstudio, operacion);
    }

    private static boolean isEstudioModificable(TipoEstudio tipoEstudio, Operacion operacion)
    {
       /* switch (tipoEstudio)
        {
        case G:
            switch (operacion)
            {
            case UPDATE:
                return false;
            case DELETE:
                return false;
            case INSERT:
                return false;
            case UPDATEGENERAL:
                return false;
            }
        case M:
            return true;
        default:
            throw new IllegalArgumentException("El tipo de estudio " + tipoEstudio + " no definido");
        }  */
        return false;
    }

    @Transactional
    public Evaluacion update() throws AsignaturaNoModificableException, AsignaturaNoAsignadaATitulacion
    {

        if (isEditable(Operacion.UPDATE))
        {
            return evaluacionDAO.update(this);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }

    }

    public static List<Evaluacion> getEvaluacionesByAsignaturaId(Long asignaturaId)
    {
        return evaluacionDAO.getEvaluacionesByAsignaturaId(asignaturaId);
    }
    
    public static Evaluacion getEvaluacion(Long id)
    {
        return evaluacionDAO.getEvaluacion(id);
    }
}