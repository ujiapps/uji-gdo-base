package es.uji.apps.gdo.model;

import es.uji.apps.gdo.dao.ApartadoDAO;
import es.uji.apps.gdo.dao.RevisionApartadoSLTDAO;
import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@BatchSize(size = 100)
@Table(name = "GDO_VW_REVISION_APARTADOS_SLT")
@Component
public class RevisionApartadoSLT implements Serializable
{
    @Id
    private Long id;

    @Column(name = "ASIGNATURA_ID")
    private Long asignaturaId;

    @Column(name = "COD_ASIGNATURA")
    private String codAsignatura;

    @Column(name = "CURSO_ACA")
    private Long cursoAca;

    @Column(name = "APARTADO_ID")
    private Long apartadoId;

    @Column(name = "ULTIMA_REVISION_SLT_ES")
    private Date ultimaRevisionSltEs;

    @Column(name = "ULTIMA_REVISION_SLT_CA")
    private Date ultimaRevisionSltCa;

    @Column(name = "ULTIMA_REVISION_SLT_UK")
    private Date ultimaRevisionSltUk;

    private static RevisionApartadoSLTDAO revisionApartadoSLTDAO;

    @Autowired
    public void setApartadoDAO(RevisionApartadoSLTDAO revisionApartadoSLTDAO)
    {
        RevisionApartadoSLT.revisionApartadoSLTDAO = revisionApartadoSLTDAO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(Long asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getCodAsignatura() {
        return codAsignatura;
    }

    public void setCodAsignatura(String codAsignatura) {
        this.codAsignatura = codAsignatura;
    }

    public Long getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getApartadoId() {
        return apartadoId;
    }

    public void setApartadoId(Long apartadoId) {
        this.apartadoId = apartadoId;
    }

    public Date getUltimaRevisionSltEs() {
        return ultimaRevisionSltEs;
    }

    public void setUltimaRevisionSltEs(Date ultimaRevisionSltEs) {
        this.ultimaRevisionSltEs = ultimaRevisionSltEs;
    }

    public Date getUltimaRevisionSltCa() {
        return ultimaRevisionSltCa;
    }

    public void setUltimaRevisionSltCa(Date ultimaRevisionSltCa) {
        this.ultimaRevisionSltCa = ultimaRevisionSltCa;
    }

    public Date getUltimaRevisionSltUk() {
        return ultimaRevisionSltUk;
    }

    public void setUltimaRevisionSltUk(Date ultimaRevisionSltUk) {
        this.ultimaRevisionSltUk = ultimaRevisionSltUk;
    }

    public static List<RevisionApartadoSLT> getRevisionApartadoSLTByAsignatura(Long asignaturaId)
    {
        return revisionApartadoSLTDAO.getRevisionesApartadosSLT(asignaturaId);
    }
}