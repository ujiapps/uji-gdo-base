package es.uji.apps.gdo.model.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.commons.rest.UIEntity;

public enum TipoPermiso
{
    DIR("Vicedirector/Vicedegà de grau"), COR("Coordinador");

    private String descripcion;

    private TipoPermiso(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public static String toXML(String... values)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<permisos>");

        for (TipoPermiso permiso : values())
        {
            if (Arrays.asList(values).contains(permiso.name()))
            {
                builder.append("<permiso><id>").append(permiso.name()).append("</id><nombre>")
                        .append(permiso.toString()).append("</nombre></permiso>");
            }
        }

        builder.append("</permisos>");

        return builder.toString();
    }

    @Override
    public String toString()
    {
        return descripcion;
    }

    public static List<UIEntity> toUI()
    {
        List<UIEntity> list = new ArrayList<>();
        for (TipoPermiso permiso : TipoPermiso.values())
        {
            UIEntity ui = new UIEntity();
            ui.put("id", permiso.name());
            ui.put("nombre", permiso.toString());
            list.add(ui);
        }
        return list;
    }
}
