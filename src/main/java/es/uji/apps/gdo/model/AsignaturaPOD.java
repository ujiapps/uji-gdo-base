package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.stereotype.Component;

/**
 * The persistent class for the GDO_EXT_ASI_TODAS database table.
 * 
 */
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_EXT_ASI_TODAS")
@Component
public class AsignaturaPOD implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    private String tipo;

    // bi-directional many-to-one association to GdoAsignaturas
    @OneToMany(mappedBy = "gdoExtAsiToda")
    private Set<Asignatura> gdoAsignaturas;

    public AsignaturaPOD()
    {
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    {
        this.nombreUk = nombreUk;
    }

    public String getTipo()
    {
        return this.tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Set<Asignatura> getGdoAsignaturas()
    {
        return this.gdoAsignaturas;
    }

    public void setGdoAsignaturas(Set<Asignatura> gdoAsignaturas)
    {
        this.gdoAsignaturas = gdoAsignaturas;
    }

}