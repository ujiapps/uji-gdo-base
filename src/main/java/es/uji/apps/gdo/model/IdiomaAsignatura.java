package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.dao.IdiomaAsignaturaDAO;

/**
 * The persistent class for the GDO_ASI_IDIOMAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_ASI_IDIOMAS")
@Component
public class IdiomaAsignatura implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura gdoAsignatura;

    // bi-directional many-to-one association to GdoIdiomas
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma gdoIdioma;

    private static IdiomaAsignaturaDAO idiomaAsignaturaDAO;
    
    @Autowired
    public void setIdiomaDAO(IdiomaAsignaturaDAO idiomaAsignaturaDAO)
    {
        IdiomaAsignatura.idiomaAsignaturaDAO = idiomaAsignaturaDAO;
    }
    
    public IdiomaAsignatura()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    public Idioma getGdoIdioma()
    {
        return this.gdoIdioma;
    }

    public void setGdoIdioma(Idioma gdoIdioma)
    {
        this.gdoIdioma = gdoIdioma;
    }
    
    public static List<IdiomaAsignatura> getIdiomasByAsignaturaId(long asignaturaId)
    {
        return idiomaAsignaturaDAO.getIdiomasByAsignaturaId(asignaturaId);
    }

    @Transactional
    public void insert()
    {
        idiomaAsignaturaDAO.insert(this);
    }
    
}