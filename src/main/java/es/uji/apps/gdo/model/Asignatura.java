package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaBasadaEnIncorrecta;
import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.AsignaturaNoModificableException;
import es.uji.apps.gdo.CamposObligatoriosNoRellenadosException;
import es.uji.apps.gdo.DemasiadasAsignaturasException;
import es.uji.apps.gdo.dao.AsignaturaDAO;
import es.uji.apps.gdo.model.domains.EstadoAsignatura;

/**
 * The persistent class for the GDO_ASIGNATURAS database table.
 */
@SuppressWarnings("serial") @Entity @BatchSize(size = 100) @Table(name = "GDO_ASIGNATURAS") @Component public class Asignatura
        implements Serializable
{
    @Transient public static final int maximoAsignaturas = 200;

    @Id @GeneratedValue(strategy = GenerationType.AUTO) private Long id;

    @Column(name = "AUTORIZA_PAS") private String autorizaPas;

    private String estado;

    private String modificaciones;

    @Column(name = "DEFICIENCIAS")
    private String deficiencias;

    @Temporal(TemporalType.DATE) @Column(name = "FECHA_FIN") private Date fechaFin;

    @Temporal(TemporalType.DATE) @Column(name = "FECHA_INI") private Date fechaIni;

    @Temporal(TemporalType.DATE) @Column(name = "FECHA_VALIDACION_CA") private Date fechaValidacionCa;

    @Temporal(TemporalType.DATE) @Column(name = "FECHA_VALIDACION_ES") private Date fechaValidacionEs;

    @Temporal(TemporalType.DATE) @Column(name = "FECHA_VALIDACION_UK") private Date fechaValidacionUk;

    @Temporal(TemporalType.DATE) @Column(name = "FECHA_FINALIZACION") private Date fechaFinalizacion;

    @Column(name = "TIPO_COMPARTIDA") private String tipoCompartida;

    @Column(name = "EN_SLT") private String enSlt;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ULTIMA_MODIFICACION")
    private Date fechaUltimaModificacion;

    // bi-directional many-to-one association to GdoAsignaturas
    @ManyToOne @JoinColumn(name = "ASIGNATURA_ID_ORIGEN") private Asignatura gdoAsignatura;

    // bi-directional many-to-one association to GdoAsignaturas
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Asignatura> gdoAsignaturas;

    // bi-directional many-to-one association to GdoCursosAcademicos
    @ManyToOne @JoinColumn(name = "CURSO_ACA") private CursoAcademico gdoCursosAcademico;

    // bi-directional many-to-one association to GdoExtAsiTodas
    @ManyToOne @JoinColumn(name = "COD_ASIGNATURA") private AsignaturaPOD gdoExtAsiToda;

    // bi-directional many-to-one association to GdoExtPersonas
    @ManyToOne @JoinColumn(name = "PERSONA_ID") private Persona gdoExtPersona;

    // bi-directional many-to-one association to GdoAsiActividades
    @OneToMany(mappedBy = "gdoAsignatura") private Set<ActividadAsignatura> gdoAsiActividades;

    // bi-directional many-to-one association to GdoAsiCompetencias
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Competencia> gdoAsiCompetencias;

    // bi-directional many-to-one association to GdoAsiEvaluaciones
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Evaluacion> gdoAsiEvaluacions;

    // bi-directional many-to-one association to GdoAsiIdiomas
    @OneToMany(mappedBy = "gdoAsignatura") private Set<IdiomaAsignatura> gdoAsiIdiomas;

    // bi-directional many-to-one association to GdoAsiPermisos
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Permiso> gdoAsiPermisos;

    // bi-directional many-to-one association to GdoAsiResultados
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Resultado> gdoAsiResultados;

    // bi-directional many-to-one association to GdoAsiTextos
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Texto> gdoAsiTextos;

    // bi-directional many-to-one association to GdoAsiComentario
    @OneToMany(mappedBy = "gdoAsignatura") private Set<Comentario> gdoAsiComentarios;

    @Transient private Boolean notificaResponsable;

    @Transient private Boolean notificaCoordinador;

    @Transient private Boolean notificaSLT;

    @Transient private Boolean notificaFinalizada;

    private static AsignaturaDAO asignaturaDAO;

    @Autowired public void setAsignaturaDAO(AsignaturaDAO asignaturaDAO)
    {
        Asignatura.asignaturaDAO = asignaturaDAO;
    }

    public Asignatura()
    {
    }

    public Asignatura clonar()
    {
        Asignatura clonada = new Asignatura();
        clonada.autorizaPas = autorizaPas;
        clonada.estado = estado;
        clonada.modificaciones = modificaciones;
        clonada.deficiencias = deficiencias;
        clonada.fechaFin = fechaFin;
        clonada.fechaIni = fechaIni;
        clonada.fechaValidacionCa = fechaValidacionCa;
        clonada.fechaValidacionEs = fechaValidacionEs;
        clonada.fechaValidacionUk = fechaValidacionUk;
        clonada.fechaFinalizacion = fechaFinalizacion;
        clonada.tipoCompartida = tipoCompartida;
        clonada.enSlt = enSlt;
        clonada.fechaUltimaModificacion = fechaUltimaModificacion;
        clonada.notificaResponsable = notificaResponsable;
        clonada.notificaCoordinador = notificaCoordinador;
        clonada.notificaSLT = notificaSLT;
        clonada.notificaFinalizada = notificaFinalizada;
        return clonada;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAutorizaPas()
    {
        return this.autorizaPas;
    }

    public void setAutorizaPas(String autorizaPas)
    {
        this.autorizaPas = autorizaPas;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String nuevoEstado)
    {
        if (EstadoAsignatura.D.name().equals(this.estado) && EstadoAsignatura.V.name()
                .equals(nuevoEstado) && fechaFinalizacion == null)
        {
            setFechaFinalizacion(new Date());
        }
        this.estado = nuevoEstado;
    }

    public String getEstadoDescripcion()
    {
        return EstadoAsignatura.valueOf(estado).toString();
    }

    public Date getFechaFin()
    {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni()
    {
        return this.fechaIni;
    }

    public void setFechaIni(Date fechaIni)
    {
        this.fechaIni = fechaIni;
    }

    public Date getFechaValidacionCa()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignatura.fechaValidacionCa;
    }

    public void setFechaValidacionCa(Date fechaValidacionCa)
    {
        this.fechaValidacionCa = fechaValidacionCa;
    }

    public Date getFechaValidacionEs()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignatura.fechaValidacionEs;
    }

    public void setFechaValidacionEs(Date fechaValidacionEs)
    {
        this.fechaValidacionEs = fechaValidacionEs;
    }

    public Date getFechaValidacionUk()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignatura.fechaValidacionUk;
    }

    public void setFechaValidacionUk(Date fechaValidacionUk)
    {
        this.fechaValidacionUk = fechaValidacionUk;
    }

    public Date getFechaFinalizacion()
    {
        return this.fechaFinalizacion;
    }

    public void setFechaFinalizacion(Date fechaFinalizacion)
    {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    public String getTipoCompartida()
    {
        return this.tipoCompartida;
    }

    public void setTipoCompartida(String tipoCompartida)
    {
        this.tipoCompartida = tipoCompartida;
    }

    public String getEnSlt()
    {
        return enSlt;
    }

    public void setEnSlt(String enSlt)
    {
        this.enSlt = enSlt;
    }

    public Date getFechaUltimaModificacion()
    {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Date fechaUltimaModificacion)
    {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public Asignatura getGdoAsignatura()
    {
        return this.gdoAsignatura;
    }

    public void setGdoAsignatura(Asignatura gdoAsignatura)
    {
        this.gdoAsignatura = gdoAsignatura;
    }

    public Set<Asignatura> getGdoAsignaturas()
    {
        return this.gdoAsignaturas;
    }

    public void setGdoAsignaturas(Set<Asignatura> gdoAsignaturas)
    {
        this.gdoAsignaturas = gdoAsignaturas;
    }

    public CursoAcademico getGdoCursosAcademico()
    {
        return this.gdoCursosAcademico;
    }

    public void setGdoCursosAcademico(CursoAcademico gdoCursosAcademico)
    {
        this.gdoCursosAcademico = gdoCursosAcademico;
    }

    public AsignaturaPOD getGdoExtAsiToda()
    {
        return this.gdoExtAsiToda;
    }

    public void setGdoExtAsiToda(AsignaturaPOD gdoExtAsiToda)
    {
        this.gdoExtAsiToda = gdoExtAsiToda;
    }

    public Persona getGdoExtPersona()
    {
        return this.gdoExtPersona;
    }

    public void setGdoExtPersona(Persona gdoExtPersona)
    {
        this.gdoExtPersona = gdoExtPersona;
    }

    public Set<ActividadAsignatura> getGdoAsiActividades()
    {
        return this.gdoAsiActividades;
    }

    public void setGdoAsiActividades(Set<ActividadAsignatura> gdoAsiActividades)
    {
        this.gdoAsiActividades = gdoAsiActividades;
    }

    public Set<Competencia> getGdoAsiCompetencias()
    {
        return this.gdoAsiCompetencias;
    }

    public void setGdoAsiCompetencias(Set<Competencia> gdoAsiCompetencias)
    {
        this.gdoAsiCompetencias = gdoAsiCompetencias;
    }

    public Set<Evaluacion> getGdoAsiEvaluacions()
    {
        return this.gdoAsiEvaluacions;
    }

    public void setGdoAsiEvaluacions(Set<Evaluacion> gdoAsiEvaluacions)
    {
        this.gdoAsiEvaluacions = gdoAsiEvaluacions;
    }

    public Set<IdiomaAsignatura> getGdoAsiIdiomas()
    {
        return this.gdoAsiIdiomas;
    }

    public void setGdoAsiIdiomas(Set<IdiomaAsignatura> gdoAsiIdiomas)
    {
        this.gdoAsiIdiomas = gdoAsiIdiomas;
    }

    public Set<Permiso> getGdoAsiPermisos()
    {
        return this.gdoAsiPermisos;
    }

    public void setGdoAsiPermisos(Set<Permiso> gdoAsiPermisos)
    {
        this.gdoAsiPermisos = gdoAsiPermisos;
    }

    public Set<Resultado> getGdoAsiResultados()
    {
        return this.gdoAsiResultados;
    }

    public void setGdoAsiResultados(Set<Resultado> gdoAsiResultados)
    {
        this.gdoAsiResultados = gdoAsiResultados;
    }

    public Set<Texto> getGdoAsiTextos()
    {
        return this.gdoAsiTextos;
    }

    public void setGdoAsiTextos(Set<Texto> gdoAsiTextos)
    {
        this.gdoAsiTextos = gdoAsiTextos;
    }

    public Set<Comentario> getGdoAsiComentarios()
    {
        return this.gdoAsiComentarios;
    }

    public void setGdoAsiComentarios(Set<Comentario> gdoAsiComentarios)
    {
        this.gdoAsiComentarios = gdoAsiComentarios;
    }

    public void setNotificaResponsable(Boolean notificaResponsable)
    {
        this.notificaResponsable = notificaResponsable;
    }

    public Boolean getNotificaResponsable()
    {
        return notificaResponsable;
    }

    public void setNotificaCoordinador(Boolean notificaCoordinador)
    {
        this.notificaCoordinador = notificaCoordinador;
    }

    public Boolean getNotificaCoordinador()
    {
        return notificaCoordinador;
    }

    public void setNotificaSLT(Boolean notificaSLT)
    {
        this.notificaSLT = notificaSLT;
    }

    public Boolean getNotificaSLT()
    {
        return notificaSLT;
    }

    public void setNotificaFinalizada(Boolean notificaFinalizada)
    {
        this.notificaFinalizada = notificaFinalizada;
    }

    public Boolean getNotificaFinalizada()
    {
        return notificaFinalizada;
    }

    public String getNombreAsignaturaCompleto()
    {
        if (getGdoExtAsiToda() != null)
        {
            return getGdoExtAsiToda().getNombreCa();
        }

        return "";
    }

    public boolean isModificable()
    {
        return (isActiva() && this.gdoCursosAcademico.isActivo());
    }

    public boolean isActiva()
    {
        Calendar hoy = Calendar.getInstance();
        Calendar inicio = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();

        if (this.fechaIni != null)
        {
            inicio.setTime(this.fechaIni);
        }
        else
        {
            inicio.setTime(this.gdoCursosAcademico.getFechaIni());
        }

        if (this.fechaFin != null)
        {
            fin.setTime(this.fechaFin);
        }
        else
        {
            fin.setTime(this.gdoCursosAcademico.getFechaFin());
        }

        fin.add(Calendar.DATE, 1);

        return (inicio.before(hoy) && fin.after(hoy));
    }

    public boolean isPeriodoOrdinario()
    {
        Calendar hoy = Calendar.getInstance();
        Calendar inicio = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();

        inicio.setTime(this.gdoCursosAcademico.getFechaIni());
        fin.setTime(this.gdoCursosAcademico.getFechaFin());
        fin.add(Calendar.DATE, 1);

        return (inicio.before(hoy) && fin.after(hoy));
    }

    public boolean isNuevaImplantacion()
    {
        return asignaturaDAO.isNuevaImplantacion(this.gdoExtAsiToda.getId());
    }

    public void delete(long asignaturaId)
    {
        asignaturaDAO.delete(Asignatura.class, asignaturaId);
    }

    public void insertIdioma(long idiomaId) throws AsignaturaNoModificableException
    {
        if (this.isModificable())
        {
            asignaturaDAO.insertIdioma(this.getId(), idiomaId);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    @Transactional public void deleteIdioma(long idiomaId) throws AsignaturaNoModificableException
    {
        if (this.isModificable())
        {
            asignaturaDAO.delete(IdiomaAsignatura.class, idiomaId);
        }
        else
        {
            throw new AsignaturaNoModificableException();
        }
    }

    private void checkAsignaturaModificable() throws AsignaturaNoModificableException
    {
        if (!isModificable())
        {
            throw new AsignaturaNoModificableException();
        }
    }

    private void checkCamposObligatoriosRellenados(Long personaId)
            throws CamposObligatoriosNoRellenadosException
    {
        if (!(Persona.isAdministrador(personaId)) && !(apartadosObligatoriosLlenos())
                && asignaturaEnEstadoPendienteDeValidar())
        {
            throw new CamposObligatoriosNoRellenadosException();
        }
    }

    private void checkAsignaturaModificadaEnPeriodoExtra(Long personaId)
            throws AsignaturaNoModificableException
    {
        if (Persona.isAdministrador(personaId))
            return;

        Boolean estadoFinalizado = estado.equals(EstadoAsignatura.F.name()) || estado.equals(EstadoAsignatura.FD.name()) || estado.equals(EstadoAsignatura.V.name());
        if ((fechaIni != null) && estadoFinalizado && (fechaUltimaModificacion == null || (fechaUltimaModificacion.before(fechaIni))))
        {
            throw new AsignaturaNoModificableException(
                    "No es pot finalitzar una guia en període extra si no s'ha realitzat algun canvi en els continguts");
        }
    }

    private void checkAsignaturaBasada() throws AsignaturaBasadaEnIncorrecta
    {
        if (this.getGdoAsignatura() != null && this.tipoCompartida == null)
        {
            throw new AsignaturaBasadaEnIncorrecta();
        }
    }

    public void checkAsignaturaNoEstaEnSlt(Long personaId) throws AsignaturaNoModificableException
    {
        if (Persona.isAdministrador(personaId)) return;

        if (enSlt.equals("S"))
        {
            throw new AsignaturaNoModificableException(
                    "No pots modificar l'assignatura perquè està en procés de revisió pel SLT");
        }
    }

    public void checkEsResposableCambioAPendienteRevisarEnPeriodoOrdinario(Long personaId) throws AsignaturaNoModificableException
    {
        if (Persona.isAdministrador(personaId)) return;

        Asignatura estadoAnterior = getEstadoAnterior();
        if (isPeriodoOrdinario() && estado.equals(EstadoAsignatura.V.name()) && !estado.equals(estadoAnterior.estado) && !personaId.equals(gdoExtPersona.getId()))
        {
            throw new AsignaturaNoModificableException(
                    "Només el professor responsable de la guia pot canviar l'estat a Pendent de validar durant el període ordinari");
        }
    }

    private void checkTieneDeficiencias() throws CamposObligatoriosNoRellenadosException
    {
        if (getEstado().equals(EstadoAsignatura.FD.name()) && (deficiencias == null || deficiencias.trim().isEmpty()))
        {
            throw new CamposObligatoriosNoRellenadosException(
                    "Heu d'especificar les deficiències de la guia");
        }
    }

    private boolean asignaturaEnEstadoPendienteDeValidar()
    {
        return this.getEstado().equals(EstadoAsignatura.V.name());
    }

    private boolean apartadosObligatoriosLlenos()
    {
        return this.getApartadosObligatoriosLlenosES().longValue() == this
                .getApartadosObligatoriosTotales().longValue()
                && this.getApartadosObligatoriosLlenosCA().longValue() == this
                .getApartadosObligatoriosTotales().longValue();
    }

    private Asignatura getEstadoAnterior()
    {
        return asignaturaDAO.get(Asignatura.class, this.getId()).get(0);
    }

    @Transactional public void updateAsignaturaSiEsModificable(Long personaId)
            throws AsignaturaNoModificableException, CamposObligatoriosNoRellenadosException,
            AsignaturaBasadaEnIncorrecta
    {
        checkAsignaturaModificable();
        checkCamposObligatoriosRellenados(personaId);
        checkAsignaturaModificadaEnPeriodoExtra(personaId);
        checkAsignaturaBasada();
        checkTieneDeficiencias();
        Asignatura estadoAnterior = getEstadoAnterior().clonar();
        asignaturaDAO.update(this);
        updateEstadoAsignaturasRelacionas();

        compruebaSiHayQueNotificar(estadoAnterior);
    }

    @Transactional public void updateEstadoAsignaturasRelacionas()
    {
        List<Asignatura> asignaturasRelacionadas = asignaturaDAO.getAsignaturasRelacionadas(this);

        List<Asignatura> asignaturasRelacionadas2;

        for (Asignatura asi : asignaturasRelacionadas)
        {
            asi.setEstado(this.estado);
            asignaturaDAO.update(asi);

            if (asi.getAsignaturaOriginal().getId() != null)
            {
                asignaturasRelacionadas2 = asignaturaDAO.getAsignaturasRelacionadas(asi);

                for (Asignatura asi2 : asignaturasRelacionadas2)
                {
                    asi2.setEstado(asi.estado);
                    asignaturaDAO.update(asi2);
                }
            }

        }
    }

    @Transactional public void updateAsignaturaSinCheckeoPorAdmin()
            throws AsignaturaBasadaEnIncorrecta, AsignaturaNoModificableException {
        Asignatura estadoAnterior = getEstadoAnterior().clonar();
        checkAsignaturaBasada();
        asignaturaDAO.update(this);
        updateEstadoAsignaturasRelacionas();
        compruebaSiHayQueNotificar(estadoAnterior);
    }

    @Transactional public void updateAsignaturaParaCopiaDeAsignaturas()
    {
        asignaturaDAO.update(this);
        updateEstadoAsignaturasRelacionas();
    }

    @Transactional public void update()
    {
        asignaturaDAO.update(this);
    }

    private void compruebaSiHayQueNotificar(Asignatura asignaturaAnterior)
    {
        this.setNotificaResponsable(false);
        this.setNotificaCoordinador(false);
        this.setNotificaSLT(false);
        this.setNotificaFinalizada(false);

        if (asignaturaAnterior.getEstado().equals(EstadoAsignatura.V.name()) && this.estado
                .equals(EstadoAsignatura.D.name()))
        {
            this.setNotificaResponsable(true);
        }

        if (asignaturaAnterior.getEstado().equals(EstadoAsignatura.F.name()) && this.estado
                .equals(EstadoAsignatura.D.name()))
        {
            this.setNotificaResponsable(true);
        }

        if (asignaturaAnterior.getEstado().equals(EstadoAsignatura.D.name()) && this.estado
                .equals(EstadoAsignatura.V.name()))
        {
            this.setNotificaCoordinador(true);
        }

        if ((!asignaturaAnterior.getEstado().equals(EstadoAsignatura.F.name())
                && !asignaturaAnterior.getEstado().equals(EstadoAsignatura.FD.name())) && (
                this.estado.equals(EstadoAsignatura.F.name()) || this.estado
                        .equals(EstadoAsignatura.FD.name())))
        {
            this.setNotificaFinalizada(true);
        }

        if (asignaturaAnterior.getEnSlt().equals("N") && this.enSlt.equals("S"))
        {
            this.setNotificaSLT(true);
        }
    }

    public static Asignatura getAsignatura(Long asignaturaId)
    {
        Asignatura asignatura = asignaturaDAO.getAsignaturaByIdSinRelaciones(asignaturaId);

        if (asignatura != null)
        {
            return asignatura;
        }
        else
        {
            return new Asignatura();
        }
    }

    public static Asignatura getAsignaturaConRelaciones(Long asignaturaId)

    {
        Asignatura asignatura = asignaturaDAO.getAsignaturaByIdConRelaciones(asignaturaId);

        if (asignatura != null)
        {
            return asignatura;
        }
        else
        {
            return new Asignatura();
        }
    }

    public static void checkDemasiadasAsignaturas(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso,
            boolean conGuia) throws DemasiadasAsignaturasException
    {
        Long numeroAsignaturas = asignaturaDAO
                .contarAsignaturasConFiltros(personaId, tipoEstudio, codigoAsignatura, estado,
                        titulacion, curso, conGuia);
        if (numeroAsignaturas > maximoAsignaturas)
        {
            throw new DemasiadasAsignaturasException();
        }

    }

    public static List<Asignatura> getAsignaturasConFiltros(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso,
            boolean conGuia, String pantalla) throws DemasiadasAsignaturasException
    {
        List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();

        if (Persona.isAdministrador(personaId))
        {
            if (pantalla == null || !pantalla.equals("Sinlimite"))
            {
                checkDemasiadasAsignaturas(null, tipoEstudio, codigoAsignatura, estado, titulacion,
                        curso, conGuia);
            }

            listaAsignaturas = asignaturaDAO
                    .getAsignaturasConFiltros(null, tipoEstudio, codigoAsignatura, estado,
                            titulacion, curso, conGuia);

        }
        else
        {
            if (pantalla == null || !pantalla.equals("Sinlimite"))
            {
                checkDemasiadasAsignaturas(personaId, tipoEstudio, codigoAsignatura, estado,
                        titulacion, curso, conGuia);
            }

            listaAsignaturas = asignaturaDAO
                    .getAsignaturasConFiltros(personaId, tipoEstudio, codigoAsignatura, estado,
                            titulacion, curso, conGuia);
        }

        return listaAsignaturas;

    }

    public static List<Asignatura> getAsignaturasByTitulacion(Long titulacionId)
    {
        return asignaturaDAO.getAsignaturasByTitulacion(titulacionId);
    }

    public Titulacion getTitulacion() throws AsignaturaNoAsignadaATitulacion
    {
        return Titulacion.getTitulacionByAsignaturaId(this.id);
    }

    public static Asignatura getAsignaturaByCodigoDelCursoActivo(String codigoAsignatura)
    {
        return asignaturaDAO.getAsignaturaByIdConRelaciones(codigoAsignatura);
    }

    public static Asignatura getAsignaturaByCodigoAndCurso(String codigoAsignatura, Long curso)
    {
        return asignaturaDAO.getAsignaturaByAsignaturaAndCursoAca(codigoAsignatura, curso);
    }

    public static List<Long> getIdAsignaturasDeUnaPersona(Long personaId)
    {
        return asignaturaDAO.getIdAsignaturasDeUnaPersona(personaId,
                CursoAcademico.getCursoActivo().getCursoAca());
    }

    public Asignatura getAsignaturaOriginal()
    {
        Asignatura asignatura = this;

        if (this.getGdoAsignatura() != null)
        {
            asignatura = this.getGdoAsignatura();
        }

        return asignatura;
    }

    public Asignatura getAsignaturaOriginalConRelaciones()
    {
        Asignatura asignatura = this;

        if (this.getGdoAsignatura() != null)
        {
            asignatura = asignaturaDAO.getAsignaturaByIdConRelaciones(getGdoAsignatura().getId());
        }

        return asignatura;
    }

    public Long getApartadosObligatoriosTotales()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignaturaDAO.apartadosObligatoriosTotales(asignatura.id);

    }

    public Long getApartadosObligatoriosLlenosCA()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignaturaDAO.apartadosObligatoriosLlenos(asignatura.id, "CA");
    }

    public Long getApartadosObligatoriosLlenosES()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignaturaDAO.apartadosObligatoriosLlenos(asignatura.id, "ES");
    }

    public Long getApartadosObligatoriosLlenosUK()
    {
        Asignatura asignatura = this.getAsignaturaOriginal();

        return asignaturaDAO.apartadosObligatoriosLlenos(asignatura.id, "UK");
    }

    public Long getNumeroDeComentarios()
    {
        return asignaturaDAO.getNumeroDeComentarios(this.id);
    }

    public Long getNumeroComentarios()
    {
        return new Long(gdoAsiComentarios.size());
    }

    public Long getNumeroDeModificaciones()
    {
        return gdoAsiTextos.stream().mapToLong(t -> t.getNumeroIdiomasModificados()).sum();
    }

    public Long getCursoAsignatura()
    {
        return asignaturaDAO.getCursoAsignatura(gdoExtAsiToda.getId());
    }

    public static Long cuentaAsignaturas(Long cursoAca)
    {
        return asignaturaDAO.cuentaAsignaturas(cursoAca);
    }

    public static List<Asignatura> getAsignaturasByCursoAcademico(Long cursoAca)
    {
        return asignaturaDAO.getAsignaturasByCursoAcademico(cursoAca);
    }

    public Asignatura insertAsignatura()
    {
        return asignaturaDAO.insert(this);
    }

    public List<Persona> getCoordinadorDeLaAsignatura()
    {
        return asignaturaDAO.getCoordinadoresDeLaAsignatura(this);
    }

    public List<Persona> getResponsablesAsignatura()
    {
        return asignaturaDAO.getResponsablesAsignatura(this);
    }

    public String getModificaciones()
    {
        return modificaciones;
    }

    public void setModificaciones(String modificaciones)
    {
        this.modificaciones = modificaciones;
    }

    public String getDeficiencias()
    {
        return deficiencias;
    }

    public void setDeficiencias(String deficiencias)
    {
        this.deficiencias = deficiencias;
    }

    public Long getNumeroSeccionesObligatorias()
    {
        Long idiomas = 1L;
        Long evaluaciones = 1L;
        Long actividades = 1L;
        Long competencias = 1L;

        String tipoEstudioAsignatura = gdoExtAsiToda.getTipo();
        Long apartados = gdoAsiTextos.stream().map(t -> t.getGdoApartado())
                .filter(a -> a.getObligatorio().equals("S") && (a.getTipoEstudio().equals("T") || a
                        .getTipoEstudio().equals(tipoEstudioAsignatura))).count();
        return apartados + idiomas + evaluaciones + actividades + competencias;
    }

    public Long getNumeroSeccionesLlenasByIdioma(String idioma)
    {
        String tipoEstudioAsignatura = gdoExtAsiToda.getTipo();
        Long numeroApartados = gdoAsiTextos.stream()
                .filter(a -> a.getGdoApartado().getObligatorio().equals("S") && (
                        a.getTextoByIdioma(idioma) != null && !a.getTextoByIdioma(idioma).isEmpty())
                        && (a.getGdoApartado().getTipoEstudio().equals("T") || a.getGdoApartado()
                        .getTipoEstudio().equals(tipoEstudioAsignatura))).count();

        Long idiomas = (gdoAsiIdiomas.size() > 0) ? 1L : 0L;
        Long evaluaciones = (gdoAsiEvaluacions.size() > 0) ? 1L : 0L;
        Long actividades = (gdoAsiActividades.size() > 0) ? 1L : 0L;
        Long competencias = (gdoAsiCompetencias.size() > 0) ? 1L : 0L;

        return numeroApartados + idiomas + evaluaciones + actividades + competencias;
    }

    @Override public int hashCode()
    {
        return (id != null) ? id.intValue() : -1;
    }

    @Override public boolean equals(Object asignatura)
    {
        if (this == asignatura)
            return true;
        if (asignatura == null)
            return false;
        if (getClass() != asignatura.getClass())
            return false;
        Asignatura otraAsignatura = (Asignatura) asignatura;
        if (!id.equals(otraAsignatura.id))
            return false;
        return true;
    }
}