package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.IdiomaAsignaturaDAO;

@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_IDIOMAS")
@Component
public class Idioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    // bi-directional many-to-one association to GdoAsiIdiomas
    @OneToMany(mappedBy = "gdoIdioma")
    private Set<IdiomaAsignatura> gdoAsiIdiomas;

    private static IdiomaAsignaturaDAO idiomaDAO;

    @Autowired
    public void setIdiomaDAO(IdiomaAsignaturaDAO idiomaDAO)
    {
        Idioma.idiomaDAO = idiomaDAO;
    }

    public Idioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    {
        this.nombreUk = nombreUk;
    }

    public Set<IdiomaAsignatura> getGdoAsiIdiomas()
    {
        return this.gdoAsiIdiomas;
    }

    public void setGdoAsiIdiomas(Set<IdiomaAsignatura> gdoAsiIdiomas)
    {
        this.gdoAsiIdiomas = gdoAsiIdiomas;
    }

    public static List<Idioma> getIdiomas()
    {
        return idiomaDAO.getIdiomas();
    }
    
    public static List<Idioma> getIdiomasByAsignaturaIdNoAsignados(long asignaturaId)
    {
        return idiomaDAO.getIdiomasByAsignaturaIdNoAsignados(asignaturaId);
    }
}