package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.dao.ApartadoDAO;
import es.uji.apps.gdo.model.domains.TipoApartado;
import es.uji.apps.gdo.model.domains.TipoEstudioQueModifica;

/**
 * The persistent class for the GDO_APARTADOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_APARTADOS")
@Component
public class Apartado implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    private String obligatorio;

    private Integer orden;

    @Column(name = "TEXTO_AYUDA")
    private String textoAyuda;

    private String tipo;

    @Column(name = "TIPO_ESTUDIO")
    private String tipoEstudio;

    // bi-directional many-to-one association to GdoCursosAcademico
    @ManyToOne
    @JoinColumn(name = "CURSO_ACA")
    private CursoAcademico gdoCursosAcademico;

    // bi-directional many-to-one association to GdoAsiTexto
    @OneToMany(mappedBy = "gdoApartado")
    private Set<Texto> gdoAsiTextos;

    private static ApartadoDAO apartadoDAO;

    @Autowired
    public void setApartadoDAO(ApartadoDAO apartadoDAO)
    {
        Apartado.apartadoDAO = apartadoDAO;
    }

    public Apartado()
    {
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    {
        this.nombreUk = nombreUk;
    }

    public String getObligatorio()
    {
        return this.obligatorio;
    }

    public void setObligatorio(String obligatorio)
    {
        this.obligatorio = obligatorio;
    }

    public Integer getOrden()
    {
        return this.orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getTextoAyuda()
    {
        return this.textoAyuda;
    }

    public void setTextoAyuda(String textoAyuda)
    {
        this.textoAyuda = textoAyuda;
    }

    public String getTipo()
    {
        return this.tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public CursoAcademico getGdoCursosAcademico()
    {
        return this.gdoCursosAcademico;
    }

    public void setGdoCursosAcademico(CursoAcademico gdoCursosAcademico)
    {
        this.gdoCursosAcademico = gdoCursosAcademico;
    }

    public Set<Texto> getGdoAsiTextos()
    {
        return this.gdoAsiTextos;
    }

    public void setGdoAsiTextos(Set<Texto> gdoAsiTextos)
    {
        this.gdoAsiTextos = gdoAsiTextos;
    }

    public static List<Apartado> getApartadosByAsignaturaAndTipo(Long asignaturaId,
            TipoApartado tipoApartado)
    {
        return apartadoDAO.getApartadosByAsignaturaAndTipo(asignaturaId, tipoApartado);
    }

    public static Apartado getApartadoByAsignaturaAndTipo(Long asignaturaId,
            TipoApartado tipoApartado)
    {
        List<Apartado> apartados = getApartadosByAsignaturaAndTipo(asignaturaId, tipoApartado);
        return apartados.get(0);
    }

    public Apartado insertApartado()
    {
        return apartadoDAO.insert(this);
    }

    public static Apartado getApartadoById(Long apartadoId)
    {
        return apartadoDAO.get(Apartado.class, apartadoId).get(0);
    }

    public static List<Apartado> getApartadosByTipoParaActivarAsignatura(TipoApartado tipoApartado,
            String tipoEstudio)
    {
        return apartadoDAO.getApartadosByTipoParaActivarAsignatura(tipoApartado, tipoEstudio);
    }

    public static String getApartadosObligatoriosVaciosByAsignatura(Long asignaturaId)
    {
        Asignatura asignatura = Asignatura.getAsignatura(asignaturaId);
        Long idAsignaturaReal = asignaturaId;
        Asignatura asignaturaOrigen = asignatura.getGdoAsignatura();

        if (asignaturaOrigen != null)
        {
            idAsignaturaReal = asignaturaOrigen.getId();
        }

        return apartadoDAO.getApartadosObligatoriosVaciosByAsignatura(idAsignaturaReal);
    }

    public Boolean isModificable(Asignatura asignatura) throws AsignaturaNoAsignadaATitulacion
    {
        if (this.tipoEstudio == null)
        {
            return false;
        }

        if (this.tipoEstudio.equals(TipoEstudioQueModifica.T.name()))
        {
            return true;
        }

        AsignaturaPOD asignaturaPod = asignatura.getGdoExtAsiToda();

        if (asignaturaPod != null && asignaturaPod.getTipo().equals(this.getTipoEstudio()))
        {
            if (asignaturaPod.getTipo().equals("M"))
            {
                Titulacion titulacion = asignatura.getTitulacion();

                if (titulacion.getErasmusMundus().equals("S") || (titulacion.getInteruniversitario().equals("S")
                        && titulacion.getCoordinaUji().equals("N")))
                {
                    return true;
                }

                return false;
            }

            return true;
        }

        return false;
    }

    public String getApartadoByIdioma(String idioma)
    {
        switch (idioma.toUpperCase())
        {
            case "ES":
                return nombreEs;
            case "CA":
                return nombreCa;
            case "UK":
                return nombreUk;
            default:
                return null;
        }
    }
}