package es.uji.apps.gdo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the GDO_EXT_TITULACIONES_ASI database table.
 * 
 */
@Embeddable
public class TitulacionAsignaturaPK implements Serializable
{
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "COD_ASIGNATURA")
    private String codAsignatura;

    @Column(name = "ESTUDIO_ID")
    private long estudioId;

    public TitulacionAsignaturaPK()
    {
    }

    public String getCodAsignatura()
    {
        return this.codAsignatura;
    }

    public void setCodAsignatura(String codAsignatura)
    {
        this.codAsignatura = codAsignatura;
    }

    public long getEstudioId()
    {
        return this.estudioId;
    }

    public void setEstudioId(long estudioId)
    {
        this.estudioId = estudioId;
    }

    public boolean equals(Object other)
    {
        if (this.equals(other))
        {
            return true;
        }
        if (!(other instanceof TitulacionAsignaturaPK))
        {
            return false;
        }
        TitulacionAsignaturaPK castOther = (TitulacionAsignaturaPK) other;
        return this.codAsignatura.equals(castOther.codAsignatura)
                && (this.estudioId == castOther.estudioId);

    }

    public int hashCode()
    {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.codAsignatura.hashCode();
        hash = hash * prime + ((int) (this.estudioId ^ (this.estudioId >>> 32)));

        return hash;
    }
}