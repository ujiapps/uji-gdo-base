package es.uji.apps.gdo.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the GDO_EXT_TITULACIONES_PER database table.
 * 
 */
@Embeddable
public class TitulacionPersonaPK implements Serializable
{
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "ESTUDIO_ID")
    private long estudioId;

    @Column(name = "PERSONA_ID")
    private long personaId;

    public TitulacionPersonaPK()
    {
    }

    public long getEstudioId()
    {
        return this.estudioId;
    }

    public void setEstudioId(long estudioId)
    {
        this.estudioId = estudioId;
    }

    public long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(long personaId)
    {
        this.personaId = personaId;
    }

    public boolean equals(Object other)
    {
        if (this.equals(other))
        {
            return true;
        }
        if (!(other instanceof TitulacionPersonaPK))
        {
            return false;
        }
        TitulacionPersonaPK castOther = (TitulacionPersonaPK) other;
        return (this.estudioId == castOther.estudioId) && (this.personaId == castOther.personaId);

    }

    public int hashCode()
    {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.estudioId ^ (this.estudioId >>> 32)));
        hash = hash * prime + ((int) (this.personaId ^ (this.personaId >>> 32)));

        return hash;
    }
}