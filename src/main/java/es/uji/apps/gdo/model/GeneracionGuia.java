package es.uji.apps.gdo.model;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.model.domains.TipoApartado;

public class GeneracionGuia
{
    @Transactional
    public static void copiaDelVerifica(Long asignaturaId) throws AsignaturaNoAsignadaATitulacion
    {
        Asignatura asignatura = Asignatura.getAsignatura(asignaturaId);

        Boolean esEditable = false;
        String editableCadena = "N";

        if (asignatura.getTitulacion().getTipo().equals("M"))
        {
            esEditable = true;
            editableCadena = "S";
        }

        insertaTextos(asignatura);
        insertaActividades(asignatura);
        insertaEvaluaciones(asignatura, esEditable);
        insertaCompetencias(asignatura, editableCadena);
        insertaResultados(asignatura, editableCadena);
    }

    private static void insertaTextos(Asignatura asignatura) throws AsignaturaNoAsignadaATitulacion
    {
        List<Texto> textosAsignatura = Texto.getTextosByAsignatura(asignatura.getId());

        if (textosAsignatura.isEmpty()
                || (textosAsignatura.size() == 1 && textosAsignatura.get(0).getGdoApartado()
                        .getId() == 8L))
        {

            String tipoEstudio = asignatura.getTitulacion().getTipo();

            List<Apartado> listaApartados = Apartado.getApartadosByTipoParaActivarAsignatura(
                    TipoApartado.T, tipoEstudio);

            if (listaApartados != null && !listaApartados.isEmpty())
            {
                for (Apartado apartado : listaApartados)
                {
                    Texto nuevoTexto = new Texto();

                    nuevoTexto.setGdoApartado(apartado);
                    nuevoTexto.setGdoAsignatura(asignatura);

                    if (apartado.getId() == 8L)
                    {
                        VerificaTexto verificaTexto = VerificaTexto.getTextoByAsignatura(asignatura
                                .getGdoExtAsiToda().getId());

                        if (verificaTexto.getCodAsignatura() == null)
                        {
                            nuevoTexto.insertTexto();
                        }
                        else
                        {
                            if (!(textosAsignatura.size() == 1 && textosAsignatura.get(0)
                                    .getGdoApartado().getId() == 8L))

                            {
                                nuevoTexto.setTextoCa(verificaTexto.getContenidosCa());
                                nuevoTexto.setTextoEs(verificaTexto.getContenidosEs());
                                nuevoTexto.insertTexto();
                            }
                            else
                            {
                                nuevoTexto = textosAsignatura.get(0);
                                if (nuevoTexto.getTextoCa() == null)
                                {
                                    nuevoTexto.setTextoCa(verificaTexto.getContenidosCa());
                                }
                                if (nuevoTexto.getTextoEs() == null)
                                {
                                    nuevoTexto.setTextoEs(verificaTexto.getContenidosEs());
                                }
                                nuevoTexto.updateParaCopia();
                            }
                        }
                    }
                    else
                    {
                        nuevoTexto.insertTexto();
                    }
                }
            }
        }
    }

    private static void insertaActividades(Asignatura asignatura)
    {
        if (ActividadAsignatura.getActividadesByAsignaturaId(asignatura.getId()).isEmpty())
        {
            List<VerificaActividad> listaActividades = VerificaActividad
                    .getVerificaActividadByAsignatura(asignatura);

            if (listaActividades != null && !listaActividades.isEmpty())
            {
                for (VerificaActividad verificaActividad : listaActividades)
                {
                    ActividadAsignatura actividad = new ActividadAsignatura();

                    actividad.setGdoAsignatura(asignatura);
                    actividad.setGdoTipoActividade(Actividad.getActividadById(verificaActividad
                            .getActId()));
                    actividad.setHorasNoPresenciales(verificaActividad.getHNoPre());
                    actividad.setHorasPresenciales(verificaActividad.getHPre());

                    actividad.insertParaCopia();
                }
            }
        }
    }

    private static void insertaEvaluaciones(Asignatura asignatura, Boolean editable)
            throws AsignaturaNoAsignadaATitulacion
    {
        if (Evaluacion.getEvaluacionesByAsignaturaId(asignatura.getId()).isEmpty())
        {
            List<VerificaEvaluacion> listaEvaluaciones = VerificaEvaluacion
                    .getVerificaEvaluacionByAsignatura(asignatura);

            if (listaEvaluaciones != null && !listaEvaluaciones.isEmpty())
            {
                for (VerificaEvaluacion verificaEvaluacion : listaEvaluaciones)
                {
                    Evaluacion evaluacion = new Evaluacion();

                    evaluacion.setGdoAsignatura(asignatura);
                    evaluacion.setEvaluacionId(verificaEvaluacion.getEvalId());
                    evaluacion.setNombreCa(verificaEvaluacion.getNombreCa());
                    evaluacion.setNombreEs(verificaEvaluacion.getNombreEs());
                    evaluacion.setNombreUk(verificaEvaluacion.getNombreUk());
                    evaluacion.setPonderacion(verificaEvaluacion.getPonderacion());
                    evaluacion.setOrden(0L);
                    evaluacion.setEditable(editable);

                    evaluacion.insertParaCopia();
                }
            }
        }
    }

    private static void insertaCompetencias(Asignatura asignatura, String editable)
    {
        if (Competencia.getCompetenciasByAsignatura(asignatura.getId()).isEmpty())
        {
            List<VerificaCompetencia> listaCompetencias = VerificaCompetencia
                    .getVerificaCompetenciaByAsignatura(asignatura);

            if (listaCompetencias != null && !listaCompetencias.isEmpty())
            {
                for (VerificaCompetencia verificaCompetencia : listaCompetencias)
                {
                    Competencia competencia = new Competencia();

                    competencia.setGdoAsignatura(asignatura);
                    competencia.setCompetenciaCa(verificaCompetencia.getNombreCa());
                    competencia.setCompetenciaEs(verificaCompetencia.getNombreEs());
                    competencia.setCompetenciaUk(verificaCompetencia.getNombreUk());
                    competencia.setEditable(editable);
                    competencia.setOrden(0L);

                    competencia.insertParaCopia();
                }
            }
        }
    }

    private static void insertaResultados(Asignatura asignatura, String editable)
    {
        if (Resultado.getResultadosByAsignatura(asignatura.getId()).isEmpty())
        {
            List<VerificaResultado> listaResultados = VerificaResultado
                    .getVerificaResultadoByAsignatura(asignatura);

            if (listaResultados != null && !listaResultados.isEmpty())
            {
                for (VerificaResultado verificaResultado : listaResultados)
                {
                    Resultado resultado = new Resultado();

                    resultado.setGdoAsignatura(asignatura);
                    resultado.setResultadoCa(verificaResultado.getNombreCa());
                    resultado.setResultadoEs(verificaResultado.getNombreEs());
                    resultado.setResultadoUk(verificaResultado.getNombreUk());
                    resultado.setEditable(editable);
                    resultado.setOrden(0L);

                    resultado.insertParaCopia();
                }
            }
        }
    }
}
