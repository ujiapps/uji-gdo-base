package es.uji.apps.gdo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.stereotype.Component;

/**
 * The persistent class for the GDO_EXT_TITULACIONES_ASI database table.
 * 
 */
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_EXT_TITULACIONES_ASI")
@Component
public class TitulacionAsignatura implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private TitulacionAsignaturaPK id;
    
    @Column(name="CURSO_ID")
    private Long cursoId;

    public TitulacionAsignatura()
    {
    }

    public TitulacionAsignaturaPK getId()
    {
        return this.id;
    }

    public void setId(TitulacionAsignaturaPK id)
    {
        this.id = id;
    }
    
    public Long getCursoId() {
        return this.cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }

}