package es.uji.apps.gdo.model.domains;

public enum Operacion
{
    UPDATE("Update"), INSERT("Insert"), DELETE("Delete"), UPDATEGENERAL("Update Todo");

    private String descripcion;

    private Operacion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    @Override
    public String toString()
    {
        return descripcion;
    }
}
