package es.uji.apps.gdo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.gdo.dao.VerificaResultadoDAO;

/**
 * The persistent class for the GDO_VW_EXT_RESULTADOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "GDO_VW_EXT_RESULTADOS")
@Component
public class VerificaResultado implements Serializable
{

    @Id
    @Column(name = "COD_ASIGNATURA")
    private String codAsignatura;

    @Id
    @Column(name = "RDO_ID")
    private Long rdoId;

    @Id
    @Column(name = "CURSO_ACA_INI")
    private Long cursoAcaIni;

    @Column(name = "CURSO_ACA_FIN")
    private Long cursoAcaFin;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;
    
    private static VerificaResultadoDAO verificaResultadoDAO;
    
    @Autowired
    public void setVerificaResultadoDAO(VerificaResultadoDAO verificaResultadoDAO)
    {
        VerificaResultado.verificaResultadoDAO = verificaResultadoDAO;
    }

    public VerificaResultado()
    {
    }

    public String getCodAsignatura()
    {
        return this.codAsignatura;
    }

    public void setCodAsignatura(String codAsignatura)
    {
        this.codAsignatura = codAsignatura;
    }

    public Long getCursoAcaFin()
    {
        return this.cursoAcaFin;
    }

    public void setCursoAcaFin(Long cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }

    public Long getCursoAcaIni()
    {
        return this.cursoAcaIni;
    }

    public void setCursoAcaIni(Long cursoAcaIni)
    {
        this.cursoAcaIni = cursoAcaIni;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    {
        this.nombreUk = nombreUk;
    }

    public Long getRdoId()
    {
        return this.rdoId;
    }

    public void setRdoId(Long rdoId)
    {
        this.rdoId = rdoId;
    }

    public static List<VerificaResultado> getVerificaResultadoByAsignatura(Asignatura asignatura)
    {
        return verificaResultadoDAO.getVerificaResultadoByAsignatura(asignatura.getGdoExtAsiToda()
                .getId(), asignatura.getGdoCursosAcademico().getCursoAca());
    }

}