package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class NotificacionSinDestinatarios extends GeneralGDOException
{
    public NotificacionSinDestinatarios()
    {
        super("No s\'han trobat destinataris de la notificació.");
    }
    
    public NotificacionSinDestinatarios(String message)
    {
        super(message);
    }

}