package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class ErrorAlLeerPlantilla extends GeneralGDOException
{
    public ErrorAlLeerPlantilla()
    {
        super("No s\'han pogut carregar el fitxer de plantilla.");
    }
    
    public ErrorAlLeerPlantilla(String message)
    {
        super(message);
    }

}
