package es.uji.apps.gdo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.uji.apps.gdo.model.Texto;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.content.WordHTMLCleaner;

public class LimpiaHTMLTablaTextos
{
    public static void main(String[] args)
    {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "classpath:applicationContext.xml");

        EntityManagerFactory entityManagerFactory = (EntityManagerFactory) context
                .getBean("entityManagerFactory");
        
        BaseDAODatabaseImpl baseDAO = new BaseDAODatabaseImpl();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        baseDAO.setEntityManager(entityManager);

        entityManager.getTransaction().begin();
        
        List<Texto> listaTextos = baseDAO.get(Texto.class);
        for (Texto texto : listaTextos)
        {
            System.out.println("Processant: " + texto.getId());

            try
            {
                WordHTMLCleaner cleaner = null;
                String resultText = "";

                if (texto.getTextoCa() != null)
                {
                    cleaner = new WordHTMLCleaner();
                    resultText = cleaner.clean(texto.getTextoCa());

                    texto.setTextoCa(resultText);
                }

                if (texto.getTextoEs() != null)
                {
                    cleaner = new WordHTMLCleaner();
                    resultText = cleaner.clean(texto.getTextoEs());

                    texto.setTextoEs(resultText);
                }

                if (texto.getTextoUk() != null)
                {
                    cleaner = new WordHTMLCleaner();
                    resultText = cleaner.clean(texto.getTextoUk());

                    texto.setTextoUk(resultText);
                }

                baseDAO.update(texto);
            }
            catch (Throwable e)
            {
                System.out.println("No he pogut netejar el registre: " + texto.getId());
            }
        }
        
        entityManager.getTransaction().commit();
    }
}