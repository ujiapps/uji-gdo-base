package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class AsignaturaNoAsignadaATitulacion extends GeneralGDOException
{
    public AsignaturaNoAsignadaATitulacion()
    {
        super("La assignatura no està assignada a cap titulació");
    }

    public AsignaturaNoAsignadaATitulacion(String message)
    {
        super(message);
    }
}