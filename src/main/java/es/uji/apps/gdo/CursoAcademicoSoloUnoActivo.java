package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class CursoAcademicoSoloUnoActivo extends GeneralGDOException
{
    public CursoAcademicoSoloUnoActivo()
    {
        super("Sols pot haver un Curs Acadèmic actiu");
    }
    
    public CursoAcademicoSoloUnoActivo(String message)
    {
        super(message);
    }

}