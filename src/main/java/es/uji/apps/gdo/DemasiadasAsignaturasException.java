package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class DemasiadasAsignaturasException extends GeneralGDOException
{
    public DemasiadasAsignaturasException()
    {
        super("Nombre excessiu d'assignatures. Defineix mes filtres  ");
    }
    
    public DemasiadasAsignaturasException(String message)
    {
        super(message);
    }
}


