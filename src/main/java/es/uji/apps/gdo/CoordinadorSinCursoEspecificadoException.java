package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class CoordinadorSinCursoEspecificadoException extends GeneralGDOException
{
    public CoordinadorSinCursoEspecificadoException()
    {
        super("Al coordinador cal especificar-li el curs del qual es responsable");
    }

    public CoordinadorSinCursoEspecificadoException(String message)
    {
        super(message);
    }
}