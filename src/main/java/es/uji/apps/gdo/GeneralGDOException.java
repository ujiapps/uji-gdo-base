package es.uji.apps.gdo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class GeneralGDOException extends CoreBaseException
{
    public GeneralGDOException()
    {
        super("S'ha produït un error en l'operació");
    }
    
    public GeneralGDOException(String message)
    {
        super(message);
    }
}