package es.uji.apps.gdo;

@SuppressWarnings("serial")
public class AsignaturaNoModificableException extends GeneralGDOException
{
    public AsignaturaNoModificableException()
    {
        super("L'assignatura seleccionada no es pot modificar");
    }
    
    public AsignaturaNoModificableException(String message)
    {
        super(message);
    }

}
