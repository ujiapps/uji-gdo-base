package es.uji.apps.gdo.ui;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResumenEstadisticas {
    private Long asignadasInactivas;
    private Long asignadasInactivasGrado;
    private Long asignadasInactivasMaster;
    private Long asignadasIntroduciendo;
    private Long asignadasIntroduciendoGrado;
    private Long asignadasIntroduciendoMaster;
    private Long asignadasPendientes;
    private Long asignadasPendientesGrado;
    private Long asignadasPendientesMaster;
    private Long asignadasTraduccion;
    private Long asignadasTraduccionGrado;
    private Long asignadasTraduccionMaster;
    private Long asignadasFinalizadas;
    private Long asignadasFinalizadasGrado;
    private Long asignadasFinalizadasMaster;
    private Long asignadasFinalizadasDeficiencias;
    private Long asignadasFinalizadasDeficienciasGrado;
    private Long asignadasFinalizadasDeficienciasMaster;

    private Long noAsignadasInactivas;
    private Long noAsignadasInactivasGrado;
    private Long noAsignadasInactivasMaster;
    private Long noAsignadasIntroduciendo;
    private Long noAsignadasIntroduciendoGrado;
    private Long noAsignadasIntroduciendoMaster;
    private Long noAsignadasPendientes;
    private Long noAsignadasPendientesGrado;
    private Long noAsignadasPendientesMaster;
    private Long noAsignadasTraduccion;
    private Long noAsignadasTraduccionGrado;
    private Long noAsignadasTraduccionMaster;
    private Long noAsignadasFinalizadas;
    private Long noAsignadasFinalizadasGrado;
    private Long noAsignadasFinalizadasMaster;
    private Long noAsignadasFinalizadasDeficiencias;
    private Long noAsignadasFinalizadasDeficienciasGrado;
    private Long noAsignadasFinalizadasDeficienciasMaster;

    private Boolean usuarioSLT;

    public Long getAsignadasInactivas() {
        return asignadasInactivas;
    }

    public void setAsignadasInactivas(Long asignadasInactivas) {
        this.asignadasInactivas = asignadasInactivas;
    }

    public Long getAsignadasInactivasGrado() {
        return asignadasInactivasGrado;
    }

    public void setAsignadasInactivasGrado(Long asignadasInactivasGrado) {
        this.asignadasInactivasGrado = asignadasInactivasGrado;
    }

    public Long getAsignadasInactivasMaster() {
        return asignadasInactivasMaster;
    }

    public void setAsignadasInactivasMaster(Long asignadasInactivasMaster) {
        this.asignadasInactivasMaster = asignadasInactivasMaster;
    }

    public Long getAsignadasIntroduciendo() {
        return asignadasIntroduciendo;
    }

    public void setAsignadasIntroduciendo(Long asignadasIntroduciendo) {
        this.asignadasIntroduciendo = asignadasIntroduciendo;
    }

    public Long getAsignadasIntroduciendoGrado() {
        return asignadasIntroduciendoGrado;
    }

    public void setAsignadasIntroduciendoGrado(Long asignadasIntroduciendoGrado) {
        this.asignadasIntroduciendoGrado = asignadasIntroduciendoGrado;
    }

    public Long getAsignadasIntroduciendoMaster() {
        return asignadasIntroduciendoMaster;
    }

    public void setAsignadasIntroduciendoMaster(Long asignadasIntroduciendoMaster) {
        this.asignadasIntroduciendoMaster = asignadasIntroduciendoMaster;
    }

    public Long getAsignadasPendientes() {
        return asignadasPendientes;
    }

    public void setAsignadasPendientes(Long asignadasPendientes) {
        this.asignadasPendientes = asignadasPendientes;
    }

    public Long getAsignadasPendientesGrado() {
        return asignadasPendientesGrado;
    }

    public void setAsignadasPendientesGrado(Long asignadasPendientesGrado) {
        this.asignadasPendientesGrado = asignadasPendientesGrado;
    }

    public Long getAsignadasPendientesMaster() {
        return asignadasPendientesMaster;
    }

    public void setAsignadasPendientesMaster(Long asignadasPendientesMaster) {
        this.asignadasPendientesMaster = asignadasPendientesMaster;
    }

    public Long getAsignadasTraduccion() {
        return asignadasTraduccion;
    }

    public void setAsignadasTraduccion(Long asignadasTraduccion) {
        this.asignadasTraduccion = asignadasTraduccion;
    }

    public Long getAsignadasTraduccionGrado() {
        return asignadasTraduccionGrado;
    }

    public void setAsignadasTraduccionGrado(Long asignadasTraduccionGrado) {
        this.asignadasTraduccionGrado = asignadasTraduccionGrado;
    }

    public Long getAsignadasTraduccionMaster() {
        return asignadasTraduccionMaster;
    }

    public void setAsignadasTraduccionMaster(Long asignadasTraduccionMaster) {
        this.asignadasTraduccionMaster = asignadasTraduccionMaster;
    }

    public Long getAsignadasFinalizadas() {
        return asignadasFinalizadas;
    }

    public void setAsignadasFinalizadas(Long asignadasFinalizadas) {
        this.asignadasFinalizadas = asignadasFinalizadas;
    }

    public Long getAsignadasFinalizadasGrado() {
        return asignadasFinalizadasGrado;
    }

    public void setAsignadasFinalizadasGrado(Long asignadasFinalizadasGrado) {
        this.asignadasFinalizadasGrado = asignadasFinalizadasGrado;
    }

    public Long getAsignadasFinalizadasMaster() {
        return asignadasFinalizadasMaster;
    }

    public void setAsignadasFinalizadasMaster(Long asignadasFinalizadasMaster) {
        this.asignadasFinalizadasMaster = asignadasFinalizadasMaster;
    }

    public Long getNoAsignadasInactivas() {
        return noAsignadasInactivas;
    }

    public void setNoAsignadasInactivas(Long noAsignadasInactivas) {
        this.noAsignadasInactivas = noAsignadasInactivas;
    }

    public Long getNoAsignadasInactivasGrado() {
        return noAsignadasInactivasGrado;
    }

    public void setNoAsignadasInactivasGrado(Long noAsignadasInactivasGrado) {
        this.noAsignadasInactivasGrado = noAsignadasInactivasGrado;
    }

    public Long getNoAsignadasInactivasMaster() {
        return noAsignadasInactivasMaster;
    }

    public void setNoAsignadasInactivasMaster(Long noAsignadasInactivasMaster) {
        this.noAsignadasInactivasMaster = noAsignadasInactivasMaster;
    }

    public Long getNoAsignadasIntroduciendo() {
        return noAsignadasIntroduciendo;
    }

    public void setNoAsignadasIntroduciendo(Long noAsignadasIntroduciendo) {
        this.noAsignadasIntroduciendo = noAsignadasIntroduciendo;
    }

    public Long getNoAsignadasIntroduciendoGrado() {
        return noAsignadasIntroduciendoGrado;
    }

    public void setNoAsignadasIntroduciendoGrado(Long noAsignadasIntroduciendoGrado) {
        this.noAsignadasIntroduciendoGrado = noAsignadasIntroduciendoGrado;
    }

    public Long getNoAsignadasIntroduciendoMaster() {
        return noAsignadasIntroduciendoMaster;
    }

    public void setNoAsignadasIntroduciendoMaster(Long noAsignadasIntroduciendoMaster) {
        this.noAsignadasIntroduciendoMaster = noAsignadasIntroduciendoMaster;
    }

    public Long getNoAsignadasPendientes() {
        return noAsignadasPendientes;
    }

    public void setNoAsignadasPendientes(Long noAsignadasPendientes) {
        this.noAsignadasPendientes = noAsignadasPendientes;
    }

    public Long getNoAsignadasPendientesGrado() {
        return noAsignadasPendientesGrado;
    }

    public void setNoAsignadasPendientesGrado(Long noAsignadasPendientesGrado) {
        this.noAsignadasPendientesGrado = noAsignadasPendientesGrado;
    }

    public Long getNoAsignadasPendientesMaster() {
        return noAsignadasPendientesMaster;
    }

    public void setNoAsignadasPendientesMaster(Long noAsignadasPendientesMaster) {
        this.noAsignadasPendientesMaster = noAsignadasPendientesMaster;
    }

    public Long getNoAsignadasTraduccion() {
        return noAsignadasTraduccion;
    }

    public void setNoAsignadasTraduccion(Long noAsignadasTraduccion) {
        this.noAsignadasTraduccion = noAsignadasTraduccion;
    }

    public Long getNoAsignadasTraduccionGrado() {
        return noAsignadasTraduccionGrado;
    }

    public void setNoAsignadasTraduccionGrado(Long noAsignadasTraduccionGrado) {
        this.noAsignadasTraduccionGrado = noAsignadasTraduccionGrado;
    }

    public Long getNoAsignadasTraduccionMaster() {
        return noAsignadasTraduccionMaster;
    }

    public void setNoAsignadasTraduccionMaster(Long noAsignadasTraduccionMaster) {
        this.noAsignadasTraduccionMaster = noAsignadasTraduccionMaster;
    }

    public Long getNoAsignadasFinalizadas() {
        return noAsignadasFinalizadas;
    }

    public void setNoAsignadasFinalizadas(Long noAsignadasFinalizadas) {
        this.noAsignadasFinalizadas = noAsignadasFinalizadas;
    }

    public Long getNoAsignadasFinalizadasGrado() {
        return noAsignadasFinalizadasGrado;
    }

    public void setNoAsignadasFinalizadasGrado(Long noAsignadasFinalizadasGrado) {
        this.noAsignadasFinalizadasGrado = noAsignadasFinalizadasGrado;
    }

    public Long getNoAsignadasFinalizadasMaster() {
        return noAsignadasFinalizadasMaster;
    }

    public void setNoAsignadasFinalizadasMaster(Long noAsignadasFinalizadasMaster) {
        this.noAsignadasFinalizadasMaster = noAsignadasFinalizadasMaster;
    }

    public Long getAsignadasFinalizadasDeficiencias()
    {
        return asignadasFinalizadasDeficiencias;
    }

    public void setAsignadasFinalizadasDeficiencias(Long asignadasFinalizadasDeficiencias)
    {
        this.asignadasFinalizadasDeficiencias = asignadasFinalizadasDeficiencias;
    }

    public Long getAsignadasFinalizadasDeficienciasGrado()
    {
        return asignadasFinalizadasDeficienciasGrado;
    }

    public void setAsignadasFinalizadasDeficienciasGrado(Long asignadasFinalizadasDeficienciasGrado)
    {
        this.asignadasFinalizadasDeficienciasGrado = asignadasFinalizadasDeficienciasGrado;
    }

    public Long getAsignadasFinalizadasDeficienciasMaster()
    {
        return asignadasFinalizadasDeficienciasMaster;
    }

    public void setAsignadasFinalizadasDeficienciasMaster(
            Long asignadasFinalizadasDeficienciasMaster)
    {
        this.asignadasFinalizadasDeficienciasMaster = asignadasFinalizadasDeficienciasMaster;
    }

    public Long getNoAsignadasFinalizadasDeficiencias()
    {
        return noAsignadasFinalizadasDeficiencias;
    }

    public void setNoAsignadasFinalizadasDeficiencias(Long noAsignadasFinalizadasDeficiencias)
    {
        this.noAsignadasFinalizadasDeficiencias = noAsignadasFinalizadasDeficiencias;
    }

    public Long getNoAsignadasFinalizadasDeficienciasGrado()
    {
        return noAsignadasFinalizadasDeficienciasGrado;
    }

    public void setNoAsignadasFinalizadasDeficienciasGrado(
            Long noAsignadasFinalizadasDeficienciasGrado)
    {
        this.noAsignadasFinalizadasDeficienciasGrado = noAsignadasFinalizadasDeficienciasGrado;
    }

    public Long getNoAsignadasFinalizadasDeficienciasMaster()
    {
        return noAsignadasFinalizadasDeficienciasMaster;
    }

    public void setNoAsignadasFinalizadasDeficienciasMaster(
            Long noAsignadasFinalizadasDeficienciasMaster)
    {
        this.noAsignadasFinalizadasDeficienciasMaster = noAsignadasFinalizadasDeficienciasMaster;
    }

    public Boolean getUsuarioSLT() {
        return usuarioSLT;
    }

    public void setUsuarioSLT(Boolean usuarioSLT) {
        this.usuarioSLT = usuarioSLT;
    }
}
