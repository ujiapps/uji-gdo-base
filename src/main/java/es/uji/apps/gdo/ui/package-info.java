@XmlJavaTypeAdapters(value = { @XmlJavaTypeAdapter(value = ExtJSDateAdapter.class, type = java.util.Date.class) })
package es.uji.apps.gdo.ui;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import es.uji.commons.db.ExtJSDateAdapter;

