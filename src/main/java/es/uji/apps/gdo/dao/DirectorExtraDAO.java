package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.DirectorExtra;
import es.uji.commons.db.BaseDAO;

public interface DirectorExtraDAO extends BaseDAO
{
    public List<DirectorExtra> getDirectoresExtra();
}
