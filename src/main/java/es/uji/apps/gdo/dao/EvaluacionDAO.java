package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Evaluacion;
import es.uji.commons.db.BaseDAO;

public interface EvaluacionDAO extends BaseDAO
{
    public List<Evaluacion> getEvaluacionesByAsignaturaId(Long asignaturaId);
    
    public Evaluacion getEvaluacion(Long id);
}
