package es.uji.apps.gdo.dao;

import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.QTitulacion;
import es.uji.apps.gdo.model.QTitulacionAsignatura;
import es.uji.apps.gdo.model.QTitulacionPersona;
import es.uji.apps.gdo.model.Titulacion;
import es.uji.apps.gdo.model.TitulacionPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TitulacionDAODatabaseImpl extends BaseDAODatabaseImpl implements TitulacionDAO
{
    @Override
    public List<Titulacion> getTitulaciones()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTitulacion titulacion = QTitulacion.titulacion;

        query.from(titulacion);

        return query.orderBy(titulacion.nombre.asc()).list(titulacion);
    }

    @Override
    public List<TitulacionPersona> getTitulacionesByPersona(long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTitulacionPersona extTitulacionesPersonas = QTitulacionPersona.titulacionPersona;

        query.from(extTitulacionesPersonas).where(
                extTitulacionesPersonas.id.personaId.eq(personaId));

        return query.orderBy(extTitulacionesPersonas.nombre.asc()).list(extTitulacionesPersonas);
    }

    @Override
    public Titulacion getTitulacionByAsignaturaId(Long asignaturaId)
            throws AsignaturaNoAsignadaATitulacion
    {
        List<Asignatura> listaAsignaturasTodas = get(Asignatura.class, asignaturaId);

        if (listaAsignaturasTodas != null && listaAsignaturasTodas.size() > 0)
        {
            String codAsignatura = listaAsignaturasTodas.get(0).getGdoExtAsiToda().getId();

            JPAQuery query = new JPAQuery(entityManager);

            QTitulacionAsignatura extTitulacionesAsignaturas = QTitulacionAsignatura.titulacionAsignatura;

            query.from(extTitulacionesAsignaturas).where(
                    extTitulacionesAsignaturas.id.codAsignatura.eq(codAsignatura));

            Long estudioId = query.list(extTitulacionesAsignaturas).get(0).getId().getEstudioId();

            if (estudioId == null)
            {
                throw new AsignaturaNoAsignadaATitulacion();
            }

            return get(Titulacion.class, estudioId).get(0);
        }

        return null;
    }

    @Override
    public List<Long> getCursosByTitulacionId(String titulacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTitulacionAsignatura cursos = QTitulacionAsignatura.titulacionAsignatura;

        query.from(cursos).where(cursos.id.estudioId.eq(Long.parseLong(titulacionId)))
                .orderBy(cursos.cursoId.asc());

        List<Long> lista = query.list(cursos.cursoId);
        eliminaDuplicados(lista);

        return lista;
    }

    @Override
    public List<Long> getCursosByTitulacionId(String titulacionId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTitulacionPersona cursos = QTitulacionPersona.titulacionPersona;

        query.from(cursos)
                .where(cursos.id.estudioId.eq(Long.parseLong(titulacionId)).and(
                        cursos.id.personaId.eq(personaId))).orderBy(cursos.cursoId.asc());

        List<Long> lista = query.list(cursos.cursoId);
        eliminaDuplicados(lista);

        return lista;
    }

    private void eliminaDuplicados(List<Long> lista)
    {
        HashSet<Long> hs = new HashSet<Long>();
        hs.addAll(lista);
        lista.clear();
        lista.addAll(hs);
    }

}