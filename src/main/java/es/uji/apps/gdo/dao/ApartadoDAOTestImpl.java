package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Apartado;
import es.uji.apps.gdo.model.CursoAcademico;
import es.uji.apps.gdo.model.domains.TipoApartado;
import es.uji.commons.db.BaseDAOTestImpl;

public class ApartadoDAOTestImpl extends BaseDAOTestImpl implements ApartadoDAO
{
    private Apartado buildApartadoTest(Long apartadoId, TipoApartado tipoApartado)
    {
        CursoAcademico curso = new CursoAcademico();
        curso.setCursoAca(2010L);

        Apartado apartado = new Apartado();
        apartado.setId(apartadoId);
        apartado.setGdoCursosAcademico(curso);
        apartado.setNombreCa("Apartat " + apartadoId);
        apartado.setNombreEs("Apartado " + apartadoId);
        apartado.setNombreUk("Section " + apartadoId);
        apartado.setTextoAyuda("Text d'ayuda de l'apartat " + apartadoId);
        apartado.setOrden(new Integer(0));
        apartado.setTipo(tipoApartado.name());

        return apartado;
    }

    @Override
    public List<Apartado> getApartadosByAsignaturaAndTipo(Long asignaturaId,
            TipoApartado tipoApartado)
    {
        List<Apartado> result = new ArrayList<Apartado>();

        for (int i = 0; i < 3; i++)
        {
            result.add(buildApartadoTest((long) i, tipoApartado));
        }

        return result;
    }

    @Override
    public List<Apartado> getApartadosByTipoParaActivarAsignatura(TipoApartado tipoApartado,
            String tipoEstudio)
    {
        List<Apartado> result = new ArrayList<Apartado>();

        for (int i = 0; i < 3; i++)
        {
            result.add(buildApartadoTest((long) i, tipoApartado));
        }

        return result;
    }

    @Override
    public String getApartadosObligatoriosVaciosByAsignatura(Long asignaturaId)
    {
        // TODO Auto-generated method stub
        return null;
    }

}