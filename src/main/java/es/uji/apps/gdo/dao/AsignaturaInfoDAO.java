package es.uji.apps.gdo.dao;


import es.uji.apps.gdo.model.AsignaturaInfo;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface AsignaturaInfoDAO extends BaseDAO
{
    List<AsignaturaInfo> getAsignaturasConFiltros(Long personaId, String tipoEstudio, String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia);
    Long contarAsignaturasConFiltros(Long personaId, String tipoEstudio, String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia);
}