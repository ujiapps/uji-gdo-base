package es.uji.apps.gdo.dao;

import es.uji.apps.gdo.model.Idioma;
import es.uji.apps.gdo.model.IdiomaAsignaturaVerifica;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface IdiomaAsignaturaVerificaDAO extends BaseDAO
{
    List<Idioma> getIdiomasByAsignatura(String codigoAsignatura);
}
