package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QResultado;
import es.uji.apps.gdo.model.Resultado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResultadoDAODatabaseImpl extends BaseDAODatabaseImpl implements ResultadoDAO
{
    @Override
    public Resultado getResultado(Long resultadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QResultado asiResultados = QResultado.resultado;
        QAsignatura asignaturas = new QAsignatura("aa");

        query.from(asiResultados).join(asiResultados.gdoAsignatura, asignaturas).fetch()
                .join(asignaturas.gdoExtAsiToda).fetch().where(asiResultados.id.eq(resultadoId));

        List<Resultado> listaResultados = query.list(asiResultados);

        if (listaResultados != null && listaResultados.size() > 0)
        {
            return listaResultados.get(0);
        }
        else
        {
            return new Resultado();
        }
    }

    @Override
    public List<Resultado> getResultadosByAsignatura(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        QResultado asiResultados = QResultado.resultado;
        QAsignatura asignaturas = new QAsignatura("aa");
        
        query.from(asiResultados).join(asiResultados.gdoAsignatura, asignaturas).fetch()
            .join(asignaturas.gdoExtAsiToda).fetch().where(asignaturas.id.eq(asignaturaId));
        
        return query.list(asiResultados);
    }

}
