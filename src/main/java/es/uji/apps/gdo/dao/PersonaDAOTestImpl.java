package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Persona;
import es.uji.commons.db.BaseDAOTestImpl;
import es.uji.commons.rest.json.lookup.LookupItem;

public class PersonaDAOTestImpl extends BaseDAOTestImpl implements PersonaDAO
{
    private Persona getPersonaTest(Long personaId)
    {
        Persona persona = new Persona();
        persona.setId(personaId);
        persona.setNombre("Nombre" + personaId);
        persona.setApellido1("apellido1-" + personaId);
        persona.setApellido2("apellido2-" + personaId);

        return persona;
    }

    @Override
    public Persona getPersona(Long personaId)
    {
        return getPersonaTest(108L);
    }

    @Override
    public List<Persona> getPersonas()
    {
        List<Persona> listaPersonas = new ArrayList<Persona>();

        for (int i = 0; i < 6; i++)
        {
            listaPersonas.add(getPersonaTest((long) i));
        }

        return listaPersonas;
    }

    @Override
    public List<LookupItem> search(String query)
    {
        return new ArrayList<LookupItem>();
    }

    @Override
    public Persona getPersonaByLogin(String login)
    {
        return new Persona();
    }
}
