package es.uji.apps.gdo.dao;

import java.util.Date;
import java.util.List;

import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Persona;
import es.uji.commons.db.BaseDAO;

public interface AsignaturaDAO extends BaseDAO
{
    public List<Asignatura> getAsignaturasByTitulacion(Long titulacionId);

    public List<String> getAsignaturasByTitulacionYCurso(Long titulacionId, Long curso);

    public Asignatura getAsignaturaByIdConRelaciones(Long asignaturaId);

    public Asignatura getAsignaturaByIdConRelaciones(String codigoAsignatura);

    public Asignatura getAsignaturaByAsignaturaAndCursoAca(String codigoAsignatura, Long cursoAca);

    public void insertIdioma(long asignaturaId, long idiomaId);

    public List<Asignatura> getAsignaturasConFiltros(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia);

    public Long contarAsignaturasConFiltros(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia);

    public List<Long> getIdAsignaturasDeUnaPersona(Long personaId, Long cursoAca);

    public Long apartadosObligatoriosTotales(Long asignaturaId);

    public Long apartadosObligatoriosLlenos(Long asignaturaId, String idioma);

    public Long getNumeroDeComentarios(Long asignaturaId);

    public Long getCursoAsignatura(String codigoAsignatura);

    public List<Asignatura> getAsignaturasRelacionadas(Asignatura asignaturaOriginal);

    public boolean isNuevaImplantacion(String codigoAsignatura);

    public Long cuentaAsignaturas(Long cursoAca);

    public List<Asignatura> getAsignaturasByCursoAcademico(Long cursoAca);

    public List<Persona> getCoordinadoresDeLaAsignatura(Asignatura asignatura);

    public List<Persona> getResponsablesAsignatura(Asignatura asignatura);

    Asignatura getAsignaturaByIdSinRelaciones(long asignaturaId);
}