package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QVerificaActividad;
import es.uji.apps.gdo.model.VerificaActividad;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VerificaActividadDAODatabaseImpl extends BaseDAODatabaseImpl implements
        VerificaActividadDAO
{

    @Override
    public List<VerificaActividad> getVerificaActividadByAsignatura(String codigoAsignatura,
            Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QVerificaActividad verificaActividad = QVerificaActividad.verificaActividad;

        query.from(verificaActividad).where(
                verificaActividad.codAsignatura.eq(codigoAsignatura).and(
                        verificaActividad.cursoAcaIni.loe(cursoAca)));

        return query.list(verificaActividad);
    }

}
