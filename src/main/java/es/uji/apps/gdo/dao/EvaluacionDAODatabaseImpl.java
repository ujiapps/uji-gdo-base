package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Evaluacion;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QEvaluacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EvaluacionDAODatabaseImpl extends BaseDAODatabaseImpl implements EvaluacionDAO
{
    @Autowired
    public AsignaturaDAO asignaturaDAO;

    @Override
    public List<Evaluacion> getEvaluacionesByAsignaturaId(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEvaluacion asiEvaluaciones = QEvaluacion.evaluacion;
        QAsignatura asignaturas = new QAsignatura("aa");

        query.from(asiEvaluaciones).join(asiEvaluaciones.gdoAsignatura, asignaturas).fetch()
                .join(asignaturas.gdoExtAsiToda).fetch().where(asignaturas.id.eq(asignaturaId))
                .orderBy(asiEvaluaciones.orden.asc());

        return query.list(asiEvaluaciones);
    }

    @Override
    public Evaluacion getEvaluacion(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEvaluacion asiEvaluacion = QEvaluacion.evaluacion;

        query.from(asiEvaluacion).where(asiEvaluacion.id.eq(id));

        return query.list(asiEvaluacion).get(0);
    }

}