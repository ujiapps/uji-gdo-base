package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Persona;
import es.uji.apps.gdo.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaDAO
{

    @Override
    public Persona getPersona(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        query.from(personas).where(personas.id.eq(personaId));

        List<Persona> listaPersonas = query.list(personas);

        if (listaPersonas != null && listaPersonas.size() > 0)
        {
            return listaPersonas.get(0);
        }
        else
        {
            return new Persona();
        }
    }

    @Override
    public List<Persona> getPersonas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        query.from(personas).orderBy(personas.apellido1.asc(), personas.apellido2.asc(),
                personas.nombre.asc());

        return query.list(personas);
    }

    @Override
    public List<LookupItem> search(String query)
    {
        JPAQuery queryPersonas = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        List<LookupItem> result = new ArrayList<>();

        if (query != null && !query.isEmpty())
        {
            queryPersonas.from(personas).where(
                    personas.id.stringValue().concat(" ").concat(personas.apellido1).concat(" ")
                            .concat(personas.apellido2).concat(", ").concat(personas.nombre)
                            .concat(" ").lower().like("%" + query.toLowerCase() + "%"));

            List<Persona> listaPersonas = queryPersonas.list(personas);

            for (Persona persona : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(persona.getId()));
                lookupItem.setNombre(persona.getApellido1() + " " + persona.getApellido2() + ", "
                        + persona.getNombre());

                result.add(lookupItem);
            }
        }

        return result;
    }

    @Override
    public Persona getPersonaByLogin(String login)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        query.from(personas).where(personas.login.eq(login));

        List<Persona> listaPersonas = query.list(personas);

        if (listaPersonas != null && listaPersonas.size() > 0)
        {
            return listaPersonas.get(0);
        }
        else
        {
            return new Persona();
        }
    }
}