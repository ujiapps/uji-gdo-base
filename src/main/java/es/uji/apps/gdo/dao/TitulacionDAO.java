package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.model.Titulacion;
import es.uji.apps.gdo.model.TitulacionPersona;
import es.uji.commons.db.BaseDAO;

public interface TitulacionDAO extends BaseDAO
{
    public List<Titulacion> getTitulaciones();
    
    public List<TitulacionPersona> getTitulacionesByPersona(long personaId);

    public Titulacion getTitulacionByAsignaturaId(Long asignaturaId) throws AsignaturaNoAsignadaATitulacion;

    public List<Long> getCursosByTitulacionId(String titulacionId);

    public List<Long> getCursosByTitulacionId(String titulacionId, Long personaId);
}