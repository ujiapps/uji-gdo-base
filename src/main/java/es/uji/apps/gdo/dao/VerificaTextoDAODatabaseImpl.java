package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QVerificaTexto;
import es.uji.apps.gdo.model.VerificaTexto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VerificaTextoDAODatabaseImpl extends BaseDAODatabaseImpl implements VerificaTextoDAO
{
    @Override
    public List<VerificaTexto> getContenidosByAsignatura(String codigoAsignatura)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        QVerificaTexto verificaTexto = QVerificaTexto.verificaTexto;
        
        query.from(verificaTexto).where(verificaTexto.codAsignatura.eq(codigoAsignatura));
        
        List<VerificaTexto> lista = query.list(verificaTexto);
   
        if (lista.isEmpty())
        {
            lista.add(new VerificaTexto());
        }
        
        return lista;
    }
}