package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.Persona;
import es.uji.commons.db.BaseDAOTestImpl;

public class AsignaturaDAOTestImpl extends BaseDAOTestImpl implements AsignaturaDAO
{
    private List<Asignatura> buildListaAsignaturas()
    {
        List<Asignatura> result = new ArrayList<Asignatura>();

        for (int i = 0; i < 10; i++)
        {
            Asignatura asignatura = new Asignatura();
            asignatura.setId((long) i);
            asignatura.setEstado("Estado " + i);
            result.add(asignatura);
        }

        return result;
    }

    @Override
    public List<Asignatura> getAsignaturasByTitulacion(Long titulacionId)
    {
        return buildListaAsignaturas();
    }

    @Override
    public Asignatura getAsignaturaByIdConRelaciones(Long asignaturaId)
    {
        Asignatura asignatura = new Asignatura();
        asignatura.setId(asignaturaId);
        asignatura.setEstado("Estado " + asignaturaId);

        return asignatura;
    }

    @Override
    public Asignatura getAsignaturaByIdConRelaciones(String codigoAsignatura)
    {
        Asignatura asignatura = new Asignatura();
        asignatura.setId(123L);
        asignatura.setEstado("Estado " + codigoAsignatura);

        return asignatura;
    }

    @Override
    public void insertIdioma(long asignaturaId, long idiomaId)
    {
    }

    @Override
    public List<Asignatura> getAsignaturasConFiltros(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia)
    {
        return null;
    }

    @Override
    public Long contarAsignaturasConFiltros(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Long> getIdAsignaturasDeUnaPersona(Long personaId, Long cursoAca)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Long apartadosObligatoriosTotales(Long asignaturaId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Long apartadosObligatoriosLlenos(Long asignaturaId, String idioma)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Long getNumeroDeComentarios(Long asignaturaId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Long getCursoAsignatura(String codigoAsignatura)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Asignatura> getAsignaturasRelacionadas(Asignatura asignaturaOriginal)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isNuevaImplantacion(String codigoAsignatura)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Long cuentaAsignaturas(Long cursoAca)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Asignatura> getAsignaturasByCursoAcademico(Long cursoAca)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getAsignaturasByTitulacionYCurso(Long titulacionId, Long curso)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Asignatura getAsignaturaByAsignaturaAndCursoAca(String codigoAsignatura, Long cursoAca)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Persona> getCoordinadoresDeLaAsignatura(Asignatura asignatura)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Persona> getResponsablesAsignatura(Asignatura asignatura)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Asignatura getAsignaturaByIdSinRelaciones(long asignaturaId)
    {
        // TODO Auto-generated method stub
        return null;
    }

}