package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Competencia;
import es.uji.commons.db.BaseDAO;

public interface CompetenciaDAO extends BaseDAO
{
    public Competencia getCompetencia(Long competenciaId);

    public List<Competencia> getCompetenciasByAsignatura(Long asignaturaId);
}
