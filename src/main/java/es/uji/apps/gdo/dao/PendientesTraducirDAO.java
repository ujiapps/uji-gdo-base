package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.PendientesTraducir;
import es.uji.commons.db.BaseDAO;

public interface PendientesTraducirDAO extends BaseDAO
{
    List<PendientesTraducir> getPendientes(Long cursoAca, String tipoEstudio);
}
