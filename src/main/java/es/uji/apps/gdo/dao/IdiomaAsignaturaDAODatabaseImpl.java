package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Idioma;
import es.uji.apps.gdo.model.IdiomaAsignatura;
import es.uji.apps.gdo.model.QIdioma;
import es.uji.apps.gdo.model.QIdiomaAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class IdiomaAsignaturaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        IdiomaAsignaturaDAO
{
    @Override
    public List<Idioma> getIdiomas()
    {
        return get(Idioma.class);
    }

    @Override
    public List<Idioma> getIdiomasByAsignaturaIdNoAsignados(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subQuery = new JPAQuery(entityManager);

        QIdioma idiomas = QIdioma.idioma;
        QIdiomaAsignatura asiIdiomas = QIdiomaAsignatura.idiomaAsignatura;

        List<Long> listaIdiomas = subQuery.from(asiIdiomas)
                .where(asiIdiomas.gdoAsignatura.id.eq(asignaturaId)).list(asiIdiomas.gdoIdioma.id);

        if (listaIdiomas.isEmpty())
        {
            query.from(idiomas).orderBy(idiomas.nombreCa.asc());
        }
        else
        {
            query.from(idiomas).where(idiomas.id.notIn(listaIdiomas))
                    .orderBy(idiomas.nombreCa.asc());
        }

        return query.list(idiomas);
    }

    @Override
    public List<IdiomaAsignatura> getIdiomasByAsignaturaId(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QIdiomaAsignatura asiIdiomas = QIdiomaAsignatura.idiomaAsignatura;
        QIdioma idiomas = QIdioma.idioma;

        query.from(asiIdiomas).join(asiIdiomas.gdoIdioma, idiomas).fetch()
                .where(asiIdiomas.gdoAsignatura.id.eq(asignaturaId))
                .orderBy(idiomas.nombreCa.asc());

        return query.list(asiIdiomas);
    }

}