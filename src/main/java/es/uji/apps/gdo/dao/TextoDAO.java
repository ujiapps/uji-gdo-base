package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Texto;
import es.uji.commons.db.BaseDAO;

public interface TextoDAO extends BaseDAO
{
    public Texto getTexto(Long textoId);

    public Texto getTextosByAsignaturaApartado(Long asignaturaId, Long apartadoId);

    public void updateTexto(Texto texto);

    public List<Texto> getTextosByAsignatura(Long asignaturaId);
}
