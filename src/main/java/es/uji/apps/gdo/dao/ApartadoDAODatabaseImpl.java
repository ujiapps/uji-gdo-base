package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Apartado;
import es.uji.apps.gdo.model.QActividadAsignatura;
import es.uji.apps.gdo.model.QApartado;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QAsignaturaPOD;
import es.uji.apps.gdo.model.QCompetencia;
import es.uji.apps.gdo.model.QEvaluacion;
import es.uji.apps.gdo.model.QIdiomaAsignatura;
import es.uji.apps.gdo.model.QTexto;
import es.uji.apps.gdo.model.Texto;
import es.uji.apps.gdo.model.domains.TipoApartado;
import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ApartadoDAODatabaseImpl extends BaseDAODatabaseImpl implements ApartadoDAO
{
    @Override
    public List<Apartado> getApartadosByAsignaturaAndTipo(Long asignaturaId,
                                                          TipoApartado tipoApartado)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QApartado apartado = QApartado.apartado;
        QTexto texto = new QTexto("t");
        QAsignatura asignatura = new QAsignatura("asi");

        if (TipoApartado.T.equals(tipoApartado))
        {
            query.from(apartado)
                    .join(apartado.gdoAsiTextos, texto)
                    .fetch()
                    .join(texto.gdoAsignatura, asignatura)
                    .fetch()
                    .where(asignatura.id.eq(asignaturaId)
                            .and(apartado.tipo.eq(tipoApartado.name())))
                    .orderBy(apartado.orden.asc());
        }
        else
        {
            query.from(apartado).where(apartado.tipo.eq(tipoApartado.name()));
        }

        return query.list(apartado);
    }

    @Override
    public List<Apartado> getApartadosByTipoParaActivarAsignatura(TipoApartado tipoApartado,
                                                                  String tipoEstudio)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QApartado apartado = QApartado.apartado;

        if (tipoEstudio.equals(TipoEstudio.M.name()))
        {
            query.from(apartado).where(apartado.tipo.eq(tipoApartado.name()));

        }
        else
        {
            query.from(apartado).where(
                    apartado.tipo.eq(tipoApartado.name()).and(apartado.id.ne(21L))
                            .and(apartado.id.ne(8L)));
        }

        return query.list(apartado);
    }

    @Override
    public String getApartadosObligatoriosVaciosByAsignatura(Long asignaturaId)
    {
        String miXML = new String();

        miXML = "<list><texto>";
        miXML += "<valorca>" + getApartadosVacios(asignaturaId, "CA") + "</valorca>"
                + "<valores>" + getApartadosVacios(asignaturaId, "ES") + "</valores>"
                + "<valoruk>" + getApartadosVacios(asignaturaId, "UK") + "</valoruk>";

        miXML += "<modificadoca>" + getApartadosModificados(asignaturaId, "CA") + "</modificadoca>"
                + "<modificadoes>" + getApartadosModificados(asignaturaId, "ES") + "</modificadoes>"
                + "<modificadouk>" + getApartadosModificados(asignaturaId, "UK") + "</modificadouk>";

        miXML = miXML + getNoHayIdiomas(asignaturaId);
        miXML = miXML + getEvaluacionesVacias(asignaturaId, "CA");
        miXML = miXML + getEvaluacionesVacias(asignaturaId, "ES");
        miXML = miXML + getEvaluacionesVacias(asignaturaId, "UK");
        miXML = miXML + getActividadesVacias(asignaturaId);
        miXML = miXML + getCompetenciasVacias(asignaturaId, "CA");
        miXML = miXML + getCompetenciasVacias(asignaturaId, "ES");
        miXML = miXML + getCompetenciasVacias(asignaturaId, "UK");

        miXML = miXML + ("</texto></list>");

        return miXML;
    }

    private String getCompetenciasVacias(Long asignaturaId, String idioma)
    {
        JPAQuery queryTotal = new JPAQuery(entityManager);
        JPAQuery queryLlenos = new JPAQuery(entityManager);
        QCompetencia competencia = QCompetencia.competencia;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(competencia.gdoAsignatura.id.eq(asignaturaId));

        queryTotal.from(competencia).where(condicionesWhere);

        Long total = queryTotal.count();

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(competencia.competenciaCa.isNotEmpty());
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(competencia.competenciaEs.isNotEmpty());
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(competencia.competenciaUk.isNotEmpty());
        }

        queryLlenos.from(competencia).where(condicionesWhere);

        Long llenos = queryLlenos.count();

        if (total.equals(llenos) && total > 0)
        {
            if (idioma.equals("CA"))
            {
                return "<competenciasca></competenciasca>";
            }
            else if (idioma.equals("ES"))
            {
                return "<competenciases></competenciases>";
            }
            else if (idioma.equals("UK"))
            {
                return "<competenciasuk></competenciasuk>";
            }
        }
        else
        {
            if (idioma.equals("CA"))
            {
                return "<competenciasca>Competències\n</competenciasca>";
            }
            else if (idioma.equals("ES"))
            {
                return "<competenciases>Competències\n</competenciases>";
            }
            else if (idioma.equals("UK"))
            {
                return "<competenciasuk>Competències\n</competenciasuk>";
            }
        }

        return "";
    }

    private String getActividadesVacias(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActividadAsignatura actividades = QActividadAsignatura.actividadAsignatura;

        query.from(actividades).where(actividades.gdoAsignatura.id.eq(asignaturaId));

        if (query.count() > 0)
        {
            return "<actividades></actividades>";
        }
        else
        {
            return "<actividades>Activitats\n</actividades>";
        }
    }

    private String getEvaluacionesVacias(Long asignaturaId, String idioma)
    {
        JPAQuery queryTotal = new JPAQuery(entityManager);
        JPAQuery queryLlenos = new JPAQuery(entityManager);
        QEvaluacion evaluaciones = QEvaluacion.evaluacion;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(evaluaciones.gdoAsignatura.id.eq(asignaturaId));

        queryTotal.from(evaluaciones).where(condicionesWhere);

        Long total = queryTotal.count();

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(evaluaciones.nombreCa.isNotEmpty());
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(evaluaciones.nombreEs.isNotEmpty());
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(evaluaciones.nombreUk.isNotEmpty());
        }

        queryLlenos.from(evaluaciones).where(condicionesWhere);

        Long llenos = queryLlenos.count();

        if (total.equals(llenos) && total > 0)
        {
            if (idioma.equals("CA"))
            {
                return "<evaluacionesca></evaluacionesca>";
            }
            else if (idioma.equals("ES"))
            {
                return "<evaluacioneses></evaluacioneses>";
            }
            else if (idioma.equals("UK"))
            {
                return "<evaluacionesuk></evaluacionesuk>";
            }
        }
        else
        {
            if (idioma.equals("CA"))
            {
                return "<evaluacionesca>Evaluacions\n</evaluacionesca>";
            }
            else if (idioma.equals("ES"))
            {
                return "<evaluacioneses>Evaluacions\n</evaluacioneses>";
            }
            else if (idioma.equals("UK"))
            {
                return "<evaluacionesuk>Evaluacions\n</evaluacionesuk>";
            }
        }

        return "";
    }

    private String getNoHayIdiomas(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIdiomaAsignatura idiomas = QIdiomaAsignatura.idiomaAsignatura;

        query.from(idiomas).where(idiomas.gdoAsignatura.id.eq(asignaturaId));

        if (query.count() > 0)
        {
            return "<idiomas></idiomas>";
        }
        else
        {
            return "<idiomas>Idiomes\n</idiomas>";
        }
    }

    private String getApartadosVacios(Long asignaturaId, String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTexto textos = QTexto.texto;
        QApartado apartado = QApartado.apartado;
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(textos.gdoAsignatura.id.eq(asignaturaId)
                .and(apartado.obligatorio.eq("S"))
                .and(apartado.tipoEstudio.eq("T").or(asignaturaPOD.tipo.eq(apartado.tipoEstudio))));

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(textos.textoCa.isNull().or(textos.textoCa.trim().isEmpty()));
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(textos.textoEs.isNull().or(textos.textoEs.trim().isEmpty()));
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(textos.textoUk.isNull().or(textos.textoUk.trim().isEmpty()));
        }

        query.from(textos).join(textos.gdoApartado, apartado).fetch()
                .join(textos.gdoAsignatura, asignatura).fetch()
                .join(asignatura.gdoExtAsiToda, asignaturaPOD).fetch().where(condicionesWhere);

        List<Texto> textosVacios = query.list(textos);

        String cadena = new String();

        for (Texto texto : textosVacios)
        {
            if (idioma.equals("CA"))
            {
                cadena = cadena + texto.getGdoApartado().getNombreCa() + "\n";
            }
            else if (idioma.equals("ES"))
            {
                cadena = cadena + texto.getGdoApartado().getNombreEs() + "\n";
            }
            else if (idioma.equals("UK"))
            {
                cadena = cadena + texto.getGdoApartado().getNombreUk() + "\n";
            }
        }

        return cadena;
    }

    private String getApartadosModificados(Long asignaturaId, String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTexto textos = QTexto.texto;
        QApartado apartado = QApartado.apartado;
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(textos.gdoAsignatura.id.eq(asignaturaId))
                .and(apartado.tipoEstudio.eq("T").or(asignaturaPOD.tipo.eq(apartado.tipoEstudio)));

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(textos.modificadoCa.isTrue());
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(textos.modificadoEs.isTrue());
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(textos.modificadoUk.isTrue());
        }

        query.from(textos).join(textos.gdoApartado, apartado).fetch()
                .join(textos.gdoAsignatura, asignatura).fetch()
                .join(asignatura.gdoExtAsiToda, asignaturaPOD).fetch().where(condicionesWhere);

        List<Texto> textosModificados = query.list(textos);

        String cadena = new String();

        for (Texto texto : textosModificados)
        {
            String data = "data-texto-id=\"" + texto.getId() + "\"";
            if (idioma.equals("CA"))
            {
                cadena = cadena + "<a href=\"#\" class=\"link-cmp\" data-idioma=\"Ca\"" + data + ">" + texto.getGdoApartado().getNombreCa() + "</a>\n";
            }
            else if (idioma.equals("ES"))
            {
                cadena = cadena + "<a href=\"#\" class=\"link-cmp\" data-idioma=\"Es\"" + data + ">" + texto.getGdoApartado().getNombreEs() + "</a>\n";
            }
            else if (idioma.equals("UK"))
            {
                cadena = cadena + "<a href=\"#\" class=\"link-cmp\" data-idioma=\"Uk\"" + data + ">" + texto.getGdoApartado().getNombreUk() + "</a>\n";
            }
        }

        return (cadena != null) ? "<![CDATA[" + cadena + "]]>" : null;
    }

}