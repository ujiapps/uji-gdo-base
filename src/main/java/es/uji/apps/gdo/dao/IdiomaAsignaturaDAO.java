package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Idioma;
import es.uji.apps.gdo.model.IdiomaAsignatura;
import es.uji.commons.db.BaseDAO;

public interface IdiomaAsignaturaDAO extends BaseDAO
{
    public List<Idioma> getIdiomasByAsignaturaIdNoAsignados(Long asignaturaId);

    public List<IdiomaAsignatura> getIdiomasByAsignaturaId(Long asignaturaId);

    public List<Idioma> getIdiomas();
}
