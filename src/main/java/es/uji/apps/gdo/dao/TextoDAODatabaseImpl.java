package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QApartado;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QTexto;
import es.uji.apps.gdo.model.Texto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TextoDAODatabaseImpl extends BaseDAODatabaseImpl implements TextoDAO
{
    @Override
    public Texto getTexto(Long textoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTexto asiTextos = QTexto.texto;
        QAsignatura asignaturas = new QAsignatura("a");

        query.from(asiTextos).join(asiTextos.gdoAsignatura, asignaturas).fetch()
                .join(asignaturas.gdoExtAsiToda).fetch().join(asiTextos.gdoApartado).fetch()
                .where(asiTextos.id.eq(textoId));

        List<Texto> listaTextos = query.list(asiTextos);

        if (listaTextos != null && listaTextos.size() > 0)
        {
            return listaTextos.get(0);
        }
        else
        {
            return new Texto();
        }
    }

    @Override
    public Texto getTextosByAsignaturaApartado(Long asignaturaId, Long apartadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTexto asiTextos = QTexto.texto;
        QAsignatura asignaturas = new QAsignatura("a");
        QApartado apartados = new QApartado("ap");

        query.from(asiTextos).join(asiTextos.gdoAsignatura, asignaturas).fetch()
                .join(asignaturas.gdoExtAsiToda).fetch().join(asiTextos.gdoApartado, apartados)
                .fetch().where(asignaturas.id.eq(asignaturaId).and(apartados.id.eq(apartadoId)));

        List<Texto> listaTextos = query.list(asiTextos);
        return !listaTextos.isEmpty() ? listaTextos.get(0) : null;
    }

    @Override
    @Transactional
    public void updateTexto(Texto textoDB)
    {

        textoDB.setGdoAsignatura(textoDB.getGdoAsignatura());
        textoDB.setGdoApartado(textoDB.getGdoApartado());

        update(textoDB);
    }

    @Override
    public List<Texto> getTextosByAsignatura(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        QTexto texto = QTexto.texto;
        
        query.from(texto).where(texto.gdoAsignatura.id.eq(asignaturaId));
        
        return query.list(texto);
    }
}
