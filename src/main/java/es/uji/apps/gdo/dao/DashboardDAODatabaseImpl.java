package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.gdo.model.*;
import es.uji.apps.gdo.model.domains.EstadoAsignatura;
import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.gdo.model.domains.TipoEstudio;
import es.uji.apps.gdo.model.ui.UIAsignatura;
import es.uji.apps.gdo.ui.ResumenEstadisticas;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DashboardDAODatabaseImpl extends BaseDAODatabaseImpl implements DashboardDAO
{

    @Override
    public ResumenEstadisticas getEstadisticas(Long personaId, Long centroId, Long titulacionId, Long cursoId)
    {
        List<UIAsignatura> asignaturas = getGuiasPersona(personaId, centroId, titulacionId, cursoId);

        ResumenEstadisticas resumenEstadisticas = new ResumenEstadisticas();
        resumenEstadisticas.setAsignadasInactivas(cuentaGuias(asignaturas, EstadoAsignatura.I.name(), null, true, null));
        resumenEstadisticas.setAsignadasInactivasGrado(cuentaGuias(asignaturas, EstadoAsignatura.I.name(), TipoEstudio.G, true, null));
        resumenEstadisticas.setAsignadasInactivasMaster(cuentaGuias(asignaturas, EstadoAsignatura.I.name(), TipoEstudio.M, true, null));

        resumenEstadisticas.setAsignadasIntroduciendo(cuentaGuias(asignaturas, EstadoAsignatura.D.name(), null, true, null));
        resumenEstadisticas.setAsignadasIntroduciendoGrado(cuentaGuias(asignaturas, EstadoAsignatura.D.name(), TipoEstudio.G, true, null));
        resumenEstadisticas.setAsignadasIntroduciendoMaster(cuentaGuias(asignaturas, EstadoAsignatura.D.name(), TipoEstudio.M, true, null));

        resumenEstadisticas.setAsignadasPendientes(cuentaGuias(asignaturas, EstadoAsignatura.V.name(), null, true, null));
        resumenEstadisticas.setAsignadasPendientesGrado(cuentaGuias(asignaturas, EstadoAsignatura.V.name(), TipoEstudio.G, true, null));
        resumenEstadisticas.setAsignadasPendientesMaster(cuentaGuias(asignaturas, EstadoAsignatura.V.name(), TipoEstudio.M, true, null));

        resumenEstadisticas.setAsignadasTraduccion(cuentaGuias(asignaturas, null, null, true, "S"));
        resumenEstadisticas.setAsignadasTraduccionGrado(cuentaGuias(asignaturas, null, TipoEstudio.G, true, "S"));
        resumenEstadisticas.setAsignadasTraduccionMaster(cuentaGuias(asignaturas, null, TipoEstudio.M, true, "S"));

        resumenEstadisticas.setAsignadasFinalizadas(cuentaGuias(asignaturas, EstadoAsignatura.F.name(), null, true, null));
        resumenEstadisticas.setAsignadasFinalizadasGrado(cuentaGuias(asignaturas, EstadoAsignatura.F.name(), TipoEstudio.G, true, null));
        resumenEstadisticas.setAsignadasFinalizadasMaster(cuentaGuias(asignaturas, EstadoAsignatura.F.name(), TipoEstudio.M, true, null));

        resumenEstadisticas.setAsignadasFinalizadasDeficiencias(cuentaGuias(asignaturas, EstadoAsignatura.FD.name(), null, true, null));
        resumenEstadisticas.setAsignadasFinalizadasDeficienciasGrado(cuentaGuias(asignaturas, EstadoAsignatura.FD.name(), TipoEstudio.G, true, null));
        resumenEstadisticas.setAsignadasFinalizadasDeficienciasMaster(cuentaGuias(asignaturas, EstadoAsignatura.FD.name(), TipoEstudio.M, true, null));

        resumenEstadisticas.setNoAsignadasInactivas(cuentaGuias(asignaturas, EstadoAsignatura.I.name(), null, false, null));
        resumenEstadisticas.setNoAsignadasInactivasGrado(cuentaGuias(asignaturas, EstadoAsignatura.I.name(), TipoEstudio.G, false, null));
        resumenEstadisticas.setNoAsignadasInactivasMaster(cuentaGuias(asignaturas, EstadoAsignatura.I.name(), TipoEstudio.M, false, null));

        resumenEstadisticas.setNoAsignadasIntroduciendo(cuentaGuias(asignaturas, EstadoAsignatura.D.name(), null, false, null));
        resumenEstadisticas.setNoAsignadasIntroduciendoGrado(cuentaGuias(asignaturas, EstadoAsignatura.D.name(), TipoEstudio.G, false, null));
        resumenEstadisticas.setNoAsignadasIntroduciendoMaster(cuentaGuias(asignaturas, EstadoAsignatura.D.name(), TipoEstudio.M, false, null));

        resumenEstadisticas.setNoAsignadasPendientes(cuentaGuias(asignaturas, EstadoAsignatura.V.name(), null, false, null));
        resumenEstadisticas.setNoAsignadasPendientesGrado(cuentaGuias(asignaturas, EstadoAsignatura.V.name(), TipoEstudio.G, false, null));
        resumenEstadisticas.setNoAsignadasPendientesMaster(cuentaGuias(asignaturas, EstadoAsignatura.V.name(), TipoEstudio.M, false, null));

        resumenEstadisticas.setNoAsignadasTraduccion(cuentaGuias(asignaturas, null, null, false, "S"));
        resumenEstadisticas.setNoAsignadasTraduccionGrado(cuentaGuias(asignaturas, null, TipoEstudio.G, false, "S"));
        resumenEstadisticas.setNoAsignadasTraduccionMaster(cuentaGuias(asignaturas, null, TipoEstudio.M, false,  "S"));

        resumenEstadisticas.setNoAsignadasFinalizadas(cuentaGuias(asignaturas, EstadoAsignatura.F.name(), null, false, null));
        resumenEstadisticas.setNoAsignadasFinalizadasGrado(cuentaGuias(asignaturas, EstadoAsignatura.F.name(), TipoEstudio.G, false, null));
        resumenEstadisticas.setNoAsignadasFinalizadasMaster(cuentaGuias(asignaturas, EstadoAsignatura.F.name(), TipoEstudio.M, false, null));

        resumenEstadisticas.setNoAsignadasFinalizadasDeficiencias(cuentaGuias(asignaturas, EstadoAsignatura.FD.name(), null, false, null));
        resumenEstadisticas.setNoAsignadasFinalizadasDeficienciasGrado(cuentaGuias(asignaturas, EstadoAsignatura.FD.name(), TipoEstudio.G, false, null));
        resumenEstadisticas.setNoAsignadasFinalizadasDeficienciasMaster(cuentaGuias(asignaturas, EstadoAsignatura.FD.name(), TipoEstudio.M, false, null));

        resumenEstadisticas.setUsuarioSLT(Persona.isSLT(personaId));

        return resumenEstadisticas;
    }

    private List<UIAsignatura> getGuiasPersona(Long personaId, Long centroId, Long titulacionId, Long cursoId)
    {
        Long cursoActivo = CursoAcademico.getCursoActivo().getCursoAca();
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPersona asignaturaPersona = QAsignaturaPersona.asignaturaPersona;
        QTitulacionAsignatura tituAsignatura = QTitulacionAsignatura.titulacionAsignatura;
        QTitulacion titulacion = QTitulacion.titulacion;

        JPAQuery queryGuias = new JPAQuery(entityManager);

        queryGuias.from(asignatura, asignaturaPersona, tituAsignatura, titulacion).where(
                asignatura.gdoCursosAcademico.cursoAca.eq(cursoActivo)
                        .and(asignatura.id.eq(asignaturaPersona.asignaturaId))
                        .and(asignaturaPersona.cursoAca.eq(cursoActivo))
                        .and(asignaturaPersona.personaId.eq(personaId))
                        .and(asignatura.gdoExtAsiToda.id.eq(tituAsignatura.id.codAsignatura))
                        .and(tituAsignatura.id.estudioId.eq(titulacion.id)));

        if (centroId != null)
        {
            queryGuias.where(titulacion.centroId.eq(centroId));
        }

        if (titulacionId != null)
        {
            queryGuias.where(tituAsignatura.id.estudioId.eq(titulacionId));
        }

        if (cursoId != null && cursoId != 0L)
        {
            queryGuias.where(tituAsignatura.cursoId.eq(cursoId));
        }

        List<Tuple> res = queryGuias.distinct().list(new QTuple(
                asignatura.id,
                asignatura.estado,
                titulacion.id,
                titulacion.tipo,
                asignaturaPersona.personaId,
                asignaturaPersona.tipoAcceso,
                asignatura.enSlt));

        return res.stream().map(t -> creaUIAsignatura(t)).collect(Collectors.toList());
    }

    private Long cuentaGuias(List<UIAsignatura> asignaturas, String estado, TipoEstudio tipoEstudio, Boolean asignada, String enSlt)
    {
        String tipoEstudioStr = (tipoEstudio != null) ? tipoEstudio.name() : null;
        return asignaturas.stream().filter(a -> a.filtra(asignada, estado, tipoEstudioStr, enSlt)).map(a -> a.getAsignaturaId()).distinct().count();
    }

    private UIAsignatura creaUIAsignatura(Tuple tuple)
    {
        UIAsignatura uiAsignatura = new UIAsignatura();
        uiAsignatura.setAsignaturaId(tuple.get(QAsignatura.asignatura.id));
        uiAsignatura.setEstado(tuple.get(QAsignatura.asignatura.estado));
        uiAsignatura.setTitulacionId(tuple.get(QTitulacion.titulacion.id));
        uiAsignatura.setTitulacionTipo(tuple.get(QTitulacion.titulacion.tipo));
        uiAsignatura.setPersonaId(tuple.get(QAsignaturaPersona.asignaturaPersona.personaId));
        uiAsignatura.setTipoAcceso(tuple.get(QAsignaturaPersona.asignaturaPersona.tipoAcceso));
        uiAsignatura.setEnSlt(tuple.get(QAsignatura.asignatura.enSlt));
        return uiAsignatura;
    }
/*

    public ResumenEstadisticas getEstadisticasOld(Long personaId)
    {
        ResumenEstadisticas resumenEstadisticas = new ResumenEstadisticas();

        resumenEstadisticas.setAsignadasInactivas(cuentaGuias(personaId, EstadoAsignatura.I.name()));
        resumenEstadisticas.setAsignadasInactivasGrado(cuentaGuias(personaId, EstadoAsignatura.I.name(), TipoEstudio.G));
        resumenEstadisticas.setAsignadasInactivasMaster(cuentaGuias(personaId, EstadoAsignatura.I.name(), TipoEstudio.M));
        resumenEstadisticas.setAsignadasIntroduciendo(cuentaGuias(personaId, EstadoAsignatura.D.name()));
        resumenEstadisticas.setAsignadasIntroduciendoGrado(cuentaGuias(personaId, EstadoAsignatura.D.name(), TipoEstudio.G));
        resumenEstadisticas.setAsignadasIntroduciendoMaster(cuentaGuias(personaId, EstadoAsignatura.D.name(), TipoEstudio.M));
        resumenEstadisticas.setAsignadasPendientes(cuentaGuias(personaId, EstadoAsignatura.V.name()));
        resumenEstadisticas.setAsignadasPendientesGrado(cuentaGuias(personaId, EstadoAsignatura.V.name(), TipoEstudio.G));
        resumenEstadisticas.setAsignadasPendientesMaster(cuentaGuias(personaId, EstadoAsignatura.V.name(), TipoEstudio.M));
        resumenEstadisticas.setAsignadasTraduccion(cuentaGuias(personaId, "T"));
        resumenEstadisticas.setAsignadasTraduccionGrado(cuentaGuias(personaId, "T", TipoEstudio.G));
        resumenEstadisticas.setAsignadasTraduccionMaster(cuentaGuias(personaId, "T", TipoEstudio.M));
        resumenEstadisticas.setAsignadasFinalizadas(cuentaGuias(personaId, EstadoAsignatura.F.name()));
        resumenEstadisticas.setAsignadasFinalizadasGrado(cuentaGuias(personaId, EstadoAsignatura.F.name(), TipoEstudio.G));
        resumenEstadisticas.setAsignadasFinalizadasMaster(cuentaGuias(personaId, EstadoAsignatura.F.name(), TipoEstudio.M));

        resumenEstadisticas.setNoAsignadasInactivas(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.I.name()));
        resumenEstadisticas.setNoAsignadasInactivasGrado(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.I.name(), TipoEstudio.G));
        resumenEstadisticas.setNoAsignadasInactivasMaster(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.I.name(), TipoEstudio.M));
        resumenEstadisticas.setNoAsignadasIntroduciendo(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.D.name()));
        resumenEstadisticas.setNoAsignadasIntroduciendoGrado(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.D.name(), TipoEstudio.G));
        resumenEstadisticas.setNoAsignadasIntroduciendoMaster(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.D.name(), TipoEstudio.M));
        resumenEstadisticas.setNoAsignadasPendientes(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.V.name()));
        resumenEstadisticas.setNoAsignadasPendientesGrado(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.V.name(), TipoEstudio.G));
        resumenEstadisticas.setNoAsignadasPendientesMaster(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.V.name(), TipoEstudio.M));
        resumenEstadisticas.setNoAsignadasTraduccion(cuentaGuiasNoAsignadas(personaId, "T"));
        resumenEstadisticas.setNoAsignadasTraduccionGrado(cuentaGuiasNoAsignadas(personaId, "T", TipoEstudio.G));
        resumenEstadisticas.setNoAsignadasTraduccionMaster(cuentaGuiasNoAsignadas(personaId, "T", TipoEstudio.M));
        resumenEstadisticas.setNoAsignadasFinalizadas(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.F.name()));
        resumenEstadisticas.setNoAsignadasFinalizadasGrado(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.F.name(), TipoEstudio.G));
        resumenEstadisticas.setNoAsignadasFinalizadasMaster(cuentaGuiasNoAsignadas(personaId, EstadoAsignatura.F.name(), TipoEstudio.M));

        return resumenEstadisticas;
    }

    private Long cuentaGuias(Long personaId, String estado)
    {
        Long cursoActivo = CursoAcademico.getCursoActivo().getCursoAca();
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPersona asignaturaPersona = QAsignaturaPersona.asignaturaPersona;
        JPAQuery queryGuias = new JPAQuery(entityManager);

        queryGuias.from(asignatura, asignaturaPersona).where(
                asignatura.gdoCursosAcademico.cursoAca.eq(cursoActivo)
                        .and(asignatura.id.eq(asignaturaPersona.asignaturaId))
                        .and(asignatura.estado.eq(estado))
                        .and(asignaturaPersona.cursoAca.eq(cursoActivo))
                        .and(asignaturaPersona.tipoAcceso.eq("AS"))
                        .and(asignaturaPersona.personaId.eq(personaId)));

        Long guias = queryGuias.distinct().count();
        return guias;
    }

    private Long cuentaGuiasNoAsignadas(Long personaId, String estado)
    {
        Long cursoActivo = CursoAcademico.getCursoActivo().getCursoAca();
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPersona asignaturaPersona = QAsignaturaPersona.asignaturaPersona;
        JPAQuery queryGuias = new JPAQuery(entityManager);

        queryGuias.from(asignatura, asignaturaPersona).where(
                asignatura.gdoCursosAcademico.cursoAca.eq(cursoActivo)
                        .and(asignatura.id.eq(asignaturaPersona.asignaturaId))
                        .and(asignatura.estado.eq(estado))
                        .and(asignaturaPersona.cursoAca.eq(cursoActivo))
                        .and(asignaturaPersona.tipoAcceso.ne("AS"))
                        .and(asignaturaPersona.personaId.eq(personaId)));

        Long guias = queryGuias.distinct().count();
        return guias;
    }

    private Long cuentaGuias(Long personaId, String estado, TipoEstudio tipoEstudio)
    {
        Long cursoActivo = CursoAcademico.getCursoActivo().getCursoAca();
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPersona asignaturaPersona = QAsignaturaPersona.asignaturaPersona;
        QTitulacionAsignatura tituAsignatura = QTitulacionAsignatura.titulacionAsignatura;
        QTitulacion titulacion = QTitulacion.titulacion;

        JPAQuery queryGuias = new JPAQuery(entityManager);

        queryGuias.from(asignatura, asignaturaPersona, tituAsignatura, titulacion).where(
                asignatura.gdoCursosAcademico.cursoAca.eq(cursoActivo)
                        .and(asignatura.id.eq(asignaturaPersona.asignaturaId))
                        .and(asignatura.estado.eq(estado))
                        .and(asignaturaPersona.cursoAca.eq(cursoActivo))
                        .and(asignaturaPersona.tipoAcceso.eq("AS"))
                        .and(asignaturaPersona.personaId.eq(personaId))
                        .and(asignatura.gdoExtAsiToda.id.eq(tituAsignatura.id.codAsignatura))
                        .and(tituAsignatura.id.estudioId.eq(titulacion.id))
                        .and(titulacion.tipo.eq(tipoEstudio.name())));

        Long guias = queryGuias.distinct().count();

        return guias;
    }


    private Long cuentaGuiasNoAsignadas(Long personaId, String estado, TipoEstudio tipoEstudio)
    {
        Long cursoActivo = CursoAcademico.getCursoActivo().getCursoAca();
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPersona asignaturaPersona = QAsignaturaPersona.asignaturaPersona;
        QTitulacionAsignatura tituAsignatura = QTitulacionAsignatura.titulacionAsignatura;
        QTitulacion titulacion = QTitulacion.titulacion;

        JPAQuery queryGuias = new JPAQuery(entityManager);

        queryGuias.from(asignatura, asignaturaPersona, tituAsignatura, titulacion).where(
                asignatura.gdoCursosAcademico.cursoAca.eq(cursoActivo)
                        .and(asignatura.id.eq(asignaturaPersona.asignaturaId))
                        .and(asignatura.estado.eq(estado))
                        .and(asignaturaPersona.tipoAcceso.ne("AS"))
                        .and(asignaturaPersona.cursoAca.eq(cursoActivo))
                        .and(asignaturaPersona.personaId.eq(personaId))
                        .and(asignatura.gdoExtAsiToda.id.eq(tituAsignatura.id.codAsignatura))
                        .and(tituAsignatura.id.estudioId.eq(titulacion.id))
                        .and(titulacion.tipo.eq(tipoEstudio.name())));

        Long guias = queryGuias.distinct().count();
        return guias;
    }*/


}