package es.uji.apps.gdo.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.gdo.model.*;

import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Repository
public class AsignaturaInfoDatabaseImplDAO extends BaseDAODatabaseImpl implements AsignaturaInfoDAO
{
    @Override
    public List<AsignaturaInfo> getAsignaturasConFiltros(Long personaId,
            String tipoEstudio, String codigoAsignatura, String estado, String titulacion,
            String curso, boolean conGuia)
    {
        QAsignaturaInfo asignaturas = QAsignaturaInfo.asignaturaInfo;

        return queryAsignaturas(personaId, tipoEstudio, codigoAsignatura, estado, titulacion,
                curso, conGuia).list(asignaturas).stream().distinct().collect(Collectors.toList());
    }

    @Override
    public Long contarAsignaturasConFiltros(Long personaId, String tipoEstudio,
            String codigoAsignatura, String estado, String titulacion, String curso,
            boolean conGuia)
    {
        QAsignaturaInfo asignaturas = QAsignaturaInfo.asignaturaInfo;
        return queryAsignaturas(personaId, tipoEstudio, codigoAsignatura, estado, titulacion,
                curso, conGuia).list(asignaturas).stream().distinct().count();
    }

    private JPAQuery queryAsignaturas(Long personaId, String tipoEstudio, String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia)
    {
        JPAQuery query = new JPAQuery(entityManager);
        Long cursoAca = CursoAcademico.getCursoActivo().getCursoAca();
        QAsignaturaInfo asignaturas = QAsignaturaInfo.asignaturaInfo;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        query = query.from(asignaturas).where(asignaturas.cursoAca.eq(cursoAca));

        if (personaId != null)
        {
            List<Long> listaPermisos = getIdAsignaturasDeUnaPersona(personaId, cursoAca);

            if (listaPermisos.isEmpty())
            {
                listaPermisos.add(-1L);
            }
            BooleanBuilder condicionesPermisosAsignatura = new BooleanBuilder();
            int BATCH_SIZE = 500;
            IntStream.range(0, (listaPermisos.size()+BATCH_SIZE-1)/BATCH_SIZE)
                    .mapToObj(i -> listaPermisos.subList(i*BATCH_SIZE, Math.min(listaPermisos.size(), (i+1)*BATCH_SIZE)))
                    .forEach((batch) ->  condicionesPermisosAsignatura.or(asignaturas.id.in(batch)));

            condicionesWhere.and(condicionesPermisosAsignatura);
        }

        if (tipoEstudio != null && !tipoEstudio.isEmpty())
        {
            condicionesWhere.and(asignaturas.tipo.eq(tipoEstudio));
        }

        if ((curso != null && !curso.isEmpty()) && !curso.equals("0"))
        {
            condicionesWhere.and(asignaturas.curso.eq(new Long(curso)));
        }

        if ((titulacion != null && !titulacion.isEmpty()))
        {
            condicionesWhere.and(asignaturas.estudioId.eq(new Long(titulacion)));
        }

        if (codigoAsignatura != null && !codigoAsignatura.isEmpty())
        {
            condicionesWhere.and(asignaturas.codAsignatura.toUpperCase().like("%" + codigoAsignatura.toUpperCase() + "%"));
        }

        if (conGuia)
        {
            condicionesWhere.and(asignaturas.estado.ne("I"));
        }

        if (estado != null && !estado.isEmpty())
        {
            condicionesWhere.and(asignaturas.estado.eq(estado));
        }

        return query.where(condicionesWhere).orderBy(asignaturas.codAsignatura.asc(), asignaturas.curso.asc());
    }

    public List<Long> getIdAsignaturasDeUnaPersona(Long personaId, Long cursoAca)
    {
        JPAQuery queryPermisos = new JPAQuery(entityManager);

        QPersonaAutorizada personasAutorizadas = QPersonaAutorizada.personaAutorizada;

        queryPermisos.from(personasAutorizadas).where(
                personasAutorizadas.personaId.eq(personaId).and(
                        personasAutorizadas.cursoAca.eq(cursoAca)));

        return queryPermisos.list(personasAutorizadas.asignaturaId);
    }


}