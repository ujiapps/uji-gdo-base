package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Actividad;
import es.uji.apps.gdo.model.ActividadAsignatura;
import es.uji.commons.db.BaseDAOTestImpl;

public class ActividadDAOTestImpl extends BaseDAOTestImpl implements ActividadDAO
{
    public ActividadAsignatura buildTestData(long id)
    {
        ActividadAsignatura actividad = new ActividadAsignatura();
        actividad.setId(id);
        actividad.setHorasPresenciales(new Float(id));
        actividad.setHorasNoPresenciales(new Float(id));

        return actividad;
    }

    @Override
    public List<ActividadAsignatura> getActividadesByAsignaturaId(Long asignaturaId)
    {
        List<ActividadAsignatura> result = new ArrayList<ActividadAsignatura>();

        for (int i = 0; i < 10; i++)
        {
            result.add(buildTestData((long) i));
        }

        return result;
    }

    @Override
    public List<ActividadAsignatura> getActividadesByCodAsignatura(String codAsignatura,
            Long cursoAcademico)
    {
        List<ActividadAsignatura> result = new ArrayList<ActividadAsignatura>();

        for (int i = 0; i < 10; i++)
        {
            result.add(buildTestData((long) i));
        }

        return result;
    }

    @Override
    public ActividadAsignatura getActividad(Long actividadId)
    {
        return buildTestData(actividadId);
    }

    @Override
    public List<Actividad> getActividades()
    {
        // TODO Auto-generated method stub
        return null;
    }

}