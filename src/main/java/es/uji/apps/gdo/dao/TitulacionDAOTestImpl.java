package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Titulacion;
import es.uji.apps.gdo.model.TitulacionPersona;
import es.uji.apps.gdo.model.TitulacionPersonaPK;
import es.uji.commons.db.BaseDAOTestImpl;

public class TitulacionDAOTestImpl extends BaseDAOTestImpl implements TitulacionDAO
{
    @Override
    public List<TitulacionPersona> getTitulacionesByPersona(long personaId)
    {
        List<TitulacionPersona> result = new ArrayList<TitulacionPersona>();

        for (int i = 0; i < 10; i++)
        {
            TitulacionPersonaPK tituPK = new TitulacionPersonaPK();
            tituPK.setPersonaId(personaId);
            tituPK.setEstudioId(i);

            TitulacionPersona titulacion = new TitulacionPersona();
            titulacion.setId(tituPK);
            titulacion.setNombre("Titulacion " + i);
            result.add(titulacion);
        }

        return result;
    }

    @Override
    public Titulacion getTitulacionByAsignaturaId(Long asignaturaId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Long> getCursosByTitulacionId(String titulacionId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Long> getCursosByTitulacionId(String titulacionId, Long personaId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Titulacion> getTitulaciones()
    {
        // TODO Auto-generated method stub
        return null;
    }

   
}
