package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.VerificaEvaluacion;
import es.uji.commons.db.BaseDAO;

public interface VerificaEvaluacionDAO extends BaseDAO
{
    public List<VerificaEvaluacion> getEvaluacionesByAsignatura(String codigoAsignatura,
            Long cursoAca);
}
