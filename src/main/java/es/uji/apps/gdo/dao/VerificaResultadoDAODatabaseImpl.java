package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QVerificaResultado;
import es.uji.apps.gdo.model.VerificaResultado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VerificaResultadoDAODatabaseImpl extends BaseDAODatabaseImpl implements
        VerificaResultadoDAO
{
    @Override
    public List<VerificaResultado> getVerificaResultadoByAsignatura(String codigoAsignatura,
            Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        QVerificaResultado verificaResultado = QVerificaResultado.verificaResultado;
        
        query.from(verificaResultado).where(verificaResultado.codAsignatura.eq(codigoAsignatura)
                .and(verificaResultado.cursoAcaIni.loe(cursoAca)));
        
        return query.list(verificaResultado);
    }
}