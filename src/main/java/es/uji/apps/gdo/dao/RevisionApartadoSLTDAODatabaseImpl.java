package es.uji.apps.gdo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.gdo.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public  class RevisionApartadoSLTDAODatabaseImpl extends BaseDAODatabaseImpl implements RevisionApartadoSLTDAO
{
    public List<RevisionApartadoSLT> getRevisionesApartadosSLT(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QRevisionApartadoSLT revisionApartadoSLT = QRevisionApartadoSLT.revisionApartadoSLT;
        query.from(revisionApartadoSLT)
                .where(revisionApartadoSLT.asignaturaId.eq(asignaturaId));

        return query.list(revisionApartadoSLT);
    }
}
