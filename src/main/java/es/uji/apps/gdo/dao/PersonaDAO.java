package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Persona;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;

public interface PersonaDAO extends BaseDAO, LookupDAO
{
    public Persona getPersona(Long personaId);

    public List<Persona> getPersonas();

    public Persona getPersonaByLogin(String login);
}
