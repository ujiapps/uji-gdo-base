package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.VerificaTexto;
import es.uji.commons.db.BaseDAO;

public interface VerificaTextoDAO extends BaseDAO
{
  public List<VerificaTexto> getContenidosByAsignatura(String codigoAsignatura);
}
