package es.uji.apps.gdo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.gdo.AsignaturaNoAsignadaATitulacion;
import es.uji.apps.gdo.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;

@Repository
public class CentroDAODatabaseImpl extends BaseDAODatabaseImpl implements CentroDAO
{
    @Override
    public List<Centro> getCentros()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCentro centro = QCentro.centro;

        query.from(centro);

        return query.orderBy(centro.nombre.asc()).list(centro);
    }
}