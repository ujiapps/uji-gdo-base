package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Comentario;
import es.uji.commons.db.BaseDAO;

public interface ComentarioDAO extends BaseDAO
{
    public List<Comentario> getComentariosByAsignatura(Long asignaturaId);

    public void insertComentario(Comentario comentario);
}
