package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Actividad;
import es.uji.apps.gdo.model.ActividadAsignatura;
import es.uji.apps.gdo.model.QActividadAsignatura;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QAsignaturaPOD;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ActividadDAODatabaseImpl extends BaseDAODatabaseImpl implements ActividadDAO
{
    @Autowired
    public AsignaturaDAO asignaturaDAO;

    @Override
    public List<ActividadAsignatura> getActividadesByCodAsignatura(String codAsignatura,
            Long cursoAcademico)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QActividadAsignatura asiActividad = QActividadAsignatura.actividadAsignatura;
        QAsignatura asignatura = new QAsignatura("ea");
        QAsignaturaPOD extAsignatura = new QAsignaturaPOD("eat");

        query.from(asiActividad).join(asiActividad.gdoAsignatura, asignatura).fetch()
                .join(asignatura.gdoExtAsiToda, extAsignatura).fetch()
                .join(asiActividad.gdoTipoActividade).fetch()
                .where(extAsignatura.id.eq(codAsignatura));

        return query.list(asiActividad);

    }

    @Override
    public ActividadAsignatura getActividad(Long actividadId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QActividadAsignatura asiActividad = QActividadAsignatura.actividadAsignatura;
        QAsignatura asignatura = new QAsignatura("aa");

        query.from(asiActividad).join(asiActividad.gdoAsignatura, asignatura).fetch()
                .join(asignatura.gdoExtAsiToda).fetch().where(asiActividad.id.eq(actividadId));

        List<ActividadAsignatura> listaActividadesDB = query.list(asiActividad);

        if (listaActividadesDB != null && listaActividadesDB.size() > 0)
        {
            return listaActividadesDB.get(0);
        }
        else
        {
            return new ActividadAsignatura();
        }
    }

    @Override
    public List<ActividadAsignatura> getActividadesByAsignaturaId(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QActividadAsignatura asiActividad = QActividadAsignatura.actividadAsignatura;
        QAsignatura asignatura = new QAsignatura("ea");

        query.from(asiActividad).join(asiActividad.gdoAsignatura, asignatura).fetch()
                .join(asignatura.gdoExtAsiToda).fetch().join(asiActividad.gdoTipoActividade)
                .fetch().where(asignatura.id.eq(asignaturaId));

        return query.list(asiActividad);
    }

    @Override
    public List<Actividad> getActividades()
    {
        return get(Actividad.class);
    }

}