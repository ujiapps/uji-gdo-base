package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Idioma;
import es.uji.apps.gdo.model.IdiomaAsignatura;
import es.uji.commons.db.BaseDAOTestImpl;

public class IdiomaAsignaturaDAOTestImpl extends BaseDAOTestImpl implements IdiomaAsignaturaDAO
{

    private Idioma buildDataTest(long id)
    {
        Idioma idioma = new Idioma();
        idioma.setId(id);
        idioma.setNombreCa("Idioma " + id);

        return idioma;
    }

    @Override
    public List<Idioma> getIdiomasByAsignaturaIdNoAsignados(Long asignaturaId)
    {
        ArrayList<Idioma> listaIdiomas = new ArrayList<Idioma>();

        for (int i = 1; i < 5; i++)
        {
            listaIdiomas.add(buildDataTest((long) i));
        }

        return listaIdiomas;
    }

    @Override
    public List<IdiomaAsignatura> getIdiomasByAsignaturaId(Long asignaturaId)
    {
         return null;
    }

    @Override
    public List<Idioma> getIdiomas()
    {
        return getIdiomasByAsignaturaIdNoAsignados(242L);
    }
}
