package es.uji.apps.gdo.dao;

import es.uji.apps.gdo.ui.ResumenEstadisticas;
import es.uji.commons.db.BaseDAO;

public interface DashboardDAO extends BaseDAO
{
    public ResumenEstadisticas getEstadisticas(Long personaId, Long centroId, Long titulacionId, Long CursoId);
}