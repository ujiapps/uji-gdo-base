package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Resultado;
import es.uji.commons.db.BaseDAOTestImpl;

public class ResultadoDAOTestImpl extends BaseDAOTestImpl implements ResultadoDAO
{
    private Resultado getResultadoTest(Long resultadoId, Long asignaturaId)
    {
        Resultado resultadoDB = new Resultado();
        resultadoDB.setId(resultadoId);
        resultadoDB.setResultadoCa("Texte resultat número " + resultadoId);
        resultadoDB.setResultadoEs("Texto resultado número " + resultadoId);
        resultadoDB.setResultadoUk("Result text number " + resultadoId);
        resultadoDB.setEditable("S");
        resultadoDB.setOrden(new Long(resultadoId.intValue()));

        return resultadoDB;
    }

    @Override
    public Resultado getResultado(Long resultadoId)
    {
        return getResultadoTest(resultadoId, 242L);
    }

    @Override
    public List<Resultado> getResultadosByAsignatura(Long asignaturaId)
    {
        List<Resultado> result = new ArrayList<Resultado>();
        
        for (int i = 0; i < 9; i++)
        {
            result.add(getResultadoTest((long) i, asignaturaId));
        }
        return null;
    }

}
