package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Apartado;
import es.uji.apps.gdo.model.domains.TipoApartado;
import es.uji.commons.db.BaseDAO;

public interface ApartadoDAO extends BaseDAO
{
    List<Apartado> getApartadosByAsignaturaAndTipo(Long asignaturaId,
                                                   TipoApartado tipoApartado);

    List<Apartado> getApartadosByTipoParaActivarAsignatura(TipoApartado tipoApartado,
                                                           String tipoEstudio);

    String getApartadosObligatoriosVaciosByAsignatura(Long asignaturaId);

}
