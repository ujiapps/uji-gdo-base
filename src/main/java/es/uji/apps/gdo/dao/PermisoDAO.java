package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Permiso;
import es.uji.commons.db.BaseDAO;

public interface PermisoDAO extends BaseDAO
{
    public List<Permiso> getPermisosByAsignaturaId(Long asignaturaId);

}
