package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Evaluacion;
import es.uji.commons.db.BaseDAOTestImpl;

public class EvaluacionDAOTestImpl extends BaseDAOTestImpl implements EvaluacionDAO
{
    public Evaluacion buildTestData(long id)
    {
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setId((long) id);
        evaluacion.setNombreCa("NombreCa");
        evaluacion.setNombreEs("NombreEs");
        evaluacion.setNombreUk("NombreUk");
        evaluacion.setEditable(true);
        evaluacion.setOrden(Long.valueOf(id));
        evaluacion.setPonderacion(Float.valueOf(id));

        return evaluacion;
    }

    @Override
    public List<Evaluacion> getEvaluacionesByAsignaturaId(Long asignaturaId)
    {
        List<Evaluacion> result = new ArrayList<Evaluacion>();

        for (int i = 1; i < 5; i++)
        {
            result.add(buildTestData((long) i));
        }

        return result;
    }

    @Override
    public Evaluacion getEvaluacion(Long id)
    {
        return buildTestData(id);
    }

}