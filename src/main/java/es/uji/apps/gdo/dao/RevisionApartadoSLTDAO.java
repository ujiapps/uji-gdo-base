package es.uji.apps.gdo.dao;

import es.uji.apps.gdo.model.RevisionApartadoSLT;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface RevisionApartadoSLTDAO extends BaseDAO
{
     List<RevisionApartadoSLT> getRevisionesApartadosSLT(Long asignaturaId);
}
