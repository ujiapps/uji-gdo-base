package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.DirectorExtra;
import es.uji.apps.gdo.model.QDirectorExtra;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DirectorExtraDatabaseImpl extends BaseDAODatabaseImpl implements DirectorExtraDAO
{

    @Override
    public List<DirectorExtra> getDirectoresExtra()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QDirectorExtra directores = QDirectorExtra.directorExtra;

        query.from(directores).orderBy(directores.gdoExtTituToda.nombre.asc(),
                directores.gdoExtPersona.apellido1.asc(), directores.gdoExtPersona.apellido2.asc());

        return query.list(directores);
    }
}
