package es.uji.apps.gdo.dao;

import java.util.Date;
import java.util.List;

import es.uji.apps.gdo.model.Texto;
import es.uji.commons.db.BaseDAOTestImpl;

public class TextoDAOTestImpl extends BaseDAOTestImpl implements TextoDAO
{
    private Texto getTextoTest(Long textoId, Long asignaturaId, Long apartadoId)
    {

        Texto texto = new Texto();
        texto.setId(textoId);
        texto.setFechaSltCa(new Date());
        texto.setFechaSltEs(new Date());
        texto.setFechaSltUk(new Date());
        texto.setTextoCa("Texte assignatura " + asignaturaId + " apartat " + apartadoId);
        texto.setTextoEs("Texto asignatura " + asignaturaId + " apartado " + apartadoId);
        texto.setTextoUk("Texto of subject " + asignaturaId + " section " + apartadoId);

        return texto;

    }

    @Override
    public Texto getTexto(Long textoId)
    {
        return getTextoTest(textoId, 242L, 1L);
    }

    @Override
    public Texto getTextosByAsignaturaApartado(Long asignaturaId, Long apartadoId)
    {
        return getTextoTest(1L, 242L, 1L);
    }

    @Override
    public void updateTexto(Texto texto)
    {
    }

    @Override
    public List<Texto> getTextosByAsignatura(Long asignaturaId)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
