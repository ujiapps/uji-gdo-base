package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Comentario;
import es.uji.apps.gdo.model.QComentario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ComentarioDAODatabaseImpl extends BaseDAODatabaseImpl implements ComentarioDAO
{
    @Override
    public List<Comentario> getComentariosByAsignatura(Long asignaturaId)

    {
        JPAQuery query = new JPAQuery(entityManager);

        QComentario comentario = QComentario.comentario1;

        query.from(comentario).where(comentario.gdoAsignatura.id.eq(asignaturaId))
                .orderBy(comentario.id.desc());

        return query.list(comentario);
    }

    @Override
    @Transactional
    public void insertComentario(Comentario comentario)
    {
        insert(comentario);
    }

}
