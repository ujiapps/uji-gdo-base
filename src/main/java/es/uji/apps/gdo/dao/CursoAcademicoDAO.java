package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.CursoAcademicoSoloUnoActivo;
import es.uji.apps.gdo.model.CursoAcademico;
import es.uji.commons.db.BaseDAO;

public interface CursoAcademicoDAO extends BaseDAO
{
    public List<CursoAcademico> getCursos();
    
    public CursoAcademico getCursoAcademicoById(Long cursoAcaId);
    
    public CursoAcademico getCursoAcademicoByAsignatura(Long asignaturaId);

    public CursoAcademico getCursoAcademicoByApartado(Long apartadoId);
    
    public CursoAcademico getCursoActivo();
    
    public CursoAcademico setCursoActivo(Long cursoAcaId) throws CursoAcademicoSoloUnoActivo;
}
