package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Resultado;
import es.uji.commons.db.BaseDAO;

public interface ResultadoDAO extends BaseDAO
{
  public Resultado getResultado (Long resultadoId);
  
  public List<Resultado> getResultadosByAsignatura(Long asignaturaId);
  
}
