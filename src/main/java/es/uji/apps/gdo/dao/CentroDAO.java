package es.uji.apps.gdo.dao;

import es.uji.apps.gdo.model.Centro;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface CentroDAO extends BaseDAO
{
    public List<Centro> getCentros();
}