package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.CursoAcademicoSoloUnoActivo;
import es.uji.apps.gdo.model.Apartado;
import es.uji.apps.gdo.model.Asignatura;
import es.uji.apps.gdo.model.CursoAcademico;
import es.uji.apps.gdo.model.QCursoAcademico;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoAcademicoDAODatabaseImpl extends BaseDAODatabaseImpl implements CursoAcademicoDAO
{
    @Override
    public List<CursoAcademico> getCursos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCursoAcademico curso = QCursoAcademico.cursoAcademico;

        query.from(curso).orderBy(curso.cursoAca.desc());

        return query.list(curso);
    }

    @Override
    public CursoAcademico getCursoAcademicoById(Long cursoAcaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCursoAcademico cursos = QCursoAcademico.cursoAcademico;
        
        query.from(cursos).where(cursos.cursoAca.eq(cursoAcaId));
        
        return query.list(cursos).get(0);
    }    
    
    @Override
    public CursoAcademico getCursoAcademicoByAsignatura(Long asignaturaId)
    {
        List<Asignatura> listaAsignaturas = get(Asignatura.class, new Long(asignaturaId));
        CursoAcademico curso = listaAsignaturas.get(0).getGdoCursosAcademico();

        return curso;
    }

    @Override
    public CursoAcademico getCursoAcademicoByApartado(Long apartadoId)
    {
        List<Apartado> listaApartados = get(Apartado.class, new Long(apartadoId));
        CursoAcademico curso = listaApartados.get(0).getGdoCursosAcademico();

        return curso;
    }

    public CursoAcademico getCursoActivo()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCursoAcademico cursos = QCursoAcademico.cursoAcademico;

        query.from(cursos).where(cursos.activo.eq(true));

        List<CursoAcademico> listaCursos = query.list(cursos);

        if (listaCursos == null || listaCursos.isEmpty())
        {
            return new CursoAcademico();
        }
        else
        {
            return query.list(cursos).get(0);
        }
    }

    @Transactional
    public CursoAcademico setCursoActivo(Long cursoAcaId) throws CursoAcademicoSoloUnoActivo
    {
        CursoAcademico cursoAcaActivo = CursoAcademico.getCursoActivo();

        if (cursoAcaActivo.getCursoAca().longValue() == cursoAcaId.longValue())
        {
            return cursoAcaActivo;
        }

        cursoAcaActivo.setActivo(false);
        cursoAcaActivo.update();

        CursoAcademico nuevoCursoActivo = activaCursoAca(cursoAcaId);
        
        return nuevoCursoActivo;
    }

    @Transactional
    private CursoAcademico activaCursoAca(Long cursoAcaId) throws CursoAcademicoSoloUnoActivo
    {
        CursoAcademico nuevoCursoActivo = CursoAcademico.getCursoAcademicoById(cursoAcaId);
        nuevoCursoActivo.setActivo(true);
        nuevoCursoActivo.update();
        return nuevoCursoActivo;
    }
}
