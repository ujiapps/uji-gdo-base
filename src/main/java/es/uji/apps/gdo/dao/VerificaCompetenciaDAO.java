package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.VerificaCompetencia;
import es.uji.commons.db.BaseDAO;

public interface VerificaCompetenciaDAO extends BaseDAO
{

    List<VerificaCompetencia> getVerificaCompetenciaByAsignatura(String codigoAsignatura,
            Long cursoAca);

}
