package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.Actividad;
import es.uji.apps.gdo.model.ActividadAsignatura;
import es.uji.commons.db.BaseDAO;

public interface ActividadDAO extends BaseDAO
{
    public List<ActividadAsignatura> getActividadesByCodAsignatura(String codAsignatura,
            Long cursoAcademico);

    public ActividadAsignatura getActividad(Long actividadId);

    public List<ActividadAsignatura> getActividadesByAsignaturaId(Long asignaturaId);

    public List<Actividad> getActividades();
    
}
