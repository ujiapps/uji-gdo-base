package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.gdo.model.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.domains.EstadoAsignatura;
import es.uji.apps.gdo.model.domains.TipoPermiso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaDAODatabaseImpl extends BaseDAODatabaseImpl implements AsignaturaDAO
{
    private Asignatura getAsignaturaDB(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignaturas = QAsignatura.asignatura;
        QTexto texto = QTexto.texto;
        QComentario comentarios = QComentario.comentario1;
        QEvaluacion evaluaciones = QEvaluacion.evaluacion;
        QActividadAsignatura actividadAsignatura = QActividadAsignatura.actividadAsignatura;
        QCompetencia competencias = QCompetencia.competencia;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;
        QApartado apartado = QApartado.apartado;
        QIdiomaAsignatura idiomaAsignatura = QIdiomaAsignatura.idiomaAsignatura;

        query.from(asignaturas).join(asignaturas.gdoExtAsiToda).fetch()
                .leftJoin(asignaturas.gdoAsiTextos, texto).fetch()
                .leftJoin(texto.gdoApartado, apartado).fetch()
                .leftJoin(asignaturas.gdoExtAsiToda, asignaturaPOD).fetch()
                .leftJoin(asignaturas.gdoAsiIdiomas, idiomaAsignatura).fetch()
                .leftJoin(asignaturas.gdoAsiIdiomas).fetch()
                .leftJoin(asignaturas.gdoAsiComentarios, comentarios).fetch()
                .leftJoin(asignaturas.gdoAsiEvaluacions, evaluaciones).fetch()
                .leftJoin(asignaturas.gdoAsiActividades, actividadAsignatura).fetch()
                .leftJoin(asignaturas.gdoAsiCompetencias, competencias).fetch()
                .where(asignaturas.id.eq(id));

        return query.list(asignaturas).get(0);
    }

    @Override
    public Asignatura getAsignaturaByIdSinRelaciones(long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignatura asignaturas = QAsignatura.asignatura;
        query.from(asignaturas).join(asignaturas.gdoExtAsiToda).fetch()
                .where(asignaturas.id.eq(asignaturaId));
        return query.list(asignaturas).get(0);
    }

    @Override
    public List<Long> getIdAsignaturasDeUnaPersona(Long personaId, Long cursoAca)
    {
        JPAQuery queryPermisos = new JPAQuery(entityManager);

        QPersonaAutorizada personasAutorizadas = QPersonaAutorizada.personaAutorizada;

        queryPermisos.from(personasAutorizadas).where(
                personasAutorizadas.personaId.eq(personaId).and(
                        personasAutorizadas.cursoAca.eq(cursoAca)));

        return queryPermisos.list(personasAutorizadas.asignaturaId);
    }

    private List<String> getCodigoAsignaturasLikeCadena(String cadena)
    {
        JPAQuery queryAsiTodas = new JPAQuery(entityManager);

        QAsignaturaPOD asiTodas = QAsignaturaPOD.asignaturaPOD;

        queryAsiTodas.from(asiTodas).where(asiTodas.id.upper().like(cadena.toUpperCase() + "%"));

        return queryAsiTodas.list(asiTodas.id);
    }

    private JPAQuery queryAsignaturas(Long personaId, String tipoEstudio, String codigoAsignatura,
                                      String estado, String titulacion, String curso, boolean conGuia, boolean incluirRelaciones)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignaturas = QAsignatura.asignatura;
        QTexto textos = QTexto.texto;
        QComentario comentarios = QComentario.comentario1;
        QEvaluacion evaluaciones = QEvaluacion.evaluacion;
        QActividadAsignatura actividadAsignatura = QActividadAsignatura.actividadAsignatura;
        QCompetencia competencias = QCompetencia.competencia;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;
        QApartado apartado = QApartado.apartado;
        QIdiomaAsignatura idiomaAsignatura = QIdiomaAsignatura.idiomaAsignatura;

        query = query.from(asignaturas)
                .leftJoin(asignaturas.gdoExtAsiToda, asignaturaPOD).fetch();

        if (incluirRelaciones)
        {
            query.leftJoin(asignaturas.gdoAsiTextos, textos).fetch()
                    .leftJoin(textos.gdoApartado, apartado).fetch()
                    .leftJoin(asignaturas.gdoAsiIdiomas, idiomaAsignatura).fetch()
                    .leftJoin(asignaturas.gdoAsiComentarios, comentarios).fetch()
                    .leftJoin(asignaturas.gdoAsiEvaluacions, evaluaciones).fetch()
                    .leftJoin(asignaturas.gdoAsiActividades, actividadAsignatura).fetch()
                    .leftJoin(asignaturas.gdoAsiCompetencias, competencias).fetch();
        }

        BooleanBuilder condicionesWhere = new BooleanBuilder();
        BooleanBuilder condicionesIn = new BooleanBuilder();

        Long cursoAca = CursoAcademico.getCursoActivo().getCursoAca();

        condicionesWhere.and(asignaturas.gdoCursosAcademico.cursoAca.eq(cursoAca));

        int fin;

        if (personaId != null)
        {
            List<Long> listaPermisos = getIdAsignaturasDeUnaPersona(personaId, cursoAca);

            if (listaPermisos.isEmpty())
            {
                listaPermisos.add(-1L);
            }


            fin = 999;

            if (listaPermisos.size() > 999)
            {
                condicionesIn.or(asignaturas.id.in(listaPermisos.subList(0, fin)));

                for (int i = fin; i < listaPermisos.size(); i = i + 999)
                {
                    fin = i + 999;

                    if (fin > listaPermisos.size())
                    {
                        fin = listaPermisos.size();
                    }

                    condicionesIn.or(asignaturas.id.in(listaPermisos.subList(i, fin)));
                }

                condicionesWhere.and(condicionesIn);

            }
            else
            {
                condicionesWhere.and(asignaturas.id.in(listaPermisos));
            }

        }

        if (tipoEstudio != null && !tipoEstudio.isEmpty())
        {
            condicionesWhere.and(asignaturas.gdoExtAsiToda.tipo.eq(tipoEstudio));
        }

        if ((curso != null && !curso.isEmpty()) && (titulacion != null && !titulacion.isEmpty()))
        {
            List<String> listaAsignaturasPorTitulacionYCurso = getAsignaturasByTitulacionYCurso(
                    Long.parseLong(titulacion), Long.parseLong(curso));

            if (listaAsignaturasPorTitulacionYCurso.isEmpty())
            {
                listaAsignaturasPorTitulacionYCurso.add("-1");
            }

            condicionesWhere.and(asignaturas.gdoExtAsiToda.id
                    .in(listaAsignaturasPorTitulacionYCurso));

        }

        if (codigoAsignatura != null && !codigoAsignatura.isEmpty())
        {
            List<String> asignaturasLikeCadena = getCodigoAsignaturasLikeCadena(codigoAsignatura);

            if (asignaturasLikeCadena.isEmpty())
            {
                asignaturasLikeCadena.add("-1");
            }

            condicionesWhere.and(asignaturas.gdoExtAsiToda.id.in(asignaturasLikeCadena));
        }

        if (conGuia)
        {
            condicionesWhere.and(asignaturas.estado.ne(EstadoAsignatura.I.name()));
        }

        if (estado != null && !estado.isEmpty())
        {
            condicionesWhere.and(asignaturas.estado.eq(estado));
        }

        return query.where(condicionesWhere).orderBy(asignaturas.gdoExtAsiToda.id.asc());
    }

    @Override
    public List<Asignatura> getAsignaturasConFiltros(Long personaId, String tipoEstudio,
                                                     String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia)
    {
        QAsignatura asignaturas = QAsignatura.asignatura;

        return queryAsignaturas(personaId, tipoEstudio, codigoAsignatura, estado, titulacion,
                curso, conGuia, true).list(asignaturas).stream().distinct().collect(Collectors.toList());
    }

    @Override
    public Long contarAsignaturasConFiltros(Long personaId, String tipoEstudio,
                                            String codigoAsignatura, String estado, String titulacion, String curso, boolean conGuia)
    {
        QAsignatura asignaturas = QAsignatura.asignatura;
        return queryAsignaturas(personaId, tipoEstudio, codigoAsignatura, estado, titulacion,
                curso, conGuia, false).list(asignaturas).stream().distinct().count();
    }

    public List<Asignatura> getAsignaturasByTitulacion(Long titulacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);

        QTitulacionAsignatura extTitulacionesAsignaturas = QTitulacionAsignatura.titulacionAsignatura;

        subquery.from(extTitulacionesAsignaturas).where(
                extTitulacionesAsignaturas.id.estudioId.eq(titulacionId));

        List<String> listaAsignaturas = subquery.list(extTitulacionesAsignaturas.id.codAsignatura);

        if (listaAsignaturas.isEmpty())
        {
            listaAsignaturas.add("noExistenAsignaturas");
        }

        QAsignatura asignaturas = QAsignatura.asignatura;
        QAsignaturaPOD extAsiTodas = new QAsignaturaPOD("at");

        query.from(asignaturas)
                .join(asignaturas.gdoExtAsiToda, extAsiTodas)
                .fetch()
                .leftJoin(asignaturas.gdoAsiIdiomas)
                .fetch()
                .where(asignaturas.gdoCursosAcademico.cursoAca.eq(
                        CursoAcademico.getCursoActivo().getCursoAca()).and(
                        extAsiTodas.id.in(listaAsignaturas)));

        return query.list(asignaturas);
    }

    public List<String> getAsignaturasByTitulacionYCurso(Long titulacionId, Long curso)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTitulacionAsignatura extTitulacionesAsignaturas = QTitulacionAsignatura.titulacionAsignatura;

        if (curso.longValue() == 0L)
        {
            query.from(extTitulacionesAsignaturas)
                    .where(extTitulacionesAsignaturas.id.estudioId.eq(titulacionId))
                    .orderBy(extTitulacionesAsignaturas.cursoId.asc(),
                            extTitulacionesAsignaturas.id.codAsignatura.asc());
        }
        else
        {
            query.from(extTitulacionesAsignaturas)
                    .where(extTitulacionesAsignaturas.id.estudioId.eq(titulacionId).and(
                            extTitulacionesAsignaturas.cursoId.eq(curso)))
                    .orderBy(extTitulacionesAsignaturas.cursoId.asc(),
                            extTitulacionesAsignaturas.id.codAsignatura.asc());

        }
        return query.list(extTitulacionesAsignaturas.id.codAsignatura);
    }

    @Override
    public Asignatura getAsignaturaByIdConRelaciones(Long asignaturaId)
    {
        return getAsignaturaDB(asignaturaId);
    }

    @Override
    public Asignatura getAsignaturaByIdConRelaciones(String codigoAsignatura)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignaturas = QAsignatura.asignatura;
        QAsignaturaPOD asiTodas = new QAsignaturaPOD("at");
        QTexto texto = QTexto.texto;
        QComentario comentario = QComentario.comentario1;

        query.from(asignaturas)
                .leftJoin(asignaturas.gdoAsiIdiomas).fetch()
                .leftJoin(asignaturas.gdoAsiComentarios, comentario).fetch()
                .join(asignaturas.gdoAsiTextos, texto).fetch()
                .join(asignaturas.gdoExtAsiToda, asiTodas)
                .fetch()
                .where(asiTodas.id.eq(codigoAsignatura).and(
                        asignaturas.gdoCursosAcademico.cursoAca.eq(CursoAcademico.getCursoActivo()
                                .getCursoAca())));

        List<Asignatura> listaAsignaturas = query.list(asignaturas);

        if (listaAsignaturas != null && listaAsignaturas.size() > 0)
        {
            return listaAsignaturas.get(0);
        }
        else
        {
            return null;
        }

    }

    @Override
    public Asignatura getAsignaturaByAsignaturaAndCursoAca(String codigoAsignatura, Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignaturas = QAsignatura.asignatura;
        QAsignaturaPOD asiTodas = new QAsignaturaPOD("at");

        query.from(asignaturas)
                .leftJoin(asignaturas.gdoAsiIdiomas)
                .fetch()
                .join(asignaturas.gdoExtAsiToda, asiTodas)
                .fetch()
                .where(asiTodas.id.eq(codigoAsignatura).and(
                        asignaturas.gdoCursosAcademico.cursoAca.eq(cursoAca)));

        List<Asignatura> listaAsignaturas = query.list(asignaturas);

        if (listaAsignaturas != null && listaAsignaturas.size() > 0)
        {
            return listaAsignaturas.get(0);
        }
        else
        {
            return null;
        }

    }

    @Override
    @Transactional
    public void insertIdioma(long asignaturaId, long idiomaId)
    {
        Idioma idioma = new Idioma();
        idioma.setId(idiomaId);

        IdiomaAsignatura gdoAsiIdiomas = new IdiomaAsignatura();
        gdoAsiIdiomas.setGdoIdioma(idioma);
        gdoAsiIdiomas.setGdoAsignatura(this.getAsignaturaByIdConRelaciones(asignaturaId));

        insert(gdoAsiIdiomas);

    }

    private Long getApartadosObligatorios(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTexto textos = QTexto.texto;
        QApartado apartado = QApartado.apartado;
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;

        query.from(textos)
                .join(textos.gdoApartado, apartado)
                .fetch()
                .join(textos.gdoAsignatura, asignatura)
                .fetch()
                .join(asignatura.gdoExtAsiToda, asignaturaPOD)
                .fetch()
                .where(textos.gdoAsignatura.id
                        .eq(asignaturaId)
                        .and(apartado.obligatorio.eq("S"))
                        .and(apartado.tipoEstudio.eq("T").or(
                                asignaturaPOD.tipo.eq(apartado.tipoEstudio))));

        return query.count();
    }

    @Override
    public Long apartadosObligatoriosTotales(Long asignaturaId)
    {
        Long apartados = getApartadosObligatorios(asignaturaId);
        Long idiomas = 1L;
        Long evaluaciones = 1L;
        Long actividades = 1L;
        Long competencias = 1L;

        return apartados + idiomas + evaluaciones + actividades + competencias;
    }

    private Long cuentaApartadosLlenos(Long asignaturaId, String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTexto textos = QTexto.texto;
        QApartado apartado = QApartado.apartado;
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;

        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(textos.gdoAsignatura.id.eq(asignaturaId)
                .and(apartado.obligatorio.eq("S"))
                .and(apartado.tipoEstudio.eq("T").or(asignaturaPOD.tipo.eq(apartado.tipoEstudio))));

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(textos.textoCa.isNotEmpty());
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(textos.textoEs.isNotEmpty());
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(textos.textoUk.isNotEmpty());
        }

        query.from(textos).join(textos.gdoApartado, apartado).fetch()
                .join(textos.gdoAsignatura, asignatura).fetch()
                .join(asignatura.gdoExtAsiToda, asignaturaPOD).fetch().where(condicionesWhere);

        return query.count();
    }

    private Long cuentaIdiomasLlenos(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIdiomaAsignatura idiomas = QIdiomaAsignatura.idiomaAsignatura;

        query.from(idiomas).where(idiomas.gdoAsignatura.id.eq(asignaturaId));

        if (query.count() > 0)
        {
            return 1L;
        }
        else
        {
            return 0L;
        }
    }

    private Long cuentaEvaluacionesLlenas(Long asignaturaId, String idioma)
    {
        JPAQuery queryTotal = new JPAQuery(entityManager);
        JPAQuery queryLlenos = new JPAQuery(entityManager);
        QEvaluacion evaluaciones = QEvaluacion.evaluacion;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(evaluaciones.gdoAsignatura.id.eq(asignaturaId));

        queryTotal.from(evaluaciones).where(condicionesWhere);

        Long total = queryTotal.count();

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(evaluaciones.nombreCa.isNotEmpty());
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(evaluaciones.nombreEs.isNotEmpty());
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(evaluaciones.nombreUk.isNotEmpty());
        }

        queryLlenos.from(evaluaciones).where(condicionesWhere);

        Long llenos = queryLlenos.count();

        if (total.equals(llenos) && total > 0)
        {
            return 1L;
        }
        else
        {
            return 0L;
        }
    }

    private Long cuentaActividadesLlenas(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActividadAsignatura actividades = QActividadAsignatura.actividadAsignatura;

        query.from(actividades).where(actividades.gdoAsignatura.id.eq(asignaturaId));

        if (query.count() > 0)
        {
            return 1L;
        }
        else
        {
            return 0L;
        }
    }

    private Long cuentaCompetenciasLlenas(Long asignaturaId, String idioma)
    {
        JPAQuery queryTotal = new JPAQuery(entityManager);
        JPAQuery queryLlenos = new JPAQuery(entityManager);
        QCompetencia competencia = QCompetencia.competencia;
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        condicionesWhere.and(competencia.gdoAsignatura.id.eq(asignaturaId));

        queryTotal.from(competencia).where(condicionesWhere);

        Long total = queryTotal.count();

        if (idioma.equals("CA"))
        {
            condicionesWhere.and(competencia.competenciaCa.isNotEmpty());
        }
        else if (idioma.equals("ES"))
        {
            condicionesWhere.and(competencia.competenciaEs.isNotEmpty());
        }
        else if (idioma.equals("UK"))
        {
            condicionesWhere.and(competencia.competenciaUk.isNotEmpty());
        }

        queryLlenos.from(competencia).where(condicionesWhere);

        Long llenos = queryLlenos.count();

        if (total.equals(llenos) && total > 0)
        {
            return 1L;
        }
        else
        {
            return 0L;
        }
    }

    @Override
    public Long apartadosObligatoriosLlenos(Long asignaturaId, String idioma)
    {
        Long apartados = cuentaApartadosLlenos(asignaturaId, idioma);
        Long idiomas = cuentaIdiomasLlenos(asignaturaId);
        Long evaluaciones = cuentaEvaluacionesLlenas(asignaturaId, idioma);
        Long actividades = cuentaActividadesLlenas(asignaturaId);
        Long competencias = cuentaCompetenciasLlenas(asignaturaId, idioma);

        return apartados + idiomas + evaluaciones + actividades + competencias;
    }

    @Override
    public Long getNumeroDeComentarios(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QComentario comentarios = QComentario.comentario1;

        query.from(comentarios).where(comentarios.gdoAsignatura.id.eq(asignaturaId));

        return query.count();
    }

    @Override
    public Long getCursoAsignatura(String codigoAsignatura)
    {
        List<Long> listaCursos;

        JPAQuery query = new JPAQuery(entityManager);
        QTitulacionAsignatura asignatura = QTitulacionAsignatura.titulacionAsignatura;

        query.from(asignatura).where(asignatura.id.codAsignatura.eq(codigoAsignatura));

        listaCursos = query.list(asignatura.cursoId);

        if (!listaCursos.isEmpty())
        {
            return listaCursos.get(0);
        }
        else
        {
            return -1L;
        }
    }

    @Override
    public List<Asignatura> getAsignaturasRelacionadas(Asignatura asignaturaOriginal)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignaturas = QAsignatura.asignatura;

        query.from(asignaturas).where(asignaturas.gdoAsignatura.eq(asignaturaOriginal));

        List<Asignatura> lista = query.list(asignaturas);

        return query.list(asignaturas);
    }

    @Override
    public boolean isNuevaImplantacion(String codigoAsignatura)
    {
        Long cursoAnterior = CursoAcademico.getCursoActivo().getCursoAca() - 1L;

        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;

        query.from(asignatura)
                .join(asignatura.gdoExtAsiToda, asignaturaPOD)
                .fetch()
                .where(asignatura.gdoCursosAcademico.cursoAca.eq(cursoAnterior)
                        .and(asignatura.estado.notIn(EstadoAsignatura.I.name()))
                        .and(asignaturaPOD.id.eq(codigoAsignatura)));

        List<Asignatura> listaAsignaturas = query.list(asignatura);

        if (listaAsignaturas.isEmpty())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public Long cuentaAsignaturas(Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignatura asignatura = QAsignatura.asignatura;

        query.from(asignatura).where(asignatura.gdoCursosAcademico.cursoAca.eq(cursoAca));

        return query.count();
    }

    @Override
    public List<Asignatura> getAsignaturasByCursoAcademico(Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignatura asignatura = QAsignatura.asignatura;
        QAsignaturaPOD asignaturaPOD = QAsignaturaPOD.asignaturaPOD;

        query.from(asignatura, asignaturaPOD)
                .where(asignatura.gdoCursosAcademico.cursoAca.eq(cursoAca).and(asignatura.gdoExtAsiToda.id.eq(asignaturaPOD.id)));

        return query.list(asignatura);
    }


    private TitulacionAsignatura getTitulacionByAsignatura(Asignatura asignatura)
    {
        String codAsignatura = asignatura.getGdoExtAsiToda().getId();

        JPAQuery queryTitulacion = new JPAQuery(entityManager);
        QTitulacionAsignatura extTitulacionesAsignaturas = QTitulacionAsignatura.titulacionAsignatura;
        queryTitulacion.from(extTitulacionesAsignaturas).where(
                extTitulacionesAsignaturas.id.codAsignatura.eq(codAsignatura));

        return queryTitulacion.list(extTitulacionesAsignaturas).get(0);
    }

    private List<Persona> getCoordinadoresDirectos(TitulacionAsignatura tituAsi)
    {
        List<Long> coordinadoresIds = new ArrayList<Long>();
        List<Persona> coordinadores = new ArrayList<Persona>();

        JPAQuery queryPersona = new JPAQuery(entityManager);
        QTitulacionPersona tituPersona = QTitulacionPersona.titulacionPersona;
        queryPersona.from(tituPersona).where(
                tituPersona.id.estudioId.eq(tituAsi.getId().getEstudioId())
                        .and(tituPersona.cursoId.eq(tituAsi.getCursoId()))
                        .and(tituPersona.cargoId.in("COR","DIR")));

        coordinadoresIds = queryPersona.list(tituPersona.id.personaId);

        for (Long idCoordinador : coordinadoresIds)
        {
            coordinadores.add(Persona.getPersona(idCoordinador));
        }

        return coordinadores;
    }

    private List<Persona> getCoordinadoresExtras(TitulacionAsignatura tituAsi)
    {
        JPAQuery queryExtras = new JPAQuery(entityManager);
        QDirectorExtra coordinadorExtra = QDirectorExtra.directorExtra;
        queryExtras.from(coordinadorExtra).where(
                coordinadorExtra.gdoExtTituToda.id.eq(tituAsi.getId().getEstudioId()).and(
                        coordinadorExtra.cursoId.eq(tituAsi.getCursoId()).or(
                                coordinadorExtra.tipoPermiso.eq(TipoPermiso.DIR.name()))));

        return queryExtras.list(coordinadorExtra.gdoExtPersona);
    }

    @Override
    public List<Persona> getCoordinadoresDeLaAsignatura(Asignatura asignatura)
    {
        List<Persona> coordinadores = new ArrayList<Persona>();

        TitulacionAsignatura tituAsi = getTitulacionByAsignatura(asignatura);

        if (tituAsi != null)
        {
            coordinadores = getCoordinadoresDirectos(tituAsi);

            coordinadores.addAll(getCoordinadoresExtras(tituAsi));
        }

        return coordinadores;

    }

    private List<Persona> getResponsableAsignatura(Asignatura asignatura)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersona persona = QPersona.persona;

        query.from(persona).where(persona.id.eq(asignatura.getGdoExtPersona().getId()));

        return query.list(persona);
    }

    private List<Persona> getResponsableAsignaturaExtras(Asignatura asignatura)
    {

        JPAQuery query = new JPAQuery(entityManager);
        QPermiso permisos = QPermiso.permiso;

        query.from(permisos).where(permisos.gdoAsignatura.id.eq(asignatura.getId()));

        return query.list(permisos.gdoExtPersona);
    }

    @Override
    public List<Persona> getResponsablesAsignatura(Asignatura asignatura)
    {
        List<Persona> responsables = new ArrayList<Persona>();

        responsables = getResponsableAsignatura(asignatura);
        responsables.addAll(getResponsableAsignaturaExtras(asignatura));

        return responsables;
    }
}