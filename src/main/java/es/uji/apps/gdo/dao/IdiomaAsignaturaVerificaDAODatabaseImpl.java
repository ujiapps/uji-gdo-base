package es.uji.apps.gdo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.gdo.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository public class IdiomaAsignaturaVerificaDAODatabaseImpl extends BaseDAODatabaseImpl
        implements IdiomaAsignaturaVerificaDAO
{
    private static final Long CASTELLANO = 1L;
    private static final Long VALENCIANO = 2L;

    @Override public List<Idioma> getIdiomasByAsignatura(String codigoAsignatura)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subQuery = new JPAQuery(entityManager);

        QIdioma idiomas = QIdioma.idioma;
        QIdiomaAsignaturaVerifica idiomaAsignaturaVerifica = QIdiomaAsignaturaVerifica.idiomaAsignaturaVerifica;

        List<Long> listaIdiomas = subQuery.from(idiomaAsignaturaVerifica)
                .where(idiomaAsignaturaVerifica.codigoAsignatura.eq(codigoAsignatura).and(idiomaAsignaturaVerifica.idiomaId.isNotNull()))
                .list(idiomaAsignaturaVerifica.idiomaId);

        if (listaIdiomas.isEmpty()) {
            listaIdiomas.add(CASTELLANO);
            listaIdiomas.add(VALENCIANO);
        }

        query.from(idiomas).where(idiomas.id.in(listaIdiomas)).orderBy(idiomas.nombreCa.asc());

        return query.list(idiomas);
    }

}