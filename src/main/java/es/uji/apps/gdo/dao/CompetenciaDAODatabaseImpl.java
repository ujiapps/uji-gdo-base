package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Competencia;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QCompetencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CompetenciaDAODatabaseImpl extends BaseDAODatabaseImpl implements CompetenciaDAO
{

    @Override
    public Competencia getCompetencia(Long competenciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCompetencia asiCompetencias = QCompetencia.competencia;
        QAsignatura asignaturas = new QAsignatura("aa");

        query.from(asiCompetencias).join(asiCompetencias.gdoAsignatura, asignaturas).fetch()
                .join(asignaturas.gdoExtAsiToda).fetch()
                .where(asiCompetencias.id.eq(competenciaId));

        List<Competencia> list = query.list(asiCompetencias);

        if (list != null && list.size() > 0)
        {
            return list.get(0);
        }
        else
        {
            return new Competencia();
        }
    }

    @Override
    public List<Competencia> getCompetenciasByAsignatura(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCompetencia asiCompetencias = QCompetencia.competencia;
        QAsignatura asignaturas = new QAsignatura("aa");

        query.from(asiCompetencias).join(asiCompetencias.gdoAsignatura, asignaturas).fetch()
                .join(asignaturas.gdoExtAsiToda).fetch().where(asignaturas.id.eq(asignaturaId));

        return query.list(asiCompetencias);
    }

}