package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.PendientesTraducir;
import es.uji.apps.gdo.model.QActividadAsignatura;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QAsignaturaPOD;
import es.uji.apps.gdo.model.QPendientesTraducir;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PendientesTraducirDAODatabaseImpl extends BaseDAODatabaseImpl implements
        PendientesTraducirDAO
{
    @Override
    public List<PendientesTraducir> getPendientes(Long cursoAca, String tipoEstudio)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPendientesTraducir pendientesTraducir = QPendientesTraducir.pendientesTraducir;

        query.from(pendientesTraducir).where(pendientesTraducir.enSlt.eq("S"));

        if (cursoAca != null)
        {
            query.where(pendientesTraducir.cursoAca.eq(cursoAca));
        }

        if (tipoEstudio != null && !tipoEstudio.isEmpty())
        {
            query.where(pendientesTraducir.tipoTitulacion.eq(tipoEstudio));
        }

        return query.list(pendientesTraducir);
    }
}
