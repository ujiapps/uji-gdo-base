package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.VerificaResultado;
import es.uji.commons.db.BaseDAO;

public interface VerificaResultadoDAO extends BaseDAO
{

    List<VerificaResultado> getVerificaResultadoByAsignatura(String codigoAsignatura, Long cursoAca);

}
