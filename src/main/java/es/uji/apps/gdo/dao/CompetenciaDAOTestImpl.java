package es.uji.apps.gdo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.gdo.model.Competencia;
import es.uji.commons.db.BaseDAOTestImpl;

public class CompetenciaDAOTestImpl extends BaseDAOTestImpl implements CompetenciaDAO
{
    private Competencia getCompetenciaTest(Long competenciaId, Long asignaturaId)
    {
        Competencia competenciaDB = new Competencia();
        competenciaDB.setId(competenciaId);
        competenciaDB.setCompetenciaCa("Texte competencia número " + competenciaId);
        competenciaDB.setCompetenciaEs("Texto competencia número " + competenciaId);
        competenciaDB.setCompetenciaUk("Competency text number " + competenciaId);
        competenciaDB.setEditable("S");
        competenciaDB.setOrden(new Long(competenciaId.intValue()));

        return competenciaDB;
    }

    @Override
    public Competencia getCompetencia(Long competenciaId)
    {
        return getCompetenciaTest(competenciaId, 242L);
    }

    @Override
    public List<Competencia> getCompetenciasByAsignatura(Long asignaturaId)
    {
        List<Competencia> result = new ArrayList<Competencia>();

        for (int i = 0; i < 9; i++)
        {
            result.add(getCompetenciaTest((long) i, asignaturaId));
        }

        return result;
    }

}