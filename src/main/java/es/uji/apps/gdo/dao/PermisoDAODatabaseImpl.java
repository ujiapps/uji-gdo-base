package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.Permiso;
import es.uji.apps.gdo.model.QAsignatura;
import es.uji.apps.gdo.model.QPermiso;
import es.uji.apps.gdo.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PermisoDAODatabaseImpl extends BaseDAODatabaseImpl implements PermisoDAO
{

    @Override
    public List<Permiso> getPermisosByAsignaturaId(Long asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPermiso asiPermisos = QPermiso.permiso;
        QAsignatura asignaturas = QAsignatura.asignatura;
        QPersona persona = QPersona.persona;

        query.from(asiPermisos).join(asiPermisos.gdoAsignatura, asignaturas).fetch()
                .join(asiPermisos.gdoExtPersona, persona).fetch()
                .where(asignaturas.id.eq(asignaturaId))
                .orderBy(persona.apellido1.asc(), persona.apellido2.asc(), persona.nombre.asc());

        return query.list(asiPermisos);
    }
}
