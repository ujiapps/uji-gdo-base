package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QVerificaEvaluacion;
import es.uji.apps.gdo.model.VerificaEvaluacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VerificaEvaluacionDAODatabaseImpl extends BaseDAODatabaseImpl implements
        VerificaEvaluacionDAO
{

    @Override
    public List<VerificaEvaluacion> getEvaluacionesByAsignatura(String codigoAsignatura,
            Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QVerificaEvaluacion verificaEvaluacion = QVerificaEvaluacion.verificaEvaluacion;

        query.from(verificaEvaluacion).where(
                verificaEvaluacion.codAsignatura.eq(codigoAsignatura).and(
                        verificaEvaluacion.cursoAcaIni.loe(cursoAca)));

        return query.list(verificaEvaluacion);
    }

}
