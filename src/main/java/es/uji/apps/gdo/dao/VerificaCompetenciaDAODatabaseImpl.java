package es.uji.apps.gdo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.gdo.model.QVerificaCompetencia;
import es.uji.apps.gdo.model.VerificaCompetencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VerificaCompetenciaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        VerificaCompetenciaDAO
{

    @Override
    public List<VerificaCompetencia> getVerificaCompetenciaByAsignatura(String codigoAsignatura,
            Long cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QVerificaCompetencia verificaCompetencia = QVerificaCompetencia.verificaCompetencia;

        query.from(verificaCompetencia).where(
                verificaCompetencia.codAsignatura.eq(codigoAsignatura).and(
                        verificaCompetencia.cursoAcaIni.loe(cursoAca)));

        return query.list(verificaCompetencia);
    }
}
