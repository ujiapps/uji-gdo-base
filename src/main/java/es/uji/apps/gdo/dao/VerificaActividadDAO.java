package es.uji.apps.gdo.dao;

import java.util.List;

import es.uji.apps.gdo.model.VerificaActividad;
import es.uji.commons.db.BaseDAO;

public interface VerificaActividadDAO extends BaseDAO
{
    public List<VerificaActividad> getVerificaActividadByAsignatura(String codigoAsignatura,
            Long cursoAca);
}
