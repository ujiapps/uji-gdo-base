Ext.ns('UJI.GDO');

UJI.GDO.Evaluaciones = Ext.extend(Ext.Panel,
{
    title : "Avaluacions",
    layout : 'vbox',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    asignatura : 0,
    cabecera : {},
    editor : {},
    descripStore : {},
    storeEvaluacion : {},
    storeEstadoPantalla : {},
    grid : {},
    botonInsert : {},
    botonDelete : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Evaluaciones.superclass.initComponent.call(this);

        this.descripStore = this.getDescripStore();
        this.storeEstadoPantalla = this.getStoreEstadoPantalla();
        this.storeEvaluacion = this.getStoreEvaluaciones();

        syncStoreLoad([ this.storeEstadoPantalla, this.descripStore, this.storeEvaluacion ]);

        this.initUI();

        this.add(this.cabecera);
        this.add(this.grid);
    },

    initUI : function()
    {
        this.cabecera = new Ext.Panel(
        {
            autoHeight : true,
            frame : true,
            flex : 0
        });

        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false
        });

        this.botonDelete = this.getBotonDelete();
        this.botonInsert = this.getBotonInsert();

        var ref = this;

        this.grid = this.getGridEvaluaciones();

        this.storeEstadoPantalla.on('load', function(store, records, options)
        {
            var resultado = records[0].data;
            if (resultado.allowDelete == "true")
            {
                ref.botonDelete.setDisabled(false);
            }
            if (resultado.allowInsert == "true")
            {
                ref.botonInsert.setDisabled(false);
            }
            if (resultado.allowUpdate == "false")
            {
                ref.grid.plugins[0].disable();
            }
            if (resultado.allowUpdateMaestro == "true")
            {
                ref.grid.colModel.columns[2].editor.enable();
            }
            else
            {
                ref.grid.colModel.columns[2].editor.disable();
            }
        });

    },

    getStoreEstadoPantalla : function()
    {
        var store = new Ext.data.Store(
        {
            url : '/gdo/rest/evaluacion/estado/' + this.asignatura,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecordUI',
                id : 'asignaturaId'
            },
                    [ 'asignaturaId', 'allowDelete', 'allowInsert', 'allowUpdate',
                            'allowUpdateMaestro' ])
        });

        return store;
    },

    getDescripStore : function()
    {

        var ref = this;

        var descripStore = new Ext.data.Store(
        {
            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/E',
            reader : new Ext.data.XmlReader(
            {
                record : 'textoAyudaFormulariosUI',
                id : 'asignatura'
            }, [ 'asignatura', 'texto' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>',
                            '<p style="margin-top:5px;">{texto}</p>');
                    var resultado = tpl.apply(records[0].data);
                    ref.cabecera.update(resultado);
                    ref.doLayout();
                }
            }
        });

        return descripStore;
    },

    getStoreEvaluaciones : function()
    {
        var ref = this;

        var reader = new Ext.data.XmlReader(
        {
            record : 'Evaluacion',
            idProperty : 'id'
        }, [
        {
            name : 'id',
            type : 'number'
        },
        {
            name : 'asignaturaId',
            type : 'number',
            allowBlank : false
        },
        {
            name : 'nombreCa',
            type : 'string',
            allowBlank : false
        },
        {
            name : 'nombreEs',
            type : 'string',
            allowBlank : false
        },
        {
            name : 'nombreUk',
            type : 'string',
            allowBlank : true
        },
        {
            name : 'ponderacion',
            type : 'number',
            allowBlank : false
        },
        {
            name : 'orden',
            type : 'number',
            allowBlank : false
        },
        {
            name : 'editable',
            type : 'bool',
            allowBlank : false
        } ]);

        var writer = new Ext.data.XmlWriter(
        {
            xmlEncoding : 'UTF-8',
            writeAllFields : true
        });

        var store = new Ext.data.Store(
        {
            restful : true,
            baseParams :
            {
                asignaturaId : this.asignatura
            },
            url : '/gdo/rest/evaluacion',
            reader : reader,
            writer : writer,
            listeners :
            {
                save : function(store, batch, data)
                {
                    this.reload();
                }
            }
        });

        return store;
    },

    getGridEvaluaciones : function()
    {
        var ref = this;

        this.editor.on(
        {
            beforeedit : function(roweditor, rowIndex)
            {
                return ref.storeEvaluacion.data.items[rowIndex].get("editable");
            }
        });

        var selectionModel = new Ext.grid.RowSelectionModel(
        {
            singleSelect : true
        });

        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            sm : selectionModel,
            plugins : [ this.editor ],
            store : this.storeEvaluacion,
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    header : 'Asignatura',
                    dataIndex : 'asignaturaId',
                    hidden : true
                },
                {
                    header : 'Nom Valencià',
                    dataIndex : 'nombreCa',
                    width : 70,
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : false,
                        disabled : true,
                        blankText : 'El camp "Nom Valencià" es requerit'
                    })
                },
                {
                    header : 'Nom Espanyol',
                    dataIndex : 'nombreEs',
                    width : 70,
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : false,
                        blankText : 'El camp "Nom Espanyol" es requerit'
                    })
                },
                {
                    header : 'Nom Anglés',
                    dataIndex : 'nombreUk',
                    width : 70,
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : true
                    })
                },
                {
                    xtype : 'numbercolumn',
                    format : '0.00',
                    header : 'Ponderació',
                    dataIndex : 'ponderacion',
                    width : 20,
                    editor : new Ext.form.NumberField(
                    {
                        allowBlank : false,
                        allowNegative : false,
                        blankText : 'El camp "Ponderació" es requerit'
                    })
                },
                {
                    header : 'Ordre',
                    dataIndex : 'orden',
                    width : 10,
                    editor : new Ext.form.NumberField(
                    {
                        allowBlank : false,
                        allowNegative : false,
                        blankText : 'El camp "Ordre" es requerit'
                    })

                } ]
            }),
            tbar :
            {
                items : [ this.botonInsert, '-', this.botonDelete ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, evt)
                {
                    var recordEvaluacion = grid.getStore().getAt(rowIndex);

                    if (ref.storeEstadoPantalla.data.items[0].data.allowDelete == "true")
                    {
                        ref.botonDelete.setDisabled(!recordEvaluacion.data.editable == "true");
                    }
                }
            }
        });

        return grid;
    },

    getBotonDelete : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = ref.grid.getSelectionModel().getSelected();

                if (!rec)
                {
                    return false;
                }

                ref.storeEvaluacion.remove(rec);
            }
        });

        return botonDelete;
    },

    getBotonInsert : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = new ref.storeEvaluacion.recordType(
                {
                    editable : true,
                    nombreCa : '',
                    nombreEs : '',
                    nombreUk : '',
                    id : '',
                    asignaturaId : ref.asignatura,
                    orden : '',
                    ponderacion : ''
                });

                ref.editor.stopEditing();
                ref.storeEvaluacion.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });

        return botonInsert;
    }
});