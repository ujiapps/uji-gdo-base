Ext.ns('UJI.GDO');

UJI.GDO.Idiomas = Ext.extend(Ext.Panel,
{
    title : 'Idiomes',
    layout : 'vbox',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    asignatura : 0,
    cabecera : {},
    descripStore : {},
    storeIdiomas : {},
    comboIdiomas : {},
    storeComboNoAsignados : {},
    panelComboIdiomas : {},
    comboIdiomasNoAsignados : {},
    readerGrid : {},
    storeGrid : {},
    gridIdiomas : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Idiomas.superclass.initComponent.call(this);

        this.descripStore = this.getDescripStore();
        this.storeIdiomas = this.getStoreIdiomas();
        this.storeComboNoAsignados = this.getStoreComboNoAsignados();
        this.readerGrid = this.getReaderGrid();
        this.storeGrid = this.getStoreGrid();

        syncStoreLoad([ this.descripStore, this.storeComboNoAsignados, this.storeIdiomas,
                this.storeGrid ]);

        this.cabecera = this.getCabecera();
        this.comboIdiomasNoAsignados = this.getComboIdiomasNoAsignados();
        this.panelComboIdiomas = this.getPanelComboIdiomas();

        this.comboIdiomas = this.getComboIdiomas();
        this.gridIdiomas = this.getGridIdiomas();

        this.add(this.cabecera);
        this.add(this.panelComboIdiomas);
        this.add(this.gridIdiomas);
    },

    getCabecera : function()
    {
        var cabecera = new Ext.Panel(
        {
            autoHeight : true,
            frame : true,
            flex : 0
        });

        return cabecera;
    },

    getDescripStore : function()
    {
        var ref = this;

        var descripStore = new Ext.data.Store(
        {
            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/I',
            reader : new Ext.data.XmlReader(
            {
                record : 'textoAyudaFormulariosUI',
                id : 'asignatura'
            }, [ 'asignatura', 'texto' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>',
                            '<p style="margin-top:5px;">{texto}</p>');
                    var resultado = tpl.apply(records[0].data);
                    ref.cabecera.update(resultado);
                    ref.doLayout();
                }
            }
        });

        return descripStore;
    },

    getStoreIdiomas : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/gdo/rest/idioma',
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                idProperty : 'id'
            }, [
            {
                name : 'id',
                type : 'number'
            },
            {
                name : 'nombreCa',
                type : 'string'
            } ])
        });

        return store;
    },

    getComboIdiomas : function()
    {

        var combo = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCa',
            valueField : 'id',
            store : this.storeIdiomas
        });

        return combo;
    },

    getStoreComboNoAsignados : function()
    {
        var store = new Ext.data.Store(
        {
            baseParams :
            {
                asignaturaId : this.asignatura
            },
            restful : true,
            url : '/gdo/rest/idioma/noasignados',
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                idProperty : 'id'
            }, [
            {
                name : 'id',
                type : 'number'
            },
            {
                name : 'nombreCa',
                type : 'string'
            } ])
        });

        return store;
    },

    getComboIdiomasNoAsignados : function()
    {
        var combo = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCa',
            valueField : 'id',
            width : 300,

            store : this.storeComboNoAsignados
        });

        return combo;
    },

    getPanelComboIdiomas : function()
    {
        var ref = this;

        var panel = new Ext.Panel(
        {
            layout : 'hbox',
            frame : true,
            items : [
                    {
                        xtype : 'label',
                        margins: '0 20 0 0',
                        text : 'Sel·lecciona un idioma docent: '
                    },
                    this.comboIdiomasNoAsignados,
                    {
                        xtype : 'button',
                        text : 'Afegir',
                        margins: '0 0 0 5',
                        handler : function(event)
                        {

                            var idIdiomaSelected = ref.comboIdiomasNoAsignados.getValue();
                            var recordComboIdioma = ref.comboIdiomasNoAsignados.store
                                    .getById(idIdiomaSelected);

                            var recordGridIdioma = new ref.gridIdiomas.store.recordType(
                            {
                                id : '',
                                idiomaId : recordComboIdioma.get('id'),
                                asignaturaId : ref.asignatura
                            });

                            ref.gridIdiomas.store.insert(0, recordGridIdioma);
                        }
                    } ]
        });

        return panel;
    },

    getStoreGrid : function()
    {
        var ref = this;

        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/gdo/rest/asignatura/' + this.asignatura + '/idiomas',
            reader : this.readerGrid,
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                save : function(store, batch, data)
                {
                    ref.comboIdiomasNoAsignados.clearValue();
                    ref.comboIdiomasNoAsignados.store.reload();
                    ref.gridIdiomas.store.reload();
                }
            }
        });

        return store;
    },

    getReaderGrid : function()
    {
        var reader = new Ext.data.XmlReader(
        {
            record : 'IdiomaAsignatura',
            id : 'id'
        }, [ 'id', 'idiomaId', 'asignaturaId' ]);

        return reader;
    },

    getGridIdiomas : function()
    {

        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storeGrid,
            colModel : new Ext.grid.ColumnModel(
            {
                defaults :
                {
                    sortable : true
                },
                columns : [
                {
                    header : 'Idioma',
                    dataIndex : 'idiomaId',
                    editor : this.comboIdiomas,
                    renderer : Ext.util.Format.comboRenderer(this.comboIdiomas)
                },
                {
                    hidden : true,
                    dataIndex : 'asignaturaId'
                } ]
            }),
            tbar :
            {
                items : [
                {
                    xtype : 'button',
                    text : 'Esborrar',
                    iconCls : 'application-delete',
                    handler : function(btn, evt)
                    {
                        var rec = grid.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        grid.store.remove(rec);
                    }
                }

                ]
            }
        });

        return grid;
    }

});
