Ext.ns('UJI.GDO');

UJI.GDO.Apartados = Ext.extend(Ext.Panel,
{
    title : 'Apartats',
    closable : true,
    layout : 'vbox',
    autoScroll : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    asignatura : 0,
    storeAsignatura : {},
    storeApartados : {},
    comboIdiomas : {},
    comboApartados : {},
    formConsulta : {},
    panelMultiLang : {},
    formTextos : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Apartados.superclass.initComponent.call(this);

        this.storeAsignatura = this.getStoreAsignatura();
        this.storeApartados = this.getStoreApartados();

        syncStoreLoad([ this.storeAsignatura, this.storeApartados ]);

        this.initUI();

        this.add(this.formConsulta);
        this.add(this.formTextos);
    },

    initUI : function()
    {
        this.comboIdiomas = this.getComboIdiomas();
        this.comboApartados = this.getComboApartados();
        this.formConsulta = this.getFormConsulta();
        this.panelMultiLang = this.getPanelMultiLang();
        this.formTextos = this.getFormTextos();
    },

    getStoreAsignatura : function()
    {
        var ref = this;

        var store = new Ext.data.Store(
        {
            url : '/gdo/rest/asignatura/' + this.asignatura,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Asignatura',
                id : 'id'
            }, [ 'nombre' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    ref.formConsulta.getForm().setValues(
                    {
                        asignatura : records[0].get("nombre")
                    });
                }
            }
        });

        return store;
    },

    getStoreApartados : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/gdo/rest/apartado?asignaturaId=' + this.asignatura + '&tipoApartado=T',
            reader : new Ext.data.XmlReader(
            {
                record : 'Apartado',
                id : 'id'
            }, [ 'id', 'nombreCa', 'textoAyuda', 'descripcion' ])
        });

        return store;
    },

    pintaFormEdicion : function(record)
    {
        var apartadoId = this.comboApartados.value;

        if (this.comboApartados.value != null && this.comboIdiomas.value != null)
        {
            var form = this.formTextos.getForm();

            this.formConsulta.getForm().setValues(
            {
                'textoAyuda' : record.get("textoAyuda")
            });

            form.load(
            {
                url : '/gdo/rest/texto?asignaturaId=' + this.asignatura + '&apartadoId=' + apartadoId,
                method : 'get'
            });

            if (this.comboIdiomas.value != 99)
            {
                for ( var i = 0; i < 3; i++)
                {
                    this.panelMultiLang.hideTabStripItem(i);
                }

                this.panelMultiLang.unhideTabStripItem(this.comboIdiomas.value);
                this.panelMultiLang.setActiveTab(this.comboIdiomas.value);
            }
            else
            {
                for ( var i = 0; i < 3; i++)
                {
                    this.panelMultiLang.unhideTabStripItem(i);
                }
                this.panelMultiLang.setActiveTab(0);
            }

            if (this.comboApartados.value != null && this.comboIdiomas.value != null)
            {
                this.panelMultiLang.setDisabled(false);
            }

        }
    },

    getComboIdiomas : function()
    {
        var ref = this;

        return new Ext.ux.uji.form.StaticComboBox(
        {
            fieldLabel : 'Idioma',
            hiddenName : 'idiomas',
            staticData : [ [ 0, 'Valencià' ], [ 1, 'Espanyol' ], [ 2, 'Anglés' ], [ 99, 'Tots els idiomes' ] ],
            listeners :
            {
                select : function(combo, record, index)
                {
                    if (ref.comboApartados.value != undefined && ref.comboApartados.value != '' && combo.value != undefined)
                    {
                        var rec = ref.comboApartados.store.getById(ref.comboApartados.value);
                        ref.pintaFormEdicion(rec);
                    }
                }
            }
        });
    },

    getComboApartados : function()
    {
        var ref = this;

        var comboApartados = new Ext.form.ComboBox(
        {
            emptyText : ' Selecciona un apartat ...',
            triggerAction : 'all',
            editable : false,
            forceSelection : true,
            anchor : '50%',
            hiddenName : 'descripcion',
            fieldLabel : 'Apartat',
            displayField : 'descripcion',
            valueField : 'id',
            store : this.storeApartados,
            listeners :
            {
                select : function(combo, record, index)
                {
                    if (combo.value != undefined && ref.comboIdiomas.value != undefined)
                    {
                        ref.pintaFormEdicion(record);
                    }
                    else
                    {
                        Ext.Msg.alert("Error", "Per a editar, primer cal seleccionar un idioma i un apartat");
                    }
                }
            }
        });

        return comboApartados;
    },

    getFormConsulta : function()
    {
        var formConsulta = new Ext.FormPanel(
        {
            name : 'formSelect',
            frame : true,
            flex : 0,
            height : 120,
            defaults :
            {
                margins : '10 10 10 0'
            },
            items : [
            {
                xtype : 'displayfield',
                name : 'asignatura',
                fieldLabel : 'Editant guía',
                height : '20'
            }, this.comboIdiomas, this.comboApartados,
            {
                xtype : 'displayfield',
                name : 'textoAyuda',
                value : '_'
            } ]
        });

        return formConsulta;
    },

    getPanelMultiLang : function()
    {
        var ref = this;

        var baseToolbar = [ [ 'Source' ], '-', [ 'Cut', 'Copy', 'PasteFromWord' ], [ 'Undo', 'Redo' ], '-', [ 'Bold', 'Italic', 'Underline', 'Strike' ], '-', [ 'NumberedList', 'BulletedList' ] ];

        var ca = new Ext.form.CKEditor(
        {
            name : 'textoCa',
            CKConfig :
            {
                height : '250',
                toolbar : baseToolbar
            },
            enableColors : false,
            enableAlignments : false
        });

        var es = new Ext.form.CKEditor(
        {
            name : 'textoEs',
            CKConfig :
            {
                height : '250',
                toolbar : baseToolbar
            },
            enableColors : false,
            enableAlignments : false
        });

        var uk = new Ext.form.CKEditor(
        {
            name : 'textoUk',
            CKConfig :
            {
                height : '250',
                toolbar : baseToolbar
            },
            enableColors : false,
            enableAlignments : false
        });

        var multiLang = new Ext.TabPanel(
        {
            activeTab : 0,
            flex : 1,
            enableTabScroll : true,
            deferredRender : false,
            height : 400,
            disabled : true,
            buttonAlign : 'center',
            items : [
            {
                title : 'Valencià',
                name : 'ca',
                frame : true,
                layout : 'fit',
                items : [ ca ]
            },
            {
                title : 'Espanyol',
                name : 'es',
                frame : true,
                layout : 'fit',
                items : [ es ]
            },
            {
                title : 'Anglés',
                name : 'uk',
                frame : true,
                layout : 'fit',
                items : [ uk ]
            } ],
            buttons : [
            {
                xtype : 'button',
                text : 'Guardar',
                width : '80',
                height : '20',
                pressed : false,
                handler : function(button, event)
                {
                    if (ref.formTextos.getForm().isValid())
                    {
                        var textoId = ref.formTextos.getForm().getValues().id;
                        ref.formTextos.getForm().submit(
                        {
                            method : 'PUT',
                            url : '/gdo/rest/texto/' + textoId,
                            failure : function()
                            {
                                alert("Error al guardar els textes.");
                            },
                            success : function()
                            {
                            }
                        });
                    }
                    else
                    {
                        alert("mal");
                    }
                }
            } ]
        });

        return multiLang;
    },

    getFormTextos : function()
    {
        var ref = this;

        var formTextos = new Ext.FormPanel(
        {
            name : 'textos',
            autoScroll : true,
            flex : 1,
            frame : true,
            url : '/gdo/rest/texto/',
            reader : new Ext.data.XmlReader(
            {
                record : 'Texto',
                id : 'id'
            }, [ 'id', 'asignaturaId', 'apartadoId', 'textoCa', 'textoEs', 'textoUk', 'esModificable' ]),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Texto',
                id : 'id',
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }, [ 'id', 'asignaturaId', 'apartadoId', 'textoCa', 'textoEs', 'textoUk', 'esModificable' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'respuestaForm',
                success : 'success'
            }, [ 'success', 'msg' ]),
            items : [
            {
                xtype : 'hidden',
                name : 'id'
            },
            {
                xtype : 'hidden',
                name : 'asignaturaId'
            },
            {
                xtype : 'hidden',
                name : 'apartadoId'
            },
            {
                xtype : 'hidden',
                name : 'esModificable'
            }, this.panelMultiLang ],
            listeners :
            {
                actioncomplete : function(form, action)
                {
                    if (action.type == 'load')
                    {
                        if (form.getValues().esModificable == 'false')
                        {
                            ref.panelMultiLang.setDisabled(true);
                        }
                        else
                        {
                            ref.panelMultiLang.setDisabled(false);
                        }
                    }
                }
            }
        });

        return formTextos;
    }
});