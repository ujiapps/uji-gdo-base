Ext.ns('UJI.GDO');

UJI.GDO.Cursos = Ext.extend(Ext.Panel,
{
    title : "Cursos",
    layout : 'vbox',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    editor : {},
    storeCursos : {},
    gridCursos : {},
    cursoActivo : {},
    panelSuperior : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Cursos.superclass.initComponent.call(this);

        this.panelSuperior = this.getPanelSuperior();
        this.editor = this.getEditor();
        this.storeCursos = this.getStoreCursos();
        this.gridCursos = this.getGridCursos();

        this.add(this.panelSuperior);
        this.add(this.gridCursos);
    },

    getPanelSuperior : function()
    {
        var panel = new Ext.Panel(
        {
            flex : 0,
            height : 33,
            frame : true,
            html : '<h1 style="font-size:150%">Gestió de cursos</h1>'
        });
        return panel;
    },

    getEditor : function()
    {
        var editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false
        });

        return editor;
    },

    getStoreCursos : function()
    {
        var ref = this;
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/gdo/rest/curso',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoAcademico',
                idProperty : 'cursoAca'
            }, [
            {
                name : 'cursoAca',
                type : 'number'
            },
            {
                name : 'activo',
                convert : function(value, record)
                {
                    return (value == 'true') ? 'Actiu' : 'Inactiu';
                }
            },
            {
                name : 'fechaInicioVisibilidad',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'fechaFinVisibilidad',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'asignaturasCurso',
                type : 'number'
            } ]),
            writer : new Ext.ux.uji.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }, [ 'cursoAca',
            {
                name : 'activo',
                convert : function(value, record)
                {
                    return (value == 'Actiu') ? true : false;
                }
            }, 'fechaInicioVisibilidad', 'fechaFinVisibilidad', 'asignaturasCurso' ]),
            listeners :
            {
                update : function(store, rec, operation)
                {
                    if (rec.data.fechaFinVisibilidad == null
                            || rec.data.fechaInicioVisibilidad == null)
                    {
                        alert('Tingueu en compte que la data Inici i data fi son necessaries');
                    }
                    else if (rec.data.fechaInicioVisibilidad > rec.data.fechaFinVisibilidad)
                    {
                        alert('Tingueu en compte que la data Fi ha de ser posterior '
                                + 'a la data Inici');
                    }
                },
                load : function(store, record)
                {
                    ref.cursoActivo = null;
                    Ext.each(record, function(rec)
                    {
                        if (rec.data.activo)
                        {
                            ref.cursoActivo = rec.id;
                        }
                    });
                }
            }
        });

        return store;
    },

    recargaSiCambiaActivo : function(data)
    {
        if ((data.update[0].activo) && (data.update[0].id != this.cursoActivo))
        {
            alert('Atenció! Curs actiu canviat. Es recarregarà la pàgina.');
            window.location.reload();
        }
    },

    getGridCursos : function()
    {
        var ref = this;
        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : [ this.editor ],
            store : this.storeCursos,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'Curs',
                    dataIndex : 'cursoAca'
                },
                {
                    header : 'Estat',
                    dataIndex : 'activo'
                },
                {
                    xtype : 'datecolumn',
                    header : 'Inici visibilitat',
                    dataIndex : 'fechaInicioVisibilidad',
                    format : 'd/m/Y',
                    editor : new Ext.form.DateField(
                    {
                        allowBlank : false,
                        format : 'd/m/Y',
                        blankText : 'El camp "Inici visibilitat" es requerit'
                    })
                },
                {
                    xtype : 'datecolumn',
                    header : 'Fi visibilitat',
                    dataIndex : 'fechaFinVisibilidad',
                    format : 'd/m/Y',
                    editor : new Ext.form.DateField(
                    {
                        allowBlank : false,
                        format : 'd/m/Y',
                        blankText : 'El camp "Fi visibilitat" es requerit'
                    })
                } ]
            }),
            tbar :
            {
                items : [
                {
                    xtype : 'button',
                    text : 'Activar curs',
                    iconCls : 'wrench',
                    handler : function(btn, evt)
                    {
                        var rec = ref.gridCursos.getSelectionModel().getSelected();
                        
                        if (!rec)
                        {
                            Ext.Msg.alert('Activar curs', 'Has de seleccionar un curs.');
                            return false;
                        }
                        
                        var cursoAca = rec.data.cursoAca;
                        
                        Ext.Ajax.request(
                        {
                            url : '/gdo/rest/curso/activar/' + cursoAca,
                            method : 'PUT',
                            success : function(result, request)
                            {
                                var xml = result.responseXML;
                                var guias = xml.getElementsByTagName('asignaturasCurso')[0];
                                var activo = xml.getElementsByTagName('activo')[0];

                                if (guias.textContent == '0' && activo.textContent)
                                {
                                    Ext.MessageBox.show(
                                    {
                                        title : 'Copiar assignatures?',
                                        msg : 'Atenció!\n ' + 'El curs no te guies.'
                                                + ' Vols copiar les de l\'any anterior?',
                                        buttons : Ext.Msg.YESNO,
                                        icon : Ext.MessageBox.QUESTION,
                                        fn : function(button, text, opt)
                                        {
                                            if (button == 'yes')
                                            {
                                                var barraProgreso = Ext.MessageBox.show(
                                                {
                                                    msg : 'Copiant guies de l\'any anterior'
                                                            + ', per favor espera...',
                                                    width : 300,
                                                    wait : true,
                                                    waitConfig :
                                                    {
                                                        interval : 100
                                                    }
                                                });
                                                ref.lanzaCopiaAsignaturas(cursoAca, barraProgreso);

                                            }
                                            else
                                            {
                                                ref.recargaVentana(cursoAca);
                                            }

                                        }
                                    });

                                }
                                else
                                {
                                    ref.recargaVentana(cursoAca);
                                }
                            }
                        });
                    }
                } ]
            }
        });

        return grid;
    },

    recargaVentana : function(cursoAca)
    {
        Ext.MessageBox.alert('Atencio!!!', 'Atenció!\nEl curs acadèmic es canviarà a ' + cursoAca
                + '.\nEs recarregarà la pàgina.', function(btn, text)
        {
            if (btn = 'ok')
            {
                window.location.reload();
            }
        });
    },

    lanzaCopiaAsignaturas : function(cursoActivado, barraProgreso)
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/gdo/rest/copiacurso/' + cursoActivado,
            method : 'POST',
            success : function(result, request)
            {
                barraProgreso.hide();
                Ext.MessageBox.alert('Atencio!!!', 'La copia de guies ha finalitzat correctament.',
                        function(btn, text)
                        {
                            if (btn = 'ok')
                            {
                                ref.recargaVentana(cursoActivado);
                            }
                        });
            },
            failure : function()
            {
                Ext.MessageBox.alert('Atencio!!!', 'Els curs acadèmic s\'ha activat '
                        + 'però la copia de guies ha fallat.', function(btn, text)
                {
                    if (btn = 'ok')
                    {
                        ref.recargaVentana(cursoActivado);
                    }
                });
            }
        });
    }
});