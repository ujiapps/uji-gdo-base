Ext.ns('UJI.GDO');

UJI.GDO.Delegaciones = Ext.extend(Ext.Panel,
{
    title : 'Delegació de permisos',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    asignatura : 0,
    comboPersonas : {},
    storeDelegaciones : {},
    storePersonas : {},
    cabeceraDelegaciones : {},
    gridDelegaciones : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Delegaciones.superclass.initComponent.call(this);

        this.storeDelegaciones = this.getStoreDelegaciones();
        this.storePersonas = this.getStorePersonas();

        syncStoreLoad([ this.storePersonas, this.storeDelegaciones ]);

        this.comboPersonas = this.getComboPersonas();
        this.cabeceraDelegaciones = this.getCabeceraDelegaciones();
        this.gridDelegaciones = this.getGridDelegaciones();

        this.add(this.cabeceraDelegaciones);
        this.add(this.gridDelegaciones);

    },

    getCabeceraDelegaciones : function()
    {
        var ref = this;

        var cabeceraDelegaciones = new Ext.Panel(
        {
            frame : true,
            autoHeight : true,
            flex : 0
        });

        var descripStore = new Ext.data.Store(
        {
            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/I',
            reader : new Ext.data.XmlReader(
            {
                record : 'textoAyudaFormulariosUI',
                id : 'asignatura'
            }, [ 'asignatura' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>');
                    var resultado = tpl.apply(records[0].data);
                    cabeceraDelegaciones.update(resultado);
                    ref.doLayout();
                }
            }
        });

        descripStore.load();
        return cabeceraDelegaciones;
    },

    getStoreDelegaciones : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            baseParams :
            {
                asignaturaId : this.asignatura
            },
            url : '/gdo/rest/permiso',
            reader : new Ext.data.XmlReader(
            {
                record : 'Permiso',
                id : 'id'
            }, [ 'id', 'personaId', 'nombre', 'asignaturaId' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                save : function(store, batch, data)
                {
                    if (data.destroy != null)
                    {
                        this.reload();
                    }
                },
                write : function(store, action, result, res, rs)
                {
                    if (action != 'destroy')
                    {
                        this.reload();
                    }
                }
            }
        });

        return store;
    },

    getStorePersonas : function()
    {
        var ref = this;

        var dsPersonas = new Ext.data.Store(
        {
            url : '/gdo/rest/persona/',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Persona',
                id : 'id'
            }, [
            {
                name : 'id',
                type : 'number'
            }, 'nombreCompleto' ]),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Persona',
                id : 'id'
            }, [ 'id', 'nombre' ])
        });

        return dsPersonas;
    },

    getComboPersonas : function()
    {
        return new Ext.ux.uji.form.LookupComboBox(
        {
            anchor : '50%',
            appPrefix : 'gdo',
            bean : 'persona'
        });
    },

    getGridDelegaciones : function()
    {
        var ref = this;

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = ref.storeDelegaciones.getAt(ref.storeDelegaciones.findExact(
                        'personaId', value));
                var valueNotFound = combo.valueNotFoundText ? combo.valueNotFoundText : value;

                return record ? record.get("nombre") : valueNotFound;
            };
        };

        var editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Guarda',
            cancelText : 'Cancel·la',
            floating :
            {
                zindex : 7000
            },
            listeners :
            {
                beforeedit : function(ed, rowIndex)
                {
                    if (grid.getSelectionModel().getSelections().length == 0)
                    {
                        return;
                    }

                    ref.comboPersonas.loadRecord(grid.getSelectionModel().getSelections()[0]
                            .get('personaId'), grid.getSelectionModel().getSelections()[0]
                            .get('nombre'));

                },
                afteredit : function(ed, rowIndex, changes)
                {
                    ed.disable();
                },
                canceledit : function(ed, rowIndex, changes)
                {
                    ed.disable();
                    ref.storeDelegaciones.reload();
                }
            }
        });

        var userColumns = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : 'id',
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Persona',
                dataIndex : 'personaId',
                editor : this.comboPersonas,
                renderer : Ext.util.Format.comboRenderer(this.comboPersonas)
            },
            {
                header : 'asignaturaId',
                hidden : true,
                dataIndex : 'asignaturaId'
            } ]
        });

        var asignaturaInsert = this.asignatura;

        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storeDelegaciones,
            frame : true,
            border : false,
            plugins : [ editor ],
            colModel : userColumns,
            tbar :
            {
                items : [
                {
                    xtype : 'button',
                    text : 'Afegir',
                    iconCls : 'application-add',
                    handler : function(btn, evt)
                    {
                        var rec = new grid.store.recordType(
                        {
                            asignaturaId : asignaturaInsert,
                            personaId : '',
                            id : ''
                        });

                        grid.plugins.push(editor);
                        editor.enable();
                        editor.stopEditing();
                        grid.store.insert(0, rec);
                        editor.startEditing(0);
                    }
                }, '-',
                {
                    xtype : 'button',
                    text : 'Esborrar',
                    iconCls : 'application-delete',
                    handler : function(btn, evt)
                    {
                        var rec = grid.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        grid.store.remove(rec);
                    }
                }

                ]
            },
            listeners :
            {
                render : function()
                {
                    editor.disable();
                }
            }
        });

        return grid;
    }

});