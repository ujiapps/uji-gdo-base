Ext.ns('UJI.GDO');

UJI.GDO.Competencias = Ext
        .extend(
                Ext.Panel,
                {
                    title : 'Competencies',
                    layout : 'vbox',
                    closable : true,
                    layoutConfig :
                    {
                        align : 'stretch'
                    },
                    asignatura : 0,
                    storeEstadoPantalla : {},
                    descripStore : {},
                    storeCompetencias : {},
                    panelCabecera : {},
                    selectionModel : {},
                    editor : {},
                    botonDelete : {},
                    botonInsert : {},
                    botonEditar : {},
                    botonGuardar : {},
                    botonCerrar : {},
                    cabeceraModal : {},
                    descripModal : {},
                    panelMultiLang : {},
                    panelesCompetencias : {},
                    gridCompetencias : {},
                    formCompetencias : {},
                    multiLangCompetencia : {},

                    initComponent : function()
                    {
                        var config = {};
                        Ext.apply(this, Ext.apply(this.initialConfig, config));

                        UJI.GDO.Competencias.superclass.initComponent.call(this);

                        this.panelCabecera = this.getPanelCabecera();

                        this.storeEstadoPantalla = this.getStoreEstadoPantalla();
                        this.descripStore = this.getDescripStore(this.panelCabecera);
                        this.storeCompetencias = this.getStoreCompetencias();

                        syncStoreLoad([ this.storeEstadoPantalla, this.descripStore,
                                this.storeCompetencias ]);

                        this.initUI();

                        this.add(this.panelCabecera);
                        this.add(this.gridCompetencias);
                    },

                    initUI : function()
                    {
                        this.selectionModel = new Ext.grid.RowSelectionModel(
                        {
                            singleSelect : true
                        });

                        this.editor = new Ext.ux.grid.RowEditor(
                        {
                            saveText : 'Guarda',
                            cancelText : 'Cancel·la',
                            errorSummary : false
                        });

                        this.botonDelete = this.getBotonDelete();
                        this.botonInsert = this.getBotonInsert();
                        this.botonEditar = this.getBotonEditar();

                        this.gridCompetencias = this.getGridCompetencias();

                    },

                    getStoreEstadoPantalla : function()
                    {
                        var ref = this;

                        var storeEstadoPantalla = new Ext.data.Store(
                        {
                            url : '/gdo/rest/competencia/estado/' + this.asignatura,
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'RecordUI',
                                id : 'asignaturaId'
                            }, [ 'asignaturaId', 'modificable' ]),
                            listeners :
                            {
                                load : function(store, records, options)
                                {
                                    ref.botonEditar.enable();
                                    var competencia = records[0].data;
                                    if (competencia.modificable == "false")
                                    {
                                        ref.botonDelete.disable();
                                        ref.botonInsert.disable();
                                    }
                                    else
                                    {
                                        ref.botonDelete.enable();
                                        ref.botonInsert.enable();
                                    }

                                }
                            }
                        });

                        return storeEstadoPantalla;
                    },

                    getStoreCompetencias : function()
                    {
                        var dsCompetencias = new Ext.data.Store(
                        {
                            baseParams :
                            {
                                asignaturaId : this.asignatura
                            },
                            restful : true,
                            url : '/gdo/rest/competencia',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'Competencia',
                                id : 'id'
                            }, [ 'asignaturaId', 'competenciaCa', 'competenciaCaBreve',
                                    'competenciaEs', 'competenciaEsBreve', 'competenciaUk',
                                    'competenciaUkBreve', 'editable' ]),
                            writer : new Ext.data.XmlReader(
                            {
                                record : 'Competencia',
                                id : 'id'
                            }, [ 'asignaturaId', 'competenciaCa', 'competenciaCaBreve',
                                    'competenciaEs', 'competenciaEsBreve', 'competenciaUk',
                                    'competenciaUkBreve' ]),
                            listeners :
                            {
                                save : function(store, batch, data)
                                {
                                    if (data.destroy != null)
                                    {
                                        this.reload();
                                    }
                                }
                            }
                        });

                        return dsCompetencias;
                    },

                    getDescripStore : function(panel)
                    {
                        var ref = this;

                        var descripStore = new Ext.data.Store(
                        {
                            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/C',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'textoAyudaFormulariosUI',
                                id : 'asignatura'
                            }, [ 'asignatura', 'texto' ]),
                            listeners :
                            {
                                load : function(store, records, options)
                                {
                                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>',
                                            '<p style="margin-top:5px;">{texto}</p>');
                                    var competencia = tpl.apply(records[0].data);
                                    panel.update(competencia);
                                    ref.doLayout();
                                }
                            }
                        });

                        return descripStore;
                    },

                    getPanelCabecera : function()
                    {
                        var panel = new Ext.Panel(
                        {
                            frame : true,
                            autoHeight : true,
                            flex : 0
                        });

                        return panel;
                    },

                    getPanelMultiLang : function()
                    {
                        this.multiLangCompetencia = new Ext.ux.uji.LanguagePanel(
                        {
                            flex : 1,
                            language : [
                            {
                                lang : 'Ca',
                                title : 'Valencià'
                            },
                            {
                                lang : 'Es',
                                title : 'Espanyol'
                            },
                            {
                                lang : 'Uk',
                                title : 'Anglés'
                            } ]
                        });

                        this.multiLangCompetencia.addComponent(new Ext.form.TextArea(
                        {
                            name : 'competencia',
                            fieldLabel : 'Competència',
                            anchor : '100%',
                            height : '210',
                            maxLength : '4000',
                            readOnly: 'true',
                            maxLengthText : "El quadre"
                                    + " de texte no pot superar els 4000 caràcters"
                        }));

                        var panelMultiLang = new Ext.Panel(
                        {
                            frame : true,
                            name : 'pestanyas',
                            layout : 'form',
                            anchor : '100%',
                            defaults :
                            {
                                margins : '0 10 0 0'
                            },
                            items : [ this.multiLangCompetencia ]
                        });

                        return panelMultiLang;
                    },

                    getBotonDelete : function()
                    {
                        var ref = this;

                        var botonDelete = new Ext.Button(
                        {
                            text : 'Esborrar',
                            iconCls : 'application-delete',
                            disabled : true,
                            handler : function(btn, evt)
                            {
                                var rec = ref.selectionModel.getSelected();
                                if (!rec)
                                {
                                    return false;
                                }
                                ;
                                ref.storeCompetencias.remove(rec);
                            }
                        });
                        return botonDelete;
                    },

                    getBotonInsert : function()
                    {
                        var ref = this;

                        var boton = new Ext.Button(
                        {
                            text : 'Afegir',
                            iconCls : 'application-add',
                            disabled : true,
                            listeners :
                            {
                                click : function(btn, evt)
                                {
                                    ref.ventanaModal = ref.getVentanaModal();

                                    ref.selectionModel.clearSelections();

                                    ref.botonGuardar.enable();

                                    ref.formCompetencias.getForm().setValues(
                                    {
                                        id : '',
                                        editable : 'S',
                                        asignaturaId : ref.asignatura,
                                        competenciaCa : '',
                                        competenciaEs : '',
                                        competenciaUk : ''
                                    });

                                    ref.storeCompetencias.baseParams.asignaturaId = ref.asignatura;

                                    ref.ventanaModal.show();
                                }
                            }
                        });

                        return boton;
                    },

                    getBotonGuardar : function()
                    {
                        var ref = this;

                        var boton = new Ext.Button(
                                {
                                    text : 'Guardar',
                                    name : 'guardar',
                                    width : '50',
                                    height : '20',
                                    pressed : false,
                                    disabled : true,
                                    listeners :
                                    {
                                        click : function(btn, evt)
                                        {
                                            if (ref.formCompetencias.getForm().isValid())
                                            {
                                                if (!ref.formCompetencias.getForm().getValues().competenciaCa
                                                        && !ref.formCompetencias.getForm()
                                                                .getValues().competenciaEs
                                                        && !ref.formCompetencias.getForm()
                                                                .getValues().competenciaUk)
                                                {
                                                    Ext.Msg.alert('Error',
                                                            'No es pot guardar un registre buit');
                                                }
                                                else
                                                {
                                                    if ((ref.formCompetencias.getForm().getValues().id))
                                                    {
                                                        ref.formCompetencias
                                                                .getForm()
                                                                .submit(
                                                                        {
                                                                            method : 'PUT',
                                                                            url : '/gdo/rest/competencia/'
                                                                                    + ref.formCompetencias
                                                                                            .getForm()
                                                                                            .getValues().id,
                                                                            success : function(
                                                                                    form, action)
                                                                            {
                                                                                ref.gridCompetencias.store.proxy.conn.url = '/'
                                                                                        + 'gdo/rest/competencia';
                                                                                ref.gridCompetencias.store
                                                                                        .reload();
                                                                            },
                                                                            failure : function(
                                                                                    form, action)
                                                                            {
                                                                                Ext.Msg
                                                                                        .alert(
                                                                                                'Error',
                                                                                                'Error en l\'actualització');
                                                                            }
                                                                        });
                                                    }
                                                    else
                                                    {
                                                        ref.formCompetencias
                                                                .getForm()
                                                                .submit(
                                                                        {
                                                                            method : 'POST',
                                                                            url : '/gdo/rest/competencia/',
                                                                            success : function(
                                                                                    form, action)
                                                                            {

                                                                                ref.gridCompetencias.store.proxy.conn.url = '/gdo/rest/competencia';
                                                                                ref.gridCompetencias.store
                                                                                        .reload();

                                                                                ref.formCompetencias
                                                                                        .getForm()
                                                                                        .setValues(
                                                                                                {
                                                                                                    id : Ext.DomQuery
                                                                                                            .selectValue(
                                                                                                                    'id',
                                                                                                                    action.response.responseXML)
                                                                                                });
                                                                            },
                                                                            failure : function(
                                                                                    form, action)
                                                                            {
                                                                                Ext.Msg
                                                                                        .alert(
                                                                                                'Error',
                                                                                                'Error en l\'actualització');
                                                                            }
                                                                        });

                                                    }

                                                }
                                            }
                                            else
                                            {
                                                Ext.Msg.alert("Error",
                                                        "Revise els valors del camps.");
                                            }
                                        }
                                    }
                                });

                        return boton;
                    },

                    getGridCompetencias : function()
                    {
                        var ref = this;

                        var gridCompetencias = new Ext.grid.GridPanel(
                                {
                                    flex : 1,
                                    loadMask : true,
                                    viewConfig :
                                    {
                                        forceFit : true
                                    },
                                    sm : this.selectionModel,
                                    frame : true,
                                    border : false,
                                    store : this.storeCompetencias,
                                    colModel : new Ext.grid.ColumnModel(
                                    {
                                        defaults :
                                        {
                                            sortable : true
                                        },
                                        columns : [
                                        {
                                            header : "id",
                                            hidden : true,
                                            dataIndex : 'id'
                                        },
                                        {
                                            header : 'Competència Valencià',
                                            dataIndex : 'competenciaCaBreve',
                                            resizable : false
                                        },
                                        {
                                            header : 'Competència Espanyol',
                                            dataIndex : 'competenciaEsBreve',
                                            resizable : false
                                        },
                                        {
                                            header : 'Competència Anglés',
                                            dataIndex : 'competenciaUkBreve',
                                            resizable : false
                                        }, ]
                                    }),
                                    tbar :
                                    {
                                        items : [ this.botonInsert, '-', this.botonDelete, '-',
                                                this.botonEditar ]
                                    },
                                    listeners :
                                    {
                                        rowclick : function(grid, rowIndex, evt)
                                        {
                                            if (grid.selModel.selections.keys.length > 0)
                                            {
                                                var id = grid.selModel.getSelected().id;
                                                if (!grid.selModel.getSelected().dirty)
                                                {
                                                    if (grid.selModel.getSelected().data.editable == 'S'
                                                            && ref.storeEstadoPantalla.data.items[0].data.modificable == "true")
                                                    {
                                                        ref.botonDelete.enable();
                                                        ref.botonEditar.enable();
                                                        ref.botonInsert.enable();
                                                    }
                                                    else if (grid.selModel.getSelected().data.editable == 'N'
                                                            && ref.storeEstadoPantalla.data.items[0].data.modificable == "true")
                                                    {
                                                        ref.botonDelete.disable();
                                                        ref.botonEditar.enable();
                                                        ref.botonInsert.enable();
                                                    }
                                                    else
                                                    // grados
                                                    {
                                                        ref.botonDelete.disable();
                                                        ref.botonEditar.enable();
                                                        ref.botonInsert.disable();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Ext.Msg.alert('Error', 'Per a editar,'
                                                        + ' primer cal fer clic en la fila');
                                            }
                                        },
                                        rowdblclick : function(grid, rowIndex, event)
                                        {
                                            ref.abreVentanaModal();

                                        }
                                    }
                                });

                        return gridCompetencias;
                    },

                    abreVentanaModal : function()
                    {
                        this.ventanaModal = this.getVentanaModal();
                        this.ventanaModal.show();
                        this.cargaCompetencias();
                    },

                    getFormCompetencias : function()
                    {
                        var formCompetencias = new Ext.FormPanel(
                        {
                            frame : true,
                            flex : 1,
                            name : 'competencies',
                            url : '/gdo/rest/competencia/',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'Competencia',
                                id : 'id'
                            }, [ 'id', 'competenciaCa', 'competenciaEs', 'competenciaUk',
                                    'editable', 'orden', 'codigoAsignatura', 'asignaturaId' ]),
                            writer : new Ext.data.XmlWriter(
                            {
                                record : 'Competencia',
                                writeAllFields : true,
                                id : 'id'
                            }, [ 'id', 'competenciaCa', 'competenciaEs', 'competenciaUk',
                                    'editable', 'orden', 'codigoAsignatura', 'asignaturaId' ]),
                            errorReader : new Ext.data.XmlReader(
                            {
                                record : 'respuestaForm',
                                success : 'success'
                            }, [ 'success', 'msg' ]),
                            items : [
                            {
                                xtype : 'hidden',
                                name : 'id'
                            },
                            {
                                xtype : 'hidden',
                                name : 'editable'
                            },
                            {
                                xtype : 'hidden',
                                name : 'orden'
                            },
                            {
                                xtype : 'hidden',
                                name : 'codigoAsignatura'
                            },
                            {
                                xtype : 'hidden',
                                name : 'asignaturaId',
                                value : this.asignatura
                            }, this.panelMultiLang ],
                            buttonAlign : 'center'
                        });

                        return formCompetencias;
                    },

                    getBotonEditar : function()
                    {
                        var ref = this;
                        var win;

                        var boton = new Ext.Button(
                        {
                            text : 'Veure competència',
                            iconCls : 'icon-add-tab',
                            handler : function()
                            {
                                if (ref.gridCompetencias.getSelectionModel().getSelections()[0])
                                {
                                    ref.abreVentanaModal();
                                }
                                else
                                {
                                    Ext.Msg.show(
                                    {
                                        title : "Triar competéncia",
                                        icon : Ext.MessageBox.WARNING,
                                        msg : "Deus triar una competència"
                                    });
                                    return false;
                                }
                            }
                        });

                        return boton;
                    },

                    cargaCompetencias : function()
                    {
                        var id = this.gridCompetencias.selModel.getSelected().id;

                        if (!this.gridCompetencias.selModel.getSelected().dirty)
                        {
                            var f = this.formCompetencias.getForm();
                            f.load(
                            {
                                url : '/gdo/rest/competencia/' + id,
                                method : 'get',
                                success : function(e)
                                {
                                },
                                failure : function(form, action)
                                {
                                    Ext.Msg.alert("Error", "Error al cargar los datos.");
                                }
                            });

                            if (this.gridCompetencias.selModel.getSelected().data.editable == 'S'
                                    && this.storeEstadoPantalla.data.items[0].data.modificable == "true")
                            {
                                this.botonGuardar.enable();
                            }
                            else
                            {
                                this.botonGuardar.disable();
                            }
                        }
                        else
                        {
                            this.botonGuardar.enable();
                        }

                    },

                    getBotonCerrar : function()
                    {
                        var ref = this;

                        var boton = new Ext.Button(
                        {
                            text : 'Tancar',
                            name : 'cerrar',
                            width : '50',
                            height : '20',
                            pressed : false,
                            listeners :
                            {
                                click : function(btn, evt)
                                {
                                    ref.ventanaModal.close();
                                }
                            }
                        });

                        return boton;
                    },

                    getVentanaModal : function()
                    {
                        this.cabeceraModal = new Ext.Panel(
                        {
                            frame : true,
                            height : 30,
                            flex : 0
                        });

                        this.descripModal = this.getDescripStore(this.cabeceraModal);
                        this.descripModal.load();

                        this.panelMultiLang = this.getPanelMultiLang();
                        this.botonGuardar = this.getBotonGuardar();
                        this.botonCerrar = this.getBotonCerrar();
                        this.formCompetencias = this.getFormCompetencias();
                        this.formCompetencias.addButton(this.botonGuardar);
                        this.formCompetencias.addButton(this.botonCerrar);

                        var win = new Ext.Window(
                        {
                            title : 'Competències',
                            layout : 'fit',
                            modal : true,
                            width : 800,
                            height : 410,
                            plain : true,
                            items : new Ext.Panel(
                            {
                                layout : 'vbox',
                                layoutConfig :
                                {
                                    align : 'stretch'
                                },
                                items : [ this.cabeceraModal, this.formCompetencias ]
                            })
                        });

                        return win;
                    }
                });
