Ext.ns('UJI.GDO');

UJI.GDO.GridComentarios = Ext.extend(Ext.grid.GridPanel,
{
    height : 200,
    viewConfig :
    {
        forceFit : true
    },
    autoScroll : true,
    frame : true,
    border : false,
    asignatura : 0,
    botonBorrar : {},
    editable : false,
    store : {},

    initComponent : function()
    {
        this.tbar = new Ext.Toolbar(
        {
            items : []
        });

        this.sm = new Ext.grid.RowSelectionModel(
        {
            singleSelect : true
        });

        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.GridComentarios.superclass.initComponent.call(this);

        this.store = this.getStore();

        this.store.proxy.setUrl('/gdo/rest/asignatura/' + this.asignatura + '/comentario');

        this.store.reload();

        if (this.editable)
        {
            this.botonBorrar = this.getBotonBorrar();
            this.getTopToolbar().addButton(this.botonBorrar);
        }

        this.colModel = this.getColModel();
    },

    getStore : function()
    {
        var ref = this;
        return new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/gdo/rest/asignatura/' + this.asignatura + '/comentario'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'Comentario',
                id : 'id'
            },

            [ 'id', 'asignaturaId', 'comentario', 'idioma' ]),

            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    getColModel : function()
    {
        return new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },

            columns : [
            {
                header : 'Id',
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Idioma',
                width : 20,
                dataIndex : 'idioma',
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {

                    return (UJI.GDO.idiomas[value]);

                }

            },
            {
                xtype : 'templatecolumn',
                header : 'Comentari',
                tpl : new Ext.XTemplate('<p style="white-space:normal;">{comentario}</p>'),
                dataIndex : 'comentario'

            } ]
        });
    },

    getBotonBorrar : function()
    {
        var ref = this;

        var boton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, evt)
            {
                var comentarioSeleccionado = ref.getSelectionModel().getSelected();
                if (!comentarioSeleccionado)
                {
                    Ext.Msg.alert('Comentaris', 'Has de seleccionar un comentari');
                    return false;
                }
                ref.store.remove(comentarioSeleccionado);
            }
        });

        return boton;
    }
});
