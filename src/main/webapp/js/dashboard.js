function initGDODashboard()
{
    var store = new Ext.data.Store(
    {
        url : '/gdo/rest/dashboard/estadisticas',
        autoLoad : true,
        restful : true,
        reader : new Ext.data.XmlReader(
        {
            record : 'resumenEstadisticas',
            id : 'id'
        }, [ 'asignadasInactivas', 'asignadasInactivasGrado', 'asignadasInactivasMaster', 'asignadasIntroduciendo',
                'asignadasIntroduciendoGrado', 'asignadasIntroduciendoMaster', 'asignadasPendientes',
                'asignadasPendientesGrado', 'asignadasPendientesMaster', 'asignadasTraduccion',
                'asignadasTraduccionGrado', 'asignadasTraduccionMaster', 'asignadasFinalizadas',
                'asignadasFinalizadasGrado', 'asignadasFinalizadasMaster', 'noAsignadasInactivas',
                'noAsignadasInactivasGrado', 'noAsignadasInactivasMaster', 'noAsignadasIntroduciendo',
                'noAsignadasIntroduciendoGrado', 'noAsignadasIntroduciendoMaster', 'noAsignadasPendientes',
                'noAsignadasPendientesGrado', 'noAsignadasPendientesMaster', 'noAsignadasTraduccion',
                'noAsignadasTraduccionGrado', 'noAsignadasTraduccionMaster', 'noAsignadasFinalizadas',
                'noAsignadasFinalizadasGrado', 'noAsignadasFinalizadasMaster' ]),
        listeners :
        {
            load : function(store, records, options)
            {
                var guiasAsignadasInactivas = records[0].get("asignadasInactivas");
                var guiasAsignadasInactivasGrado = records[0].get("asignadasInactivasGrado");
                var guiasAsignadasInactivasMaster = records[0].get("asignadasInactivasMaster");
                var guiasAsignadasIntroduciendo = records[0].get("asignadasIntroduciendo");
                var guiasAsignadasIntroduciendoGrado = records[0].get("asignadasIntroduciendoGrado");
                var guiasAsignadasIntroduciendoMaster = records[0].get("asignadasIntroduciendoMaster");
                var guiasAsignadasPendientes = records[0].get("asignadasPendientes");
                var guiasAsignadasPendientesGrado = records[0].get("asignadasPendientesGrado");
                var guiasAsignadasPendientesMaster = records[0].get("asignadasPendientesMaster");
                var guiasAsignadasTraduccion = records[0].get("asignadasTraduccion");
                var guiasAsignadasTraduccionGrado = records[0].get("asignadasTraduccionGrado");
                var guiasAsignadasTraduccionMaster = records[0].get("asignadasTraduccionMaster");
                var guiasAsignadasFinalizadas = records[0].get("asignadasFinalizadas");
                var guiasAsignadasFinalizadasGrado = records[0].get("asignadasFinalizadasGrado");
                var guiasAsignadasFinalizadasMaster = records[0].get("asignadasFinalizadasMaster");

                labelGuiasAsignadasInactivas.setText(guiasAsignadasInactivas);
                labelGuiasAsignadasInactivasGrado.setText(guiasAsignadasInactivasGrado);
                labelGuiasAsignadasInactivasMaster.setText(guiasAsignadasInactivasMaster);
                labelGuiasAsignadasIntroduciendo.setText(guiasAsignadasIntroduciendo);
                labelGuiasAsignadasIntroduciendoGrado.setText(guiasAsignadasIntroduciendoGrado);
                labelGuiasAsignadasIntroduciendoMaster.setText(guiasAsignadasIntroduciendoMaster);
                labelGuiasAsignadasPendientes.setText(guiasAsignadasPendientes);
                labelGuiasAsignadasPendientesGrado.setText(guiasAsignadasPendientesGrado);
                labelGuiasAsignadasPendientesMaster.setText(guiasAsignadasPendientesMaster);
                labelGuiasAsignadasTraduccion.setText(guiasAsignadasTraduccion);
                labelGuiasAsignadasTraduccionGrado.setText(guiasAsignadasTraduccionGrado);
                labelGuiasAsignadasTraduccionMaster.setText(guiasAsignadasTraduccionMaster);
                labelGuiasAsignadasFinalizadas.setText(guiasAsignadasFinalizadas);
                labelGuiasAsignadasFinalizadasGrado.setText(guiasAsignadasFinalizadasGrado);
                labelGuiasAsignadasFinalizadasMaster.setText(guiasAsignadasFinalizadasMaster);

                var guiasNoAsignadasInactivas = records[0].get("noAsignadasInactivas");
                var guiasNoAsignadasInactivasGrado = records[0].get("noAsignadasInactivasGrado");
                var guiasNoAsignadasInactivasMaster = records[0].get("noAsignadasInactivasMaster");
                var guiasNoAsignadasIntroduciendo = records[0].get("noAsignadasIntroduciendo");
                var guiasNoAsignadasIntroduciendoGrado = records[0].get("noAsignadasIntroduciendoGrado");
                var guiasNoAsignadasIntroduciendoMaster = records[0].get("noAsignadasIntroduciendoMaster");
                var guiasNoAsignadasPendientes = records[0].get("noAsignadasPendientes");
                var guiasNoAsignadasPendientesGrado = records[0].get("noAsignadasPendientesGrado");
                var guiasNoAsignadasPendientesMaster = records[0].get("noAsignadasPendientesMaster");
                var guiasNoAsignadasTraduccion = records[0].get("noAsignadasTraduccion");
                var guiasNoAsignadasTraduccionGrado = records[0].get("noAsignadasTraduccionGrado");
                var guiasNoAsignadasTraduccionMaster = records[0].get("noAsignadasTraduccionMaster");
                var guiasNoAsignadasFinalizadas = records[0].get("noAsignadasFinalizadas");
                var guiasNoAsignadasFinalizadasGrado = records[0].get("noAsignadasFinalizadasGrado");
                var guiasNoAsignadasFinalizadasMaster = records[0].get("noAsignadasFinalizadasMaster");

                labelGuiasNoAsignadasInactivas.setText(guiasNoAsignadasInactivas);
                labelGuiasNoAsignadasInactivasGrado.setText(guiasNoAsignadasInactivasGrado);
                labelGuiasNoAsignadasInactivasMaster.setText(guiasNoAsignadasInactivasMaster);
                labelGuiasNoAsignadasIntroduciendo.setText(guiasNoAsignadasIntroduciendo);
                labelGuiasNoAsignadasIntroduciendoGrado.setText(guiasNoAsignadasIntroduciendoGrado);
                labelGuiasNoAsignadasIntroduciendoMaster.setText(guiasNoAsignadasIntroduciendoMaster);
                labelGuiasNoAsignadasPendientes.setText(guiasNoAsignadasPendientes);
                labelGuiasNoAsignadasPendientesGrado.setText(guiasNoAsignadasPendientesGrado);
                labelGuiasNoAsignadasPendientesMaster.setText(guiasNoAsignadasPendientesMaster);
                labelGuiasNoAsignadasTraduccion.setText(guiasNoAsignadasTraduccion);
                labelGuiasNoAsignadasTraduccionGrado.setText(guiasNoAsignadasTraduccionGrado);
                labelGuiasNoAsignadasTraduccionMaster.setText(guiasNoAsignadasTraduccionMaster);
                labelGuiasNoAsignadasFinalizadas.setText(guiasNoAsignadasFinalizadas);
                labelGuiasNoAsignadasFinalizadasGrado.setText(guiasNoAsignadasFinalizadasGrado);
                labelGuiasNoAsignadasFinalizadasMaster.setText(guiasNoAsignadasFinalizadasMaster);

                if (guiasAsignadasInactivas == 0 && guiasAsignadasIntroduciendo == 0 && guiasAsignadasPendientes == 0
                        && guiasAsignadasFinalizadas == 0 && guiasAsignadasTraduccion == 0
                        && guiasNoAsignadasInactivas == 0 && guiasNoAsignadasIntroduciendo == 0
                        && guiasNoAsignadasPendientes == 0 && guiasAsignadasTraduccion == 0
                        && guiasNoAsignadasFinalizadas == 0)
                {
                    alert("Aquest usuari no te disponible cap guia");
                }
            }
        }
    });

    var labelTotalPersona = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        html : '<b>Total</b><br><br>'
    });

    var labelGrauPersona = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        html : '<b>Grau</b><br><br>'
    });

    var labelMasterPersona = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        html : '<b>Master</b><br><br>'
    });

    var labelGuiasAsignadasInactivas = new Ext.form.Label(
    {
        fieldLabel : '<b>Inactives</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasInactivasGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasInactivasMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasIntroduciendo = new Ext.form.Label(
    {
        fieldLabel : '<b>Introduint dades</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasIntroduciendoGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasIntroduciendoMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasPendientes = new Ext.form.Label(
    {
        fieldLabel : '<b>Pendent de revisar - validar</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasPendientesGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasPendientesMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasTraduccion = new Ext.form.Label(
    {
        fieldLabel : '<b>Pendent de traduir</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasTraduccionGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasTraduccionMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasFinalizadas = new Ext.form.Label(
    {
        fieldLabel : '<b>Finalitzat</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasFinalizadasGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasAsignadasFinalizadasMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasInactivas = new Ext.form.Label(
    {
        fieldLabel : '<b>Inactives</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasInactivasGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasInactivasMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasIntroduciendo = new Ext.form.Label(
    {
        fieldLabel : '<b>Introduint dades</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasIntroduciendoGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasIntroduciendoMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasPendientes = new Ext.form.Label(
    {
        fieldLabel : '<b>Pendent de revisar - validar</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasPendientesGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasPendientesMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasTraduccion = new Ext.form.Label(
    {
        fieldLabel : '<b>Pendent de traduir</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasTraduccionGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasTraduccionMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasFinalizadas = new Ext.form.Label(
    {
        fieldLabel : '<b>Finalitzat</b>',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasFinalizadasGrado = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelGuiasNoAsignadasFinalizadasMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        text : '0'
    });

    var labelTotal = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        html : '<b>Total</b><br><br>'
    });

    var labelGrau = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        html : '<b>Grau</b><br><br>'
    });

    var labelMaster = new Ext.form.Label(
    {
        fieldLabel : ' ',
        labelSeparator : ' ',
        html : '<b>Master</b><br><br>'
    });

    var panelAsignadas = new Ext.form.FormPanel(
    {
        title : 'Guies assignades',
        flex : 1,
        height : 200,
        frame : true,
        labelWidth : 60,
        bodyStyle : "padding:5px 10px 0 10px;",
        items : [
        {
            autoHeight : true,
            layout : "table",
            layoutConfig :
            {
                columns : 3
            },
            defaults :
            {
                border : false
            },
            items : [
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelTotalPersona ]
            },
            {
                layout : 'form',
                items : [ labelGrauPersona ]
            },
            {
                layout : 'form',
                items : [ labelMasterPersona ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasAsignadasInactivas ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasInactivasGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasInactivasMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasAsignadasIntroduciendo ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasIntroduciendoGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasIntroduciendoMaster ]
            },
            {
                layout : "form",
                labelWidth : 180,
                items : [ labelGuiasAsignadasPendientes ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasPendientesGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasPendientesMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasAsignadasTraduccion ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasTraduccionGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasTraduccionMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasAsignadasFinalizadas ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasFinalizadasGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasAsignadasFinalizadasMaster ]
            } ]
        } ]
    });

    var panelNoAsignadas = new Ext.form.FormPanel(
    {
        title : 'Guies per càrrec',
        flex : 1,
        height : 200,
        frame : true,
        labelWidth : 60,
        bodyStyle : "padding:5px 10px 0 10px;",
        items : [
        {
            autoHeight : true,
            layout : "table",
            layoutConfig :
            {
                columns : 3
            },
            defaults :
            {
                border : false
            },
            items : [
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelTotal ]
            },
            {
                layout : 'form',
                items : [ labelGrau ]
            },
            {
                layout : 'form',
                items : [ labelMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasNoAsignadasInactivas ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasInactivasGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasInactivasMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasNoAsignadasIntroduciendo ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasIntroduciendoGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasIntroduciendoMaster ]
            },
            {
                layout : "form",
                labelWidth : 180,
                items : [ labelGuiasNoAsignadasPendientes ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasPendientesGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasPendientesMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasNoAsignadasTraduccion ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasTraduccionGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasTraduccionMaster ]
            },
            {
                layout : 'form',
                labelWidth : 180,
                items : [ labelGuiasNoAsignadasFinalizadas ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasFinalizadasGrado ]
            },
            {
                layout : 'form',
                items : [ labelGuiasNoAsignadasFinalizadasMaster ]
            } ]
        } ]
    });

    var dashboardPanel = new Ext.Panel(
    {
        xtype : 'panel',
        frame : true,
        padding : '10',
        title : 'Estadístiques de les guies docents',
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        items : [ panelAsignadas,
        {
            height : 15
        }, panelNoAsignadas ]
    });

    return dashboardPanel;
}