Ext.ns('UJI.GDO');

UJI.GDO.Administracion = Ext.extend(Ext.Panel,
    {
        title: 'Gestió de guies docents',
        layout: 'vbox',
        closable: true,
        layoutConfig: {
            align: 'stretch'
        },
        cursoActivo: {},
        asignatura: 0,
        storeEstados: {},
        storeEstadosAsi: {},
        storeTiposEstudio: {},
        storeTiposCompartida: {},
        storeAsignaturas: {},
        descripStore: {},
        comboEstados: {},
        checkBoxGuia: {},
        comboTiposEstudio: {},
        filtroAsignaturaPorCodigo: {},
        botonLimpiarFiltros: {},
        botonFiltrar: {},
        panelFiltroGuias: {},
        gridAsignaturas: {},
        panelSuperior: {},
        panelResumen: {},
        ventanaModal: {},
        cabeceraModal: {},
        descripModal: {},
        panelModificaciones: {},
        botonGuardar: {},

        initComponent: function () {
            var config = {};
            Ext.apply(this, Ext.apply(this.initialConfig, config));

            UJI.GDO.Administracion.superclass.initComponent.call(this);

            this.initUI();

            syncStoreLoad([this.storeEstados, this.storeTiposEstudio, this.storeEstadosAsi, this.storeTiposCompartida, this.storeAsignaturas]);
            this.panelSuperior = this.getPanelSuperior();

            this.add(this.panelSuperior);
            this.add(this.panelFiltroGuias);
            this.add(this.gridAsignaturas);
            this.add(this.panelResumen);
            this.addEvents("newtab");
        },

        initUI: function () {
            this.storeEstados = this.getStoreEstados();
            this.storeEstadosAsi = this.getStoreEstadosAsi();
            this.storeTiposEstudio = this.getStoreTiposEstudio();
            this.storeTiposCompartida = this.getStoreTiposCompartida();
            this.storeAsignaturas = this.getStoreAsignaturas();

            this.comboEstados = this.getComboEstados();
            this.checkBoxGuia = this.getCheckBoxGuia();
            this.comboTiposEstudio = this.getComboTiposEstudio();
            this.filtroAsignaturaPorCodigo = this.getFiltroAsignaturaPorCodigo();
            this.botonLimpiarFiltros = this.getBotonLimpiarFiltros();
            this.botonFiltrar = this.getBotonFiltrar();
            this.panelFiltroGuias = this.getPanelFiltroGuias();
            this.gridAsignaturas = this.getGridAsignaturas();
            this.panelResumen = this.getPanelResumen();
        },

        abreVentanaModal: function () {

            this.ventanaModal = this.getVentanaModal();
            this.ventanaModal.show();
            this.cargaModificaciones();
        },

        getPanelSuperior: function () {
            var panel = new Ext.Panel(
                {
                    flex: 0,
                    height: 33,
                    frame: true,
                    html: '<h1 style="font-size:150%">Gestió de guies docents</h1>'
                });
            return panel;
        },

        getPanelResumen: function () {
            var panel = new Ext.Panel(
                {
                    flex: 0,
                    title: 'Apartats pendents',
                    frame: true,
                    autoScroll: true,
                    height: 150
                });

            return panel;
        },

        getStoreEstados: function () {
            var store = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/asignatura/estados'
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'estado',
                            id: 'id'
                        }, ['id', 'nombre'])
                });

            return store;
        },

        getStoreEstadosAsi: function () {
            var storeEstadosAsi = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/asignatura/estados'
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'estado',
                            id: 'id'
                        }, ['id', 'nombre'])
                });

            return storeEstadosAsi;
        },

        getStoreTiposCompartida: function () {
            var storeTiposCompartida = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/asignatura/tiposcompartida'
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'tipoCompartida',
                            id: 'id'
                        }, ['id', 'nombre'])
                }
            );

            return storeTiposCompartida;
        },

        getStoreTiposEstudio: function () {
            var store = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/titulacion/tipos'
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'tipo',
                            id: 'id'
                        }, ['id', 'nombre'])
                });

            return store;
        },

        getStoreAsignaturas: function () {
            var ref = this;

            var store = new Ext.data.Store(
                {
                    baseParams: {
                        inicial: true
                    },
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/asignatura',
                            api: {
                                update: '/gdo/rest/asignatura/guia'
                            }
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Asignatura',
                            id: 'id'
                        }, ['id', 'nombre',
                            {
                                name: 'activa',
                                convert: function (value, record) {
                                    return (value == 'true') ? 'Sí' : 'No';
                                }
                            }, 'estado', 'asignaturaRelacionada', 'tipoCompartida', 'fechaInicio', 'fechaFin', 'autorizaPas', 'cursoAca', 'numeroComentarios', 'numeroModificaciones', 'notificaResponsable', 'notificaCoordinador',
                            'notificaSLT', 'notificaFinalizada',
                            {
                                name: 'nuevaImplantacion',
                                convert: function (value, record) {
                                    return (value == 'true') ? 'Sí' : 'No';
                                }
                            }]),
                    writer: new Ext.data.XmlWriter(
                        {
                            record: 'Asignatura',
                            id: 'id',
                            writeAllFields: true
                        }, ['id', 'nombre', 'estado',
                            {
                                name: 'activa',
                                convert: function (value, record) {
                                    return (value == 'Sí') ? true : false;
                                }
                            }, 'asignaturaRelacionada', 'tipoCompartida', 'fechaInicio', 'fechaFin', 'autorizaPas', 'cursoAca', 'numeroComentarios', 'numeroModificaciones', 'notificaResponsable', 'notificaCoordinador', 'NotificaSLT',
                            'notificaFinalizada']),
                    listeners: {
                        write: function (store, action, result, res, rs) {
                            if (action == 'update') {
                                ref.storeAsignaturas.baseParams =
                                    {
                                        conGuia: ref.checkBoxGuia.getValue(),
                                        estado: ref.comboEstados.getValue(),
                                        tipoEstudio: ref.comboTiposEstudio.getValue(),
                                        codigoAsignatura: ref.filtroAsignaturaPorCodigo.getValue(),
                                        inicial: false
                                    };

                                var asignaturaId = rs.data.id;

                                if (rs.data.notificaCoordinador == 'true') {
                                    ref.enviaNotificacion('/gdo/rest/notifica/coordinador/' + asignaturaId, 'S\'ha enviat notificació' + ' al cooordinador o coordinadors.');
                                }
                                else if (rs.data.notificaResponsable == 'true') {
                                    ref.enviaNotificacion('/gdo/rest/notifica/responsable/' + asignaturaId, 'S\'ha enviat notificació' + ' al responsable o responsables.');
                                }
                                else if (rs.data.notificaSLT == 'true') {
                                    ref.enviaNotificacion('/gdo/rest/notifica/slt/' + asignaturaId, 'S\'ha enviat notificació al Servei de Llengües.');
                                }
                                else if (rs.data.notificaFinalizada == 'true') {
                                    ref.enviaNotificacion('/gdo/rest/notifica/finalizada/' + asignaturaId, 'S\'ha enviat notificació' + ' al responsable o responsables.');
                                }
                                else {
                                    ref.storeAsignaturas.load();
                                }
                            }
                        }
                    }
                });

            return store;
        },

        getDescripStore: function (panel) {
            var ref = this;

            var descripStore = new Ext.data.Store(
                {
                    url: '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/C',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'textoAyudaFormulariosUI',
                            id: 'asignatura'
                        }, ['asignatura', 'texto']),
                    listeners: {
                        load: function (store, records, options) {
                            var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>',
                                '<p style="margin-top:5px;">{texto}</p>');
                            var competencia = tpl.apply(records[0].data);
                            panel.update(competencia);
                            ref.doLayout();
                        }
                    }
                });

            return descripStore;
        },

        enviaNotificacion: function (url, message) {
            var ref = this;

            Ext.Ajax.request(
                {
                    url: url,
                    method: 'POST',
                    success: function (result, request) {
                        var nombreDestinarios = Ext.DomQuery.selectValue('nombres', result.responseXML.documentElement);

                        Ext.Msg.alert('Informació de la notificació', message + '<br>(' + nombreDestinarios + ')');
                        ref.storeAsignaturas.load();
                    },
                    failure: function () {
                        Ext.Msg.alert('Error', 'Error en l\'enviament de la notificació');
                        ref.storeAsignaturas.load();
                    }
                });
        },

        getComboEstados: function () {
            var combo = new Ext.form.ComboBox(
                {
                    mode: 'local',
                    emptyText: 'trieu...',
                    triggerAction: 'all',
                    forceSelection: true,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldLabel: 'Estat assignatura',
                    width: 200,
                    store: this.storeEstados
                });

            return combo;
        },

        getCheckBoxGuia: function () {
            var checkBoxGuia = new Ext.form.Checkbox(
                {
                    xtype: 'checkbox',
                    boxLabel: 'Mostrar sols les assignatures amb guia  '
                });

            return checkBoxGuia;
        },

        getComboTiposEstudio: function () {
            var comboTiposEstudio = new Ext.form.ComboBox(
                {
                    emptyText: 'trieu...',
                    triggerAction: 'all',
                    forceSelection: true,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldLabel: 'Tipus estudi',
                    width: 200,
                    store: this.storeTiposEstudio
                });

            return comboTiposEstudio;
        },

        getFiltroAsignaturaPorCodigo: function () {
            var filtroAsignaturaPorCodigo = new Ext.form.TextField(
                {
                    fieldLabel: 'Codi assignatura',
                    width: 200
                });
            return filtroAsignaturaPorCodigo;
        },

        getBotonLimpiarFiltros: function () {
            var ref = this;

            var botonLimpiarFiltros = new Ext.Button(
                {
                    text: 'Netejar filtres',
                    margins: '0 0 0 20',
                    width: 80,
                    handler: function (button, event) {
                        ref.comboEstados.clearValue();
                        ref.comboTiposEstudio.clearValue();
                        ref.filtroAsignaturaPorCodigo.reset();
                        ref.checkBoxGuia.reset();
                        ref.storeAsignaturas.baseParams =
                            {
                                inicial: true
                            };
                        ref.storeAsignaturas.load();
                    }
                });

            return botonLimpiarFiltros;
        },

        getBotonFiltrar: function () {
            var ref = this;

            var botonFiltrar = new Ext.Button(
                {
                    text: 'Filtrar',
                    width: 80,
                    handler: function (button, event) {
                        ref.storeAsignaturas.baseParams =
                            {
                                conGuia: ref.checkBoxGuia.getValue(),
                                estado: ref.comboEstados.getValue(),
                                tipoEstudio: ref.comboTiposEstudio.getValue(),
                                codigoAsignatura: ref.filtroAsignaturaPorCodigo.getValue(),
                                inicial: false
                            };
                        ref.storeAsignaturas.load();
                    }
                });

            return botonFiltrar;
        },

        getPanelFiltroGuias: function () {
            var panelFiltroGuias = new Ext.Panel(
                {
                    layout: 'hbox',
                    frame: true,
                    items: [
                        {
                            layout: 'form',
                            flex: 0,
                            border: false,
                            items: [this.comboTiposEstudio, this.filtroAsignaturaPorCodigo, this.comboEstados, this.checkBoxGuia]
                        },
                        {
                            layout: 'hbox',
                            flex: 1,
                            border: false,
                            items: [this.botonFiltrar, this.botonLimpiarFiltros]
                        }]
                });

            return panelFiltroGuias;
        },

        actualizaEstadoBotones: function (recordAsignatura, botones, apartadoButton) {
            {
                var disabled = true;
                var copiaParcial = false;

                if (recordAsignatura.data.activa == 'Sí' && recordAsignatura.data.estado == 'D' && recordAsignatura.data.asignaturaRelacionada.length == 0) {
                    disabled = false;
                }

                if (recordAsignatura.data.activa == 'Sí' && recordAsignatura.data.estado == 'D' && recordAsignatura.data.asignaturaRelacionada.length > 0 && recordAsignatura.data.tipoCompartida == 'P') {
                    copiaParcial = true;
                }

                for (var i = 0; i < botones.length; i++) {
                    botones[i].setDisabled(disabled);
                }

                if (disabled == true && copiaParcial == true) {
                    for (var i = 0; i < botones.length; i++) {
                        if (i != 1) {
                            botones[i].setDisabled(false);
                        }
                    }
                }

            }
        },

        getPanelAyuda: function () {
            var ref = this;

            var panelAyuda = new Ext.Panel(
                {
                    frame: true,
                    height: 30,
                    flex: 0
                }
            );

            var descripStore = new Ext.data.Store(
                {
                    url: '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/I',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'textoAyudaFormulariosUI',
                            id: 'asignatura'
                        }, ['asignatura']),
                    listeners: {
                        load: function (store, records, options) {
                            var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>');
                            var resultado = tpl.apply(records[0].data);
                            panelAyuda.update(resultado);
                            ref.doLayout();
                        }
                    }
                });

            descripStore.load();
            return panelAyuda;

        },

        getCampoIntroModificaciones: function () {
            var campoIntroModificaciones = new Ext.form.TextArea(
                {
                    fieldLabel: 'Modificacions',
                    name: 'modificaciones',
                    anchor: '100%',
                    height: '260',
                    maxLength: '4000',
                    maxLengthText: 'El quadre'
                    + ' de texte no pot superar els 4000 caràcters'
                });
            return campoIntroModificaciones;
        },

        getPanelModificaciones: function () {
            this.campoIntroModificaciones = this.getCampoIntroModificaciones();
            this.botonGuardar = this.getBotonGuardar();

            var formModifica = new Ext.FormPanel(
                {
                    frame: true,
                    flex: 1,
                    name: 'Modificaciones',
                    buttonAlign: 'center',
                    labelAlign: 'top',
                    url: '/gdo/rest/asignatura/',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Asignatura',
                            id: 'id'
                        }, ['id', 'modificaciones']),
                    writer: new Ext.data.XmlWriter(
                        {
                            record: 'Asignatura',
                            writeAllFields: true,
                            id: 'id'
                        }, ['id', 'modificaciones']),
                    errorReader: new Ext.data.XmlReader(
                        {
                            record: 'respuestaForm',
                            success: 'success'
                        }, ['success', 'msg']),
                    items: [{
                        xtype: 'hidden',
                        name: 'id'
                    }, this.campoIntroModificaciones, this.botonGuardar]

                }
            );

            return formModifica;
        },

        getVentanaModal: function () {
            this.panelAyuda = this.getPanelAyuda();
            this.panelModificaciones = this.getPanelModificaciones();

            var win = new Ext.Window(
                {
                    title: 'Modificacions',
                    layout: 'fit',
                    modal: true,
                    width: 800,
                    height: 410,
                    plain: true,
                    items: new Ext.Panel(
                        {
                            layout: 'vbox',
                            layoutConfig: {
                                align: 'stretch'
                            },
                            items: [this.panelAyuda, this.panelModificaciones]
                        })
                });

            return win;
        },

        cargaModificaciones: function () {
            var id = this.gridAsignaturas.selModel.getSelected().id;

            var f = this.panelModificaciones.getForm();
            f.load(
                {
                    url: '/gdo/rest/asignatura/' + id,
                    method: 'get',
                    success: function (e) {
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Error', 'Error al cargar el campo Modificaciones.');
                    }
                });

        },

        getGridAsignaturas: function () {
            var ref = this;

            var apartadoButton = new Ext.Button(
                {
                    text: 'Apartats',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];
                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: 'Asignatura',
                                    icon: Ext.MessageBox.WARNING,
                                    msg: 'Deus triar una assignatura'
                                });
                            return false;
                        }

                        ref.fireEvent("newtab",
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'textos'
                            });

                    }
                });

            var evaluacionButton = new Ext.Button(
                {
                    text: 'Avaluacions',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: 'Assignatura',
                                    icon: Ext.MessageBox.WARNING,
                                    msg: 'Deus triar una assignatura'
                                });
                            return false;
                        }

                        ref.fireEvent('newtab',
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'evaluaciones'
                            });

                    }
                });

            var actividadButton = new Ext.Button(
                {
                    text: 'Activitats',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: 'Assignatura',
                                    icon: Ext.MessageBox.WARNING,
                                    msg: 'Deus triar una assignatura'
                                });
                            return false;
                        }

                        ref.fireEvent('newtab',
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'actividades'
                            });
                    }
                });

            var competenciaButton = new Ext.Button(
                {
                    text: 'Competències',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: 'Competència',
                                    icon: Ext.MessageBox.WARNING,
                                    msg: 'Deus triar una assignatura'
                                });
                            return false;
                        }

                        ref.fireEvent("newtab",
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'competencias'
                            });
                    }
                });

            var resultadoButton = new Ext.Button(
                {
                    text: 'Resultats d\'aprenentatge',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: 'Resultat',
                                    icon: Ext.MessageBox.WARNING,
                                    msg: 'Deus triar una assignatura'
                                });
                            return false;
                        }

                        ref.fireEvent("newtab",
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'resultados'
                            });
                    }
                });

            var idiomaButton = new Ext.Button(
                {
                    text: 'Idiomes docents',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: "Idioma docent",
                                    icon: Ext.MessageBox.WARNING,
                                    msg: "Deus triar una assignatura"
                                });
                            return false;
                        }

                        ref.fireEvent("newtab",
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'idiomas'
                            });
                    }
                });

            var delegacionButton = new Ext.Button(
                {
                    text: 'Delegar',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: "Delegar",
                                    icon: Ext.MessageBox.WARNING,
                                    msg: "Deus triar una assignatura"
                                });
                            return false;
                        }

                        ref.fireEvent("newtab",
                            {
                                asignaturaId: rec.data.id,
                                pantalla: 'delegacion'
                            });
                    }
                });

            var modificacionButton = new Ext.Button(
                {
                    text: 'Modificacions',
                    iconCls: 'icon-add-tab',
                    disabled: true,
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: "Modificacions",
                                    icon: Ext.MessageBox.WARNING,
                                    msg: "Deus triar una assignatura"
                                });
                            return false;
                        }

                        ref.abreVentanaModal();
                    }
                }
            );

            var comentarioButton = new Ext.Button(
                {
                    text: 'Comentaris',
                    iconCls: 'icon-add-tab',
                    handler: function () {
                        var rec = grid.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: "Comentaris",
                                    icon: Ext.MessageBox.WARNING,
                                    msg: "Deus triar una assignatura"
                                });
                            return false;
                        }

                        ref.fireEvent("newtab",
                            {
                                asignaturaId: rec.data.id,
                                editable: false,
                                pantalla: 'comentarios'
                            });

                    }
                });

            var comboEstadosAsi = new Ext.form.ComboBox(
                {
                    mode: 'local',
                    emptyText: 'trieu...',
                    triggerAction: 'all',
                    forceSelection: true,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    autoCreate: {
                        tag: 'input',
                        size: '8'
                    },
                    store: this.storeEstadosAsi,
                    listeners: {
                        beforeshow: function (combo) {
                            var record = grid.getSelectionModel().getSelections()[0];

                            ref.storeEstadosAsi.proxy.conn.url = '/gdo/rest/asignatura/' + record.get("id") + '/estados';

                            ref.storeEstadosAsi.load(
                                {
                                    callback: function () {
                                        comboEstadosAsi.setValue(record.get("estado"));
                                        Ext.util.Format.comboRenderer(comboEstadosAsi);
                                    }
                                });
                        }
                    }
                });

            var comboTiposCompartida = new Ext.form.ComboBox(
                {
                    mode: 'local',
                    emptyText: 'trieu...',
                    triggerAction: 'all',
                    forceSelection: true,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    autoCreate: {
                        tag: 'input',
                        size: '8'
                    },
                    store: this.storeTiposCompartida,
                    listeners: {
                        beforeshow: function (combo) {
                            var record = grid.getSelectionModel().getSelections()[0];

                            ref.storeTiposCompartida.proxy.conn.url = '/gdo/rest/asignatura/tiposcompartida';

                            ref.storeTiposCompartida.load(
                                {
                                    callback: function () {
                                        comboTiposCompartida.setValue(record.get("tipoCompartida"));
                                        Ext.util.Format.comboRenderer(comboTiposCompartida);
                                    }
                                });
                        }
                    }
                });

            var editor = new Ext.ux.grid.RowEditor(
                {
                    saveText: 'Actualizar',
                    listeners: {
                        beforeedit: function (rowEditor, rowIndex) {
                            var columnIndex = ref.gridAsignaturas.colModel.findColumnIndex('estado');
                            var columna = ref.gridAsignaturas.colModel.getColumnById(columnIndex);

                            if (ref.gridAsignaturas.getSelectionModel().getSelections()[0].data.asignaturaRelacionada != "") {
                                columna.editor.disable();
                            }
                            else {
                                columna.editor.enable();
                            }
                            return true;
                        }
                    }
                });

            Ext.util.Format.comboRenderer = function (combo) {
                return function (value) {
                    var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            };

            function idiomaGuia(idioma) {

                var recordAsignatura = grid.getSelectionModel().getSelections()[0];

                if (recordAsignatura) {
                    var codigoAsignatura = recordAsignatura.get("nombre").split("-")[0].trim();

                    window.open("https://e-ujier.uji.es" + "/pls/www/gri_www.euji22883_html" + '?p_curso_aca=' + ref.cursoActivo + '&p_asignatura_id=' + codigoAsignatura + "&p_idioma=" + idioma);
                }
                else {
                    Ext.Msg.show(
                        {
                            title: "Imprimir guia",
                            icon: Ext.MessageBox.WARNING,
                            msg: "Deus triar una assignatura"
                        });
                    return false;
                }
            }

            var grid;
            grid = new Ext.grid.GridPanel(
                {
                    viewConfig: {
                        forceFit: true
                    },
                    flex: 1,
                    sm: new Ext.grid.RowSelectionModel(
                        {
                            singleSelect: true
                        }),
                    plugins: [editor],
                    frame: true,
                    store: this.storeAsignaturas,
                    loadMask: true,
                    colModel: new Ext.grid.ColumnModel(
                        {
                            defaults: {
                                sortable: true
                            },
                            columns: [
                                {
                                    header: 'Id',
                                    dataIndex: 'id',
                                    hidden: true

                                },
                                {
                                    dataIndex: 'fechaInicio',
                                    hidden: true

                                },
                                {
                                    dataIndex: 'fechaFin',
                                    hidden: true

                                },
                                {
                                    header: 'Asignatura',
                                    dataIndex: 'nombre',
                                    width: 220

                                },
                                {
                                    header: 'Activa',
                                    dataIndex: 'activa',
                                    width: 25

                                },
                                {
                                    header: 'Estat',
                                    dataIndex: 'estado',
                                    editor: comboEstadosAsi,
                                    width: 50,
                                    renderer: Ext.util.Format.comboRenderer(comboEstadosAsi)
                                },
                                {
                                    header: 'Nova Implantació',
                                    dataIndex: 'nuevaImplantacion',
                                    width: 30
                                },
                                {
                                    header: 'Coment',
                                    dataIndex: 'numeroComentarios',
                                    width: 30
                                },
                                {
                                    header: 'Modific',
                                    dataIndex: 'numeroModificaciones',
                                    width: 30
                                },
                                {
                                    header: 'Basada en la guia',
                                    dataIndex: 'asignaturaRelacionada',
                                    width: 40,
                                    editor: new Ext.form.TextField({})
                                },
                                {
                                    header: 'Tipus copia',
                                    dataIndex: 'tipoCompartida',
                                    editor: comboTiposCompartida,
                                    width: 60,
                                    renderer: Ext.util.Format.comboRenderer(comboTiposCompartida)
                                }]
                        }),
                    listeners: {
                        rowclick: function (grid, rowIndex, evt) {

                            var recordAsignatura = grid.getStore().getAt(rowIndex);

                            var botones = [idiomaButton, apartadoButton, evaluacionButton, actividadButton, competenciaButton, resultadoButton, delegacionButton, modificacionButton];
                            ref.actualizaEstadoBotones(recordAsignatura, botones);

                            ref.asignatura = grid.getSelectionModel().getSelections()[0].data.id;

                            var resumenesJson = [];

                            function actualizaPanelResumen(resumenesJson) {

                                var resumenPlantilla = new Ext.XTemplate('<div style="float:left;width:250px;"><b>Valencià</b><br\>', '<tpl for=".">', '<span>{valorca}</span>', '</tpl>', '</div>',
                                    '<div style="float:left;width:250px;"><b>Espanyol</b><br\>', '<tpl for=".">', '<span>{valores}</span>', '</tpl>', '</div>',
                                    '<div style="float:left;width:250px;"><b>Anglés</b><br\>', '<tpl for=".">', '<span>{valoruk}</span>', '</tpl>', '</div>');

                                if (resumenesJson && resumenesJson.length > 0) {
                                    var contenido = resumenPlantilla.apply(resumenesJson);
                                    ref.panelResumen.update(contenido);
                                }
                                else {
                                    ref.panelResumen.update("");
                                }
                            }

                            function setResumenJson(resumenValorCa, resumenValorEs, resumenValorUk) {
                                if (resumenValorCa && resumenValorEs && resumenValorUk) {
                                    resumenesJson.push(
                                        {
                                            valorca: resumenValorCa,
                                            valores: resumenValorEs,
                                            valoruk: resumenValorUk
                                        });
                                }
                            }

                            function convierteXMLaJSON(response) {
                                var xml = response.responseXML;
                                var resumenesXml = Ext.DomQuery.jsSelect('texto', xml);

                                for (var i = 0, len = resumenesXml.length; i < len; i++) {
                                    var resumenValorCa = "";
                                    var resumenValorEs = "";
                                    var resumenValorUk = "";

                                    if (Ext.DomQuery.selectValue('valorca', resumenesXml[i])) {
                                        resumenValorCa = Ext.DomQuery.selectValue('valorca', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('valores', resumenesXml[i])) {
                                        resumenValorEs = Ext.DomQuery.selectValue('valores', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('valoruk', resumenesXml[i])) {
                                        resumenValorUk = Ext.DomQuery.selectValue('valoruk', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('idiomas', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('idiomas', resumenesXml[i]);
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('idiomas', resumenesXml[i]);
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('idiomas', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('evaluacionesca', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('evaluacionesca', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('evaluacioneses', resumenesXml[i])) {
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('evaluacioneses', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('evaluacionesuk', resumenesXml[i])) {
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('evaluacionesuk', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('actividades', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('actividades', resumenesXml[i]);
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('actividades', resumenesXml[i]);
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('actividades', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('competenciasca', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('competenciasca', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('competenciases', resumenesXml[i])) {
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('competenciases', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('competenciasuk', resumenesXml[i])) {
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('competenciasuk', resumenesXml[i]);
                                    }

                                    if (!resumenValorCa) {
                                        resumenValorCa = " ";
                                    }
                                    if (!resumenValorEs) {
                                        resumenValorEs = " ";
                                    }
                                    if (!resumenValorUk) {
                                        resumenValorUk = " ";
                                    }
                                    setResumenJson(resumenValorCa.replace(/\n/g, "<br>"), resumenValorEs.replace(/\n/g, "<br>"), resumenValorUk.replace(/\n/g, "<br>"));
                                }
                            }

                            if (ref.asignatura != null && ref.asignatura != "") {
                                Ext.Ajax.request(
                                    {
                                        url: '/gdo/rest/asignatura/' + ref.asignatura + '/apartados/obligatoriosvacios',
                                        success: function (response, opts) {
                                            convierteXMLaJSON(response);
                                            actualizaPanelResumen(resumenesJson);
                                        }
                                    });
                            }

                        }
                    },
                    tbar: {
                        autoScroll: true,
                        items: [idiomaButton, '-', apartadoButton, '-', evaluacionButton, '-', actividadButton, '-', competenciaButton, '-', resultadoButton, '-', delegacionButton, '-', modificacionButton, '-', comentarioButton,
                            '-',
                            {
                                text: 'Imprimir guía',
                                iconCls: 'printer',
                                menu: {
                                    xtype: 'menu',
                                    border: false,
                                    plain: true,
                                    items: [
                                        {
                                            text: 'Valencià',
                                            handler: function () {
                                                var idioma = 'CA';
                                                idiomaGuia(idioma);
                                            }
                                        },
                                        {
                                            text: 'Castellà',
                                            handler: function () {

                                                var idioma = 'ES';
                                                idiomaGuia(idioma);
                                            }
                                        },
                                        {
                                            text: 'Angleś',
                                            handler: function () {

                                                var idioma = 'UK';
                                                idiomaGuia(idioma);
                                            }
                                        }]
                                }
                            }]

                    }
                });

            ref.storeAsignaturas.on('save', function (store, batch, data) {
                var recordAsignatura = grid.getSelectionModel().getSelections()[0];

                var botones = [idiomaButton, apartadoButton, evaluacionButton, actividadButton, competenciaButton, resultadoButton, delegacionButton, modificacionButton];
                ref.actualizaEstadoBotones(recordAsignatura, botones, apartadoButton);
            });

            return grid;
        },

        getBotonGuardar: function () {
            var ref = this;
            var boton = new Ext.Button(
                {
                    text: 'Guardar',
                    name: 'guardar',
                    width: '50',
                    height: '20',
                    pressed: false,
                    listeners: {
                        click: function (btn, evt) {
                            if (ref.panelModificaciones.getForm().isValid()) {

                                if (ref.panelModificaciones.getForm().getValues().id) {
                                    ref.panelModificaciones.getForm().submit(
                                        {
                                            method: 'PUT',
                                            url: '/gdo/rest/asignatura/' + ref.panelModificaciones.getForm().getValues().id + '/modificaciones',
                                            success: function (form, action) {
                                                ref.ventanaModal.close();
                                                ref.storeAsignaturas.load();
                                            },
                                            failure: function (form, action) {
                                                Ext.Msg.alert('Error', 'Error en l\'actualització de les modificacions');
                                            }
                                        });
                                }
                            }

                        }
                    }
                });
            return boton;
        }

    })
;