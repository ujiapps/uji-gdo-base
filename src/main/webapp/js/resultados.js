Ext.ns('UJI.GDO');

UJI.GDO.Resultados = Ext
        .extend(
                Ext.Panel,
                {
                    title : 'Resultats d\'aprenentatge',
                    layout : 'vbox',
                    closable : true,
                    layoutConfig :
                    {
                        align : 'stretch'
                    },
                    asignatura : 0,
                    storeEstadoPantalla : {},
                    descripStore : {},
                    storeResultados : {},
                    panelCabecera : {},
                    selectionModel : {},
                    editor : {},
                    botonDelete : {},
                    botonInsert : {},
                    botonEditar : {},
                    botonGuardar : {},
                    botonCerrar : {},
                    cabeceraModal : {},
                    descripModal : {},
                    panelMultiLang : {},
                    panelesResultados : {},
                    gridResultados : {},
                    formResultados : {},
                    multiLangResultado : {},

                    initComponent : function()
                    {
                        var config = {};
                        Ext.apply(this, Ext.apply(this.initialConfig, config));

                        UJI.GDO.Resultados.superclass.initComponent.call(this);

                        this.panelCabecera = this.getPanelCabecera();

                        this.storeEstadoPantalla = this.getStoreEstadoPantalla();
                        this.descripStore = this.getDescripStore(this.panelCabecera);
                        this.storeResultados = this.getStoreResultados();

                        syncStoreLoad([ this.storeEstadoPantalla, this.descripStore,
                                this.storeResultados ]);

                        this.initUI();

                        this.add(this.panelCabecera);
                        this.add(this.gridResultados);
                    },

                    initUI : function()
                    {
                        this.selectionModel = new Ext.grid.RowSelectionModel(
                        {
                            singleSelect : true
                        });

                        this.editor = new Ext.ux.grid.RowEditor(
                        {
                            saveText : 'Guarda',
                            cancelText : 'Cancel·la',
                            errorSummary : false
                        });

                        this.botonDelete = this.getBotonDelete();
                        this.botonInsert = this.getBotonInsert();
                        this.botonEditar = this.getBotonEditar();

                        this.gridResultados = this.getGridResultados();

                    },

                    getStoreEstadoPantalla : function()
                    {
                        var ref = this;

                        var storeEstadoPantalla = new Ext.data.Store(
                        {
                            url : '/gdo/rest/resultado/estado/' + this.asignatura,
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'RecordUI',
                                id : 'asignaturaId'
                            }, [ 'asignaturaId', 'modificable' ]),
                            listeners :
                            {

                                load : function(store, records, options)
                                {
                                    var resultado = records[0].data;
                                    if (resultado.modificable == "true")
                                    {
                                        ref.botonInsert.enable();
                                    }
                                }
                            }
                        });

                        return storeEstadoPantalla;
                    },

                    getStoreResultados : function()
                    {
                        var dsResultados = new Ext.data.Store(
                        {
                            baseParams :
                            {
                                asignaturaId : this.asignatura
                            },
                            restful : true,
                            url : '/gdo/rest/resultado',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'Resultado',
                                id : 'id'
                            }, [ 'asignaturaId', 'resultadoCa', 'resultadoCaBreve', 'resultadoEs',
                                    'resultadoEsBreve', 'resultadoUk', 'resultadoUkBreve',
                                    'editable' ]),
                            writer : new Ext.data.XmlReader(
                            {
                                record : 'Resultado',
                                id : 'id'
                            }, [ 'asignaturaId', 'resultadoCa', 'resultadoCaBreve', 'resultadoEs',
                                    'resultadoEsBreve', 'resultadoUk', 'resultadoUkBreve' ]),

                            listeners :
                            {
                                save : function(store, batch, data)
                                {
                                    if (data.destroy != null)
                                    {
                                        this.reload();
                                    }
                                }
                            }
                        });

                        return dsResultados;
                    },

                    getDescripStore : function(panel)
                    {
                        var ref = this;

                        var descripStore = new Ext.data.Store(
                        {
                            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/C',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'textoAyudaFormulariosUI',
                                id : 'asignatura'
                            }, [ 'asignatura', 'texto' ]),
                            listeners :
                            {
                                load : function(store, records, options)
                                {
                                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>',
                                            '<p style="margin-top:5px;">{texto}</p>');
                                    var resultado = tpl.apply(records[0].data);
                                    panel.update(resultado);
                                    ref.doLayout();
                                }
                            }
                        });

                        return descripStore;
                    },

                    getPanelCabecera : function()
                    {
                        var panel = new Ext.Panel(
                        {
                            frame : true,
                            autoHeight : true,
                            flex : 0
                        });

                        return panel;
                    },

                    getPanelMultiLang : function()
                    {
                        this.multiLangResultado = new Ext.ux.uji.LanguagePanel(
                        {
                            flex : 1,
                            language : [
                            {
                                lang : 'Ca',
                                title : 'Valencià'
                            },
                            {
                                lang : 'Es',
                                title : 'Espanyol'
                            },
                            {
                                lang : 'Uk',
                                title : 'Anglés'
                            } ]
                        });

                        this.multiLangResultado.addComponent(new Ext.form.TextArea(
                        {
                            name : 'resultado',
                            fieldLabel : 'Resultat',
                            anchor : '100%',
                            height : '210',
                            maxLength : '4000',
                            maxLengthText : "El quadre"
                                    + " de texte no pot superar els 4000 caràcters"
                        }));

                        var panelMultiLang = new Ext.Panel(
                        {
                            frame : true,
                            name : 'pestanyas',
                            layout : 'form',
                            anchor : '100%',
                            defaults :
                            {
                                margins : '0 10 0 0'
                            },
                            items : [ this.multiLangResultado ]
                        });

                        return panelMultiLang;
                    },

                    getBotonDelete : function()
                    {
                        var ref = this;

                        var botonDelete = new Ext.Button(
                        {
                            text : 'Esborrar',
                            iconCls : 'application-delete',
                            disabled : true,
                            handler : function(btn, evt)
                            {
                                var rec = ref.selectionModel.getSelected();
                                if (!rec)
                                {
                                    return false;
                                }
                                ;
                                ref.storeResultados.remove(rec);
                            }
                        });
                        return botonDelete;
                    },

                    getBotonInsert : function()
                    {
                        var ref = this;

                        var boton = new Ext.Button(
                        {
                            text : 'Afegir',
                            iconCls : 'application-add',
                            disabled : true,
                            listeners :
                            {
                                click : function(btn, evt)
                                {
                                    ref.ventanaModal = ref.getVentanaModal();

                                    ref.selectionModel.clearSelections();

                                    ref.botonGuardar.enable();

                                    ref.formResultados.getForm().setValues(
                                    {
                                        id : '',
                                        editable : 'S',
                                        asignaturaId : ref.asignatura,
                                        resultadoCa : '',
                                        resultadoEs : '',
                                        resultadoUk : ''
                                    });

                                    ref.storeResultados.baseParams.asignaturaId = ref.asignatura;

                                    ref.ventanaModal.show();
                                }
                            }
                        });

                        return boton;
                    },

                    getBotonGuardar : function()
                    {
                        var ref = this;

                        var boton = new Ext.Button(
                                {
                                    text : 'Guardar',
                                    name : 'guardar',
                                    width : '50',
                                    height : '20',
                                    pressed : false,
                                    disabled : true,
                                    listeners :
                                    {
                                        click : function(btn, evt)
                                        {
                                            if (ref.formResultados.getForm().isValid())
                                            {
                                                if (!ref.formResultados.getForm().getValues().resultadoCa
                                                        && !ref.formResultados.getForm()
                                                                .getValues().resultadoEs
                                                        && !ref.formResultados.getForm()
                                                                .getValues().resultadoUk)
                                                {
                                                    Ext.Msg.alert('Error',
                                                            'No es pot guardar un registre buit');
                                                }
                                                else
                                                {
                                                    if ((ref.formResultados.getForm().getValues().id))
                                                    {
                                                        ref.formResultados
                                                                .getForm()
                                                                .submit(
                                                                        {
                                                                            method : 'PUT',
                                                                            url : '/gdo/rest/resultado/'
                                                                                    + ref.formResultados
                                                                                            .getForm()
                                                                                            .getValues().id,
                                                                            success : function(
                                                                                    form, action)
                                                                            {
                                                                                ref.gridResultados.store.proxy.conn.url = '/'
                                                                                        + 'gdo/rest/resultado';
                                                                                ref.gridResultados.store
                                                                                        .reload();
                                                                            },
                                                                            failure : function(
                                                                                    form, action)
                                                                            {
                                                                                Ext.Msg
                                                                                        .alert(
                                                                                                'Error',
                                                                                                'Error en l\'actualització');
                                                                            }
                                                                        });
                                                    }
                                                    else
                                                    {
                                                        ref.formResultados
                                                                .getForm()
                                                                .submit(
                                                                        {
                                                                            method : 'POST',
                                                                            url : '/gdo/rest/resultado/',
                                                                            success : function(
                                                                                    form, action)
                                                                            {

                                                                                ref.gridResultados.store.proxy.conn.url = '/gdo/rest/resultado';
                                                                                ref.gridResultados.store
                                                                                        .reload();

                                                                                ref.formResultados
                                                                                        .getForm()
                                                                                        .setValues(
                                                                                                {
                                                                                                    id : Ext.DomQuery
                                                                                                            .selectValue(
                                                                                                                    'id',
                                                                                                                    action.response.responseXML)
                                                                                                });
                                                                            },
                                                                            failure : function(
                                                                                    form, action)
                                                                            {
                                                                                Ext.Msg
                                                                                        .alert(
                                                                                                'Error',
                                                                                                'Error en l\'actualització');
                                                                            }
                                                                        });

                                                    }

                                                }
                                            }
                                            else
                                            {
                                                Ext.Msg.alert("Error",
                                                        "Revise els valors del camps.");
                                            }
                                        }
                                    }
                                });

                        return boton;
                    },

                    getGridResultados : function()
                    {
                        var ref = this;

                        var gridResultados = new Ext.grid.GridPanel(
                        {
                            flex : 1,
                            loadMask : true,
                            viewConfig :
                            {
                                forceFit : true
                            },
                            sm : this.selectionModel,
                            frame : true,
                            border : false,
                            store : this.storeResultados,
                            colModel : new Ext.grid.ColumnModel(
                            {
                                defaults :
                                {
                                    sortable : true
                                },
                                columns : [
                                {
                                    header : "id",
                                    hidden : true,
                                    dataIndex : 'id'
                                },
                                {
                                    header : 'Resultat d\'aprenentatge Valencià',
                                    dataIndex : 'resultadoCaBreve',
                                    resizable : false
                                },
                                {
                                    header : 'Resultat d\'aprenentatge Espanyol',
                                    dataIndex : 'resultadoEsBreve',
                                    resizable : false
                                },
                                {
                                    header : 'Resultat d\'aprenentatge Anglés',
                                    dataIndex : 'resultadoUkBreve',
                                    resizable : false
                                }, ]
                            }),
                            tbar :
                            {
                                items : [ this.botonInsert, '-', this.botonDelete, '-',
                                        this.botonEditar ]
                            },
                            listeners :
                            {
                                rowclick : function(grid, rowIndex, evt)
                                {
                                    if (grid.selModel.selections.keys.length > 0)
                                    {
                                        var id = grid.selModel.getSelected().id;
                                        if (!grid.selModel.getSelected().dirty)
                                        {
                                            if (grid.selModel.getSelected().data.editable == 'S'
                                                && ref.storeEstadoPantalla.data.items[0].data.modificable == "true")
                                            {
                                                ref.botonDelete.enable();
                                            }
                                            else
                                            {
                                                ref.botonDelete.disable();
                                            }
                                            ref.botonEditar.enable();
                                        }
                                    }
                                    else
                                    {
                                        Ext.Msg.alert("Error", "Per a editar,"
                                                + " primer cal fer clic en la fila");
                                    }
                                },
                                rowdblclick : function(grid, rowIndex, event)
                                {
                                    ref.abreVentanaModal();
                                }
                            }
                        });

                        return gridResultados;
                    },

                    abreVentanaModal : function()
                    {
                        this.ventanaModal = this.getVentanaModal();
                        this.ventanaModal.show();
                        this.cargaResultados();
                    },

                    getFormResultados : function()
                    {
                        var formResultados = new Ext.FormPanel(
                        {
                            frame : true,
                            flex : 1,
                            name : 'resultados',
                            url : '/gdo/rest/resultado/',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'Resultado',
                                id : 'id'
                            }, [ 'id', 'resultadoCa', 'resultadoEs', 'resultadoUk', 'editable',
                                    'orden', 'codigoAsignatura', 'asignaturaId' ]),
                            writer : new Ext.data.XmlWriter(
                            {
                                record : 'Resultado',
                                writeAllFields : true,
                                id : 'id'
                            }, [ 'id', 'resultadoCa', 'resultadoEs', 'resultadoUk', 'editable',
                                    'orden', 'codigoAsignatura', 'asignaturaId' ]),
                            errorReader : new Ext.data.XmlReader(
                            {
                                record : 'respuestaForm',
                                success : 'success'
                            }, [ 'success', 'msg' ]),
                            items : [
                            {
                                xtype : 'hidden',
                                name : 'id'
                            },
                            {
                                xtype : 'hidden',
                                name : 'editable'
                            },
                            {
                                xtype : 'hidden',
                                name : 'orden'
                            },
                            {
                                xtype : 'hidden',
                                name : 'codigoAsignatura'
                            },
                            {
                                xtype : 'hidden',
                                name : 'asignaturaId',
                                value : this.asignatura
                            }, this.panelMultiLang ],
                            buttonAlign : 'center'
                        });

                        return formResultados;
                    },

                    getBotonEditar : function()
                    {
                        var ref = this;
                        var win;

                        var boton = new Ext.Button(
                        {
                            text : 'Editar resultat',
                            iconCls : 'icon-add-tab',
                            disabled : true,
                            handler : function()
                            {
                                if (ref.gridResultados.getSelectionModel().getSelections()[0])
                                {
                                    ref.abreVentanaModal();
                                }
                                else
                                {
                                    Ext.Msg.show(
                                    {
                                        title : "Triar resultat",
                                        icon : Ext.MessageBox.WARNING,
                                        msg : "Deus triar un resultat"
                                    });
                                    return false;
                                }
                            }
                        });

                        return boton;
                    },

                    cargaResultados : function()
                    {
                        var id = this.gridResultados.selModel.getSelected().id;

                        if (!this.gridResultados.selModel.getSelected().dirty)
                        {
                            var f = this.formResultados.getForm();
                            f.load(
                            {
                                url : '/gdo/rest/resultado/' + id,
                                method : 'get',
                                success : function(e)
                                {
                                },
                                failure : function(form, action)
                                {
                                    Ext.Msg.alert("Error", "Error al cargar los datos.");
                                }
                            });

                            if (this.gridResultados.selModel.getSelected().data.editable == 'S'
                                && this.storeEstadoPantalla.data.items[0].data.modificable == "true")
                            {
                                this.botonGuardar.enable();
                            }
                            else
                            {
                                this.botonGuardar.disable();
                            }
                        }
                        else
                        {
                            this.botonGuardar.enable();
                        }

                    },

                    getBotonCerrar : function()
                    {
                        var ref = this;

                        var boton = new Ext.Button(
                        {
                            text : 'Tancar',
                            name : 'cerrar',
                            width : '50',
                            height : '20',
                            pressed : false,
                            listeners :
                            {
                                click : function(btn, evt)
                                {
                                    ref.ventanaModal.close();
                                }
                            }
                        });

                        return boton;
                    },

                    getVentanaModal : function()
                    {
                        this.cabeceraModal = new Ext.Panel(
                        {
                            frame : true,
                            height : 30,
                            flex : 0
                        });

                        this.descripModal = this.getDescripStore(this.cabeceraModal);
                        this.descripModal.load();

                        this.panelMultiLang = this.getPanelMultiLang();
                        this.botonGuardar = this.getBotonGuardar();
                        this.botonCerrar = this.getBotonCerrar();
                        this.formResultados = this.getFormResultados();
                        this.formResultados.addButton(this.botonGuardar);
                        this.formResultados.addButton(this.botonCerrar);

                        var win = new Ext.Window(
                        {
                            title : 'Resultats',
                            layout : 'fit',
                            modal : true,
                            width : 800,
                            height : 410,
                            plain : true,
                            items : new Ext.Panel(
                            {
                                layout : 'vbox',
                                layoutConfig :
                                {
                                    align : 'stretch'
                                },
                                items : [ this.cabeceraModal, this.formResultados ]
                            })
                        });

                        return win;
                    }
                });
