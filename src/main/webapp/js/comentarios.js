Ext.ns('UJI.GDO');

UJI.GDO.Comentarios = Ext.extend(Ext.Panel,
{
    title : 'Comentaris',
    layaut : 'vbox',
    closable : true,
    layautConfig :
    {
        align : 'stretch'
    },
    asignatura : 0,
    descripStore : {},
    cabeceraComentarios : {},
    gridComentarios : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Comentarios.superclass.initComponent.call(this);

        this.descripStore = this.getDescripStore();

        syncStoreLoad([ this.descripStore ]);
        
        this.cabeceraComentarios = this.getCabeceraComentarios();
        this.gridComentarios = this.getGridComentarios();

        this.add(this.cabeceraComentarios);
        this.add(this.gridComentarios);
    },

    getDescripStore : function()
    {
        var ref = this;

        var descripStore = new Ext.data.Store(
        {
            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/I',
            reader : new Ext.data.XmlReader(
            {
                record : 'textoAyudaFormulariosUI',
                id : 'asignatura'
            }, [ 'asignatura' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>');
                    var resultado = tpl.apply(records[0].data);
                    ref.cabeceraComentarios.update(resultado);
                    ref.doLayout();
                }
            }
        });

        return descripStore;
    },

    getCabeceraComentarios : function()
    {

        var cabeceraComentarios = new Ext.Panel(
        {
            frame : true,
            autoHeight : true,
            flex : 0
        });

        return cabeceraComentarios;
    },

    getGridComentarios : function()
    {

        var grid = new UJI.GDO.GridComentarios(
        {
            height : 499,
            autoScroll : true,
            'asignatura' : this.asignatura,
            'editable' : false
        });

        return grid;
    }
});