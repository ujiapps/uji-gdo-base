Ext.ns('UJI.GDO');

UJI.GDO.DirectoresExtra = Ext.extend(Ext.Panel, {
	title : "Permisos extra",
	layout : 'vbox',
	closable : true,
	layoutConfig : {
		align : 'stretch'
	},
	editor : {},
	storePersonas : {},
	storeTitulaciones : {},
	storeTiposPermisos : {},
	comboPersonas : {},
	comboTitulaciones : {},
	comboTiposPermisos : {},
	gridDirectores : {},
	storeGrid : {},
	panelSuperior : {},

	initComponent : function() {
		var config = {};
		Ext.apply(this, Ext.apply(this.initialConfig, config));

		UJI.GDO.DirectoresExtra.superclass.initComponent.call(this);

		this.initUI();
		syncStoreLoad([this.storePersonas, this.storeTitulaciones,
				this.storeTiposPermisos, this.storeGrid]);

		this.panelSuperior = this.getPanelSuperior();
		this.add(this.panelSuperior);
		this.add(this.gridDirectores);

	},

	initUI : function() {
		var ref = this;
		this.editor = new Ext.ux.grid.RowEditor({
			saveText : 'Actualitzar',
			cancelText : 'Cancelar',
			errorSummary : false,
			floating : {
				zindex : 7000
			},
			listeners : {
				beforeedit : function(ed, rowIndex) {
					if (ref.gridDirectores.getSelectionModel().getSelections().length == 0) {
						return;
					}

					var personaId = ref.gridDirectores.getSelectionModel()
							.getSelections()[0].get('personaId');
					var personaNombre = ref.storePersonas.getById(personaId).data.nombreCompleto;

					ref.comboPersonas.loadRecord(personaId, personaNombre);
				}
			}
		});

		this.storePersonas = this.getStorePersonas();
		this.storeTitulaciones = this.getStoreTitulaciones();
		this.storeTiposPermisos = this.getStoreTiposPermisos();
		this.storeGrid = this.getStoreGrid();

		this.comboPersonas = this.getComboPersonas();
		this.comboTitulaciones = this.getComboTitulaciones();
		this.comboTiposPermisos = this.getComboTiposPermisos();
		this.gridDirectores = this.getGridDirectores();

	},

	getPanelSuperior : function() {
		var panel = new Ext.Panel({
					flex : 0,
					height : 33,
					frame : true,
					html : '<h1 style="font-size:150%">Gestió permisos extra</h1>'
				});
		return panel;
	},
	getStorePersonas : function() {
		var store = new Ext.data.Store({
					restful : true,
					url : '/gdo/rest/persona',
					reader : new Ext.data.XmlReader({
								record : 'Persona',
								idProperty : 'id'
							}, [{
										name : 'id',
										type : 'number'
									}, 'nombreCompleto'])
				});

		return store;
	},

	getComboPersonas : function() {

		var combo = new Ext.ux.uji.form.LookupComboBox({
					anchor : '50%',
					appPrefix : 'gdo',
					bean : 'persona'
				});

		return combo;
	},

	getStoreTitulaciones : function() {
		var store = new Ext.data.Store({
					restful : true,
					url : '/gdo/rest/titulacion/all',
					reader : new Ext.data.XmlReader({
								record : 'Titulacion',
								idProperty : 'id'
							}, [{
										name : 'id',
										type : 'number'
									}, {
										name : 'nombre',
										type : 'string'
									}])
				});

		return store;
	},

	getComboTitulaciones : function() {
		var combo = new Ext.form.ComboBox({
					triggerAction : 'all',
					forceSelection : true,
					editable : false,
					displayField : 'nombre',
					valueField : 'id',
					store : this.storeTitulaciones
				});

		return combo;
	},

	getStoreTiposPermisos : function() {
		var store = new Ext.data.Store({
					restful : true,
					url : '/gdo/rest/director/tipospermiso',
					reader : new Ext.data.XmlReader({
								record : 'permiso',
								idProperty : 'id'
							}, [{
										name : 'id',
										type : 'string'
									}, {
										name : 'nombre',
										type : 'string'
									}])
				});

		return store;
	},

	getComboTiposPermisos : function() {
		var combo = new Ext.form.ComboBox({
					triggerAction : 'all',
					forceSelection : true,
					editable : false,
					displayField : 'nombre',
					valueField : 'id',
					store : this.storeTiposPermisos
				});

		return combo;
	},

	getStoreGrid : function() {
		var store = new Ext.data.Store({
					restful : true,
					url : '/gdo/rest/director',
					reader : new Ext.data.XmlReader({
								record : 'DirectorExtra',
								idProperty : 'id'
							}, ['id', 'personaId', 'titulacionId', 'usuario',
									'tipoPermiso', 'cursoId']),
					writer : new Ext.data.XmlWriter({
								xmlEncoding : 'UTF-8',
								writeAllFields : true
							}),
					listeners : {
						save : function(store, batch, data) {
							if (data.destroy != null) {
								this.reload();
							}
						}
					}
				});
		return store;
	},

	getGridDirectores : function() {
		var ref = this;

		Ext.util.Format.comboRenderer = function(combo) {
			return function(value) {
				var record = combo.findRecord(combo.valueField, value);

				return record
						? record.get(combo.displayField)
						: combo.valueNotFoundText;

			};
		};
		var comboEditor = new Ext.form.ComboBox({
					triggerAction : 'all',
					forceSelection : true,
					editable : false,
					displayField : 'nombreCompleto',
					valueField : 'id',
					store : this.storePersonas
				});

		var gridDirectores = new Ext.grid.GridPanel({
			frame : true,
			flex : 1,
			loadMask : true,
			viewConfig : {
				forceFit : true
			},
			sm : new Ext.grid.RowSelectionModel({
						singleSelect : true
					}),
			plugins : [this.editor],
			store : this.storeGrid,
			colModel : new Ext.grid.ColumnModel({
						columns : [{
									header : 'Id',
									dataIndex : 'id',
									hidden : true
								}, {
									header : 'Estudi',
									dataIndex : 'titulacionId',
									allowBlank : false,
									editor : ref.comboTitulaciones,
									renderer : Ext.util.Format
											.comboRenderer(ref.comboTitulaciones)
								}, {
									header : 'Nom',
									dataIndex : 'personaId',
									allowBlank : false,
									editor : ref.comboPersonas,
									renderer : Ext.util.Format
											.comboRenderer(comboEditor)
								}, {
									header : 'Usuari',
									dataIndex : 'usuario',
									width : 40
								}, {
									header : 'Permis',
									dataIndex : 'tipoPermiso',
									allowBlank : false,
									width : 40,
									editor : ref.comboTiposPermisos,
									renderer : Ext.util.Format
											.comboRenderer(ref.comboTiposPermisos)
								}, {
									header : 'Curs',
									dataIndex : 'cursoId',
									width : 20,
									editor : new Ext.form.NumberField({
												allowDecimals : false
											})
								}]
					}),
			tbar : {
				items : [{
							xtype : 'button',
							text : 'Afegir',
							iconCls : 'application-add',
							handler : function(btn, evt) {
								var rec = new gridDirectores.store.recordType({
											id : '',
											usuario : '',
											personaId : '',
											titulacionId : ''
										});

								ref.editor.stopEditing();
								gridDirectores.store.insert(0, rec);
								ref.editor.startEditing(0);
							}
						}, '-', {
							xtype : 'button',
							text : 'Esborrar',
							iconCls : 'application-delete',
							handler : function(btn, evt) {
								var rec = gridDirectores.getSelectionModel()
										.getSelected();
								if (!rec) {
									return false;
								}
								gridDirectores.store.remove(rec);
							}
						}]
			}

		});

		return gridDirectores;
	}
});