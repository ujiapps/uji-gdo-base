Ext.ns('UJI.GDO');

UJI.GDO.AsignaProfesor = Ext
        .extend(
                Ext.Panel,
                {
                    title : 'Assignació de professors a assignatures',
                    closable : true,
                    layout : 'vbox',
                    // frame : true,
                    layoutConfig :
                    {
                        align : 'stretch'
                    },
                    dsAsignaturas : {},
                    dsPersonas : {},
                    comboTitulaciones : {},
                    comboPersonas : {},
                    formConsulta : {},
                    editor : {},
                    gridAsignaturas : {},
                    panelSuperior : {},

                    initComponent : function()
                    {
                        var config = {};
                        Ext.apply(this, Ext.apply(this.initialConfig, config));

                        UJI.GDO.AsignaProfesor.superclass.initComponent.call(this);

                        this.panelSuperior = this.getPanelSuperior();
                        this.dsPersonas = this.getDsPersonas();
                        this.dsAsignaturas = this.getDsAsignaturas();

                        this.initUI();

                        this.add(this.panelSuperior);
                        this.add(this.formConsulta);
                        this.add(this.gridAsignaturas);

                        var wait = Ext.Msg.show(
                        {
                            msg : 'S\'estan carregant les dades',
                            modal : true,
                            wait : true,
                            width : 300
                        });

                        syncStoreLoad([ this.dsPersonas, this.comboTitulaciones.store ], '',
                                function()
                                {
                                    wait.hide();
                                });
                    },

                    initUI : function()
                    {
                        var ref = this;
                        this.editor = new Ext.ux.grid.RowEditor(
                                {
                                    saveText : 'Actualizar',
                                    floating :
                                    {
                                        zindex : 7000
                                    },
                                    listeners :
                                    {
                                        beforeedit : function(ed, rowIndex)
                                        {
                                            if (ref.gridAsignaturas.getSelectionModel()
                                                    .getSelections().length == 0)
                                            {
                                                return;
                                            }

                                            var profesorId = ref.gridAsignaturas
                                                    .getSelectionModel().getSelections()[0]
                                                    .get('personaId');
                                            if (profesorId != '' && profesorId != null
                                                    && profesorId != undefined)
                                            {
                                                var profesorNombre = ref.dsPersonas
                                                        .getById(profesorId).data.nombreCompleto;
                                                ref.comboPersonas.loadRecord(profesorId,
                                                        profesorNombre);
                                            }
                                        }
                                    }
                                });

                        this.comboTitulaciones = this.getComboTitulaciones();
                        this.comboPersonas = this.getComboPersonas();
                        this.formConsulta = this.getFomConsulta();
                        this.gridAsignaturas = this.getGridAsignaturas();
                    },

                    getPanelSuperior : function()
                    {
                        var panel = new Ext.Panel(
                        {
                            flex : 0,
                            height : 33,
                            frame : true,
                            html : '<h1 style="font-size:150%">Asignació de professors</h1>'
                        });
                        return panel;
                    },

                    getDsAsignaturas : function()
                    {
                        var store = new Ext.data.Store(
                        {
                            restful : true,
                            proxy : new Ext.data.HttpProxy(
                            {
                                url : '/gdo/rest/asignatura?curso=0&titulacion=0',
                                api :
                                {
                                    update : '/gdo/rest/asignatura/asignaprofe'
                                }
                            }),
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'Asignatura',
                                id : 'id'
                            }, [ 'id', 'nombre', 'personaId', 'activa', 'estado',
                                    'asignaturaRelacionada', 'fechaInicio', 'fechaFin','tipoCompartida' ]),
                            writer : new Ext.data.XmlWriter(
                            {
                                writeAllFields : true,
                                record : 'Asignatura',
                                id : 'id'
                            }, [ 'id', 'nombre', 'personaId', 'activa', 'estado',
                                    'asignaturaRelacionada', 'fechaInicio', 'fechaFin','tipoCompartida' ])
                        });

                        return store;
                    },

                    getDsPersonas : function()
                    {
                        var store = new Ext.data.Store(
                        {
                            restful : true,
                            url : '/gdo/rest/persona',
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'Persona',
                                idProperty : 'id'
                            }, [
                            {
                                name : 'id',
                                type : 'number'
                            }, 'nombreCompleto' ])
                        });

                        return store;
                    },

                    getComboPersonas : function()
                    {
                        return new Ext.ux.uji.form.LookupComboBox(
                        {
                            anchor : '50%',
                            appPrefix : 'gdo',
                            bean : 'persona'
                        });
                    },

                    getComboTitulaciones : function()
                    {
                        var ref = this;

                        var combo = new Ext.form.ComboBox(
                        {
                            emptyText : 'Selecciona una titulació',
                            triggerAction : 'all',
                            editable : false,
                            forceSelection : true,
                            width : 500,
                            defaults :
                            {
                                width : 500
                            },
                            hiddenName : 'nombre',
                            fieldLabel : 'Titulació',
                            displayField : 'nombre',
                            valueField : 'id',
                            store : new Ext.data.Store(
                            {
                                restful : true,
                                url : '/gdo/rest/titulacion',
                                reader : new Ext.data.XmlReader(
                                {
                                    record : 'TitulacionPersona',
                                    id : 'id'
                                }, [ 'id', 'nombre' ])
                            }),
                            listeners :
                            {
                                select : function(combo, value)
                                {

                                    if (combo.value)
                                    {
                                        var titulacionId = combo.value;
                                        ref.dsAsignaturas.proxy
                                                .setUrl('/gdo/rest/asignatura/?curso=0&titulacion='
                                                        + titulacionId + '&pantalla=Sinlimite');
                                        ref.dsAsignaturas.reload();

                                    }
                                    else
                                    {
                                        Ext.Msg.alert("Error", "Per a editar, primer cal"
                                                + " seleccionar una titulació");
                                    }

                                }
                            }
                        });

                        return combo;
                    },

                    getFomConsulta : function()
                    {
                        var form = new Ext.FormPanel(
                        {
                            name : 'formSelect',
                            frame : true,
                            layout : 'form',
                            defaults :
                            {
                                margins : '10 10 10 0'
                            },
                            items : [ this.comboTitulaciones ]
                        });

                        return form;
                    },

                    getGridAsignaturas : function()
                    {
                        var ref = this;

                        Ext.util.Format.comboRenderer = function(combo)
                        {
                            return function(value)
                            {
                                if (value != '' && value != null && value != undefined)
                                {
                                    var profesorNombre = ref.dsPersonas.getById(value).data.nombreCompleto;
                                    var valueNotFound = combo.valueNotFoundText ? combo.valueNotFoundText
                                            : value;

                                    return profesorNombre ? profesorNombre : valueNotFound;
                                }
                                else
                                {
                                    return '';
                                }
                            };
                        };

                        var comboEditor = new Ext.form.ComboBox(
                        {
                            triggerAction : 'all',
                            forceSelection : true,
                            editable : false,
                            displayField : 'nombre',
                            valueField : 'id',
                            store : this.dsPersonas
                        });

                        var grid = new Ext.grid.GridPanel(
                        {
                            viewConfig :
                            {
                                forceFit : true
                            },
                            sm : new Ext.grid.RowSelectionModel(
                            {
                                singleSelect : true
                            }),
                            flex : 1,
                            frame : true,
                            plugins : [ this.editor ],
                            loadMask : true,
                            border : false,
                            store : this.dsAsignaturas,
                            colModel : new Ext.grid.ColumnModel(
                            {
                                defaults :
                                {
                                    sortable : true
                                },
                                columns : [
                                {
                                    hidden : true,
                                    dataIndex : 'id'
                                },
                                {
                                    hidden : true,
                                    dataIndex : 'fechaInicio'
                                },
                                {
                                    hidden : true,
                                    dataIndex : 'fechaFin'
                                },
                                {
                                    header : 'Asignatura',
                                    dataIndex : 'nombre'
                                },
                                {
                                    header : 'Professor',
                                    renderer : Ext.util.Format.comboRenderer(comboEditor),
                                    dataIndex : 'personaId',
                                    editor : this.comboPersonas
                                } ]
                            })
                        });

                        return grid;
                    }
                });
