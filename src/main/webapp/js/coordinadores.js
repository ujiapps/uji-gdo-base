Ext.ns('UJI.GDO');

UJI.GDO.Coordinadores = Ext.extend(Ext.Panel,
    {
        title: 'Coordinadors',
        closable: true,
        layout: 'vbox',
        asignatura: 0,
        cursoActivo: {},
        ventanaModal: {},
        cabeceraModal: {},
        descripModal: {},
        panelModificaciones: {},
        botonGuardar: {},
        layoutConfig: {
            align: 'stretch'
        },
        initComponent: function () {
            var config = {};
            Ext.apply(this, Ext.apply(this.initialConfig, config));

            UJI.GDO.Coordinadores.superclass.initComponent.call(this);

            this.storeAsignaturas = this.getStoreAsignaturas();
            this.storeEstadosAsi = this.getStoreEstadosAsi();
            this.storeTitulaciones = this.getStoreTitulaciones();
            this.storeCursos = this.getStoreCursos();

            this.botonComentarios = this.getBotonComentarios();
            this.botonModificaciones = this.getBotonModificaciones();
            this.botonGuardarComentario = this.getBotonGuardaComentario();

            this.comboEstadosAsi = this.getComboEstadosAsi();
            this.comboTitulaciones = this.getComboTitulaciones();
            this.comboCursos = this.getComboCursos();

            this.campoIntroComentario = this.getCampoIntroComentario();

            this.gridAsignaturas = this.getGridAsignaturas();
            this.panelResumen = this.getPanelResumen();

            syncStoreLoad([this.storeEstadosAsi, this.storeAsignaturas]);

            this.panelSuperior = this.getPanelSuperior();
            this.panelFiltro = this.getPanelFiltro();
            this.panelIntroComentarios = this.getPanelIntroComentarios();

            this.add(this.panelSuperior);
            this.add(this.panelFiltro);
            this.add(this.gridAsignaturas);
            this.add(this.panelResumen);
        },

        getBotonComentarios: function () {
            var ref = this;
            var win;
            var boton = new Ext.Button(
                {
                    text: 'Comentaris a la validació',
                    iconCls: 'icon-add-tab',
                    handler: function () {
                        sm = ref.gridAsignaturas.getSelectionModel();
                        if (sm.getSelections()[0]) {
                            if (sm.getSelections()[0].data.asignaturaRelacionada != "") {
                                Ext.Msg.show(
                                    {
                                        title: "Guia no editable",
                                        icon: Ext.MessageBox.WARNING,
                                        msg: "Aquesta assignatura està basada en una altra." + " Fiqueu els comentaris en l\'assignatura original."
                                    });
                                return false;
                            }
                            else {
                                win = ref.getVentanaModal();
                                win.show();
                            }
                        }
                        else {
                            Ext.Msg.show(
                                {
                                    title: "Triar guia",
                                    icon: Ext.MessageBox.WARNING,
                                    msg: "Deus triar una assignatura"
                                });
                            return false;
                        }
                    }
                });

            return boton;
        },

        getBotonModificaciones: function () {
            var ref = this;

            var boton = new Ext.Button(
                {
                    text: 'Veure Modificacions',
                    iconCls: 'icon-add-tab',
                    handler: function () {
                        var rec = ref.gridAsignaturas.getSelectionModel().getSelections()[0];

                        if (!rec) {
                            Ext.Msg.show(
                                {
                                    title: "Modificacions",
                                    icon: Ext.MessageBox.WARNING,
                                    msg: "Deus triar una assignatura"
                                });
                            return false;
                        }

                        ref.abreVentanaModal();
                    }
                }
            );

            return boton;
        },

        getBotonGuardaComentario: function () {
            var ref = this;
            var boton = new Ext.Button(
                {
                    text: 'Afegir',
                    iconCls: 'icon-add-tab',
                    handler: function () {
                        if ((ref.campoIntroComentario.getValue() != '') && (ref.comboIdiomas.value != undefined) && (ref.comboIdiomas.value != '')) {
                            var rec = new ref.gridComentarios.store.recordType(
                                {
                                    comentario: ref.campoIntroComentario.getValue(),
                                    asignaturaId: ref.asignatura,
                                    idioma: ref.comboIdiomas.value
                                });
                            ref.gridComentarios.store.insert(0, rec);
                            ref.gridComentarios.store.proxy.setUrl('/gdo/rest/asignatura/' + ref.asignatura + '/comentario');
                            ref.campoIntroComentario.setValue('');
                        }
                        else {
                            Ext.Msg.alert('Comentaris', 'El camp comentari y el idioma és obligatori');
                        }

                    }
                });
            return boton;
        },

        getComboEstadosAsi: function () {
            var ref = this;
            return new Ext.form.ComboBox(
                {
                    mode: 'local',
                    emptyText: 'trieu...',
                    triggerAction: 'all',
                    forceSelection: true,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    autoCreate: {
                        tag: 'input',
                        size: '8'
                    },
                    store: this.storeEstadosAsi,
                    listeners: {
                        beforeshow: function (combo) {
                            var record = ref.gridAsignaturas.getSelectionModel().getSelections()[0];

                            ref.storeEstadosAsi.proxy.conn.url = '/gdo/rest/asignatura/' + record.get("id") + '/estados?coordinador=true';

                            ref.storeEstadosAsi.load(
                                {
                                    callback: function () {
                                        ref.comboEstadosAsi.setValue(record.get("estado"));
                                        Ext.util.Format.comboRenderer(ref.comboEstadosAsi);
                                    }
                                });
                        }
                    }
                });
        },

        getComboIdiomas: function () {

            return new Ext.ux.uji.form.StaticComboBox(
                {
                    fieldLabel: 'Idioma',
                    hiddenName: 'idioma',
                    staticData: [['', '-- Triar idioma --'], ['TD', 'Tots'], ['CA', 'Valencià'], ['ES', 'Espanyol'], ['UK', 'Anglés']]
                });
        },

        getComboTitulaciones: function () {
            var ref = this;
            var combo = new Ext.form.ComboBox(
                {
                    emptyText: 'Selecciona una titulació',
                    triggerAction: 'all',
                    editable: false,
                    forceSelection: true,
                    width: 660,
                    hiddenName: 'nombre',
                    fieldLabel: 'Titulació',
                    displayField: 'nombre',
                    valueField: 'id',
                    defaults: {
                        width: 660
                    },
                    store: this.storeTitulaciones,
                    listeners: {
                        'select': function (combo, value) {
                            ref.comboCursos.clearValue();
                            ref.storeCursos.proxy.setUrl('/gdo/rest/titulacion/' + value.id);
                            ref.storeCursos.reload();
                            ref.storeAsignaturas.proxy.conn.url = '/gdo/rest/asignatura' + '?titulacion=' + 0 + '&curso=' + 0;
                            ref.storeAsignaturas.reload();

                        }
                    }
                });

            return combo;
        },

        getComboCursos: function () {
            var ref = this;
            var combo = new Ext.form.ComboBox(
                {
                    emptyText: 'Selecciona un curs',
                    triggerAction: 'all',
                    editable: false,
                    forceSelection: true,
                    mode: 'local',
                    width: 150,
                    defaults: {
                        width: 150
                    },
                    hiddenName: 'nombre',
                    fieldLabel: 'Curs',
                    displayField: 'nombre',
                    valueField: 'id',
                    store: this.storeCursos,
                    listeners: {
                        'select': function (combo, value) {
                            var titulacionId = ref.comboTitulaciones.getValue();
                            ref.storeAsignaturas.proxy.conn.url = '/gdo/rest/asignatura' + '?titulacion=' + titulacionId + '&curso=' + value.id + '&pantalla=Sinlimite';
                            ref.storeAsignaturas.reload();
                        }
                    }
                });

            return combo;
        },

        getVentanaModal: function () {
            this.panelAyuda = this.getPanelAyuda();
            this.gridComentarios = this.getGridComentarios();
            this.panelIntroComentarios = this.getPanelIntroComentarios();

            var win = new Ext.Window(
                {
                    title: 'Comentaris',
                    layout: 'fit',
                    modal: true,
                    width: 800,
                    height: 410,
                    plain: true,
                    items: new Ext.Panel(
                        {
                            layout: 'vbox',
                            layoutConfig: {
                                align: 'stretch'
                            },

                            items: [this.panelAyuda, this.panelIntroComentarios, this.gridComentarios]
                        })
                });

            return win;
        },

        getPanelIntroComentarios: function () {
            this.comboIdiomas = this.getComboIdiomas();
            this.campoIntroComentario = this.getCampoIntroComentario();
            this.botonGuardarComentario = this.getBotonGuardaComentario();

            var panel = new Ext.Panel(
                {
                    layout: 'form',
                    flex: 0,
                    height: 150,
                    padding: 10,
                    frame: true,
                    items: [this.comboIdiomas, this.campoIntroComentario, this.botonGuardarComentario]
                });
            return panel;
        },

        getPanelFiltro: function () {
            var panelFiltro = new Ext.Panel(
                {
                    layout: 'hbox',
                    flex: 0,
                    height: 90,
                    padding: 10,
                    frame: true,
                    items: [
                        {
                            layout: 'form',
                            flex: 0,
                            border: false,
                            labelWidth: 55,
                            items: [this.comboTitulaciones, this.comboCursos]
                        }]
                });
            return panelFiltro;
        },

        getCampoIntroComentario: function () {
            var campoIntroComentario = new Ext.form.TextArea(
                {
                    fieldLabel: 'Comentari',
                    width: 600
                });
            return campoIntroComentario;
        },

        getPanelAyuda: function () {
            var ref = this;

            var panelAyuda = new Ext.Panel(
                {
                    frame: true,
                    height: 30,
                    flex: 0
                });

            var descripStore = new Ext.data.Store(
                {
                    url: '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/I',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'textoAyudaFormulariosUI',
                            id: 'asignatura'
                        }, ['asignatura']),
                    listeners: {
                        load: function (store, records, options) {
                            var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>');
                            var resultado = tpl.apply(records[0].data);
                            panelAyuda.update(resultado);
                            ref.doLayout();
                        }
                    }
                });

            descripStore.load();
            return panelAyuda;
        },

        getPanelSuperior: function () {
            var panel = new Ext.Panel(
                {
                    flex: 0,
                    height: 33,
                    frame: true,
                    html: '<h1 style="font-size:150%">Gestió de coordinadors</h1>'
                });
            return panel;
        },

        getPanelResumen: function () {
            var panel = new Ext.Panel(
                {
                    flex: 0,
                    title: 'Apartats pendents',
                    frame: true,
                    autoScroll: true,
                    height: 150
                });

            return panel;
        },

        getGridAsignaturas: function () {
            Ext.util.Format.comboRenderer = function (combo) {
                return function (value) {
                    var record = combo.findRecord(combo.valueField, value);
                    return record ? record.get(combo.displayField) : combo.valueNotFoundText;
                };
            };

            var ref = this;

            var editor = new Ext.ux.grid.RowEditor(
                {
                    saveText: 'Actualizar',
                    floating: {
                        zindex: 7000
                    },
                    listeners: {
                        beforeedit: {
                            fn: function (rowEditor, obj, data, rowIndex) {
                                sm = ref.gridAsignaturas.getSelectionModel();
                                reg = sm.getSelections()[0].data;

                                compruebaCheck(checkEditorCa, reg.apartadosCaLlenos, reg.apartadosTotales, reg.fechaValidacionCa);
                                compruebaCheck(checkEditorEs, reg.apartadosEsLlenos, reg.apartadosTotales, reg.fechaValidacionEs);
                                compruebaCheck(checkEditorUk, reg.apartadosUkLlenos, reg.apartadosTotales, reg.fechaValidacionUk);

                                if (sm.getSelections()[0].data.asignaturaRelacionada != "") {
                                    Ext.Msg.show(
                                        {
                                            title: "Guia no editable",
                                            icon: Ext.MessageBox.WARNING,
                                            msg: "Aquesta assignatura està basada en una altra." + " No es pot editar."
                                        });
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
                        }
                    }
                });

            function compruebaCheck(check, rellenados, totales, validacion) {

                if ((validacion == '') && (parseFloat(rellenados) < parseFloat(totales))) {
                    check.setDisabled(true);
                }
                else {
                    check.setDisabled(false);
                }
            }

            function renderCA(value, metaData, record) {
                return renderGeneral(record.data.apartadosCaLlenos, record.data.apartadosTotales, record.data.fechaValidacionCa);
            }

            function renderES(value, metaData, record) {
                return renderGeneral(record.data.apartadosEsLlenos, record.data.apartadosTotales, record.data.fechaValidacionEs);
            }

            function renderUK(value, metaData, record) {
                return renderGeneral(record.data.apartadosUkLlenos, record.data.apartadosTotales, record.data.fechaValidacionUk);
            }

            function renderGeneral(rellenados, totales, validacion) {

                var cadena = padLeft(rellenados, "0", 2) + ' / ' + padLeft(totales, "0", 2);

                if (validacion != '') {
                    return '<img src="/gdo/img/check-status.png"' + ' style="vertical-align:text-bottom;" />&nbsp;' + cadena;
                }
                else {
                    if (parseFloat(rellenados) >= parseFloat(totales)) {
                        return '<img src="/gdo/img/bola-naranja.png"' + ' style="vertical-align:text-bottom;" />&nbsp;' + cadena;
                    }
                    else {
                        return '<img src="/gdo/img/bola-roja.png"' + ' style="vertical-align:text-bottom;" />&nbsp;' + cadena;
                    }
                }
            }

            var checkEditorCa = new Ext.form.Checkbox({});
            var checkEditorEs = new Ext.form.Checkbox({});
            var checkEditorUk = new Ext.form.Checkbox({});

            function imprimirGuia(idiomaId) {
                var recordAsignatura = ref.gridAsignaturas.getSelectionModel().getSelections()[0];
                if (recordAsignatura) {
                    var codigoAsignatura = recordAsignatura.get("nombre").split("-")[0].trim();

                    window.open("https://e-ujier.uji.es/pls/www/gri_www.euji22883_html" + '?p_curso_aca=' + ref.cursoActivo + '&p_asignatura_id=' + codigoAsignatura + "&p_idioma=" + idiomaId);
                }
                else {
                    Ext.Msg.show(
                        {
                            title: "Imprimir guia",
                            icon: Ext.MessageBox.WARNING,
                            msg: "Deus triar una assignatura"
                        });
                    return false;
                }
            }

            var grid = new Ext.grid.GridPanel(
                {
                    viewConfig: {
                        forceFit: true
                    },
                    sm: new Ext.grid.RowSelectionModel(
                        {
                            singleSelect: true
                        }),
                    flex: 1,
                    frame: true,
                    plugins: [editor],
                    loadMask: true,
                    store: this.storeAsignaturas,
                    colModel: new Ext.grid.ColumnModel(
                        {
                            defaults: {
                                sortable: true
                            },

                            columns: [
                                {
                                    id: 'id',
                                    header: 'Id',
                                    hidden: true,
                                    dataIndex: 'id'
                                },
                                {
                                    header: 'Asignatura',
                                    width: 150,
                                    dataIndex: 'nombre'
                                },
                                {
                                    header: 'Activa',
                                    dataIndex: 'activa',
                                    hidden: true
                                },
                                {
                                    header: 'Curs',
                                    width: 20,
                                    dataIndex: 'curso'
                                },
                                {
                                    header: 'Basada en',
                                    width: 50,
                                    dataIndex: 'asignaturaRelacionada'
                                },
                                {
                                    header: 'Nova Implantació',
                                    width: 50,
                                    dataIndex: 'nuevaImplantacion'
                                },
                                {
                                    header: 'Estat',
                                    dataIndex: 'estado',
                                    width: 65,
                                    editor: ref.comboEstadosAsi,
                                    renderer: Ext.util.Format.comboRenderer(ref.comboEstadosAsi)
                                },
                                {
                                    header: 'Comentaris',
                                    width: 40,
                                    dataIndex: 'numeroComentarios'
                                },
                                {
                                    header: 'Modific.',
                                    width: 35,
                                    dataIndex: 'numeroModificaciones'
                                },
                                {
                                    header: 'Inici visibilitat',
                                    width: 50,
                                    dataIndex: 'fechaInicio',
                                    xtype: 'datecolumn',
                                    format: 'd/m/Y',
                                    editor: new Ext.form.DateField(
                                        {
                                            startDay: 1,
                                            allowBlank: true,
                                            format: 'd/m/Y'
                                        })
                                },
                                {
                                    header: 'Fi visibilitat',
                                    width: 50,
                                    dataIndex: 'fechaFin',
                                    xtype: 'datecolumn',
                                    format: 'd/m/Y',
                                    editor: new Ext.form.DateField(
                                        {
                                            startDay: 1,
                                            allowBlank: true,
                                            format: 'd/m/Y'
                                        })
                                },
                                {
                                    header: 'autorizaPas',
                                    dataIndex: 'autorizaPas',
                                    hidden: true
                                },
                                {
                                    header: 'Valencià',
                                    width: 45,
                                    renderer: renderCA,
                                    dataIndex: 'fechaValidacionCa',
                                    editor: checkEditorCa
                                },
                                {
                                    header: 'Espanyol',
                                    width: 45,
                                    renderer: renderES,
                                    dataIndex: 'fechaValidacionEs',
                                    editor: checkEditorEs
                                },
                                {
                                    header: 'Anglès',
                                    width: 45,
                                    renderer: renderUK,
                                    dataIndex: 'fechaValidacionUk',
                                    editor: checkEditorUk
                                },
                                {
                                    xtype: 'datecolumn',
                                    header: 'F. Fin.',
                                    width: 55,
                                    dataIndex: 'fechaFinalizacion',
                                    format: 'd/m/Y'
                                }]
                        }),
                    listeners: {
                        rowclick: function (grid, rowIndex, evento) {
                            ref.asignatura = grid.getSelectionModel().getSelections()[0].data.id;

                            var resumenesJson = [];

                            function actualizaPanelResumen(resumenesJson) {

                                var resumenPlantilla = new Ext.XTemplate('<div style="float:left;width:250px;"><b>Valencià</b><br\>', '<tpl for=".">', '<span>{valorca}</span>', '</tpl>', '</div>',
                                    '<div style="float:left;width:250px;"><b>Espanyol</b><br\>', '<tpl for=".">', '<span>{valores}</span>', '</tpl>', '</div>',
                                    '<div style="float:left;width:250px;"><b>Anglés</b><br\>', '<tpl for=".">', '<span>{valoruk}</span>', '</tpl>', '</div>');

                                if (resumenesJson && resumenesJson.length > 0) {
                                    var contenido = resumenPlantilla.apply(resumenesJson);
                                    ref.panelResumen.update(contenido);
                                }
                                else {
                                    ref.panelResumen.update("");
                                }
                            }

                            function setResumenJson(resumenValorCa, resumenValorEs, resumenValorUk) {
                                if (resumenValorCa && resumenValorEs && resumenValorUk) {
                                    resumenesJson.push(
                                        {
                                            valorca: resumenValorCa,
                                            valores: resumenValorEs,
                                            valoruk: resumenValorUk
                                        });
                                }
                            }

                            function convierteXMLaJSON(response) {
                                var xml = response.responseXML;
                                var resumenesXml = Ext.DomQuery.jsSelect('texto', xml);

                                for (var i = 0, len = resumenesXml.length; i < len; i++) {
                                    var resumenValorCa = "";
                                    var resumenValorEs = "";
                                    var resumenValorUk = "";

                                    if (Ext.DomQuery.selectValue('valorca', resumenesXml[i])) {
                                        resumenValorCa = Ext.DomQuery.selectValue('valorca', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('valores', resumenesXml[i])) {
                                        resumenValorEs = Ext.DomQuery.selectValue('valores', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('valoruk', resumenesXml[i])) {
                                        resumenValorUk = Ext.DomQuery.selectValue('valoruk', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('idiomas', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('idiomas', resumenesXml[i]);
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('idiomas', resumenesXml[i]);
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('idiomas', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('evaluacionesca', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('evaluacionesca', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('evaluacioneses', resumenesXml[i])) {
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('evaluacioneses', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('evaluacionesuk', resumenesXml[i])) {
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('evaluacionesuk', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('actividades', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('actividades', resumenesXml[i]);
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('actividades', resumenesXml[i]);
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('actividades', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('competenciasca', resumenesXml[i])) {
                                        resumenValorCa = resumenValorCa + Ext.DomQuery.selectValue('competenciasca', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('competenciases', resumenesXml[i])) {
                                        resumenValorEs = resumenValorEs + Ext.DomQuery.selectValue('competenciases', resumenesXml[i]);
                                    }

                                    if (Ext.DomQuery.selectValue('competenciasuk', resumenesXml[i])) {
                                        resumenValorUk = resumenValorUk + Ext.DomQuery.selectValue('competenciasuk', resumenesXml[i]);
                                    }

                                    if (!resumenValorCa) {
                                        resumenValorCa = " ";
                                    }
                                    if (!resumenValorEs) {
                                        resumenValorEs = " ";
                                    }
                                    if (!resumenValorUk) {
                                        resumenValorUk = " ";
                                    }
                                    setResumenJson(resumenValorCa.replace(/\n/g, "<br>"), resumenValorEs.replace(/\n/g, "<br>"), resumenValorUk.replace(/\n/g, "<br>"));
                                }
                            }

                            if (ref.asignatura != null && ref.asignatura != "") {
                                Ext.Ajax.request(
                                    {
                                        url: '/gdo/rest/asignatura/' + ref.asignatura + '/apartados/obligatoriosvacios',
                                        success: function (response, opts) {
                                            convierteXMLaJSON(response);
                                            actualizaPanelResumen(resumenesJson);
                                        }
                                    });
                            }
                        }
                    },
                    tbar: {
                        items: [ref.botonComentarios, '-', ref.botonModificaciones, '-',
                            {
                                text: 'Imprimir guía',
                                iconCls: 'printer',
                                menu: {
                                    xtype: 'menu',
                                    border: false,
                                    plain: true,
                                    items: [
                                        {
                                            text: 'Valencià',
                                            handler: function () {
                                                var idioma = 'CA';
                                                imprimirGuia(idioma);
                                            }
                                        },
                                        {
                                            text: 'Castellà',
                                            handler: function () {

                                                var idioma = 'ES';
                                                imprimirGuia(idioma);
                                            }
                                        },
                                        {
                                            text: 'Angleś',
                                            handler: function () {

                                                var idioma = 'UK';
                                                imprimirGuia(idioma);
                                            }
                                        }]
                                }
                            }]
                    }
                });

            return grid;
        },

        getGridComentarios: function () {
            var grid = new UJI.GDO.GridComentarios(
                {
                    'asignatura': this.asignatura,
                    'editable': true
                });

            return grid;
        },

        getStoreCursos: function () {
            return new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/titulacion/'
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'TitulacionCurso',
                            id: 'id'
                        }, ['id', 'nombre'])
                });
        },

        getStoreTitulaciones: function () {
            return new Ext.data.Store(
                {
                    restful: true,
                    url: '/gdo/rest/titulacion',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'TitulacionPersona',
                            id: 'id'
                        }, ['id', 'nombre'])
                });
        },

        getStoreAsignaturas: function () {
            var ref = this;

            function campoValidado(value, record) {
                return (value != '') ? true : false;
            }

            var sAsignaturas = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/asignatura?titulacion=0&curso=0',
                            api: {
                                update: '/gdo/rest/asignatura/coordinadores'
                            }
                        }),

                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Asignatura',
                            id: 'id'
                        }, ['id', 'nombre', 'curso',
                            {
                                name: 'fechaInicio',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaFin',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaFinalizacion',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaValidacionCa',
                                convert: campoValidado
                            },
                            {
                                name: 'fechaValidacionEs',
                                convert: campoValidado
                            },
                            {
                                name: 'fechaValidacionUk',
                                convert: campoValidado
                            },
                            {
                                name: 'nuevaImplantacion',
                                convert: function (value, record) {
                                    return (value == 'true') ? 'Sí' : 'No';
                                }
                            }, 'asignaturaRelacionada', 'estado', 'apartadosCaLlenos', 'apartadosEsLlenos', 'apartadosUkLlenos', 'apartadosTotales', 'numeroComentarios','numeroModificaciones', 'notificaResponsable', 'notificaCoordinador',
                            'notificaSLT', 'notificaFinalizada']),

                    writer: new Ext.ux.uji.data.XmlWriter(
                        {
                            record: 'Asignatura',
                            id: 'id',
                            writeAllFields: true
                        }, ['id', 'nombre', 'estado', 'fechaInicio', 'fechaFin', 'apartadosCaLlenos', 'apartadosEsLlenos', 'apartadosUkLlenos', 'apartadosTotales', 'fechaValidacionCa', 'fechaValidacionEs',
                            'fechaValidacionUk', 'numeroComentarios','numeroModificaciones', 'notificaResponsable', 'notificaCoordinador', 'NotificaSLT', 'notificaFinalizada']),
                    listeners: {
                        write: function (store, action, result, res, rs) {
                            var titulacionId = ref.comboTitulaciones.getValue();
                            var cursoId = ref.comboCursos.getValue();
                            this.proxy.setUrl('/gdo/rest/asignatura?titulacion=' + titulacionId + '&curso=' + cursoId + '&pantalla=Sinlimite');

                            var asignaturaId = rs.data.id;

                            if (rs.data.notificaCoordinador == 'true') {
                                ref.enviaNotificacion('/gdo/rest/notifica/coordinador/' + asignaturaId, 'S\'ha enviat notificació' + ' al cooordinador o coordinadors.');
                            }
                            else if (rs.data.notificaResponsable == 'true') {
                                ref.enviaNotificacion('/gdo/rest/notifica/responsable/' + asignaturaId, 'S\'ha enviat notificació' + ' al responsable o responsables.');
                            }
                            else if (rs.data.notificaSLT == 'true') {
                                ref.enviaNotificacion('/gdo/rest/notifica/slt/' + asignaturaId, 'S\'ha enviat notificació al Servei de Llengües.');
                            }
                            else if (rs.data.notificaFinalizada == 'true') {
                                ref.enviaNotificacion('/gdo/rest/notifica/finalizada/' + asignaturaId, 'S\'ha enviat notificació' + ' al responsable o responsables.');
                            }
                            else {
                                ref.storeAsignaturas.reload();
                            }

                        }
                    }
                });

            return sAsignaturas;
        },

        enviaNotificacion: function (url, message) {
            var ref = this;

            Ext.Ajax.request(
                {
                    url: url,
                    method: 'POST',
                    success: function (result, request) {
                        var nombreDestinarios = Ext.DomQuery.selectValue('nombres', result.responseXML.documentElement);

                        Ext.Msg.alert('Informació de la notificació', message + '<br>(' + nombreDestinarios + ')');
                        ref.storeAsignaturas.load();
                    },
                    failure: function () {
                        Ext.Msg.alert('Error', 'Error en l\'enviament de la notificació');
                        ref.storeAsignaturas.load();
                    }
                });
        },

        getStoreEstadosAsi: function () {
            var sEstadosAsi = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/gdo/rest/asignatura/estados'
                        }),
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'estado',
                            id: 'id'
                        }, ['id', 'nombre'])
                });

            return sEstadosAsi;
        },

        getCampoIntroModificaciones: function () {
            var campoIntroModificaciones = new Ext.form.TextArea(
                {
                    fieldLabel: 'Modificacions',
                    name: 'modificaciones',
                    anchor: '100%',
                    height: '260',
                    readOnly : 'true',
                    maxLength: '4000',
                    maxLengthText: 'El quadre'
                    + ' de texte no pot superar els 4000 caràcters'
                });
            return campoIntroModificaciones;
        },

        getPanelModificaciones: function () {
            this.campoIntroModificaciones = this.getCampoIntroModificaciones();

            var formModifica = new Ext.FormPanel(
                {
                    frame: true,
                    flex: 1,
                    name: 'Modificaciones',
                    buttonAlign: 'center',
                    labelAlign: 'top',
                    url: '/gdo/rest/asignatura/',
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Asignatura',
                            id: 'id'
                        }, ['id', 'modificaciones']),
                    writer: new Ext.data.XmlWriter(
                        {
                            record: 'Asignatura',
                            writeAllFields: true,
                            id: 'id'
                        }, ['id', 'modificaciones']),
                    errorReader: new Ext.data.XmlReader(
                        {
                            record: 'respuestaForm',
                            success: 'success'
                        }, ['success', 'msg']),
                    items: [{
                        xtype: 'hidden',
                        name: 'id'
                    }, this.campoIntroModificaciones]

                }
            );

            return formModifica;
        },

        getVentanaModalModif: function () {
            this.panelAyuda = this.getPanelAyuda();
            this.panelModificaciones = this.getPanelModificaciones();

            var win = new Ext.Window(
                {
                    title: 'Modificacions',
                    layout: 'fit',
                    modal: true,
                    width: 800,
                    height: 410,
                    plain: true,
                    items: new Ext.Panel(
                        {
                            layout: 'vbox',
                            layoutConfig: {
                                align: 'stretch'
                            },
                            items: [this.panelAyuda, this.panelModificaciones]
                        })
                });

            return win;
        },

        cargaModificaciones: function () {
            var id = this.gridAsignaturas.selModel.getSelected().id;

            var f = this.panelModificaciones.getForm();
            f.load(
                {
                    url: '/gdo/rest/asignatura/' + id,
                    method: 'get',
                    success: function (e) {
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Error', 'Error al cargar el campo Modificaciones.');
                    }
                });

        },

        abreVentanaModal: function () {

            this.ventanaModal = this.getVentanaModalModif();
            this.ventanaModal.show();
            this.cargaModificaciones();
        }

    });