Ext.ns('UJI.GDO');

UJI.GDO.Actividades = Ext.extend(Ext.Panel,
{
    title : 'Activitats',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    asignatura : 0,
    descripStore : {},
    storeActividades : {},
    storeTiposActividad : {},
    comboActividades : {},
    editor : {},
    selectionModel : {},
    botonDelete : {},
    botonInsert : {},
    gridActividades : {},
    cabeceraActividades : {},
    storeEstadoPantalla : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.GDO.Actividades.superclass.initComponent.call(this);

        this.initUI();

        syncStoreLoad([ this.descripStore, this.storeEstadoPantalla, this.storeTiposActividad,
                this.storeActividades ]);

        this.add(this.cabeceraActividades);
        this.add(this.gridActividades);
    },

    initUI : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Guarda',
            cancelText : 'Cancel·la',
            errorSummary : false
        });

        this.selectionModel = new Ext.grid.RowSelectionModel(
        {
            singleSelect : true
        });

        this.botonDelete = this.getBotonActividadesDelete();
        this.botonInsert = this.getBotonActividadesInsert();
        this.descripStore = this.getDescripStore();
        this.storeActividades = this.getStoreActividades();
        this.storeEstadoPantalla = this.getStoreEstadoPantallaActividades();
        this.storeTiposActividad = this.getStoreTiposActividad();
        this.comboActividades = this.getComboActividades();
        this.gridActividades = this.getGridActividades();
        this.cabeceraActividades = this.getCabeceraActividades();
    },

    getDescripStore : function()
    {
        var ref = this;

        var store = new Ext.data.Store(
        {
            url : '/gdo/rest/asignatura/ayuda/' + this.asignatura + '/A',
            reader : new Ext.data.XmlReader(
            {
                record : 'textoAyudaFormulariosUI',
                id : 'asignatura'
            }, [ 'asignatura', 'texto' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    var tpl = new Ext.XTemplate('<p><b>{asignatura}</b></p>',
                            '<p style="margin-top:5px;">{texto}</p>');
                    var resultado = tpl.apply(records[0].data);
                    ref.cabeceraActividades.update(resultado);
                    ref.doLayout();
                }
            }
        });

        return store;
    },

    getCabeceraActividades : function()
    {
        var cabecera = new Ext.Panel(
        {
            frame : true,
            autoHeight : true,
            flex : 0
        });

        return cabecera;
    },

    getStoreActividades : function()
    {
        var ref = this;

        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/gdo/rest/actividad',
            baseParams :
            {
                asignaturaId : ref.asignatura
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'ActividadAsignatura',
                id : 'id'
            }, [ 'id', 'actividadId', 'horasPresenciales', 'horasNoPresenciales' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                save : function(store, batch, data)
                {
                    if (data.destroy != null)
                    {
                        this.reload();
                    }
                }
            }
        });

        return store;
    },

    getStoreTiposActividad : function()
    {
        var dsActividades = new Ext.data.Store(
        {
            url : '/gdo/rest/actividad/tiposactividad/',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Actividad',
                id : 'id'
            }, [ 'id',
            {
                name : 'nombre',
                mapping : 'nombreCa'
            } ])
        });

        return dsActividades;
    },

    getComboActividades : function()
    {
        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        var comboEditor = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            store : this.storeTiposActividad
        });

        return comboEditor;
    },

    getStoreEstadoPantallaActividades : function()
    {
        var ref = this;

        var storeEstadoPantalla = new Ext.data.Store(
        {
            url : '/gdo/rest/actividad/estado/' + this.asignatura,
            reader : new Ext.data.XmlReader(
            {
                record : 'OperacionesUI',
                id : 'asignaturaId'
            },
                    [ 'asignaturaId', 'allowDelete', 'allowInsert', 'allowUpdate',
                            'allowUpdateMaestro' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    var resultado = records[0].data;
                    if (resultado.allowUpdate == "false")
                    {
                        ref.gridActividades.plugins[0].disable();
                    }

                    if (resultado.allowUpdateMaestro == "true")
                    {
                        ref.comboActividades.enable();
                        ref.gridActividades.getColumnModel().setEditable(0, true);
                    }
                    else
                    {
                        ref.comboActividades.disable();
                        ref.gridActividades.getColumnModel().setEditable(0, false);
                    }

                    if (resultado.allowDelete == "true")
                    {
                        ref.botonDelete.setDisabled(false);
                    }

                    if (resultado.allowInsert == "true")
                    {
                        ref.botonInsert.setDisabled(false);
                    }
                }
            }
        });
        return storeEstadoPantalla;
    },

    getGridActividades : function()
    {

        var userColumns = [
        {
            header : "id",
            width : 25,
            sortable : true,
            hidden : true,
            dataIndex : 'id'
        },
        {
            header : 'Activitat',
            dataIndex : 'actividadId',
            editor : this.comboActividades,
            renderer : Ext.util.Format.comboRenderer(this.comboActividades)
        },
        {
            xtype : 'numbercolumn',
            format : '0.00',
            header : 'Hores presencials',
            width : 25,
            sortable : true,
            dataIndex : 'horasPresenciales',
            editor : new Ext.form.NumberField(
            {
                allowBlank : false,
                allowNegative : false,
                blankText : 'El camp "Hores presencials" es requereix',
                maxValue : 999,
                maxText : 'El valor màxim del camp "Hores presencials" és 999'
            })
        },
        {
            xtype : 'numbercolumn',
            format : '0.00',
            header : 'Hores no presencials',
            width : 25,
            sortable : true,
            dataIndex : 'horasNoPresenciales',
            editor : new Ext.form.NumberField(
            {
                allowBlank : false,
                allowNegative : false,
                blankText : 'El camp "Hores no presencials" es requereix',
                maxValue : 999,
                maxText : 'El valor màxim del camp "Hores no presencials" és 999'
            })
        } ];

        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : this.storeActividades,
            sm : this.selectionModel,
            plugins : [ this.editor ],
            columns : userColumns,
            tbar :
            {
                items : [ this.botonInsert, '-', this.botonDelete ]
            }
        });

        return grid;
    },

    getBotonActividadesDelete : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = ref.selectionModel.getSelected();
                if (!rec)
                {
                    return false;
                }
                ref.storeActividades.remove(rec);
            }
        });
        return botonDelete;
    },

    getBotonActividadesInsert : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = new ref.storeActividades.recordType(
                {
                    editable : true,
                    nombre : '',
                    id : '',
                    asignaturaId : ref.asignatura,
                    orden : '',
                    ponderacion : ''
                });

                ref.editor.stopEditing();
                ref.storeActividades.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
        return botonInsert;
    }
});