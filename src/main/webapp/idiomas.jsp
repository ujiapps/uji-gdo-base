<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>


<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>

<script type="text/javascript" src="js/idiomas.js"></script>

<title>Idiomas</title>

<script type="text/javascript">
   Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';   
   
   var asignatura = "<%=request.getParameter("asignaturaId")%>";

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelEdicion = new UJI.GDO.Resultados(
        {
            'asignatura' : asignatura
        });

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelEdicion ]
        });

    });
</script>

</head>

<body>
</body>
</html>