<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Lookup</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>

<script type="text/javascript" src="js/lookup.js"></script>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var item = new Ext.form.ComboBox(
        {
            fieldLabel : 'Test',
            triggerAction : 'all',
            mode : 'local',
            editable : false,
            width : 200,
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'id', 'nombre' ],
                data : [ [ '', 'Cargant cerca de registres ...' ] ]
            }),
            valueField : 'id',
            displayField : 'nombre',
            listeners :
            {
                'expand' : function(combo)
                {
                    var lookupWindow = getLookupWindow(item);
                    lookupWindow.show();
                }
            }
        });

        var form = new Ext.form.FormPanel(
        {
            items : [ item,
            {
                xtype : 'button',
                text : 'Buscar',
                handler : function(e)
                {
                    var lookupWindow = getLookupWindow(item);
                    lookupWindow.show();
                }
            } ]
        });

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ form ]
        });
    });
</script>
</head>
<body>
</body>
</html>
