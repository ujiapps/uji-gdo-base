<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>
<script type="text/javascript" src="js/competencias.js"></script>

<title>Administració de competències</title>

<script type="text/javascript">
   Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';   
   
   var asignaturaId = "<%=request.getParameter("asignaturaId")%> ";

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelEdicion = new UJI.GDO.Competencias(
        {
            asignatura : asignaturaId
        });

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelEdicion ]
        });

    });
</script>

</head>
<body>

</body>
</html>