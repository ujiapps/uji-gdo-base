<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Guía docente</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/data/XmlWriter/0.0.1/XmlWriter.js"></script>	
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/StaticComboBox/0.0.1/StaticComboBox.js"></script>

<script type="text/javascript" src="js/coordinadores.js"></script>
<script type="text/javascript" src="js/gridComentarios.js"></script>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelCoord = new UJI.GDO.Coordinadores({});

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelCoord ]
        });
    });
</script>
</head>

<body>
</body>
</html>