<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Guía docente</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>

<script type="text/javascript" src="js/actividades.js"></script>

<script type="text/javascript">
       Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';
       
       var asignaturaId = "<%=request.getParameter("asignaturaId")%>
    ";

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelActiv = new UJI.GDO.Actividades(
        {
            asignatura : asignaturaId
        });

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelActiv ]
        });
    });
</script>
</head>

<body>
</body>
</html>
