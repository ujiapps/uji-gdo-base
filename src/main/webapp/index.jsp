<%@page import="es.uji.apps.gdo.model.CursoAcademico"%>
<%@page import="es.uji.commons.sso.User"%>
<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link type="image/x-icon" rel="shortcut icon"
	href="http://e-ujier.uji.es/img/portal2/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/ext-3.3.0/resources/css/ext-all.css" />
<link rel="stylesheet"
	href="http://static.uji.es/js/extjs/ext-3.3.0/examples/ux/css/RowEditor.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.0/ext-all-debug.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/ckeditor/ckeditor-3.5.2/ckeditor.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/form/CKEditor/0.0.1/CKEditor.js"></script>

<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/TabCloseMenu/0.0.1/TabCloseMenu.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/TabPanel/0.0.1/TabPanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/ext-3.3.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/StaticComboBox/0.0.1/StaticComboBox.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupComboBox/0.0.1/LookupComboBox.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/data/XmlWriter/0.0.1/XmlWriter.js"></script>

<script type="text/javascript" src="js/administracion.js"></script>
<script type="text/javascript" src="js/coordinadores.js"></script>
<script type="text/javascript" src="js/actividades.js"></script>
<script type="text/javascript" src="js/textos.js"></script>
<script type="text/javascript" src="js/dashboard.js"></script>
<script type="text/javascript" src="js/evaluaciones.js"></script>
<script type="text/javascript" src="js/competencias.js"></script>
<script type="text/javascript" src="js/resultados.js"></script>
<script type="text/javascript" src="js/idiomas.js"></script>
<script type="text/javascript" src="js/cursos.js"></script>
<script type="text/javascript" src="js/directores.js"></script>
<script type="text/javascript" src="js/asignaProfesor.js"></script>
<script type="text/javascript" src="js/delegacion.js"></script>
<script type="text/javascript" src="js/comentarios.js"></script>
<script type="text/javascript" src="js/idiomasComentario.js"></script>
<script type="text/javascript" src="js/gridComentarios.js"></script>

<title>Gestió de guies docents</title>

<script type="text/javascript">
    function reemplazaCadenasDateField()
    {
        Ext.override(Ext.DatePicker, {
            todayText: 'Avui',
            monthNames : ['gener', 'febrer', 'març', 'abril', 'maig', 'juny', 'juliol', 'agost', 'setembre', 'octubre', 'novembre', 'decembre'],
            dayNames : ['g','l', 'm', 'c', 'j', 'v', 's'],
            nextText : 'Mes següent (Control+Dreta)',
            prevText : 'Mes anterior (Control+Esquerra)',
            monthYearText : 'Tria un mes (Control+Dalt/Baix per canviar l\'any)'
        });
    }
    
    Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';

    Ext.Ajax.defaultHeaders = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    };

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        reemplazaCadenasDateField();
        
        Ext.Ajax.on('requestcomplete', function(conn, response, options)
        {
        	var contentType = response.getResponseHeader("Content-Type");

            if (contentType && contentType.substring(0, 9) == "text/html")
            {
                window.location = "index.jsp?";
            }
        });

        Ext.Ajax.on('requestexception', function(conn, response, options)
        {
            if (response.responseXML)
            {
                var msgList = response.responseXML.getElementsByTagName("msg");

                if (msgList && msgList[0] && msgList[0].firstChild)
                {
                    alert(msgList[0].firstChild.nodeValue);
                }
	         }
	
	         if (response.status == 0)
	         {
	             window.location = "index.jsp?";
	         }
	     });
        
        var navigationTree = new Ext.tree.TreePanel(
        {
            title : 'Conectat com <%=((User) session.getAttribute(User.SESSION_USER))
					.getName()%> @',
                            region : 'west',
                            lines : false,
                            width : 235,
                            split : true,
                            collapsible : true,
                            autoScroll : true,
                            rootVisible : false,
                            bodyStyle : 'padding-bottom:20px;',
                            root : new Ext.tree.AsyncTreeNode(
                            {
                                expanded : true
                            }),
                            loader : new Ext.ux.tree.XmlTreeLoader(
                            {
                            	baseParams : {
                            		codigoAplicacion : 'GDO'
                            	},
                                dataUrl : '/gdo/rest/navigation/class',
                                processAttributes : function(attr)
                                {
                                    return false;
                                }
                            }),
                            listeners :
                            {
                                click : function(node, event)
                                {
                                    if (node.id == "administracion.js")
                                    {
                                        var newPanel = new UJI.GDO.Administracion(
                                                {
                                                    'cursoActivo' : <%=CursoAcademico.getCursoActivo().getCursoAca()%>
                                                });
                                        newPanel.on("newtab", function(params)
                                        {
                                            switch (params.pantalla)
                                            {
                                            case "actividades":
                                                var panelEdicion = new UJI.GDO.Actividades(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            case "evaluaciones":
                                                var panelEdicion = new UJI.GDO.Evaluaciones(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            case "competencias":
                                                var panelEdicion = new UJI.GDO.Competencias(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            case "resultados":
                                                var panelEdicion = new UJI.GDO.Resultados(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            case "textos":
                                                var panelEdicion = new UJI.GDO.Apartados(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                panelEdicion.doLayout(false, true);
                                                break;
                                            case "idiomas":
                                                var panelEdicion = new UJI.GDO.Idiomas(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            case "delegacion":
                                                var panelEdicion = new UJI.GDO.Delegaciones(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            case "comentarios":
                                                var panelEdicion = new UJI.GDO.Comentarios(
                                                {
                                                    'asignatura' : params.asignaturaId
                                                });
                                                tabPanel.addTab(panelEdicion);
                                                break;
                                            break;
                                        }
                                    }   );

                                        tabPanel.addTab(newPanel);
                                    }
                                    else if (node.id == "asignaProfesor.js")
                                    {
                                        var panelEdicion = new UJI.GDO.AsignaProfesor();
                                        tabPanel.addTab(panelEdicion);
                                    }
                                    else if (node.id == "cursos.js")
                                    {
                                        var panelEdicion = new UJI.GDO.Cursos();
                                        tabPanel.addTab(panelEdicion);
                                    }
                                    else if (node.id == "roles.js")
                                    {
                                        var panelEdicion = new UJI.GDO.DirectoresExtra();
                                        tabPanel.addTab(panelEdicion);
                                    }
                                    else if (node.id == "coordinadores.js")
                                    {
                                        var panelEdicion = new UJI.GDO.Coordinadores(
                                                {
                                                    'cursoActivo' : <%=CursoAcademico.getCursoActivo().getCursoAca()%>
                                                });
                                        tabPanel.addTab(panelEdicion);
                                    }
                                }
                            }
                        });

                var tabPanel = new Ext.ux.uji.TabPanel(
                {
                    deferredRender : false,
                    region : 'center'
                });

                var dashboardTabPanel = initGDODashboard();
                tabPanel.addTab(dashboardTabPanel);

                var logoPanel = new Ext.Panel(
                        {
                            region : 'north',
                            style : 'border:0px',
                            html : '<div style="background: url(http://e-ujier.uji.es/img/portal2/imagenes/cabecera_1px.png) repeat-x scroll left top transparent; height: 70px;">'
                                    + '  <img src="http://e-ujier.uji.es/img/portal2/imagenes/logo_uji_horizontal.png" style="float: left;margin: 10px 16px;">'
                                    + '  <div style="float:left; margin-top:11px;">'
                                    + '    <span style="color: rgb(255, 255, 255); font-family: Helvetica,Arial,sans-serif;font-size:1.2em;">E-UJIER@</span><br/>'
                                    + '    <span style="color: #CDCCE5; font-family: Helvetica,Arial,sans-serif;">Guies docents <%=CursoAcademico.getCursoActivo().getCursoAca()%>/<%=CursoAcademico.getCursoActivo().getCursoAca() + 1%> </span>'
                                    + '  </div>' + '</div>'
                        });

                new Ext.Viewport(
                {
                    layout : 'border',
                    items : [ logoPanel, navigationTree, tabPanel ]
                });
            });
</script>
</head>
<body>
</body>
</html>
