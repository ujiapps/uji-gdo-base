
CREATE TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     comentario VARCHAR2 (2000)  NOT NULL 
    ) 
;


CREATE INDEX uji_guiasdocentes.gdo_asi_comentarios__IDX ON gdo_asi_comentarios 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ADD CONSTRAINT gdo_asi_comentarios_PK PRIMARY KEY ( id ) ;

ALTER TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ADD CONSTRAINT gdo_asi_comentarios_FK FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;
   
grant select on uji_guiasdocentes.gdo_asi_comentarios to uji_guiasdocentes_usr;
grant insert on uji_guiasdocentes.gdo_asi_comentarios to uji_guiasdocentes_usr;
grant update on uji_guiasdocentes.gdo_asi_comentarios to uji_guiasdocentes_usr;
grant delete on uji_guiasdocentes.gdo_asi_comentarios to uji_guiasdocentes_usr;



   
	
	
   