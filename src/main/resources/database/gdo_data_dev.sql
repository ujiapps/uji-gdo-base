delete from uji_guiasdocentes.gdo_idiomas;
delete from uji_guiasdocentes.gdo_asi_textos;
delete from uji_guiasdocentes.gdo_tipo_actividades;
delete from uji_guiasdocentes.gdo_asignaturas;
delete from uji_guiasdocentes.gdo_apartados;
delete from uji_guiasdocentes.gdo_cursos_academicos;
delete from uji_guiasdocentes.gdo_ext_personas;
delete from uji_guiasdocentes.GDO_EXT_ASI_TODAS;
delete from uji_guiasdocentes.GDO_EXT_TITU_TODAS;
delete from uji_guiasdocentes.GDO_EXT_TITULACIONES_PER;
delete from uji_guiasdocentes.GDO_EXT_TITULACIONES_ASI;


-- gdo_cursos_academicos

insert into uji_guiasdocentes.gdo_cursos_academicos (curso_aca, activo, fecha_ini, fecha_fin)
  values (2010, 'S', sysdate-20, sysdate+20);

-- gdo_apartados

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '1. Informació general de l''assignatura', '1. Información general de la asignatura', 10, 'I');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '2. Justificació', '2. Justificación', 20, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '3. Coneixements previs recomanables', '3. Conocimientos previos recomendables', 30, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '4. Competències i resultats d''aprenentatge', '4. Competencias y resultados de aprendizaje', 40, 'C');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '5. Continguts', '5. Contenidos', 50, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '6. Temari', '6. Temario', 60, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '7. Bibliografia', '7. Bibliografía', 70, 'M');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '7.1. Bibliografia bàsica', '7.1. Bibliografía básica', 71, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '7.2. Bibliografia complementària', '7.2. Bibliografía complementaria', 72, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '7.3. Adreces web d''interés', '7.3. Direcciones web de interés', 73, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '7.4. Altres recursos', '7.4. Otros recursos', 74, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '8. Metodologia didàctica', '8. Metodología didáctica', 80, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '9. Planificació d''activitats', '9. Planificación de actividades', 90, 'A');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '10. Sistema d''avaluació', '10. Sistema de evaluación', 100, 'M');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '10.1. Tipus de prova', '10.1. Tipo de prueba', 101, 'E');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '10.2. Criteris de superació de l''assignatura', '10.2. Criterios de superación de la asignatura', 102, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '10.3. Alumne com a superat', '10.3. Alumno como superado', 103, 'T');

insert into uji_guiasdocentes.gdo_apartados (id, curso_aca, nombre_ca, nombre_es, orden, tipo)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, '11. Altra informació', '11. Otra información', 102, 'T');



-- gdo_ext_asi_todas

--select id, nombre_ca, nombre_es, nombre_uk, tipo
--from (
--  select id, nombre_ca, nombre_es, nombre_uk, tipo, row_number() over (partition by tipo, length(id) order by id) orden
--  from exp_v_asi_todas
--  )
--where orden <= 20



Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA001', 'Principis dels estudis de gènere', 'Principios de los estudios de género', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA002', 'Marc normatiu i estratègic per a l''aplicació de les polítiques d''igualtat', 'Marco normativo y estratégico para la aplicación de las políticas de igualdad', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA003', 'Metodologia i investigació social des de la perspectiva de gènere', 'Metodología e investigación social desde la perspectiva de género', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA004', 'Eines analítiques i conceptuals del treball amb grups i dinàmiques grupals des de la perspectiva de gènere', 'Herramientas analíticas y conceptuales del trabajo con grupos y dinámicas grupales desde la perspectiva de género', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA005', 'Violència contra les dones: eines analítiques i conceptuals', 'Violencia contra las mujeres: herramientas analíticas y conceptuales', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA006', 'Aportacions teòriques per a treballar amb els conflictes', 'Aportaciones teóricas para trabajar con los conflictos', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA007', 'Poder i empoderament: eines analítiques i conceptuals', 'Poder y empoderamiento: herramientas analíticas y conceptuales', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA008', 'Conceptes bàsics i pautes per a una comunicació no sexista', 'Conceptos básicos y pautas para una comunicación no sexista', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA009', 'Eines analítiques i conceptuals sobre les noves masculinitats', 'Herramientas analíticas y conceptuales sobre las nuevas masculinidades', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA010', 'Estratègies per a la qualitat de vida: com conciliar treball, atenció a terceres persones i oci', 'Estrategias para la calidad de vida: como conciliar trabajo, atención a terceras personas y ocio', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA011', 'Iniciatives emprenedores i claus per al treball des de la perspectiva de gènere', 'Iniciativas emprendedoras y claves para el trabajo desde la perspectiva de género', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA012', 'Recursos i eines coeducatives a través de les TIC', 'Recursos y herramientas coeducativas a través de las TIC', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA013', 'Investigació i acció participativa', 'Investigación y acción participativa', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA014', 'Animació i dinàmica de grups', 'Animación y dinámica de grupos', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA015', 'Context i metodologia de la intervenció social', 'Contexto y metodología de la intervención social', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA016', 'Processos d''avaluació', 'Procesos de evaluación', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA017', 'Temps i espais', 'Tiempos y espacios', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA018', 'L''escolta activa', 'La escolta activa', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA019', 'Comunicació i tractament no sexista', 'Comunicación y tratamiento no sexista', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, TIPO)
 Values
   ('EAA020', 'La transferència d''informació. Tècniques per a parlar en públic', 'La transferencia de información. Técnicas para hablar en público', 'M');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1001', 'Introducción a la Contabilidad (Empresa)', 'Introducción a la Contabilidad (Empresa)', 'Introducción a la Contabilidad (Empresa)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1002', 'Introducción a la Administración de Empresas (Empresa)', 'Introducción a la Administración de Empresas (Empresa)', 'Introducción a la Administración de Empresas (Empresa)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1003', 'Matemática de las Operaciones Financieras (Empresa)', 'Matemática de las Operaciones Financieras (Empresa)', 'Matemática de las Operaciones Financieras (Empresa)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1004', 'Introducción a la Microeconomía (Economía)', 'Introducción a la Microeconomía (Economía)', 'Introducción a la Microeconomía (Economía)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1005', 'Matemáticas I (Matemáticas)', 'Matemáticas I (Matemáticas)', 'Matemáticas I (Matemáticas)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1006', 'Introducción a la Macroeconomía (Economía)', 'Introducción a la Macroeconomía (Economía)', 'Introducción a la Macroeconomía (Economía)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1007', 'Matemáticas II (Matemáticas)', 'Matemáticas II (Matemáticas)', 'Matemáticas II (Matemáticas)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1008', 'Historia Económica (Economía)', 'Historia Económica (Economía)', 'Historia Económica (Economía)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1009', 'Introducción al Derecho (Derecho)', 'Introducción al Derecho (Derecho)', 'Introducción al Derecho (Derecho)', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1010', 'Introducción a los Sistemas de Información de la Empresa', 'Introducción a los Sistemas de Información de la Empresa', 'Introducción a los Sistemas de Información de la Empresa', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1011', 'Estadística', 'Estadística', 'Estadística', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1012', 'Contabilidad financiera', 'Contabilidad financiera', 'Contabilidad financiera', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1013', 'Dirección financiera', 'Dirección financiera', 'Dirección financiera', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1014', 'Dirección de Empresas', 'Dirección de Empresas', 'Dirección de Empresas', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1015', 'Macroeconomía', 'Macroeconomía', 'Macroeconomía', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1016', 'Análisis de estados financieros', 'Análisis de estados financieros', 'Análisis de estados financieros', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1017', 'Economía Española y Mundial', 'Economía Española y Mundial', 'Economía Española y Mundial', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1018', 'Fundamentos de marketing', 'Fundamentos de marketing', 'Fundamentos de marketing', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1019', 'Métodos Cuantitativos', 'Métodos Cuantitativos', 'Métodos Cuantitativos', '12C');
Insert into uji_guiasdocentes.GDO_EXT_ASI_TODAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   ('AE1020', 'Microeconomía', 'Microeconomía', 'Microeconomía', '12C');

-- gdo_asignaturas
Insert into uji_guiasdocentes.GDO_ASIGNATURAS
 (ID, CURSO_ACA, COD_ASIGNATURA, FECHA_INI, FECHA_FIN, ESTADO, PERSONA_ID, AUTORIZA_PAS)
 Values
 (uji_guiasdocentes.hibernate_sequence.nextval, 2010, 'AE1002', null, null, 'D', null, 'N');
 Insert into uji_guiasdocentes.GDO_ASIGNATURAS
 (ID, CURSO_ACA, COD_ASIGNATURA, FECHA_INI, FECHA_FIN, ESTADO, PERSONA_ID, AUTORIZA_PAS)
 Values
 (uji_guiasdocentes.hibernate_sequence.nextval, 2010, 'AE1003', null, null, 'D', null, 'N'); 
 Insert into uji_guiasdocentes.GDO_ASIGNATURAS
 (ID, CURSO_ACA, COD_ASIGNATURA, FECHA_INI, FECHA_FIN, ESTADO, PERSONA_ID, AUTORIZA_PAS)
 Values
 (uji_guiasdocentes.hibernate_sequence.nextval, 2010, 'AE1004', null, null, 'D', null, 'N'); 


-- gdo_ext_personas


--select id, nombre||orden nombre, apellido1, apellido2
--from (
--select 100+id id, 'Persona ' nombre, apellido1, apellido2, row_number() over (partition by 1 order by id) orden 
--from per_personas
--where rownum <= 10)


Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (100, 'Persona 1', 'Persona 1', 'Persona','PDI', 10, 'N', 'Persona1');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (101, 'Persona 2', 'Silvestre', 'Aparicio','PDI', 10, 'N', 'Persona2');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (102, 'Persona 3', 'Valero', 'Aicart', 'PDI', 11, 'N', 'Persona3');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (103, 'Persona 4', 'Albert', 'Martí', 'PAS', 10, 'S', 'Persona4');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (104, 'Persona 5', 'Alemany', 'Juliá', 'PAS', 11, 'S', 'Persona5');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (105, 'Persona 6', 'Alemany', 'Palomo', 'PAS', 10, 'N', 'Persona6');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (106, 'Persona 7', 'Amer', 'Martín', 'PDI', 11, 'N', 'Persona7');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (107, 'Persona 8', 'Barrachina', 'Gisbert', 'PDI', 12, 'N', 'Persona8');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (108, 'Persona 9', 'Carregui', 'Vilar', 'PDI', 12, 'N', 'Persona9');
Insert into UJI_GUIASDOCENTES.GDO_EXT_PERSONAS
   (ID, NOMBRE, APELLIDO1, APELLIDO2, actividad_id, ubicacion_id, es_auxiliar, login)
 Values
   (109, 'Persona 10', 'Casas', 'Beltrán', 'PDI', 10, 'N', 'Persona10');


-- gdo_ext_titu_todas

--select id, nombre, tipo
--from   exp_v_titu_todas
--where  tipo in ('G', 'M')
--and    oficial = 'S'
--and    activa = 'S'

Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (201, 'Grau en Relacions Laborals i Recursos Humans', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (202, 'Grau en Turisme', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (203, 'Grau en Comunicació Audiovisual', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (204, 'Grau en Periodisme', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (205, 'Grau en Estudis Anglesos', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (206, 'Grau en Publicitat i Relacions Públiques', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (207, 'Grau en Traducció i Interpretació', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (208, 'Grau en Química', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (209, 'Grau en Enginyeria d''Edificació', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (210, 'Grau en Administració d''Empreses', 'G');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42101, 'Màster Universitari en Internacionalització Econòmica: Integració i Comerç Internacional', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42102, 'Màster Universitari en Màrqueting i Investigació de Mercats', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42103, 'Màster Universitari Internacional en Estudis de Pau, Conflictes i Desenvolupament', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42105, 'Màster Universitari en Ciència, Tecnologia i Aplicacions dels Materials Ceràmics', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42106, 'Màster Universitari en Matemàtica Computacional', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42107, 'Màster Universitari en Prevenció de Riscos Laborals', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42108, 'Màster Universitari en Química Sostenible', 'M');
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
   (ID, NOMBRE, TIPO)
 Values
   (42109, 'Màster Universitari en Sistemes Intel·ligents', 'M');


-- gdo_ext_titulaciones_per

--select tit_id, nombre, persona_id
--from (
--select t.id tit_id,  null nombre, p.id persona_id, row_number() over (partition by t.id order by p.id) orden, substr(t.id,length(t.id),1) primera
--from gdo_ext_titu_todas t, gdo_ext_personas p
--)
--where substr(tit_id,length(tit_id),1)  = orden

Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (201, 'Grau en Relacions Laborals i Recursos Humans', 16);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (202, 'Grau en Turisme', 78);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (203, 'Grau en Comunicació Audiovisual', 99);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (204, 'Grau en Periodisme', 103);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (205, 'Grau en Estudis Anglesos', 235);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (206, 'Grau en Publicitat i Relacions Públiques', 263);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (207, 'Grau en Traducció i Interpretació', 297);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (208, 'Grau en Química', 346);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (209, 'Grau en Enginyeria d''Edificació', 355);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (211, 'Grau en Economia', 16);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (212, 'Grau en Finances i Comptabilitat', 78);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (213, 'Grau en Dret', 99);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (214, 'Grau en Criminologia i Seguretat', 103);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (215, 'Grau en Història i Patrimoni', 235);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (216, 'Grau en Humanitats: Estudis Interculturals', 263);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (217, 'Grau en Mestre o Mestra d''Educació Infantil', 297);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (218, 'Grau en Mestre o Mestra d''Educació Primària', 346);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (219, 'Grau en Psicologia', 355);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (221, 'Grau en Enginyeria en Tecnologies Industrials', 16);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (222, 'Grau en Enginyeria Mecànica', 78);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (223, 'Grau en Matemàtica Computacional', 99);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 103);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (225, 'Grau en Enginyeria Informàtica', 235);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (226, 'Grau en Enginyeria Telemàtica (pendent de verificació)', 263);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (227, 'Grau en Enginyeria Agroalimentària i del Medi Rural', 297);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (228, 'Grau en Enginyeria Elèctrica', 346);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
   (estudio_id, NOMBRE, PERSONA_ID)
 Values
   (229, 'Grau en Medicina (pendent de verificació)', 355);
COMMIT;



-- gdo_ext_titulaciones_asi

--select a.id asi_id, t.id tit_id
--from gdo_ext_asi_todas a, gdo_ext_titu_todas t
--where (a.id like 'AE%' and t.id = 203)
--or (a.id like 'EAA%' and t.id = 42106)



Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1001', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1002', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1003', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1004', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1005', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1006', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1007', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1008', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1009', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1010', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1011', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1012', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1013', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1014', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1015', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1016', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1017', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1018', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1019', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('AE1020', 203);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA001', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA002', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA003', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA004', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA005', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA006', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA007', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA008', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA009', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA010', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA011', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA012', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA013', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA014', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA015', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA016', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA017', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA018', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA019', 42106);
Insert into UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_ASI
   (cod_asignatura, ESTUDIO_ID)
 Values
   ('EAA020', 42106);


-- gdo_asignaturas

insert into uji_guiasdocentes.gdo_ext_personas(id, nombre, apellido1, apellido2, actividad_id, ubicacion_id, es_auxiliar, login)
  values (9792, 'Ricardo', 'Borillo', 'Domenech','PAS', 218, 'N', 'borillo');

insert into uji_guiasdocentes.gdo_asignaturas(id, curso_aca, cod_asignatura, asignatura_id_origen, 
                                              fecha_ini, fecha_fin, estado, persona_id, autoriza_pas)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, 'EAA001', null, sysdate, sysdate+200, 'D', 9792, 'N');

insert into uji_guiasdocentes.gdo_asignaturas(id, curso_aca, cod_asignatura, persona_id, fecha_ini, estado, autoriza_pas)
  values (uji_guiasdocentes.hibernate_sequence.nextval, 2010, 'AE1001', 9792, trunc(sysdate), 'D', 'N');


  
-- gdo_tipo_actividades

Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (1, 'Ensenyaments teòrics', 'Enseñanzas teóricas', 'Ensenyaments teòrics', 'P');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (2, 'Ensenyaments pràctics (problemes)', 'Enseñanzas prácticas (problemas)', 'Ensenyaments pràctics (problemes)', 'P');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (3, 'Seminaris', 'Seminarios', 'Seminaris', 'P');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (4, 'Tutories', 'Tutorías', 'Tutories', 'P');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (5, 'Avaluació', 'Evaluación', 'Avaluació', 'P');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (6, 'Treball personal', 'Trabajo personal', 'Treball personal', 'N');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (7, 'Treball de preparació dels exàmens', 'Trabajo de preparación de los exámenes', 'Treball de preparació dels exàmens', 'N');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (8, 'Ensenyaments pràctics (laboratori)', 'Enseñanzas prácticas (laboratorio)', 'Ensenyaments pràctics (laboratori)', 'P');
Insert into UJI_GUIASDOCENTES.GDO_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (9, 'Ensenyaments pràctics (pràctiques externes)', 'Enseñanzas prácticas (prácticas externas)', 'Ensenyaments pràctics (pràctiques externes)', 'P');

  
-- gdo_textos

insert into uji_guiasdocentes.gdo_asi_textos(id, apartado_id, asignatura_id,  
                                         fecha_slt_ca, fecha_slt_es, fecha_slt_uk, texto_ca, texto_es, texto_uk)
  values (uji_guiasdocentes.hibernate_sequence.nextval,
          (select id from uji_guiasdocentes.gdo_apartados where nombre_ca like '2. %'),
          (select id from uji_guiasdocentes.gdo_asignaturas where cod_asignatura = 'EAA001'),
          null, null, null, 'Texto ca', 'Texto es', 'Texto uk');

insert into uji_guiasdocentes.gdo_asi_textos(id, apartado_id, asignatura_id,  
                                         fecha_slt_ca, fecha_slt_es, fecha_slt_uk, texto_ca, texto_es, texto_uk)
  values (uji_guiasdocentes.hibernate_sequence.nextval,
          (select id from uji_guiasdocentes.gdo_apartados where nombre_ca like '3. %'),
          (select id from uji_guiasdocentes.gdo_asignaturas where cod_asignatura = 'EAA001'),
          null, null, null, 'Texto ca', 'Texto es', 'Texto uk');

insert into uji_guiasdocentes.gdo_asi_textos(id, apartado_id, asignatura_id,  
                                         fecha_slt_ca, fecha_slt_es, fecha_slt_uk, texto_ca, texto_es, texto_uk)
  values (uji_guiasdocentes.hibernate_sequence.nextval,
          (select id from uji_guiasdocentes.gdo_apartados where nombre_ca like '4. %'),
          (select id from uji_guiasdocentes.gdo_asignaturas where cod_asignatura = 'EAA001'),
          null, null, null, 'Texto ca', 'Texto es', 'Texto uk');


--- god_idiomas
insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (1, 'Valenciano', 'Valenciano', null);

insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (2, 'Castellà', 'Castellano', null);

insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (3, 'Anglés', 'Inglés', null);

insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (4, 'Francés', 'Fraincés', null);

insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (5, 'Alemany', 'Alemán', null);

insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (6, 'Portugués', 'Portugués', null);

insert into uji_guiasdocentes.gdo_idiomas (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK) values (101, 'Català', 'Català', null);


