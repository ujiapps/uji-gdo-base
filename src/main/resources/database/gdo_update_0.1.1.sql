alter table uji_guiasdocentes.gdo_asi_competencias modify (competencia_ca varchar2(4000), competencia_es varchar2(4000), competencia_uk varchar2(4000));

alter table uji_guiasdocentes.gdo_asi_resultados modify (resultado_ca varchar2(4000), resultado_es varchar2(4000), resultado_uk varchar2(4000));

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_PERSONAS (ID,
                                                                 NOMBRE,
                                                                 APELLIDO1,
                                                                 APELLIDO2,
                                                                 ACTIVIDAD_ID,
                                                                 UBICACION_ID,
                                                                 ES_AUXILIAR,
                                                                 LOGIN
                                                                ) AS
   select p.id, p.nombre, apellido1, apellido2, act_id actividad_id, ubicacion_id,
          decode (act_id, 'PDI', 'N', 'S') es_auxiliar,
          substr (busca_cuenta (p.id), 1, instr (busca_cuenta (p.id), '@') - 1) login
   from   grh_grh.grh_vw_contrataciones_ult c,
          gri_per.per_personas p
   where  c.per_id = p.id
   and    act_id in ('PAS', 'PDI')
   and    act_id = (select max (act_id)
                    from   grh_vw_contrataciones_ult c2
                    where  c.per_id = c2.per_id
                    and    c2.act_id in ('PAS', 'PDI'))
   union
   select id, nombre, apellido1, apellido2, 'PDI' act_id, 0 ubicacion_id, 'N' es_auxiliar,
          substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
   from   per_personas
   where  id in (252381, 95178)
   union
   select id, nombre, apellido1, apellido2, 'PAS' act_id, 0 ubicacion_id, 'N' es_auxiliar,
          substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
   from   per_personas
   where  id in (840)
   union
   select   per_id, nombre, apellido1, apellido2, act_id, min (ubicacion_id), es_auxiliar, login
   from     (select distinct per_id, nombre, apellido1, apellido2, 'PDI' act_id,
                             (select min (ubicacion_id)
                              from   pop_asignaturas_master am,
                                     pop_comisiones c,
                                     grh_vw_contrataciones_ult cont
                              where  am.pasi_id = g.pgr_pof_pasi_id
                              and    am.pmas_id not in (42000, 42100)
                              and    am.pmas_id = c.pmas_id
                              and    c.per_id = cont.per_id
                              and    act_id = 'PDI') ubicacion_id,
                             'N' es_auxiliar, substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
             from            pop_grupos_pdi g,
                             per_personas p
             where           pgr_pof_curso_aca >= (select min (curso_aca)
                                                   from   gdo_cursos_academicos
                                                   where  activo = 'S') - 1
             and             per_id not in (select per_id
                                            from   grh_vw_contrataciones_ult)
             and             pgr_pof_pmas_id in (select id
                                                 from   pop_masters
                                                 where  oficial = 'S')
             and             per_id = p.id)
   where    login is not null
   group by per_id,
            nombre,
            apellido1,
            apellido2,
            act_id,
            es_auxiliar,
            login;
			
grant select on gra_pop.pop_asi_detalle to uji_guiasdocentes;
grant select on gra_pop.pop_asi_detalle to uji_guiasdocentes_usr;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE (COD_ASIGNATURA,
                                                                    CURSO_ACA_INI,
                                                                    CURSO_ACA_FIN,
                                                                    HORAS_AL,
                                                                    PREREQUISITOS_CA,
                                                                    PREREQUISITOS_ES,
                                                                    CONTENIDOS_CA,
                                                                    CONTENIDOS_ES,
                                                                    JUSTIFICACION_CA,
                                                                    JUSTIFICACION_ES
                                                                   ) AS
   select ASI_ID, CURSO_ACA_INI, CURSO_ACA_FIN, HORAS_AL, PREREQUISITOS_CA, PREREQUISITOS_ES, CONTENIDOS_CA,
          CONTENIDOS_ES, JUSTIFICACION_CA, JUSTIFICACION_ES
   from   gra_pod.pod_asi_detalle
   union all
   select PASI_ID, CURSO_ACA_INI, CURSO_ACA_FIN, HORAS_AL, PREREQUISITOS_CA, PREREQUISITOS_ES, CONTENIDOS_CA,
          CONTENIDOS_ES, JUSTIFICACION_CA, JUSTIFICACION_ES
   from   gra_pop.pop_asi_detalle;

grant select on gra_pop.GDO_EXT_ASI_DETALLE_ACT to uji_guiasdocentes;
grant select on gra_pop.GDO_EXT_ASI_DETALLE_ACT to uji_guiasdocentes_usr;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_ACT (COD_ASIGNATURA,
                                                                        CURSO_ACA_INI,
                                                                        ACT_ID,
                                                                        NOMBRE_CA,
                                                                        NOMBRE_ES,
                                                                        H_PRE,
                                                                        H_NO_PRE,
                                                                        TIPO_GRUPO,
                                                                        TIPO,
                                                                        ORDEN
                                                                       ) AS
   select asi_id, curso_aca_ini, act_id, nombre_ca, nombre_es, h_pre, h_no_pre, tipo_grupo, tipo, orden
   from   gra_pod.pod_asi_detalle_act
   union all
   select pasi_id, curso_aca_ini, act_id, nombre_ca, nombre_es, h_pre, h_no_pre, tipo_grupo, tipo, orden
   from   gra_pop.pop_asi_detalle_act;

grant select on gra_pop.GDO_EXT_ASI_DETALLE_ACT_COMP to uji_guiasdocentes;
grant select on gra_pop.GDO_EXT_ASI_DETALLE_ACT_COMP to uji_guiasdocentes_usr;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_ACT_COMP (COD_ASIGNATURA,
                                                                             CURSO_ACA_INI,
                                                                             ACT_ID,
                                                                             COMP_ID,
                                                                             NOMBRE_CA,
                                                                             NOMBRE_ES,
                                                                             NOMBRE_UK,
                                                                             TIPO
                                                                            ) AS
   select asi_id, curso_aca_ini, act_id, comp_id, nombre_ca, nombre_es, nombre_uk, tipo
   from   gra_pod.pod_asi_detalle_act_comp
   union all
   select pasi_id, curso_aca_ini, act_id, comp_id, nombre_ca, nombre_es, nombre_uk, tipo
   from   gra_pop.pop_asi_detalle_act_comp;
   
grant select on gra_pop.GDO_EXT_ASI_DETALLE_EVAL to uji_guiasdocentes;
grant select on gra_pop.GDO_EXT_ASI_DETALLE_EVAL to uji_guiasdocentes_usr;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_EVAL (COD_ASIGNATURA,
                                                                         CURSO_ACA_INI,
                                                                         EVAL_ID,
                                                                         NOMBRE_CA,
                                                                         NOMBRE_ES,
                                                                         NOMBRE_UK,
                                                                         PONDERACION
                                                                        ) AS
   select asi_id, curso_aca_ini, eval_id, nombre_ca, nombre_es, nombre_uk, ponderacion
   from   gra_pod.pod_asi_detalle_eval
   union all
   select pasi_id, curso_aca_ini, eval_id, nombre_ca, nombre_es, nombre_uk, ponderacion
   from   gra_pop.pop_asi_detalle_eval;

grant select on gra_pop.GDO_EXT_ASI_DETALLE_RDO to uji_guiasdocentes;
grant select on gra_pop.GDO_EXT_ASI_DETALLE_RDO to uji_guiasdocentes_usr;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_RDO (COD_ASIGNATURA,
                                                                        CURSO_ACA_INI,
                                                                        RDO_ID,
                                                                        NOMBRE_CA,
                                                                        NOMBRE_ES,
                                                                        NOMBRE_UK
                                                                       ) AS
   select asi_id, curso_aca_ini, rdo_id, nombre_ca, nombre_es, nombre_uk
   from   gra_pod.pod_asi_detalle_rdo
   union all
   select pasi_id, curso_aca_ini, rdo_id, nombre_ca, nombre_es, nombre_uk
   from   gra_pop.pop_asi_detalle_rdo;

   
drop table uji_guiasdocentes.gdo_ext_personas cascade constraints;



