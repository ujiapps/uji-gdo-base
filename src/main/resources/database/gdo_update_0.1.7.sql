CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_actividades AS
SELECT d.cod_asignatura,
  da.curso_aca_ini,
  d.curso_aca_fin,
  da.act_id,
  (da.h_pre    - to_date('1-1-1', 'dd-mm-yyyy')) * 24 h_pre,
  (da.h_no_pre - to_date('1-1-1', 'dd-mm-yyyy')) * 24 h_no_pre
FROM gdo_ext_asi_detalle_act da,
  gdo_ext_asi_detalle d
WHERE d.cod_asignatura = da.cod_asignatura
AND d.curso_aca_ini    = da.curso_aca_ini ;




CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_competencias AS
SELECT DISTINCT d.cod_asignatura,
  d.curso_aca_ini,
  d.curso_aca_fin,
  c.comp_id,
  c.nombre_ca,
  c.nombre_es,
  c.nombre_uk,
  c.tipo
FROM gdo_ext_asi_detalle_act_comp c,
  gdo_ext_asi_detalle d
WHERE d.cod_asignatura = c.cod_asignatura
AND d.curso_aca_ini    = c.curso_aca_ini
AND (c.comp_id        <> 0) ;




CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_evaluaciones AS
SELECT d.cod_asignatura,
  d.curso_aca_ini,
  d.curso_aca_fin,
  e.eval_id,
  e.nombre_ca,
  e.nombre_es,
  e.nombre_uk,
  e.ponderacion
FROM gdo_ext_asi_detalle_eval e,
  gdo_ext_asi_detalle d
WHERE d.cod_asignatura = e.cod_asignatura
AND d.curso_aca_ini    = e.curso_aca_ini ;




CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_resultados AS
SELECT d.cod_asignatura,
  d.curso_aca_ini,
  d.curso_aca_fin,
  r.rdo_id,
  r.nombre_ca,
  r.nombre_es,
  r.nombre_uk
FROM gdo_ext_asi_detalle d,
  gdo_ext_asi_detalle_rdo r
WHERE d.cod_asignatura = r.cod_asignatura
AND d.curso_aca_ini    = r.curso_aca_ini ;

