-- gdo_vw_idiomas

 
--create or replace view uji_guiasdocentes.gdo_vw_idiomas (id, nombre) as
select 120 id, 'Lenguas oficiales de la Comunidad Valenciana' nombre
from dual
union all
select 1 id, 'Valenci�' nombre
from dual
union all
select 2 id, 'Castell�' nombre
from dual
union all
select 3 id, 'Angl�s' nombre
from dual
union all
select 4 id, 'Franc�s' nombre
from dual
union all
select 5 id, 'Alemany' nombre
from dual
union all
select 6 id, 'Portugu�s' nombre
from dual

insert into gdo_vw_idiomas
   select id, nombre, nombre, nombre
   from   (select 120 id, 'Lenguas oficiales de la Comunidad Valenciana' nombre
           from   dual
           union all
           select 1 id, 'Valenci�' nombre
           from   dual
           union all
           select 2 id, 'Castell�' nombre
           from   dual
           union all
           select 3 id, 'Angl�s' nombre
           from   dual
           union all
           select 4 id, 'Franc�s' nombre
           from   dual
           union all
           select 5 id, 'Alemany' nombre
           from   dual
           union all
           select 6 id, 'Portugu�s' nombre
           from   dual)
		   
		   
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (120, 'Lenguas oficiales de la Comunidad Valenciana', 'Lenguas oficiales de la Comunidad Valenciana', 'Lenguas oficiales de la Comunidad Valenciana');
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (1, 'Valenci�', 'Valenci�', 'Valenci�');
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (2, 'Castell�', 'Castell�', 'Castell�');
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (3, 'Angl�s', 'Angl�s', 'Angl�s');
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (4, 'Franc�s', 'Franc�s', 'Franc�s');
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (5, 'Alemany', 'Alemany', 'Alemany');
Insert into GDO_VW_IDIOMAS
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   (6, 'Portugu�s', 'Portugu�s', 'Portugu�s');
COMMIT;

		   



-- gdo_vw_tipos_evaluacion

cod_asignatura
id
nombre_ca
nombre_es
nombre_uk

select t.sol_id, row_number () over (partition by t.sol_id order by t.id) id, t.nombre, t.nombre_ca, t.nombre_uk,
       g.codigo
from   ver_tipos_Evaluacion t,
       ver_asignaturas a,
       ver_asi_generar g
where  t.sol_id = a.sol_id
and    a.id = g.asi_id
and    a.sol_id = grado_destino


select g.codigo cod_asignatura, row_number () over (partition by t.sol_id, g.codigo order by t.id) id, nvl(t.nombre_ca, t.nombre) nombre_ca, t.nombre nombre_es, nvl(t.nombre_uk, t.nombre) nombre_uk
from   ver_tipos_Evaluacion t,
       ver_asignaturas a,
       ver_asi_generar g
where  t.sol_id = a.sol_id
and    a.id = g.asi_id
and    a.sol_id = grado_destino
order by 1,2

SET DEFINE OFF;
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1001', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1002', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1003', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1004', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1005', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1006', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1007', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1008', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1009', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1010', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1011', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1012', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1013', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1014', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1015', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1016', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1017', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1018', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1019', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1020', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1021', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1022', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1023', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1024', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1025', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1026', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1027', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1028', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1029', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1030', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1031', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1032', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1033', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1034', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1035', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1036', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1037', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1038', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1039', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1040', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1041', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 3, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 4, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 5, 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante', 'Evaluaci�n del informe elaborado por el estudiante');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 6, 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto', 'Redacci�n y contenido del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('AG1042', 8, 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado', 'Presentaci�n y defensa p�blica de la memoria del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1001', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1001', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1001', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1001', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1001', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1001', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1002', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1002', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1002', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1002', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1002', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1002', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1003', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1003', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1003', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1003', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1003', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1003', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1004', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1004', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1004', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1004', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1004', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1004', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1005', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1005', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1005', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1005', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1005', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1005', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1006', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1006', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1006', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1006', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1006', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1006', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1007', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1007', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1007', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1007', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1007', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1007', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1008', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1008', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1008', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1008', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1008', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1008', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1009', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1009', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1009', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1009', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1009', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1009', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1010', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1010', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1010', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1010', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1010', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1010', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1011', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1011', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1011', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1011', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1011', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1011', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1012', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1012', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1012', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1012', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1012', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1012', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1013', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1013', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1013', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1013', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1013', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1013', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1014', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1014', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1014', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1014', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1014', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1014', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1015', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1015', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1015', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1015', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1015', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1015', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1016', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1016', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1016', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1016', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1016', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1016', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1017', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1017', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1017', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1017', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1017', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1017', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1018', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1018', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1018', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1018', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1018', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1018', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1019', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1019', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1019', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1019', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1019', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1019', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1020', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1020', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1020', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1020', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1020', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1020', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1021', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1021', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1021', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1021', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1021', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1021', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1022', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1022', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1022', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1022', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1022', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1022', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1023', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1023', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1023', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1023', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1023', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1023', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1024', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1024', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1024', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1024', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1024', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1024', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1025', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1025', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1025', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1025', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1025', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1025', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1026', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1026', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1026', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1026', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1026', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1026', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1027', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1027', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1027', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1027', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1027', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1027', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1028', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1028', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1028', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1028', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1028', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1028', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1029', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1029', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1029', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1029', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1029', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1029', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1030', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1030', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1030', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1030', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1030', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1030', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1031', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1031', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1031', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1031', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1031', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1031', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1032', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1032', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1032', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1032', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1032', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1032', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('Di1033', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('Di1033', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('Di1033', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('Di1033', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('Di1033', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('Di1033', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1034', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1034', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1034', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1034', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1034', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1034', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1035', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1035', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1035', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1035', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1035', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1035', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1036', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1036', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1036', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1036', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1036', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1036', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1037', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1037', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1037', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1037', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1037', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1037', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1038', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1038', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1038', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1038', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1038', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1038', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1039', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1039', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1039', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1039', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1039', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1039', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1040', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1040', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1040', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1040', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1040', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1040', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1041', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1041', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1041', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1041', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1041', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1041', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1042', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1042', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1042', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1042', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1042', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1042', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1043', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1043', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1043', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1043', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1043', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1043', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1044', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1044', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1044', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1044', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1044', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1044', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1045', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1045', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1045', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1045', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1045', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1045', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1046', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1046', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1046', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1046', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1046', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1046', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1047', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1047', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1047', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1047', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1047', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1047', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1048', 1, 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)', 'Evaluaci�n de pr�cticas (observaci�n de la ejecuci�n, resoluci�n de casos, memorias, presentaciones orales, portfolio, proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1048', 2, 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)', 'Evaluaci�n de seminario (presentaci�n oral, participaci�n, elaboraci�n de trabajos , proyectos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1048', 3, 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)', 'Evaluaci�n de tutor�a (entrevista de tutorizaci�n, observaci�n, memorias e informes, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1048', 4, 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)', 'Evaluaci�n de trabajos acad�micos (prototipos, proyectos, casos, trabajos, etc.)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1048', 5, 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral', 'Memoria del proyecto y defensa oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('DI1048', 6, 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico', 'Examen te�rico-pr�ctico');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0901', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0902', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0903', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0904', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0905', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0906', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0907', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0908', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0909', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0910', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0911', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0912', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0913', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0914', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0915', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0916', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0917', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0918', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0919', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0920', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0921', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0922', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0923', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0924', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0925', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0926', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0927', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0928', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0929', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0930', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0931', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0932', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0933', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0934', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0935', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0936', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0937', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0938', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0939', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0940', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0941', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0942', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0943', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0944', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ED0945', 1, 'Projecte de final de grau', 'Proyecto fin de grado', 'Proyecto fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1001', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1002', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1003', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1004', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1005', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1006', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1007', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1008', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1009', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1010', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1011', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1012', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1013', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1014', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1015', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1016', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1017', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1018', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1019', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1020', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1021', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1022', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1023', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1024', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1025', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1026', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1027', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1028', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1029', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1030', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1031', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1032', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1033', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1034', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1035', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1036', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1037', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1038', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1039', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1040', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1041', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1042', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1043', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1044', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 4, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 5, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 6, 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado', 'Defensa oral del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 7, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EE1045', 8, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1001', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1002', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1003', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1004', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1005', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1006', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1007', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1008', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1009', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1010', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1011', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1012', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1013', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1014', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1015', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1016', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1017', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1018', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1019', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1020', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1021', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1022', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1023', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1024', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1025', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1026', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1027', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1028', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1029', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1030', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1031', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1032', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1033', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1034', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1035', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1036', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1037', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1038', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1039', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1040', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1041', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1042', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1043', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1044', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1045', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1046', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1047', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1048', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1049', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1050', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1051', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1052', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1053', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 7, 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas', 'Evaluaci�n de Estancia en Pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EI1054', 8, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1001', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1001', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1001', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1001', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1001', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1001', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1002', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1002', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1002', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1002', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1002', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1002', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1003', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1003', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1003', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1003', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1003', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1003', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1004', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1004', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1004', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1004', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1004', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1004', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1005', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1005', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1005', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1005', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1005', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1005', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1006', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1006', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1006', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1006', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1006', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1006', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1007', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1007', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1007', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1007', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1007', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1007', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1008', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1008', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1008', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1008', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1008', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1008', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1009', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1009', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1009', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1009', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1009', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1009', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1010', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1010', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1010', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1010', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1010', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1010', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1011', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1011', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1011', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1011', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1011', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1011', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1012', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1012', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1012', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1012', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1012', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1012', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1013', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1013', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1013', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1013', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1013', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1013', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1014', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1014', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1014', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1014', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1014', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1014', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1015', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1015', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1015', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1015', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1015', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1015', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1016', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1016', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1016', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1016', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1016', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1016', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1017', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1017', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1017', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1017', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1017', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1017', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1018', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1018', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1018', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1018', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1018', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1018', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1019', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1019', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1019', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1019', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1019', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1019', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1020', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1020', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1020', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1020', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1020', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1020', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1021', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1021', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1021', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1021', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1021', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1021', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1022', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1022', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1022', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1022', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1022', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1022', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1023', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1023', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1023', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1023', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1023', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1023', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1024', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1024', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1024', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1024', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1024', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1024', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1025', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1025', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1025', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1025', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1025', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1025', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1026', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1026', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1026', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1026', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1026', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1026', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1027', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1027', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1027', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1027', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1027', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1027', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1028', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1028', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1028', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1028', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1028', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1028', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1029', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1029', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1029', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1029', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1029', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1029', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1030', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1030', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1030', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1030', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1030', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1030', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1031', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1031', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1031', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1031', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1031', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1031', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1032', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1032', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1032', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1032', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1032', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1032', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1033', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1033', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1033', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1033', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1033', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1033', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1034', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1034', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1034', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1034', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1034', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1034', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1035', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1035', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1035', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1035', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1035', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1035', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1036', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1036', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1036', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1036', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1036', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1036', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1037', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1037', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1037', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1037', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1037', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1037', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1038', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1038', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1038', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1038', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1038', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1038', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1039', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1039', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1039', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1039', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1039', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1039', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1040', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1040', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1040', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1040', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1040', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1040', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1041', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1041', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1041', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1041', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1041', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1041', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1042', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1042', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1042', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1042', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1042', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1042', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1043', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1043', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1043', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1043', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1043', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1043', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1044', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1044', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1044', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1044', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1044', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1044', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1045', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1045', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1045', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1045', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1045', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1045', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1046', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1046', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1046', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1046', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1046', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1046', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1047', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1047', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1047', 3, 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas', 'Evaluaci�n de la estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1047', 4, 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado', 'Contenido T�cnico del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1047', 5, 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado', 'Defensa Oral del Trabajo Fin de Grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EM1047', 6, 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas', 'Evaluaci�n de las pr�cticas externas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1001', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1002', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1003', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1004', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1005', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1006', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1007', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1008', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1009', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1010', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1011', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1012', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1013', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1014', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1015', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1016', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1017', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1018', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1019', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1020', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1021', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1022', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1023', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1024', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1025', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1026', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1027', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1028', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1029', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1030', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1031', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1032', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1033', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1034', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1035', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1036', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1037', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1038', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1039', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1040', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1041', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1042', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1043', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 3, 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa', 'Presentaci�n y defensa de la memoria de las pr�cticas en empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 4, 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa', 'Evaluaci�n del supervisor de la empresa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 5, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 6, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 7, 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto', 'Evaluaci�n del tutor del proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 8, ' <span class=', ' <span class=', ' <span class=');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 9, 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria', 'Presentaci�n y defensa de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 10, 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo', 'Presentaci�n y defensa del trabajo');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 11, 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria', 'Redacci�n y contenido de la memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('EQ1044', 12, 'Evaluaci�n del tutor', 'Evaluaci�n del tutor', 'Evaluaci�n del tutor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1001', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1002', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1003', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1004', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1005', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1006', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1007', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1008', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1009', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1010', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1011', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1012', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1013', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1014', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1015', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1016', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1017', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1018', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1019', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1020', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1021', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1022', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1023', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1024', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1025', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1026', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1027', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1028', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1029', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1030', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1031', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1032', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1033', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1034', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1035', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1036', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1037', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1038', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1039', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 3, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 4, 'Valoraci�n del supervisor', 'Valoraci�n del supervisor', 'Valoraci�n del supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 5, 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas', 'Contenido de la memoria de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 6, 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado', 'Contenido t�cnico del trabajo fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('ET1040', 7, 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ', 'Defensa oral del trabajo fin de grado ');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1001', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1001', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1001', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1001', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1001', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1002', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1002', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1002', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1002', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1002', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1003', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1003', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1003', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1003', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1003', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1004', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1004', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1004', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1004', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1004', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1005', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1005', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1005', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1005', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1005', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1006', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1006', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1006', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1006', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1006', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1007', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1007', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1007', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1007', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1007', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1008', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1008', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1008', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1008', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1008', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1009', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1009', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1009', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1009', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1009', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1010', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1010', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1010', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1010', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1010', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1011', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1011', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1011', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1011', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1011', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1012', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1012', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1012', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1012', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1012', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1013', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1013', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1013', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1013', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1013', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1014', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1014', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1014', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1014', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1014', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1015', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1015', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1015', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1015', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1015', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1016', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1016', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1016', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1016', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1016', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1017', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1017', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1017', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1017', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1017', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1018', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1018', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1018', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1018', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1018', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1019', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1019', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1019', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1019', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1019', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1020', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1020', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1020', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1020', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1020', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1021', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1021', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1021', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1021', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1021', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1022', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1022', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1022', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1022', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1022', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1023', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1023', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1023', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1023', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1023', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1024', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1024', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1024', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1024', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1024', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1025', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1025', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1025', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1025', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1025', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1026', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1026', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1026', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1026', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1026', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1027', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1027', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1027', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1027', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1027', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1028', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1028', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1028', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1028', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1028', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1029', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1029', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1029', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1029', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1029', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1030', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1030', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1030', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1030', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1030', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1031', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1031', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1031', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1031', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1031', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1032', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1032', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1032', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1032', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1032', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1033', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1033', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1033', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1033', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1033', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1034', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1034', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1034', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1034', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1034', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1035', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1035', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1035', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1035', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1035', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1036', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1036', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1036', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1036', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1036', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1037', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1037', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1037', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1037', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1037', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1038', 1, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de microbiolog�a/histolog�a');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1038', 2, 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas', 'Observaci�n/ejecuci�n de tareas y pr�cticas tuteladas/cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1038', 3, 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas', 'Memorias e informes de pr�cticas cl�nicas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1038', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica', 'Observaci�n/ejecuci�n de tareas y pr�cticas de laboratorio de fisiolog�a/bioqu�mica');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('IN1038', 5, 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa', 'Memoria del Trabajo fin de grado. Exposici�n y defensa');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1001', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1002', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1003', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1004', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1005', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1006', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1007', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1008', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1009', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1010', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1011', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1012', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1013', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1014', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1015', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1016', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1017', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1018', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1019', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1020', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1021', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1022', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1023', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1024', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1025', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1026', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1027', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1028', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1029', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1030', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1031', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1032', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1033', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1034', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1035', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1036', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1037', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1038', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1039', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1040', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1041', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1042', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1043', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1044', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1045', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1046', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1047', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1048', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1049', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1050', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1051', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1052', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1053', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1054', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1055', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1056', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1057', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MD1058', 1, 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n', 'Taller de habilidades y simulaci�n');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1001', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1001', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1001', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1001', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1002', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1002', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1002', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1002', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1003', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1003', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1003', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1003', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1004', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1004', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1004', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1004', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1005', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1005', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1005', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1005', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1006', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1006', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1006', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1006', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1007', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1007', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1007', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1007', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1008', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1008', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1008', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1008', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1009', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1009', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1009', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1009', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1010', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1010', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1010', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1010', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1011', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1011', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1011', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1011', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1012', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1012', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1012', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1012', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1013', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1013', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1013', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1013', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1014', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1014', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1014', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1014', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1015', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1015', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1015', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1015', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1016', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1016', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1016', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1016', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1017', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1017', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1017', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1017', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1018', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1018', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1018', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1018', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1019', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1019', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1019', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1019', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1020', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1020', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1020', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1020', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1021', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1021', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1021', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1021', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1022', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1022', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1022', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1022', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1023', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1023', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1023', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1023', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1024', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1024', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1024', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1024', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1025', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1025', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1025', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1025', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1026', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1026', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1026', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1026', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1027', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1027', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1027', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1027', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1028', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1028', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1028', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1028', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1029', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1029', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1029', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1029', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1030', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1030', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1030', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1030', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1031', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1031', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1031', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1031', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1032', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1032', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1032', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1032', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1033', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1033', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1033', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1033', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1034', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1034', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1034', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1034', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1035', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1035', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1035', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1035', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1036', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1036', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1036', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1036', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1037', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1037', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1037', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1037', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1038', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1038', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1038', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1038', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1039', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1039', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1039', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1039', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1040', 1, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1040', 2, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1040', 3, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MI1040', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1001', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1001', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1001', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1001', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1002', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1002', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1002', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1002', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1003', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1003', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1003', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1003', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1004', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1004', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1004', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1004', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1005', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1005', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1005', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1005', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1006', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1006', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1006', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1006', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1007', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1007', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1007', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1007', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1008', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1008', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1008', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1008', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1009', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1009', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1009', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1009', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1010', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1010', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1010', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1010', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1011', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1011', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1011', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1011', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1012', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1012', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1012', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1012', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1013', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1013', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1013', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1013', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1014', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1014', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1014', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1014', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1015', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1015', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1015', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1015', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1016', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1016', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1016', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1016', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1017', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1017', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1017', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1017', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1018', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1018', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1018', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1018', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1019', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1019', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1019', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1019', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1020', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1020', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1020', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1020', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1021', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1021', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1021', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1021', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1022', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1022', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1022', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1022', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1023', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1023', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1023', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1023', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1024', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1024', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1024', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1024', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1025', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1025', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1025', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1025', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1026', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1026', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1026', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1026', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1029', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1029', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1029', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1029', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1030', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1030', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1030', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1030', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1031', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1031', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1031', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1031', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1032', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1032', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1032', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1032', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1033', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1033', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1033', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1033', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1034', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1034', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1034', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1034', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1036', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1036', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1036', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1036', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1037', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1037', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1037', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1037', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1039', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1039', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1039', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1039', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1040', 1, 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes', 'Proceso de autoevaluaci�n y evaluaci�n entre estudiantes');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1040', 2, 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor', 'Informe Maestro-Supervisor');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1040', 3, 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as', 'Participaci�n en Seminarios y/o Tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MP1040', 4, 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos', 'Elaboraci�n y/o exposici�n de trabajos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1001', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1002', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1003', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1004', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1005', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1006', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1007', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1008', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1009', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1010', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1011', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1012', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1013', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1014', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1015', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1016', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1017', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1018', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1019', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1020', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1021', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1022', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1023', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1024', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1025', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1026', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1027', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1028', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1029', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1030', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1031', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1032', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1033', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1034', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1035', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1036', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1037', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1038', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1045', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1046', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 1, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 2, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 4, 'Memoria', 'Memoria', 'Memoria');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 5, 'Presentaci�n oral', 'Presentaci�n oral', 'Presentaci�n oral');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('MT1047', 7, 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado', 'Evaluaci�n de pr�cticas externas y proyecto de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0901', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0901', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0901', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0901', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0901', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 2, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 3, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 4, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 5, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 6, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 7, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 8, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 9, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0902', 10, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0903', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0903', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0903', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0903', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0903', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0904', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0904', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0904', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0904', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0904', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0905', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0905', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0905', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0905', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0905', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0906', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0906', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0906', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0906', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0906', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0907', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0907', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0907', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0907', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0907', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0908', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0908', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0908', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0908', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0908', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0909', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0909', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0909', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0909', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0909', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0910', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0910', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0910', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0910', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0910', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0911', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0911', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0911', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0911', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0911', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0912', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0912', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0912', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0912', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0912', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0913', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0913', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0913', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0913', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0913', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0914', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0914', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0914', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0914', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0914', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0915', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0915', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0915', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0915', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0915', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0916', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0916', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0916', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0916', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0916', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0917', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0917', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0917', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0917', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0917', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0918', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0918', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0918', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0918', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0918', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0919', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0919', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0919', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0919', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0919', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0920', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0920', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0920', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0920', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0920', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0921', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0921', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0921', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0921', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0921', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0922', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0922', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0922', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0922', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0922', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0923', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0923', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0923', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0923', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0923', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0924', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0924', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0924', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0924', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0924', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0925', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0925', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0925', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0925', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0925', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0926', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0926', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0926', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0926', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0926', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0927', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0927', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0927', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0927', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0927', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0928', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0928', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0928', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0928', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0928', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0929', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0929', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0929', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0929', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0929', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0930', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0930', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0930', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0930', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0930', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0931', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0931', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0931', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0931', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0931', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0932', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0932', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0932', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0932', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0932', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0933', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0933', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0933', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0933', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0933', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0934', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0934', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0934', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0934', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0934', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0935', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0935', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0935', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0935', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0935', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0936', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0936', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0936', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0936', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0936', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0937', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0937', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0937', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0937', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0937', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0938', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0938', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0938', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0938', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0938', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0939', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0939', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0939', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0939', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0939', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0940', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0940', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0940', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0940', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0940', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0941', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0941', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0941', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0941', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0941', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0942', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0942', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0942', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0942', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0942', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0943', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0943', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0943', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0943', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0943', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0944', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0944', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0944', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0944', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0944', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0945', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0945', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0945', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0945', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0945', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0946', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0946', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0946', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0946', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0946', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0947', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0947', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0947', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0947', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0947', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0948', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0948', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0948', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0948', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0948', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0949', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0949', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0949', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0949', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0949', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0950', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0950', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0950', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0950', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0950', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0951', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0951', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0951', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0951', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0951', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0952', 1, 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas', 'Diarios y/o cuadernos de notas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0952', 2, 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos', 'Elaboraci�n de trabajos acad�micos');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0952', 3, 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)', 'Examen escrito (test, desarrollo y/o problemas)');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0952', 4, 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas', 'Observaci�n/ejecuci�n de tareas y pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('PU0952', 5, 'Presentacions orals i p�sters', 'Presentaciones orales y p�sters', 'Presentaciones orales y p�sters');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0901', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0901', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0901', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0901', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0902', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0902', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0902', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0902', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0903', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0903', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0903', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0903', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0904', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0904', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0904', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0904', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0905', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0905', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0905', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0905', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0906', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0906', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0906', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0906', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0907', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0907', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0907', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0907', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0908', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0908', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0908', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0908', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0909', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0909', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0909', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0909', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0910', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0910', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0910', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0910', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0911', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0911', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0911', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0911', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0912', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0912', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0912', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0912', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0913', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0913', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0913', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0913', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0914', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0914', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0914', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0914', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0915', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0915', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0915', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0915', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0916', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0916', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0916', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0916', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0917', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0917', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0917', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0917', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0918', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0918', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0918', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0918', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0919', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0919', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0919', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0919', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0920', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0920', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0920', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0920', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0921', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0921', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0921', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0921', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0922', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0922', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0922', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0922', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0923', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0923', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0923', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0923', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0924', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0924', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0924', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0924', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0925', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0925', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0925', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0925', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0926', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0926', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0926', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0926', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0927', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0927', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0927', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0927', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0928', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0928', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0928', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0928', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0929', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0929', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0929', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0929', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0930', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0930', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0930', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0930', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0931', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0931', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0931', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0931', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0932', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0932', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0932', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0932', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0933', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0933', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0933', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0933', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0934', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0934', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0934', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0934', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0935', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0935', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0935', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0935', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0936', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0936', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0936', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0936', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0937', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0937', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0937', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0937', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0938', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0938', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0938', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0938', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0939', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0939', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0939', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0939', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0940', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0940', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0940', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0940', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0941', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0941', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0941', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0941', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0942', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0942', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0942', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0942', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0943', 1, 'Proves escrites', 'Pruebas escritas', 'Pruebas escritas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0943', 2, 'Participaci� en seminaris i tutories', 'Participaci�n en seminarios y tutor�as', 'Participaci�n en seminarios y tutor�as');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0943', 3, 'Laboratori', 'Laboratorio', 'Laboratorio');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('QU0943', 4, 'Aprenentatge basat en problemes', 'Aprendizaje basado en problemas', 'Aprendizaje basado en problemas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1001', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1002', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1003', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1004', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1005', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1006', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1007', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1008', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1009', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1010', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1011', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1012', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1013', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1014', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1015', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1016', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1017', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1018', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1019', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1020', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1021', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1022', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1023', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1024', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1025', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1026', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1027', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1028', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1029', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1030', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1031', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1032', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1033', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1034', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1035', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1036', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1037', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 1, 'Examen', 'Examen', 'Examen');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 2, 'Evaluaci�n continua', 'Evaluaci�n continua', 'Evaluaci�n continua');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 3, 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas', 'Evaluaci�n de pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 4, 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s', 'Presentaci�n oral en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 5, 'Proyecto', 'Proyecto', 'Proyecto');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 6, 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s', 'Trabajo escrito en ingl�s');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 7, 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas', 'Evaluaci�n de estancia en pr�cticas');
Insert into GDO_VW_TIPOS_EVALUACION
   (COD_ASIGNATURA, ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK)
 Values
   ('TE1051', 8, 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado', 'Evaluaci�n de pr�cticas externas y trabajo de fin de grado');
COMMIT;



-- gdo_vw_tipos_actividades

select id, nombre_ca, nombre nombre_es, nvl(nombre_uk, nombre) nombre_uk, tipo
from ver_tipos_act

Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (8, 'Ensenyaments pr�ctics (laboratori)', 'Ense�anzas pr�cticas (laboratorio)', 'Ense�anzas pr�cticas (laboratorio)', 'P');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (1, 'Ensenyaments te�rics', 'Ense�anzas te�ricas', 'Ense�anzas te�ricas', 'P');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (2, 'Ensenyaments pr�ctics (problemes)', 'Ense�anzas pr�cticas (problemas)', 'Ense�anzas pr�cticas (problemas)', 'P');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (3, 'Seminaris', 'Seminarios', 'Seminarios', 'P');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (4, 'Tutories', 'Tutor�as', 'Tutor�as', 'P');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (5, 'Avaluaci�', 'Evaluaci�n', 'Evaluaci�n', 'P');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (6, 'Treball personal', 'Trabajo personal', 'Trabajo personal', 'N');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (7, 'Treball de preparaci� dels ex�mens', 'Trabajo de preparaci�n de los ex�menes', 'Trabajo de preparaci�n de los ex�menes', 'N');
Insert into GDO_VW_TIPO_ACTIVIDADES
   (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO)
 Values
   (9, 'Ensenyaments pr�ctics (pr�ctiques externes)', 'Ense�anzas pr�cticas (pr�cticas externas)', 'Ense�anzas pr�cticas (pr�cticas externas)', 'P');
COMMIT;



-- gdo_vw_titulaciones_per

select tit_id, nombre, per_id
from   (SELECT tit_id, per_id
        FROM   GRH_CARGOS_PER
        where  crg_id in (108, 192, 193, 257, 258)
        and    (   f_fin is null
                or f_fin >= sysdate)
        and    tit_id is not null
        union
        select t.id, a.persona_id
        from   pod_titulaciones t,
               gdo_administradores a
        where  tipo_estudio = 'G'
        and    a.tipo = 'A'
        union
        select t.estudio_id, a.persona_id
        from   gdo_administradores a
        where  a.tipo = 'D'
        union
        select pmas_id, per_id
        from   pop_comisiones
        where  crg_id in (1, 3)
        and    trunc (fecha_ini) <= trunc (sysdate)
        and    trunc (nvl (fecha_fin, sysdate)) >= trunc (sysdate)) x,
       exp_v_titu_todas t
where  tit_id = t.id(+)


-- gdo_vw_titulaciones_asi

select asi_id, tit_id
from   pod_asignaturas_titulaciones
union
select pasi_id, pmas_id
from   pop_asignaturas_master


-- gdo_vw_asignaturas_per

/* Formatted on 16/12/2010 14:07 (Formatter Plus v4.8.8) */
select id, curso_aca, per_id
from   gdo_asignaturas asi,
       gdo_administradores adm
where  adm.tipo in ('A')
union
select id, curso_aca, persona_id
from   gdo_asignaturas
union
select asi.id, asi.curso_aca, per.per_id
from   gdo_asignaturas asi,
       gdo_permisos per
where  asi.id = per.asignatura_id

