  CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS_PER(ASIGNATURA_ID, PERSONA_ID, CURSO_ACA, TIPO_ACCESO) as
   SELECT a."ID", a."PERSONA_ID", a."CURSO_ACA", 'AS' tipo
     FROM gdo_asignaturas a
    WHERE persona_id is not null
   UNION ALL
   /* asignaturas delegadas */
   SELECT a."ID", ap."PERSONA_ID", a."CURSO_ACA", 'DE' tipo
     FROM gdo_asi_permisos ap, gdo_asignaturas a
    WHERE a.id = ap.asignatura_id
   UNION ALL
   /* directores de titulacion */
   SELECT a."ID", tp."PERSONA_ID", a."CURSO_ACA", 'DIR' tipo
     FROM gdo_asignaturas a, gdo_ext_titulaciones_asi ta, gdo_ext_titulaciones_per tp
    WHERE a.cod_asignatura = ta.cod_asignatura
      AND tp.estudio_id = ta.estudio_id
      AND cargo_id in ('DIR', 'D+', 'ADM')
   UNION ALL
   /* coordinadores de curso */
   SELECT a."ID", tp."PERSONA_ID", a."CURSO_ACA", 'COR' tipo
     FROM gdo_asignaturas a, gdo_ext_titulaciones_asi ta, gdo_ext_titulaciones_per tp
    WHERE a.cod_asignatura = ta.cod_asignatura
      AND tp.estudio_id = ta.estudio_id
      AND tp.cargo_id = 'COR'
      AND tp.curso_id = decode(tp.curso_id, 0, tp.curso_id, ta.curso_id)
   UNION ALL
   /* pas de departamento */
   SELECT a."ID", p2.id "PERSONA_ID", a."CURSO_ACA", 'PAS' tipo
     FROM gdo_asignaturas a, gdo_ext_contrataciones p, gdo_ext_personas p2
    WHERE a.persona_id is not null
      AND a.persona_id = p.per_id
      AND p.ubicacion_id = p2.ubicacion_id
      AND p2.actividad_id = 'PAS'
      AND p2.es_auxiliar = 'S'
      AND p2.login is not null
      and p2.ubicacion_id <> 0
   UNION ALL
   /* PAS dels centres */
   /* Veuen les asignatures que pertanyen a titulacions del seu centre */
   SELECT a.ID, c.per_id "PERSONA_ID", a."CURSO_ACA", c.act_id tipo
      FROM gdo_Ext_titu_todas t, gdo_Ext_titulaciones_asi ta, gdo_Ext_contrataciones c, gdo_asignaturas a
      WHERE t.id= ta.estudio_id
      AND centro_id= c.ubicacion_id
      AND c.act_id='PAS'
      AND c.ubicacion_id IN (SELECT id
                          FROM est_ubic_estructurales
                         WHERE tuest_id = 'CE')
      AND ta.cod_asignatura = a.cod_asignatura
   /* pas dels centres */
   /* Veuen les asignatures que estan assignades a professors del seu centre */
   /*select a."ID", p2.id "PERSONA_ID", a."CURSO_ACA", 'PAS' tipo
     from gdo_asignaturas a, gdo_ext_contrataciones p, est_estructuras r, gdo_ext_personas p2
    where a.persona_id is not null
      and a.persona_id = p.per_id
      and p2.actividad_id = 'PAS'
      and p2.es_auxiliar = 'N'
      and p2.login is not null
      and p2.ubicacion_id <> 0
      and p.ubicacion_id = r.uest_id_relacionad
      and r.uest_id = p2.ubicacion_id
      and r.uest_id in (select id
                          from est_ubic_estructurales
                         where tuest_id = 'CE')
      and r.uest_id_relacionad not in (218);*/

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS(ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO, NOMBRE, INTERUNIVERSITARIO, COORDINA_UJI, CENTRO_ID) as
   SELECT id, nombre nombre_ca, nomval nombre_es, nomang nombre_uk, 'G' tipo, nombre, 'N' interuniversitario, 'N' coordina_uji, uest_id
     FROM pod_titulaciones
    WHERE id BETWEEN 201 AND 9999
   UNION ALL
   SELECT id, nombre nombre_ca, nombre_es, nombre_uk, 'M' tipo, nombre, DECODE(id, 42127, 'N', 42161, 'N', 42108, 'N', interuniversitario) interuniversitario, coordina_uji, uest_id
     FROM pop_masters
    WHERE oficial = 'S'
      AND id not like '420%';
