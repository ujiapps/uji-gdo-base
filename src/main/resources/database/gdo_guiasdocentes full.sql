-- Generado por Oracle SQL Developer Data Modeler 3.3.0.744
--   en:        2013-09-06 10:25:14 CEST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



DROP VIEW uji_guiasdocentes.gdo_ext_contrataciones 
;
DROP VIEW uji_guiasdocentes.gdo_vw_asignaturas_per 
;
DROP VIEW uji_guiasdocentes.gdo_vw_ext_actividades 
;
DROP VIEW uji_guiasdocentes.gdo_vw_ext_competencias 
;
DROP VIEW uji_guiasdocentes.gdo_vw_ext_evaluaciones 
;
DROP VIEW uji_guiasdocentes.gdo_vw_ext_resultados 
;
DROP TABLE uji_guiasdocentes.gdo_apartados CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_actividades CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_comentarios CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_competencias CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_evaluacion CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_permisos CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_resultados CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asi_textos CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_asignaturas CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_cursos_academicos CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_directores_extra CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_act CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_act_comp CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_eval CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_rdo CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_todas CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_personas CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_titu_todas CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_titulaciones_asi CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_ext_titulaciones_per CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_textos_pdf CASCADE CONSTRAINTS 
;
DROP TABLE uji_guiasdocentes.gdo_tipo_actividades CASCADE CONSTRAINTS 
;
CREATE TABLE uji_guiasdocentes.gdo_apartados 
    ( 
     id NUMBER  NOT NULL , 
     curso_aca NUMBER (4)  NOT NULL , 
     nombre_ca VARCHAR2 (200)  NOT NULL , 
     nombre_es VARCHAR2 (200)  NOT NULL , 
     nombre_uk VARCHAR2 (200) , 
     texto_ayuda VARCHAR2 , 
     orden NUMBER , 
     tipo VARCHAR2 (1)  NOT NULL , 
     obligatorio VARCHAR2 DEFAULT 'N'  NOT NULL , 
     tipo_estudio VARCHAR2 (1) 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_es_tipo_CK 
    CHECK (tipo in ('T','C','A','E','I','M'))
;


ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_ob_CK 
    CHECK (obligatorio in ('S','N'))
;


ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_tip_est_CK 
    CHECK (tipo_estudio in ('T','G','M'))
;

CREATE INDEX uji_guiasdocentes.gdo_apartados_curso_IDX ON uji_guiasdocentes.gdo_apartados 
    ( 
     curso_aca ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_pk PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_actividades 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     actividad_id NUMBER  NOT NULL , 
     horas_presenciales NUMBER DEFAULT 0  NOT NULL , 
     horas_no_presenciales NUMBER DEFAULT 0  NOT NULL 
    ) 
;


CREATE INDEX uji_guiasdocentes.gdo_asi_actividades_asi_IDX ON uji_guiasdocentes.gdo_asi_actividades 
    ( 
     asignatura_id ASC 
    ) 
;
CREATE INDEX uji_guiasdocentes.gdo_asi_actividades_act_IDX ON uji_guiasdocentes.gdo_asi_actividades 
    ( 
     actividad_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_actividades 
    ADD CONSTRAINT gdo_asi_actividades_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_asi_actividades 
    ADD CONSTRAINT gdo_asi_actividades__UN UNIQUE ( asignatura_id , actividad_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     comentario VARCHAR2  NOT NULL , 
     idioma VARCHAR2 (200) 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ADD CONSTRAINT gdo_asi_comentarios_CK 
    CHECK (idioma in ('Castella','Valencia','Angles','Comentari general'))
;

CREATE INDEX uji_guiasdocentes.gdo_asi_comentarios__IDX ON uji_guiasdocentes.gdo_asi_comentarios 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ADD CONSTRAINT gdo_asi_comentarios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_competencias 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     competencia_ca VARCHAR2 , 
     competencia_es VARCHAR2 , 
     competencia_uk VARCHAR2 , 
     editable VARCHAR2 (1)  NOT NULL , 
     orden NUMBER 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_asi_competencias 
    ADD CONSTRAINT gdo_asi_competencias_CK 
    CHECK (editable in ('S','N'))
;

CREATE INDEX uji_guiasdocentes.gdo_asi_competencias_asi_IDX ON uji_guiasdocentes.gdo_asi_competencias 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_competencias 
    ADD CONSTRAINT gdo_asi_competencias_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_evaluacion 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     evaluacion_id NUMBER  NOT NULL , 
     ponderacion NUMBER , 
     nombre_ca VARCHAR2 (200) , 
     nombre_es VARCHAR2 (200) , 
     nombre_uk VARCHAR2 (200) , 
     editable VARCHAR2 (1)  NOT NULL , 
     orden NUMBER 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_asi_evaluacion 
    ADD CONSTRAINT gdo_asi_evaluacion_editable_CK 
    CHECK (editable in ('S','N'))
;

CREATE INDEX uji_guiasdocentes.gdo_asi_evaluacion_asi_IDX ON uji_guiasdocentes.gdo_asi_evaluacion 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_evaluacion 
    ADD CONSTRAINT gdo_asi_evaluacion_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_asi_evaluacion 
    ADD CONSTRAINT gdo_asi_evaluacion_UN_1 UNIQUE ( asignatura_id , evaluacion_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     idioma_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_guiasdocentes.gdo_asi_idiomas_asi_IDX ON uji_guiasdocentes.gdo_asi_idiomas 
    ( 
     asignatura_id ASC 
    ) 
;
CREATE INDEX uji_guiasdocentes.gdo_asi_idiomas_idi_IDX ON uji_guiasdocentes.gdo_asi_idiomas 
    ( 
     idioma_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_idiomas 
    ADD CONSTRAINT gdo_asi_idiomas_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_asi_idiomas 
    ADD CONSTRAINT gdo_asi_idiomas__UN UNIQUE ( asignatura_id , idioma_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_permisos 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_asi_permisos 
    ADD CONSTRAINT gdo_asi_permisos_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_asi_permisos 
    ADD CONSTRAINT gdo_asi_permisos__UN UNIQUE ( asignatura_id , persona_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_resultados 
    ( 
     id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     resultado_ca VARCHAR2 , 
     resultado_es VARCHAR2 , 
     resultado_uk VARCHAR2 , 
     editable VARCHAR2 (1)  NOT NULL , 
     orden NUMBER 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_asi_resultados 
    ADD CONSTRAINT gdo_asi_resultados_CK 
    CHECK (editable in ('S','N'))
;

CREATE INDEX uji_guiasdocentes.gdo_asi_resultados_asi_IDX ON uji_guiasdocentes.gdo_asi_resultados 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_resultados 
    ADD CONSTRAINT gdo_asi_resultados_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asi_textos 
    ( 
     id NUMBER  NOT NULL , 
     apartado_id NUMBER  NOT NULL , 
     asignatura_id NUMBER  NOT NULL , 
     fecha_slt_ca DATE , 
     fecha_slt_es DATE , 
     fecha_slt_uk DATE , 
     texto_ca CLOB , 
     texto_es CLOB , 
     texto_uk CLOB 
    ) 
;


CREATE INDEX uji_guiasdocentes.gdo_textos_asi_IDX ON uji_guiasdocentes.gdo_asi_textos 
    ( 
     asignatura_id ASC 
    ) 
;
CREATE INDEX uji_guiasdocentes.gdo_textos_apr_IDX ON uji_guiasdocentes.gdo_asi_textos 
    ( 
     apartado_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asi_textos 
    ADD CONSTRAINT gdo_textos_pk PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_asi_textos 
    ADD CONSTRAINT gdo_textos_uk1 UNIQUE ( apartado_id , asignatura_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     curso_aca NUMBER (4)  NOT NULL , 
     cod_asignatura VARCHAR2 (10)  NOT NULL , 
     asignatura_id_origen NUMBER , 
     fecha_ini DATE , 
     fecha_fin DATE , 
     estado VARCHAR2 (1)  NOT NULL , 
     persona_id NUMBER , 
     autoriza_pas VARCHAR2 (1)  NOT NULL , 
     fecha_validacion_ca DATE , 
     fecha_validacion_es DATE , 
     fecha_validacion_uk DATE , 
     fecha_finalizacion DATE 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_CK 
    CHECK (autoriza_pas in ('S','N'))
;

CREATE INDEX uji_guiasdocentes.gdo_asignaturas_curso_IDX ON uji_guiasdocentes.gdo_asignaturas 
    ( 
     curso_aca ASC 
    ) 
;
CREATE INDEX uji_guiasdocentes.gdo_asignaturas_per_IDX ON uji_guiasdocentes.gdo_asignaturas 
    ( 
     persona_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_pk PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_uk1 UNIQUE ( curso_aca , cod_asignatura ) ;



CREATE TABLE uji_guiasdocentes.gdo_cursos_academicos 
    ( 
     curso_aca NUMBER (4)  NOT NULL , 
     activo VARCHAR2 (1)  NOT NULL , 
     fecha_ini DATE  NOT NULL , 
     fecha_fin DATE  NOT NULL 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_cursos_academicos 
    ADD CONSTRAINT gdo_cursos_academicos_pk PRIMARY KEY ( curso_aca ) ;



CREATE TABLE uji_guiasdocentes.gdo_directores_extra 
    ( 
     id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     tipo_permiso VARCHAR2 DEFAULT 'DIR'  NOT NULL , 
     curso_id NUMBER 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_directores_extra 
    ADD CONSTRAINT gdo_directores_extra_CK 
    CHECK (tipo_permiso in ('DIR','COR'))
;

CREATE INDEX uji_guiasdocentes.gdo_directores_per_IDX ON uji_guiasdocentes.gdo_directores_extra 
    ( 
     persona_id ASC 
    ) 
;
CREATE INDEX uji_guiasdocentes.gdo_directores_est_IDX ON uji_guiasdocentes.gdo_directores_extra 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_guiasdocentes.gdo_directores_extra 
    ADD CONSTRAINT gdo_directores_pk PRIMARY KEY ( id ) ;


ALTER TABLE uji_guiasdocentes.gdo_directores_extra 
    ADD CONSTRAINT gdo_directores_UN UNIQUE ( persona_id , estudio_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_ext_asi_detalle 
    ( 
     cod_asignatura VARCHAR2 (10) , 
     curso_aca_ini NUMBER (4) , 
     curso_aca_fin NUMBER (4) , 
     horas_al NUMBER , 
     prerequisitos_ca CLOB , 
     prerequisitos_es CLOB , 
     contenidos_ca CLOB , 
     contenidos_es CLOB , 
     justificacion_ca VARCHAR2 , 
     justificacion_es VARCHAR2 
    ) 
;




CREATE TABLE uji_guiasdocentes.gdo_ext_asi_detalle_act 
    ( 
     cod_asignatura VARCHAR2 (10) , 
     curso_aca_ini NUMBER , 
     act_id NUMBER , 
     nombre_ca VARCHAR2 (200) , 
     nombre_es VARCHAR2 (200) , 
     h_pre DATE , 
     h_no_pre DATE , 
     tipo_grupo VARCHAR2 (1) , 
     tipo VARCHAR2 (1) , 
     orden NUMBER 
    ) 
;




CREATE TABLE uji_guiasdocentes.gdo_ext_asi_detalle_act_comp 
    ( 
     cod_asignatura VARCHAR2 (10) , 
     curso_aca_ini NUMBER , 
     act_id NUMBER , 
     comp_id NUMBER , 
     nombre_ca VARCHAR2 , 
     nombre_es VARCHAR2 , 
     nombre_uk VARCHAR2 , 
     tipo VARCHAR2 (1) 
    ) 
;




CREATE TABLE uji_guiasdocentes.gdo_ext_asi_detalle_eval 
    ( 
     cod_asignatura VARCHAR2 (10) , 
     curso_aca_ini NUMBER , 
     eval_id NUMBER , 
     nombre_ca VARCHAR2 (200) , 
     nombre_es VARCHAR2 (200) , 
     nombre_uk VARCHAR2 (200) , 
     ponderacion NUMBER 
    ) 
;




CREATE TABLE uji_guiasdocentes.gdo_ext_asi_detalle_rdo 
    ( 
     cod_asignatura VARCHAR2 (10) , 
     curso_aca_ini NUMBER , 
     rdo_id NUMBER , 
     nombre_ca VARCHAR2 , 
     nombre_es VARCHAR2 , 
     nombre_uk VARCHAR2 
    ) 
;




CREATE TABLE uji_guiasdocentes.gdo_ext_asi_todas 
    ( 
     id VARCHAR2 (10)  NOT NULL , 
     nombre_ca VARCHAR2 (200) , 
     nombre_es VARCHAR2 (200) , 
     nombre_uk VARCHAR2 (200) , 
     tipo VARCHAR2 (10)  NOT NULL 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_ext_asi_todas 
    ADD CONSTRAINT gdo_ext_asi_todas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_ext_personas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (200) , 
     apellido1 VARCHAR2 (200) , 
     apellido2 VARCHAR2 (200) , 
     actividad_id VARCHAR2 (10)  NOT NULL , 
     ubicacion_id NUMBER  NOT NULL , 
     es_auxiliar VARCHAR2 (1)  NOT NULL , 
     login VARCHAR2 (200)  NOT NULL 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_ext_personas 
    ADD CONSTRAINT gdo_ext_personas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_ext_titu_todas 
    ( 
     id NUMBER  NOT NULL , 
     nombre_ca VARCHAR2 (200)  NOT NULL , 
     nombre_es VARCHAR2 (200) , 
     nombre_uk VARCHAR2 (200) , 
     tipo VARCHAR2 (10)  NOT NULL 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_ext_titu_todas 
    ADD CONSTRAINT gdo_ext_titu_todas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_ext_titulaciones_asi 
    ( 
     cod_asignatura VARCHAR2 (10)  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     curso_id NUMBER 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_ext_titulaciones_asi 
    ADD CONSTRAINT gdo_ext_titulaciones_asi_PK PRIMARY KEY ( cod_asignatura, estudio_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_ext_titulaciones_per 
    ( 
     estudio_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (200) , 
     persona_id NUMBER  NOT NULL , 
     curso_id NUMBER , 
     cargo_id VARCHAR2 (200) 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_ext_titulaciones_per 
    ADD CONSTRAINT gdo_ext_titulaciones_per_PK PRIMARY KEY ( estudio_id, persona_id ) ;



CREATE TABLE uji_guiasdocentes.gdo_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     nombre_ca VARCHAR2 (200)  NOT NULL , 
     nombre_es VARCHAR2 (200)  NOT NULL , 
     nombre_uk VARCHAR2 (200) 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_idiomas 
    ADD CONSTRAINT gdo_idiomas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_textos_pdf 
    ( 
     id NUMBER  NOT NULL , 
     nombre_ca VARCHAR2 (200) , 
     nombre_es VARCHAR2 (200) , 
     nombre_uk VARCHAR2 (200) 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_textos_pdf 
    ADD CONSTRAINT gdo_textos_pdf_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_guiasdocentes.gdo_tipo_actividades 
    ( 
     id NUMBER  NOT NULL , 
     nombre_ca VARCHAR2 (200)  NOT NULL , 
     nombre_es VARCHAR2 (200)  NOT NULL , 
     nombre_uk VARCHAR2 (200) , 
     tipo VARCHAR2 (1)  NOT NULL , 
     orden NUMBER 
    ) 
;



ALTER TABLE uji_guiasdocentes.gdo_tipo_actividades 
    ADD CONSTRAINT gdo_tipo_actividades_CK 
    CHECK (tipo in ('P','N'))
;


ALTER TABLE uji_guiasdocentes.gdo_tipo_actividades 
    ADD CONSTRAINT gdo_tipo_actividades_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_guiasdocentes.gdo_directores_extra 
    ADD CONSTRAINT gdo_admin_exp_v_titu_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_ext_titu_todas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_directores_extra 
    ADD CONSTRAINT gdo_admin_gdo_vw_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_fk1 FOREIGN KEY 
    ( 
     curso_aca
    ) 
    REFERENCES uji_guiasdocentes.gdo_cursos_academicos 
    ( 
     curso_aca
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_actividades 
    ADD CONSTRAINT gdo_asi_actividades_FK1 FOREIGN KEY 
    ( 
     actividad_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_tipo_actividades 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_actividades 
    ADD CONSTRAINT gdo_asi_actividades_FK2 FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ADD CONSTRAINT gdo_asi_comentarios_FK FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_evaluacion 
    ADD CONSTRAINT gdo_asi_evaluacion_FK FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_idiomas 
    ADD CONSTRAINT gdo_asi_idiomas_FK1 FOREIGN KEY 
    ( 
     idioma_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_idiomas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_idiomas 
    ADD CONSTRAINT gdo_asi_idiomas_FK2 FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_permisos 
    ADD CONSTRAINT gdo_asi_permiso_FK1 FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_permisos 
    ADD CONSTRAINT gdo_asi_permisos_fk2 FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_competencias 
    ADD CONSTRAINT gdo_asignaturas_FK FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_FK2 FOREIGN KEY 
    ( 
     asignatura_id_origen
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_FK3 FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_FK4 FOREIGN KEY 
    ( 
     cod_asignatura
    ) 
    REFERENCES uji_guiasdocentes.gdo_ext_asi_todas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asignaturas 
    ADD CONSTRAINT gdo_asignaturas_fk1 FOREIGN KEY 
    ( 
     curso_aca
    ) 
    REFERENCES uji_guiasdocentes.gdo_cursos_academicos 
    ( 
     curso_aca
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_resultados 
    ADD CONSTRAINT gdo_rdo_asi_FK FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_textos 
    ADD CONSTRAINT gdo_textos_fk1 FOREIGN KEY 
    ( 
     apartado_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_apartados 
    ( 
     id
    ) 
;


ALTER TABLE uji_guiasdocentes.gdo_asi_textos 
    ADD CONSTRAINT gdo_textos_fk2 FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_guiasdocentes.gdo_asignaturas 
    ( 
     id
    ) 
;

CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_ext_contrataciones  AS
SELECT NREG_EXP,
  FECHA_INICIO,
  FECHA_FIN,
  ID,
  GRUPO,
  NIVEL,
  ESPECIFICO,
  IDENTIFICACION,
  NOMBRE,
  PER_ID,
  FECHA_INI_PLZ,
  CTNES_ID,
  SADM_CODIGO,
  SADM_FECHA,
  SADM_COBRA,
  TPR_CODIGO,
  GASO_HOR_PRD_ID,
  GASO_HOR_ID,
  GASO_CTGP_ACT_ID,
  GASO_CTGP_ID,
  ACT_ID,
  N_PLAZA,
  TITULAR,
  TCON_CODIGO,
  CALT_CODIGO,
  CPER_ACT_ID,
  CPER_ID,
  CTGP_ID,
  FECHA_PRO_FIN,
  EXENTO_EXTRA,
  NOMBRE_MIN,
  UBICACION_ID,
  AREA_ID
FROM grh_vw_contrataciones_ult ;



CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_asignaturas_per  AS
SELECT a.id,
  a.persona_id,
  a.curso_aca,
  'AS' tipo
FROM gdo_asignaturas a
WHERE a.persona_id IS NOT NULL
UNION ALL
SELECT a.id,
  ap.persona_id,
  a.curso_aca,
  'DE' tipo
FROM gdo_asi_permisos ap,
  gdo_asignaturas a
WHERE a.id = ap.asignatura_id
UNION ALL
SELECT a.id,
  tp.persona_id,
  a.curso_aca,
  'DIR' tipo
FROM gdo_asignaturas a,
  gdo_ext_titulaciones_asi ta,
  gdo_ext_titulaciones_per tp
WHERE a.cod_asignatura = ta.cod_asignatura
AND tp.estudio_id      = ta.estudio_id
AND (tp.cargo_id      IN ('DIR', 'D+', 'ADM'))
UNION ALL
SELECT a.id,
  tp.persona_id,
  a.curso_aca,
  'COR' tipo
FROM gdo_asignaturas a,
  gdo_ext_titulaciones_asi ta,
  gdo_ext_titulaciones_per tp
WHERE a.cod_asignatura = ta.cod_asignatura
AND tp.estudio_id      = ta.estudio_id
AND tp.curso_id        = DECODE(tp.curso_id, 0, tp.curso_id, ta.curso_id)
AND (tp.cargo_id       = 'COR')
UNION ALL
SELECT a.id,
  p2.id PERSONA_ID,
  a.curso_aca,
  'PAS' tipo
FROM gdo_asignaturas a,
  gdo_ext_contrataciones p,
  gdo_ext_personas p2
WHERE a.persona_id   = p.per_id
AND p.ubicacion_id   = p2.ubicacion_id
AND (a.persona_id   IS NOT NULL
AND p2.actividad_id  = 'PAS'
AND p2.es_auxiliar   = 'S'
AND p2.login        IS NOT NULL
AND p2.ubicacion_id <> 0) ;



CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_actividades  AS
SELECT d.cod_asignatura,
  da.curso_aca_ini,
  d.curso_aca_fin,
  da.act_id,
  (da.h_pre    - to_date('1-1-1', 'dd-mm-yyyy')) * 24 h_pre,
  (da.h_no_pre - to_date('1-1-1', 'dd-mm-yyyy')) * 24 h_no_pre
FROM gdo_ext_asi_detalle_act da,
  gdo_ext_asi_detalle d
WHERE d.cod_asignatura = da.cod_asignatura
AND d.curso_aca_ini    = da.curso_aca_ini ;



CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_competencias  AS
SELECT DISTINCT d.cod_asignatura,
  d.curso_aca_ini,
  d.curso_aca_fin,
  c.comp_id,
  c.nombre_ca,
  c.nombre_es,
  c.nombre_uk,
  c.tipo
FROM gdo_ext_asi_detalle_act_comp c,
  gdo_ext_asi_detalle d
WHERE d.cod_asignatura = c.cod_asignatura
AND d.curso_aca_ini    = c.curso_aca_ini
AND (c.comp_id        <> 0) ;



CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_evaluaciones  AS
SELECT d.cod_asignatura,
  d.curso_aca_ini,
  d.curso_aca_fin,
  r.curso_aca_ini,
  r.rdo_id,
  r.nombre_ca,
  r.nombre_es,
  r.nombre_uk
FROM gdo_ext_asi_detalle_rdo r,
  gdo_ext_asi_detalle d
WHERE d.cod_asignatura = r.cod_asignatura
AND d.curso_aca_ini    = r.curso_aca_ini ;



CREATE OR REPLACE VIEW uji_guiasdocentes.gdo_vw_ext_resultados  AS
SELECT d.cod_asignatura,
  d.curso_aca_ini,
  d.curso_aca_fin,
  r.curso_aca_ini,
  r.rdo_id,
  r.nombre_ca,
  r.nombre_es,
  r.nombre_uk
FROM gdo_ext_asi_detalle d,
  gdo_ext_asi_detalle_rdo r
WHERE d.cod_asignatura = r.cod_asignatura
AND d.curso_aca_ini    = r.curso_aca_ini ;





-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            25
-- CREATE INDEX                            15
-- ALTER TABLE                             56
-- CREATE VIEW                              6
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
