ALTER TABLE UJI_GUIASDOCENTES.GDO_APARTADOS
 ADD (tipo_estudio  VARCHAR2(1));


ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_tip_est_CK 
    CHECK (tipo_estudio in ('T','G','M'))
;

update uji_guiasdocentes.gdo_apartados
set tipo_estudio = 'T';

commit;

ALTER TABLE UJI_GUIASDOCENTES.GDO_APARTADOS
 MODIFY (tipo_estudio  VARCHAR2(1) NOT NULL);

 
/* Formatted on 27/02/2013 10:31 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_PERSONAS (ID,
                                                                 NOMBRE,
                                                                 APELLIDO1,
                                                                 APELLIDO2,
                                                                 ACTIVIDAD_ID,
                                                                 UBICACION_ID,
                                                                 ES_AUXILIAR,
                                                                 LOGIN
                                                                ) AS
   select id, nombre, apellido1, apellido2, act_id, ubicacion_id, es_auxiliar, login
   from   (select id, nombre, apellido1, apellido2, act_id, ubicacion_id, es_auxiliar, login,
                  row_number () over (partition by id order by ubicacion_id desc) orden
           from   (                                                                                      /* pas y pdi */
                   select distinct id, nombre, apellido1, apellido2, act_id,
                                   nvl ((select ubicacion_id
                                         from   grh_vw_contrataciones_ult c
                                         where  x.id = per_id
                                         and    x.act_id = c.act_id), 0) ubicacion_id,
                                   decode (act_id,
                                           'PDI', 'N',
                                           nvl ((SELECT distinct 'S'
                                                 FROM            grh_vw_contrataciones_ult cu,
                                                                 grh_plazas p,
                                                                 est_ubic_estructurales ue
                                                 WHERE           p.n_plaza = cu.n_plaza
                                                 AND             p.ID = cu.ID
                                                 AND             p.uest_id = ue.ID
                                                 AND             ue.tuest_id IN ('DE')
                                                 AND             ue.status = 'A'
                                                 AND             cu.cper_act_id = 'PAS'
                                                 and             p.nombre like '%dministrat%'
                                                 and             cu.per_id = x.id),
                                                'N')
                                          ) es_auxiliar,
                                   nvl(substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1),'uth') login
                   from            (select id, nombre, apellido1, apellido2, act_id, fecha_fin,
                                           row_number () over (partition by id order by fecha_fin desc,
                                            act_id asc) orden
                                    from   (select p.id, nombre, apellido1, apellido2,
                                                   decode (svi_vin_id, 4, 'PAS', 9, 'PDI', 'Error') act_id,
                                                   nvl (fecha_fin, add_months (trunc (sysdate), 1000)) fecha_fin
                                            from   per_personas_subvinculos ps,
                                                   per_personas p
                                            where  svi_vin_id in (9, 4)
                                            and    per_id = p.id)) x
                   where           orden = 1
                   union
                   /* pdi extra */
                   select id, nombre, apellido1, apellido2, 'PDI' act_id, 0 ubicacion_id, 'N' es_auxiliar,
                          nvl(substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1),'uth') login
                   from   per_personas
                   where  id in
                             (252381, 95178, 65297, 184937, 8218, 467765, 96364, 496930, 150325, 170986, 123225, 405744,
                              150227, 123227, 150315, 150323, 404084, 65174)
                   union
                   /* pas extra */
                   select id, nombre, apellido1, apellido2, 'PAS' act_id, 0 ubicacion_id, 'N' es_auxiliar,
                          nvl(substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1),'uth') login
                   from   per_personas
                   where  id in (840, 143302, 93307)
                   union
                   /* coordinadores de master */
                   select   per_id, nombre, apellido1, apellido2, act_id, min (ubicacion_id), es_auxiliar, login
                   from     (select distinct per_id, nombre, apellido1, apellido2, 'PDI' act_id,
                                             (select min (ubicacion_id)
                                              from   pop_asignaturas_master am,
                                                     pop_comisiones c,
                                                     grh_vw_contrataciones_ult cont
                                              where  am.pasi_id = g.pgr_pof_pasi_id
                                              and    am.pmas_id not in (42000, 42100)
                                              and    am.pmas_id = c.pmas_id
                                              and    c.per_id = cont.per_id
                                              and    act_id = 'PDI') ubicacion_id,
                                             'N' es_auxiliar,
                                             nvl(substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1),'uth') login
                             from            pop_grupos_pdi g,
                                             per_personas p
                             where           pgr_pof_curso_aca >= (select min (curso_aca)
                                                                   from   gdo_cursos_academicos
                                                                   where  activo = 'S') - 2
                             and             per_id not in (select per_id
                                                            from   grh_vw_contrataciones_ult)
                             and             pgr_pof_pmas_id in (select id
                                                                 from   pop_masters
                                                                 where  oficial = 'S')
                             and             per_id = p.id)
                   where    login is not null
                   group by per_id,
                            nombre,
                            apellido1,
                            apellido2,
                            act_id,
                            es_auxiliar,
                            login)
           where  login is not null
                                   --and    ubicacion_id <> 0
          )
   where  orden = 1;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER (ESTUDIO_ID,
                                                                         NOMBRE,
                                                                         PERSONA_ID,
                                                                         CURSO_ID,
                                                                         CARGO_ID
                                                                        ) AS
   select tit_id, nombre, per_id, curso_id, cargo_id
   from   (select tit_id, per_id, nvl (ciclo_cargo, 0) curso_id, 'DIR' cargo_id
           from   grh_grh.grh_cargos_per
           where  crg_id in (108, 192, 193, 257, 258)
           and    (   f_fin is null
                   or f_fin >= sysdate)
           and    tit_id is not null
           union
           select tit_id, per_id, nvl (ciclo_cargo, 1) curso_id, 'COR' cargo_id
           from   grh_grh.grh_cargos_per
           where  crg_id in (292, 293, 305, 307)
           and    (   f_fin is null
                   or f_fin >= sysdate)
           and    tit_id is not null
           union
           select a.estudio_id, a.persona_id, nvl (curso_id, 0) curso_id, tipo_permiso cargo_id
           from   uji_guiasdocentes.gdo_directores_extra a
           union
           select pmas_id, per_id, 0 curso_id, 'DIR' cargo_id
           from   gra_pop.pop_comisiones
           where  crg_id in (1, 3)
           and    trunc (fecha_ini) <= trunc (sysdate)
           and    trunc (nvl (fecha_fin, sysdate)) >= trunc (sysdate)) x,
          gra_exp.exp_v_titu_todas t
   where  tit_id = t.id(+)
   and    (   t.tipo = 'G'
           or (    t.tipo = 'M'
               and t.id not like '420%'))
   union all
   select tt.id estudio_id, tt.nombre_ca, persona_id, 0 curso_id, 'ADM' cargo_id
   from   gdo_ext_titu_todas tt,
          uji_apa.APA_APLICACIONES_EXTRAS e
   where  (   tipo = 'G'
           or (    tipo = 'M'
               and tt.id not like '420%'))
   and    role_id = 1
   and    aplicacion_id = 1;


ALTER TABLE UJI_GUIASDOCENTES.GDO_ASIGNATURAS
 ADD (fecha_finalizacion  DATE);
   