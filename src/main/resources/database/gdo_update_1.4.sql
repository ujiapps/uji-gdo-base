ALTER TABLE UJI_GUIASDOCENTES.GDO_ASI_TEXTOS ADD (MODIFICADO_CA NUMBER DEFAULT 0 NOT NULL);
ALTER TABLE UJI_GUIASDOCENTES.GDO_ASI_TEXTOS ADD (MODIFICADO_ES NUMBER DEFAULT 0 NOT NULL);
ALTER TABLE UJI_GUIASDOCENTES.GDO_ASI_TEXTOS ADD (MODIFICADO_UK NUMBER DEFAULT 0 NOT NULL);

create or replace trigger UJI_GUIASDOCENTES.gdo_asi_textos_update
   before update
   on UJI_GUIASDOCENTES.GDO_ASI_TEXTOS
   referencing new as New old as Old
   for each row
begin
   if :old.texto_ca <> :new.texto_ca or (:old.texto_ca is null and :new.texto_ca is not null) or (:old.texto_ca is not null and :new.texto_ca is null) then
      if :new.fecha_slt_ca is not null then
         :new.fecha_slt_ca := null;
      end if;
      :new.modificado_ca := 1;
   end if;

   if :old.texto_es <> :new.texto_es or (:old.texto_es is null and :new.texto_es is not null) or (:old.texto_es is not null and :new.texto_es is null) then
      if :new.fecha_slt_es is not null then
         :new.fecha_slt_es := null;
      end if;
      :new.modificado_es := 1;
   end if;

   if :old.texto_uk <> :new.texto_uk or (:old.texto_uk is null and :new.texto_uk is not null) or (:old.texto_uk is not null and :new.texto_uk is null) then
      if :new.fecha_slt_uk is not null then
         :new.fecha_slt_uk := null;
      end if;
      :new.modificado_uk := 1;
   end if;
end gdo_asi_textos_update;

create or replace force view UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER
(
   ESTUDIO_ID,
   NOMBRE,
   PERSONA_ID,
   CURSO_ID,
   CARGO_ID
)
   bequeath definer as
   select tit_id, nombre, per_id, curso_id, cargo_id
     from (select plan.tit_id, per_id, nvl(ciclo_cargo, 0) curso_id, 'DIR' cargo_id
             from grh_grh.grh_cargos_per c,
                  (select p2.tit_id, p.tit_id tit_id_permiso
                     from gra_pod.uji_Estudios e, gra_pod.uji_planes p, gra_pod.uji_planes p2
                    where e.id = p.estudio_id and e.id = p2.estudio_id) plan
            where crg_id in (108, 192, 193, 257, 258, 422) and (f_fin is null or f_fin >= sysdate) and c.tit_id is not null and c.tit_id = plan.tit_id_permiso
           union
           select plan.tit_id, per_id, nvl(ciclo_cargo, 1) curso_id, 'COR' cargo_id
             from grh_grh.grh_cargos_per c,
                  (select p2.tit_id, p.tit_id tit_id_permiso
                     from gra_pod.uji_Estudios e, gra_pod.uji_planes p, gra_pod.uji_planes p2
                    where e.id = p.estudio_id and e.id = p2.estudio_id) plan
            where crg_id in (292, 293, 305, 307) and (f_fin is null or f_fin >= sysdate) and c.tit_id is not null and c.tit_id = plan.tit_id_permiso
           union
           select a.estudio_id, a.persona_id, nvl(curso_id, 0) curso_id, tipo_permiso cargo_id
             from uji_guiasdocentes.gdo_directores_extra a
           union
           select pmas_id, per_id, 0 curso_id, 'DIR' cargo_id
             from gra_pop.pop_comisiones
            where crg_id in (1, 3) and trunc(fecha_ini) <= trunc(sysdate) and trunc(nvl(fecha_fin, sysdate)) >= trunc(sysdate)) x,
          gra_exp.exp_v_titu_todas t
    where tit_id = t.id(+) and (t.tipo = 'G' or (t.tipo = 'M' and t.id not like '420%'))
   union all
   select tt.id estudio_id, tt.nombre_ca, persona_id, 0 curso_id, 'ADM' cargo_id
     from gdo_ext_titu_todas tt, uji_apa.APA_APLICACIONES_EXTRAS e
    where (tipo = 'G' or (tipo = 'M' and tt.id not like '420%')) and role_id = 1 and aplicacion_id = 1;

drop view UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS;

create or replace force view UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS
(
   ID,
   CURSO_ACA,
   COD_ASIGNATURA,
   ASIGNATURA_ID_ORIGEN,
   FECHA_INI,
   FECHA_FIN,
   ESTADO,
   PERSONA_ID,
   AUTORIZA_PAS,
   FECHA_VALIDACION_CA,
   FECHA_VALIDACION_ES,
   FECHA_VALIDACION_UK,
   FECHA_FINALIZACION,
   MODIFICACIONES,
   TIPO_COMPARTIDA,
   EN_PLAZO,
   TIPO,
   NOMBRE,
   CURSO_ID,
   ESTUDIO_ID,
   NOMBRE_PROFESOR,
   ACTIVA,
   APARTADOS_TEXTO_TOTALES,
   APARTADOS_ES_LLENOS,
   APARTADOS_CA_LLENOS,
   APARTADOS_UK_LLENOS,
   IDIOMAS,
   EVALUACIONES,
   ACTIVIDADES,
   COMPETENCIAS,
   NUMERO_MODIFICACIONES,
   NUMERO_COMENTARIOS,
   ASIGNATURA_RELACIONADA
)
   bequeath definer as
   select a."ID",
          a."CURSO_ACA",
          a."COD_ASIGNATURA",
          a."ASIGNATURA_ID_ORIGEN",
          a."FECHA_INI",
          a."FECHA_FIN",
          a."ESTADO",
          a."PERSONA_ID",
          a."AUTORIZA_PAS",
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_ca
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_ca),
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_es
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_es),
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_uk
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_uk),
          a."FECHA_FINALIZACION",
          a."MODIFICACIONES",
          a."TIPO_COMPARTIDA",
          a."EN_PLAZO",
          at.tipo,
          a.cod_asignatura || ' - ' || at.nombre_ca                                                                           nombre,
          ta.curso_id,
          ta.estudio_id,
          busca_nombre_persona(a.persona_id, 2)                                                                               nombre_profesor,
          case when sysdate between nvl(a.fecha_ini, ca.FECHA_INI) and (nvl(a.fecha_fin, ca.fecha_fin) + 1) then 1 else 0 end as activa,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo))
             apartados_texto_totales,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_es), empty_clob())
                  != 0)
             apartados_es_llenos,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_ca), empty_clob())
                  != 0)
             apartados_ca_llenos,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_uk), empty_clob())
                  != 0)
             apartados_uk_llenos,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_idiomas
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             idiomas,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_evaluacion
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             evaluaciones,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_actividades
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             actividades,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_competencias
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             competencias,
          (select sum(modificado_ca + modificado_es + modificado_uk)
             from gdo_asi_textos
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             numero_modificaciones,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_comentarios
            where asignatura_id = a.id)
             numero_comentarios,
          (select cod_asignatura
             from gdo_asignaturas
            where id = a.asignatura_id_origen)
             asignatura_relacionada
     from gdo_asignaturas a
          join gdo_ext_asi_todas at on a.cod_asignatura = at.id
          join gdo_ext_titulaciones_asi ta on ta.cod_asignatura = a.cod_asignatura
          join gdo_cursos_academicos ca on ca.curso_aca = a.curso_aca
    where ca.ACTIVO = 'S';

ALTER TABLE UJI_GUIASDOCENTES.GDO_ASIGNATURAS ADD (deficiencias VARCHAR2(4000 Byte));
ALTER TABLE UJI_GUIASDOCENTES.GDO_ASIGNATURAS MODIFY estado VARCHAR2(2 char);

ALTER TABLE UJI_GUIASDOCENTES.GDO_ASIGNATURAS ADD (EN_SLT VARCHAR2(1 Char) DEFAULT 'N');

ALTER TABLE UJI_GUIASDOCENTES.GDO_ASIGNATURAS ADD
CONSTRAINT GDO_ASIGNATURAS_C01
 CHECK (EN_SLT in ('S', 'N'))
 ENABLE
 VALIDATE;

 update UJI_GUIASDOCENTES.gdo_Asignaturas set en_slt = 'S' where estado = 'T';
 update UJI_GUIASDOCENTES.gdo_Asignaturas set estado = 'F' where en_slt = 'S';

 create or replace force view UJI_GUIASDOCENTES.GDO_VW_TRADUCIR_GUIAS
 (
    ID,
    COD_ASIGNATURA,
    CURSO_ACA,
    NOMBRE,
    TIPO,
    ESTADO,
    EN_SLT,
    BASADA_EN,
    UK,
    PEND_CA,
    PEND_ES,
    PEND_UK,
    MOSTRAR_UK
 )
    bequeath definer as
    select "ID", "COD_ASIGNATURA", "CURSO_ACA", "NOMBRE", "TIPO", "ESTADO", "EN_SLT", "BASADA_EN", "UK", "PEND_CA", "PEND_ES", "PEND_UK", "MOSTRAR_UK"
      from (select a.id,
                   a.cod_asignatura,
                   a.curso_aca,
                   tt.nombre_ca                                        nombre,
                   tt.tipo,
                   a.estado,
                   a.en_slt,
                   a2.cod_asignatura                                   basada_en,
                   gri_www.eujifu22898b(a.cod_asignatura, a.curso_aca) uk,
                   decode(a.en_slt,
                          'N', 0,
                          (select count(*)
                             from uji_guiasdocentes.gdo_asi_textos t
                            where asignatura_id = a.id and fecha_slt_ca is null))
                      pend_ca,
                   decode(a.en_slt,
                          'N', 0,
                          (select count(*)
                             from uji_guiasdocentes.gdo_asi_textos t
                            where asignatura_id = a.id and fecha_slt_es is null))
                      pend_es,
                   decode(gri_www.eujifu22898b(a.cod_asignatura, a.curso_aca),
                          'S', decode(a.en_slt,
                                      'N', 0,
                                      (select count(*)
                                         from uji_guiasdocentes.gdo_asi_textos t
                                        where asignatura_id = a.id and fecha_slt_uk is null)),
                          0)
                      pend_uk,
                   'N'                                                 mostrar_uk
              from uji_guiasdocentes.gdo_asignaturas a, uji_guiasdocentes.gdo_asignaturas a2, uji_guiasdocentes.gdo_ext_asi_todas tt
             where a.estado in ('T', 'F') and a.asignatura_id_origen = a2.id(+) and a.cod_asignatura = tt.id and a2.cod_asignatura is null
            union
            select a.id,
                   a.cod_asignatura,
                   a.curso_aca,
                   tt.nombre_ca                                        nombre,
                   tt.tipo,
                   a.estado,
                   a.en_slt,
                   a2.cod_asignatura                                   basada_en,
                   gri_www.eujifu22898b(a.cod_asignatura, a.curso_aca) uk,
                   decode(a.en_slt,
                          'N', 0,
                          (select count(*)
                             from uji_guiasdocentes.gdo_asi_textos t
                            where asignatura_id = a.id and fecha_slt_ca is null))
                      pend_ca,
                   decode(a.en_slt,
                          'N', 0,
                          (select count(*)
                             from uji_guiasdocentes.gdo_asi_textos t
                            where asignatura_id = a.id and fecha_slt_es is null))
                      pend_es,
                   decode(gri_www.eujifu22898b(a.cod_asignatura, a.curso_aca),
                          'S', decode(a.en_slt,
                                      'N', 0,
                                      (select count(*)
                                         from uji_guiasdocentes.gdo_asi_textos t
                                        where asignatura_id = a.id and fecha_slt_uk is null)),
                          0)
                      pend_uk,
                   'S'                                                 mostrar_uk
              from uji_guiasdocentes.gdo_asignaturas a, uji_guiasdocentes.gdo_asignaturas a2, uji_guiasdocentes.gdo_ext_asi_todas tt
             where a.estado in ('T', 'F') and a.asignatura_id_origen = a2.id(+) and a.cod_asignatura = tt.id and gri_www.eujifu22898b(a.cod_asignatura, a.curso_aca) = 'S' and a2.cod_asignatura is null)
           x;

create or replace force view UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS
(
   ID,
   CURSO_ACA,
   COD_ASIGNATURA,
   ASIGNATURA_ID_ORIGEN,
   FECHA_INI,
   FECHA_FIN,
   ESTADO,
   EN_SLT,
   PERSONA_ID,
   AUTORIZA_PAS,
   FECHA_VALIDACION_CA,
   FECHA_VALIDACION_ES,
   FECHA_VALIDACION_UK,
   FECHA_FINALIZACION,
   MODIFICACIONES,
   TIPO_COMPARTIDA,
   EN_PLAZO,
   TIPO,
   NOMBRE,
   CURSO_ID,
   ESTUDIO_ID,
   NOMBRE_PROFESOR,
   ACTIVA,
   APARTADOS_TEXTO_TOTALES,
   APARTADOS_ES_LLENOS,
   APARTADOS_CA_LLENOS,
   APARTADOS_UK_LLENOS,
   IDIOMAS,
   EVALUACIONES,
   ACTIVIDADES,
   COMPETENCIAS,
   NUMERO_MODIFICACIONES,
   NUMERO_COMENTARIOS,
   ASIGNATURA_RELACIONADA
)
   bequeath definer as
   select a."ID",
          a."CURSO_ACA",
          a."COD_ASIGNATURA",
          a."ASIGNATURA_ID_ORIGEN",
          a."FECHA_INI",
          a."FECHA_FIN",
          a."ESTADO",
          a."EN_SLT",
          a."PERSONA_ID",
          a."AUTORIZA_PAS",
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_ca
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_ca),
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_es
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_es),
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_uk
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_uk),
          a."FECHA_FINALIZACION",
          a."MODIFICACIONES",
          a."TIPO_COMPARTIDA",
          a."EN_PLAZO",
          at.tipo,
          a.cod_asignatura || ' - ' || at.nombre_ca                                                                           nombre,
          ta.curso_id,
          ta.estudio_id,
          busca_nombre_persona(a.persona_id, 2)                                                                               nombre_profesor,
          case when sysdate between nvl(a.fecha_ini, ca.FECHA_INI) and (nvl(a.fecha_fin, ca.fecha_fin) + 1) then 1 else 0 end as activa,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo))
             apartados_texto_totales,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_es),
                  empty_clob()) != 0)
             apartados_es_llenos,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_ca),
                  empty_clob()) != 0)
             apartados_ca_llenos,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_uk),
                  empty_clob()) != 0)
             apartados_uk_llenos,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_idiomas
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             idiomas,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_evaluacion
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             evaluaciones,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_actividades
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             actividades,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_competencias
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             competencias,
          (select sum(modificado_ca + modificado_es + modificado_uk)
             from gdo_asi_textos
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             numero_modificaciones,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_comentarios
            where asignatura_id = a.id)
             numero_comentarios,
          (select cod_asignatura
             from gdo_asignaturas
            where id = a.asignatura_id_origen)
             asignatura_relacionada
     from gdo_asignaturas a
          join gdo_ext_asi_todas at on a.cod_asignatura = at.id
          join gdo_ext_titulaciones_asi ta on ta.cod_asignatura = a.cod_asignatura
          join gdo_cursos_academicos ca on ca.curso_aca = a.curso_aca
    where ca.ACTIVO = 'S';

create view GDO_EXT_CENTROS as
   select id, nombre
     from est_ubic_estructurales
    where id in (2, 3, 4, 2922);

ALTER TABLE UJI_GUIASDOCENTES.GDO_ASIGNATURAS ADD (FECHA_ULTIMA_MODIFICACION DATE);

CREATE OR REPLACE TRIGGER UJI_GUIASDOCENTES.FECHA_MOD_GDO_ASI_ACTIVIDADES
   after insert or update or delete
   on UJI_GUIASDOCENTES.GDO_ASI_ACTIVIDADES
   for each row
begin
   update gdo_asignaturas
      set fecha_ultima_modificacion = sysdate
    where id = :new.asignatura_id;
end;
/

CREATE OR REPLACE TRIGGER UJI_GUIASDOCENTES.FECHA_MOD_GDO_ASI_COMENTARIOS
   after insert or update or delete
   on UJI_GUIASDOCENTES.GDO_ASI_COMENTARIOS
   for each row
begin
   update gdo_asignaturas
      set fecha_ultima_modificacion = sysdate
    where id = :new.asignatura_id;
end;
/

CREATE OR REPLACE TRIGGER UJI_GUIASDOCENTES.FECHA_MOD_GDO_ASI_COMPETENCIAS
   after insert or update or delete
   on UJI_GUIASDOCENTES.GDO_ASI_COMPETENCIAS
   for each row
begin
   update gdo_asignaturas
      set fecha_ultima_modificacion = sysdate
    where id = :new.asignatura_id;
end;
/

CREATE OR REPLACE TRIGGER UJI_GUIASDOCENTES.FECHA_MOD_GDO_ASI_EVALUACION
   after insert or update or delete
   on UJI_GUIASDOCENTES.GDO_ASI_EVALUACION
   for each row
begin
   update gdo_asignaturas
      set fecha_ultima_modificacion = sysdate
    where id = :new.asignatura_id;
end;
/

CREATE OR REPLACE TRIGGER UJI_GUIASDOCENTES.FECHA_MOD_GDO_ASI_IDIOMAS
   after insert or update or delete
   on UJI_GUIASDOCENTES.GDO_ASI_IDIOMAS
   for each row
begin
   update gdo_asignaturas
      set fecha_ultima_modificacion = sysdate
    where id = :new.asignatura_id;
end;
/

CREATE OR REPLACE TRIGGER UJI_GUIASDOCENTES.FECHA_MOD_GDO_ASI_TEXTOS
   after insert or update or delete
   on UJI_GUIASDOCENTES.GDO_ASI_TEXTOS
   for each row
begin
   update gdo_asignaturas
      set fecha_ultima_modificacion = sysdate
    where id = :new.asignatura_id;
end;
/

create or replace force view UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS
(
   ID,
   CURSO_ACA,
   COD_ASIGNATURA,
   ASIGNATURA_ID_ORIGEN,
   FECHA_INI,
   FECHA_FIN,
   ESTADO,
   EN_SLT,
   DEFICIENCIAS,
   PERSONA_ID,
   AUTORIZA_PAS,
   FECHA_VALIDACION_CA,
   FECHA_VALIDACION_ES,
   FECHA_VALIDACION_UK,
   FECHA_FINALIZACION,
   MODIFICACIONES,
   TIPO_COMPARTIDA,
   EN_PLAZO,
   TIPO,
   NOMBRE,
   CURSO_ID,
   ESTUDIO_ID,
   NOMBRE_PROFESOR,
   ACTIVA,
   APARTADOS_TEXTO_TOTALES,
   APARTADOS_ES_LLENOS,
   APARTADOS_CA_LLENOS,
   APARTADOS_UK_LLENOS,
   IDIOMAS,
   EVALUACIONES,
   ACTIVIDADES,
   COMPETENCIAS,
   NUMERO_MODIFICACIONES,
   NUMERO_COMENTARIOS,
   ASIGNATURA_RELACIONADA
)
   bequeath definer as
   select a."ID",
          a."CURSO_ACA",
          a."COD_ASIGNATURA",
          a."ASIGNATURA_ID_ORIGEN",
          a."FECHA_INI",
          a."FECHA_FIN",
          a."ESTADO",
          a."EN_SLT",
          a."DEFICIENCIAS",
          a."PERSONA_ID",
          a."AUTORIZA_PAS",
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_ca
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_ca),
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_es
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_es),
          nvl2(a.asignatura_id_origen,
               (select fecha_validacion_uk
                  from gdo_asignaturas
                 where id = a.asignatura_id_origen),
               a.fecha_validacion_uk),
          a."FECHA_FINALIZACION",
          a."MODIFICACIONES",
          a."TIPO_COMPARTIDA",
          a."EN_PLAZO",
          at.tipo,
          a.cod_asignatura || ' - ' || at.nombre_ca                                                                           nombre,
          ta.curso_id,
          ta.estudio_id,
          busca_nombre_persona(a.persona_id, 2)                                                                               nombre_profesor,
          case when sysdate between nvl(a.fecha_ini, ca.FECHA_INI) and (nvl(a.fecha_fin, ca.fecha_fin) + 1) then 1 else 0 end as activa,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo))
             apartados_texto_totales,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_es),
                  empty_clob()) != 0)
             apartados_es_llenos,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_ca),
                  empty_clob()) != 0)
             apartados_ca_llenos,
          (select count(*)
             from gdo_asi_textos txt
                  join gdo_apartados apar on apar.id = txt.apartado_id
            where asignatura_id = nvl(a.asignatura_id_origen, a.id) and obligatorio = 'S' and (apar.tipo_estudio = 'T' or apar.tipo_estudio = at.tipo) and dbms_lob.compare(trim(txt.texto_uk),
                  empty_clob()) != 0)
             apartados_uk_llenos,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_idiomas
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             idiomas,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_evaluacion
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             evaluaciones,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_actividades
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             actividades,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_competencias
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             competencias,
          (select sum(modificado_ca + modificado_es + modificado_uk)
             from gdo_asi_textos
            where asignatura_id = nvl(a.asignatura_id_origen, a.id))
             numero_modificaciones,
          (select decode(count(*), 0, 0, 1)
             from gdo_asi_comentarios
            where asignatura_id = a.id)
             numero_comentarios,
          (select cod_asignatura
             from gdo_asignaturas
            where id = a.asignatura_id_origen)
             asignatura_relacionada
     from gdo_asignaturas a
          join gdo_ext_asi_todas at on a.cod_asignatura = at.id
          join gdo_ext_titulaciones_asi ta on ta.cod_asignatura = a.cod_asignatura
          join gdo_cursos_academicos ca on ca.curso_aca = a.curso_aca
    where ca.ACTIVO = 'S';

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS
(
    ID,
    NOMBRE_CA,
    NOMBRE_ES,
    NOMBRE_UK,
    TIPO,
    NOMBRE,
    INTERUNIVERSITARIO,
    COORDINA_UJI,
    CENTRO_ID,
    ERASMUS_MUNDUS
)
BEQUEATH DEFINER
AS
    SELECT id,
           nombre     nombre_ca,
           nomval     nombre_es,
           nomang     nombre_uk,
           'G'        tipo,
           nombre,
           'N'        interuniversitario,
           'N'        coordina_uji,
           uest_id,
           'N'        erasmus_mundus
      FROM pod_titulaciones
     WHERE id BETWEEN 201 AND 9999
    UNION ALL
    SELECT id,
           nombre                                                                      nombre_ca,
           nombre_es,
           nombre_uk,
           'M'                                                                         tipo,
           nombre,
           DECODE (id,  42127, 'N',  42161, 'N',  42108, 'N',  interuniversitario)     interuniversitario,
           coordina_uji,
           uest_id,
           erasmus_mundus
      FROM pop_masters
     WHERE oficial = 'S' AND id NOT LIKE '420%';