
-- gdo_ext_personas 
drop table uji_guiasdocentes.gdo_ext_personas cascade constraints;

grant references on gri_per.per_personas to uji_guiasdocentes;

grant select on gri_per.per_personas to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_masters to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_grupos_pdi to uji_guiasdocentes with grant option;
grant select on grh_grh.grh_vw_contrataciones_ult to uji_guiasdocentes with grant option;

alter table uji_guiasdocentes.gdo_asignaturas
  add constraint gdo_asignaturas_gdo_vw_per_fk foreign key (persona_id)
        references per_personas (id);
  
alter table uji_guiasdocentes.gdo_directores_extra
  add constraint gdo_admin_gdo_vw_per_fk foreign key (persona_id)
        references per_personas (id);
  
alter table uji_guiasdocentes.gdo_asi_permisos
  add constraint gdo_asi_permisos_gdo_vw_per_fk foreign key (persona_id)
        references per_personas (id);

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_PERSONAS (ID,
                                                                 NOMBRE,
                                                                 APELLIDO1,
                                                                 APELLIDO2,
                                                                 ACTIVIDAD_ID,
                                                                 UBICACION_ID,
                                                                 ES_AUXILIAR,
                                                                 LOGIN
                                                                ) AS
   select id, nombre, apellido1, apellido2, act_id, ubicacion_id, es_auxiliar, login
   from   (select id, nombre, apellido1, apellido2, act_id, ubicacion_id, es_auxiliar, login,
                  row_number () over (partition by id order by ubicacion_id desc) orden
           from   (                                                                                      /* pas y pdi */
                   select distinct id, nombre, apellido1, apellido2, act_id,
                                   nvl ((select ubicacion_id
                                         from   grh_vw_contrataciones_ult c
                                         where  x.id = per_id
                                         and    x.act_id = c.act_id), 0) ubicacion_id,
                                   decode (act_id,
                                           'PDI', 'N',
                                           nvl ((SELECT distinct 'S'
                                                 FROM            grh_vw_contrataciones_ult cu,
                                                                 grh_plazas p,
                                                                 est_ubic_estructurales ue
                                                 WHERE           p.n_plaza = cu.n_plaza
                                                 AND             p.ID = cu.ID
                                                 AND             p.uest_id = ue.ID
                                                 AND             ue.tuest_id IN ('DE')
                                                 AND             ue.status = 'A'
                                                 AND             cu.cper_act_id = 'PAS'
                                                 and             p.nombre like '%dministrat%'
                                                 and             cu.per_id = x.id),
                                                'N')
                                          ) es_auxiliar,
                                   substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
                   from            (select id, nombre, apellido1, apellido2, act_id, fecha_fin,
                                           row_number () over (partition by id order by fecha_fin desc,
                                            act_id asc) orden
                                    from   (select p.id, nombre, apellido1, apellido2,
                                                   decode (svi_vin_id, 4, 'PAS', 9, 'PDI', 'Error') act_id,
                                                   nvl (fecha_fin, add_months (trunc (sysdate), 1000)) fecha_fin
                                            from   per_personas_subvinculos ps,
                                                   per_personas p
                                            where  svi_vin_id in (9, 4)
                                            and    per_id = p.id)) x
                   where           orden = 1
                   union
                   /* pdi extra */
                   select id, nombre, apellido1, apellido2, 'PDI' act_id, 0 ubicacion_id, 'N' es_auxiliar,
                          substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
                   from   per_personas
                   where  id in (252381, 95178, 65297, 184937, 8218, 467765)
                   union
                   /* pas extra */
                   select id, nombre, apellido1, apellido2, 'PAS' act_id, 0 ubicacion_id, 'N' es_auxiliar,
                          substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
                   from   per_personas
                   where  id in (840, 143302, 93307)
                   union
                   /* coordinadores de master */
                   select   per_id, nombre, apellido1, apellido2, act_id, min (ubicacion_id), es_auxiliar, login
                   from     (select distinct per_id, nombre, apellido1, apellido2, 'PDI' act_id,
                                             (select min (ubicacion_id)
                                              from   pop_asignaturas_master am,
                                                     pop_comisiones c,
                                                     grh_vw_contrataciones_ult cont
                                              where  am.pasi_id = g.pgr_pof_pasi_id
                                              and    am.pmas_id not in (42000, 42100)
                                              and    am.pmas_id = c.pmas_id
                                              and    c.per_id = cont.per_id
                                              and    act_id = 'PDI') ubicacion_id,
                                             'N' es_auxiliar,
                                             substr (busca_cuenta (id), 1, instr (busca_cuenta (id), '@') - 1) login
                             from            pop_grupos_pdi g,
                                             per_personas p
                             where           pgr_pof_curso_aca >= (select min (curso_aca)
                                                                   from   gdo_cursos_academicos
                                                                   where  activo = 'S') - 2
                             and             per_id not in (select per_id
                                                            from   grh_vw_contrataciones_ult)
                             and             pgr_pof_pmas_id in (select id
                                                                 from   pop_masters
                                                                 where  oficial = 'S')
                             and             per_id = p.id)
                   where    login is not null
                   group by per_id,
                            nombre,
                            apellido1,
                            apellido2,
                            act_id,
                            es_auxiliar,
                            login)
           where  login is not null
                                   --and    ubicacion_id <> 0
          )
   where  orden = 1;



 -- gdo_ext_asi_todas 

 drop table uji_guiasdocentes.gdo_ext_asi_todas cascade constraints;

grant references on gra_pod.pod_asignaturas to uji_guiasdocentes;

grant select on gra_exp.exp_v_asi_todas to uji_guiasdocentes with grant option;


alter table uji_guiasdocentes.gdo_asignaturas
  add constraint gdo_asignaturas_pod_asi_fk foreign key (cod_asignatura)
        references gra_pod.pod_asignaturas (id);

create or replace view uji_guiasdocentes.gdo_ext_asi_todas as
 select id, nombre_ca, nombre_es, nombre_uk, decode(tipo,'12C','G',tipo) tipo
 from gra_exp.exp_v_asi_todas
 where tipo in ('12C','M')
 and length(id) = 6;
  
-- gdo_ext_titu_todas   
drop table uji_guiasdocentes.gdo_ext_titu_todas cascade constraints;

grant references on gra_pod.pod_titulaciones to uji_guiasdocentes;

grant select on gra_exp.exp_v_titu_todas to uji_guiasdocentes with grant option;

alter table uji_guiasdocentes.gdo_directores_extra
  add constraint gdo_admin_exp_v_titu_fk foreign key (estudio_id)
        references pod_titulaciones (id);

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_TITU_TODAS (ID, NOMBRE_CA, NOMBRE_ES, NOMBRE_UK, TIPO) AS
   select id, nombre nombre_ca, nombre_es, nvl(nombre_uk, nombre_es) nombre_uk, tipo
   from   gra_exp.exp_v_titu_todas
   where  (    oficial = 'S'
           and activa = 'S'
           and tipo in ('G', 'M'))
   or     id in (229, 230);

 
-- gdo_ext_titulaciones_per   
drop table uji_guiasdocentes.gdo_ext_titulaciones_per;

grant select on grh_grh.grh_cargos_per to uji_guiasdocentes with grant option;
grant select on gra_pod.pod_titulaciones to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_comisiones to uji_guiasdocentes with grant option;
grant select on gra_exp.exp_v_titu_todas to uji_guiasdocentes with grant option;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_TITULACIONES_PER (ESTUDIO_ID,
                                                                         NOMBRE,
                                                                         PERSONA_ID,
                                                                         CURSO_ID,
                                                                         CARGO_ID
                                                                        ) AS
   select tit_id, nombre, per_id, curso_id, cargo_id
   from   (select tit_id, per_id, nvl (ciclo_cargo, 0) curso_id, 'DIR' cargo_id
           from   grh_grh.grh_cargos_per
           where  crg_id in (108, 192, 193, 257, 258)
           and    (   f_fin is null
                   or f_fin >= sysdate)
           and    tit_id is not null
           union
           select tit_id, per_id, nvl (ciclo_cargo, 1) curso_id, 'COR' cargo_id
           from   grh_grh.grh_cargos_per
           where  crg_id in (292, 293, 305, 307)
           and    (   f_fin is null
                   or f_fin >= sysdate)
           and    tit_id is not null
           union
           select a.estudio_id, a.persona_id, nvl (curso_id, 0) curso_id, tipo_permiso cargo_id
           from   uji_guiasdocentes.gdo_directores_extra a
           union
           select pmas_id, per_id, 0 curso_id, 'DIR' cargo_id
           from   gra_pop.pop_comisiones
           where  crg_id in (1, 3)
           and    trunc (fecha_ini) <= trunc (sysdate)
           and    trunc (nvl (fecha_fin, sysdate)) >= trunc (sysdate)) x,
          gra_exp.exp_v_titu_todas t
   where  tit_id = t.id(+)
   and    (   t.tipo = 'G'
           or (    t.tipo = 'M'
               and t.id like '421%'))
   union all
   select tt.id estudio_id, tt.nombre_ca, persona_id, 0 curso_id, 'ADM' cargo_id
   from   gdo_ext_titu_todas tt,
          uji_apa.APA_APLICACIONES_EXTRAS e
   where  (   tipo = 'G'
           or (    tipo = 'M'
               and tt.id like '421%'))
   and    role_id = 1;

-- gdo_ext_titulaciones_asi   
drop table uji_guiasdocentes.gdo_ext_titulaciones_asi;

grant select on gra_pop.pop_asignaturas_master to uji_guiasdocentes with grant option;
grant select on gra_pod.pod_asignaturas_titulaciones to uji_guiasdocentes with grant option;

create or replace view uji_guiasdocentes.gdo_ext_titulaciones_asi (cod_asignatura, estudio_id) as
select asi_id, tit_id
  from gra_pod.pod_asignaturas_titulaciones
 union
select pasi_id, pmas_id
  from gra_pop.pop_asignaturas_master;

-- vistas complementarias

DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle CASCADE CONSTRAINTS ;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_act CASCADE CONSTRAINTS ;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_act_comp CASCADE CONSTRAINTS ;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_eval CASCADE CONSTRAINTS ;
DROP TABLE uji_guiasdocentes.gdo_ext_asi_detalle_rdo CASCADE CONSTRAINTS ;

grant select on gra_pod.pod_asi_detalle to uji_guiasdocentes with grant option;
grant select on gra_pod.pod_asi_detalle_act to uji_guiasdocentes with grant option;
grant select on gra_pod.pod_asi_detalle_eval to uji_guiasdocentes with grant option;
grant select on gra_pod.pod_asi_detalle_rdo to uji_guiasdocentes with grant option;
grant select on gra_pod.pod_asi_detalle_act_comp to uji_guiasdocentes with grant option;

grant select on gra_pop.pop_asi_detalle to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_asi_detalle_act to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_asi_detalle_eval to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_asi_detalle_rdo to uji_guiasdocentes with grant option;
grant select on gra_pop.pop_asi_detalle_act_comp to uji_guiasdocentes with grant option;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE (COD_ASIGNATURA,
                                                                    CURSO_ACA_INI,
                                                                    CURSO_ACA_FIN,
                                                                    HORAS_AL,
                                                                    PREREQUISITOS_CA,
                                                                    PREREQUISITOS_ES,
                                                                    CONTENIDOS_CA,
                                                                    CONTENIDOS_ES,
                                                                    JUSTIFICACION_CA,
                                                                    JUSTIFICACION_ES
                                                                   ) AS
   select ASI_ID, CURSO_ACA_INI, CURSO_ACA_FIN, HORAS_AL, PREREQUISITOS_CA, PREREQUISITOS_ES,
          CONTENIDOS_CA, CONTENIDOS_ES, JUSTIFICACION_CA, JUSTIFICACION_ES
   from   gra_pod.pod_asi_detalle
   union all
   select PASI_ID, CURSO_ACA_INI, CURSO_ACA_FIN, HORAS_AL, PREREQUISITOS_CA, PREREQUISITOS_ES,
          CONTENIDOS_CA, CONTENIDOS_ES, JUSTIFICACION_CA, JUSTIFICACION_ES
   from   gra_pop.pop_asi_detalle
   ;
   
CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_ACT (COD_ASIGNATURA,
                                                                        CURSO_ACA_INI,
                                                                        ACT_ID,
                                                                        NOMBRE_CA,
                                                                        NOMBRE_ES,
                                                                        H_PRE,
                                                                        H_NO_PRE,
                                                                        TIPO_GRUPO,
                                                                        TIPO,
                                                                        ORDEN
                                                                       ) AS
   select asi_id, curso_aca_ini, act_id, nombre_ca, nombre_es, h_pre, h_no_pre, tipo_grupo, tipo, orden
   from   gra_pod.pod_asi_detalle_act
   union all
   select pasi_id, curso_aca_ini, act_id, nombre_ca, nombre_es, h_pre, h_no_pre, tipo_grupo, tipo, orden
   from   gra_pop.pop_asi_detalle_act
   ;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_ACT_COMP (COD_ASIGNATURA,
                                                                             CURSO_ACA_INI,
                                                                             ACT_ID,
                                                                             COMP_ID,
                                                                             NOMBRE_CA,
                                                                             NOMBRE_ES,
                                                                             NOMBRE_UK,
                                                                             TIPO
                                                                            ) AS
   select asi_id, curso_aca_ini, act_id, comp_id, nombre_ca, nombre_es, nombre_uk, tipo
   from   gra_pod.pod_asi_detalle_act_comp
   union all
   select pasi_id, curso_aca_ini, act_id, comp_id, nombre_ca, nombre_es, nombre_uk, tipo
   from   gra_pop.pop_asi_detalle_act_comp;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_EVAL (COD_ASIGNATURA,
                                                                         CURSO_ACA_INI,
                                                                         EVAL_ID,
                                                                         NOMBRE_CA,
                                                                         NOMBRE_ES,
                                                                         NOMBRE_UK,
                                                                         PONDERACION
                                                                        ) AS
   select asi_id, curso_aca_ini, eval_id, nombre_ca, nombre_es, nombre_uk, ponderacion
   from   gra_pod.pod_asi_detalle_eval
   union all
   select pasi_id, curso_aca_ini, eval_id, nombre_ca, nombre_es, nombre_uk, ponderacion
   from   gra_pop.pop_asi_detalle_eval
   ;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_EXT_ASI_DETALLE_RDO (COD_ASIGNATURA,
                                                                        CURSO_ACA_INI,
                                                                        RDO_ID,
                                                                        NOMBRE_CA,
                                                                        NOMBRE_ES,
                                                                        NOMBRE_UK
                                                                       ) AS
   select asi_id, curso_aca_ini, rdo_id, nombre_ca, nombre_es, nombre_uk
   from   gra_pod.pod_asi_detalle_rdo
   union all
   select pasi_id, curso_aca_ini, rdo_id, nombre_ca, nombre_es, nombre_uk
   from   gra_pop.pop_asi_detalle_rdo;
   
drop table uji_guiasdocentes.gdo_vw_asignaturas_per cascade constraints;

CREATE OR REPLACE VIEW UJI_GUIASDOCENTES.gdo_vw_asignaturas_per (ASIGNATURA_ID,
                                                                    PERSONA_ID,
                                                                    CURSO_ACA
                                                                   ) AS
   select a."ID", a."PERSONA_ID", a."CURSO_ACA"
   from   gdo_asignaturas a
   union
   select a."ID", ap."PERSONA_ID", a."CURSO_ACA"
   from   gdo_asi_permisos ap,
          gdo_asignaturas a
   where  a.id = ap.asignatura_id
   union
   select a."ID", de."PERSONA_ID", a."CURSO_ACA"
   from   gdo_asignaturas a,
          gdo_ext_titulaciones_asi ta,
          gdo_directores_extra de
   where  a.cod_asignatura = ta.cod_asignatura
   and    de.estudio_id = ta.estudio_id;

   
   
-- Permisos para el usuario puente 
grant select, insert, update, delete on uji_guiasdocentes.gdo_directores_extra to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_apartados to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_actividades to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_competencias to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_resultados to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_evaluacion to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_idiomas to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_permisos to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asignaturas to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_cursos_academicos to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_idiomas to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_asi_textos to uji_guiasdocentes_usr;
grant select, insert, update, delete on uji_guiasdocentes.gdo_tipo_actividades to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_vw_asignaturas_per to uji_guiasdocentes_usr;

grant select on uji_guiasdocentes.gdo_ext_asi_todas to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_personas to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_titu_todas to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_titulaciones_asi to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_titulaciones_per to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_asi_detalle to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_asi_detalle_act to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_asi_detalle_act_comp to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_asi_detalle_eval to uji_guiasdocentes_usr;
grant select on uji_guiasdocentes.gdo_ext_asi_detalle_rdo to uji_guiasdocentes_usr;


