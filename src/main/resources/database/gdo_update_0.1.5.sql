alter table uji_guiasdocentes.gdo_asi_comentarios  add
    ( 
     idioma VARCHAR2 (200)  
    ) 
;

alter table uji_guiasdocentes.gdo_asi_comentarios drop constraint gdo_asi_comentarios_CK;

ALTER TABLE uji_guiasdocentes.gdo_asi_comentarios 
    ADD CONSTRAINT gdo_asi_comentarios_CK 
    CHECK (idioma in ('ES','CA','UK','TD'))
;

alter table uji_guiasdocentes.gdo_apartados  drop constraint gdo_apartados_ob_CK;

ALTER TABLE uji_guiasdocentes.gdo_apartados 
    ADD CONSTRAINT gdo_apartados_ob_CK 
    CHECK (obligatorio in ('S','N'))
;
