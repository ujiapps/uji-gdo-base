/* ejecutar solo en desarrollo */
alter table UJI_GUIASDOCENTES.gdo_ext_titulaciones_asi add (curso_id number);

update UJI_GUIASDOCENTES.gdo_ext_titulaciones_asi 
set curso_id = trunc((to_number(substr(cod_asignatura,length(cod_asignatura)-1))+1)/10)+1;
commit;

alter table UJI_GUIASDOCENTES.gdo_ext_titulaciones_per add (curso_id number, cargo_id varchar2(200));

update UJI_GUIASDOCENTES.gdo_ext_titulaciones_per
set curso_id = 0, cargo_id = 'DIR'

insert into UJI_GUIASDOCENTES.gdo_ext_titulaciones_per (ESTUDIO_ID, NOMBRE, PERSONA_ID, CURSO_ID, CARGO_ID)
values(201, 'Grau en Relacions Laborals i Recursos Humans', 109, 1,'COR');

insert into UJI_GUIASDOCENTES.gdo_ext_titulaciones_per (ESTUDIO_ID, NOMBRE, PERSONA_ID, CURSO_ID, CARGO_ID)
values(201, 'Grau en Relacions Laborals i Recursos Humans', 110, 2,'COR');

insert into UJI_GUIASDOCENTES.gdo_ext_titulaciones_per (ESTUDIO_ID, NOMBRE, PERSONA_ID, CURSO_ID, CARGO_ID)
values(201, 'Grau en Relacions Laborals i Recursos Humans', 108, 3,'COR');

commit;

/* ejecutar siempre */

drop table UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS_PER;

DROP VIEW UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS_PER;

CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS_PER (ASIGNATURA_ID, PERSONA_ID, CURSO_ACA) AS
        /* asignaciones normales */
        select a."ID", a."PERSONA_ID", a."CURSO_ACA"
        from   gdo_asignaturas a
        where  persona_id is not null
        union
        /* asignaturas delegadas */
        select a."ID", ap."PERSONA_ID", a."CURSO_ACA"
        from   gdo_asi_permisos ap,
               gdo_asignaturas a
        where  a.id = ap.asignatura_id
        union
        /* directores de titulacion */
        select a."ID", tp."PERSONA_ID", a."CURSO_ACA"
        from   gdo_asignaturas a,
               gdo_ext_titulaciones_asi ta,
               gdo_ext_titulaciones_per tp
        where  a.cod_asignatura = ta.cod_asignatura
        and    tp.estudio_id = ta.estudio_id
        and    cargo_id in ('DIR', 'D+', 'ADM')
        union
        /* coordinadores de curso */
        select a."ID", tp."PERSONA_ID", a."CURSO_ACA"
        from   gdo_asignaturas a,
               gdo_ext_titulaciones_asi ta,
               gdo_ext_titulaciones_per tp
        where  a.cod_asignatura = ta.cod_asignatura
        and    tp.estudio_id = ta.estudio_id
        and    tp.cargo_id = 'COR'
        and    tp.curso_id = decode (tp.curso_id, 0, tp.curso_id, ta.curso_id)
        union
        /* pas de departamento */
        select a."ID", p2.id "PERSONA_ID", a."CURSO_ACA"
        from   gdo_asignaturas a,
               gdo_ext_personas p,
               gdo_ext_personas p2
        where  a.persona_id is not null
        and    a.persona_id = p.id
        and    p.ubicacion_id = p2.ubicacion_id
        and    p2.actividad_id = 'PAS'
        and    p2.es_auxiliar = 'S'
        and    p2.login is not null
        and    p2.ubicacion_id <> 0;

   
grant select on  UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS_PER to uji_guiasdocentes_usr;
  
alter table uji_guiasdocentes.gdo_apartados add (obligatorio varchar2(1) default 'S');

alter table uji_guiasdocentes.gdo_asignaturas add  (fecha_validacion_ca date, fecha_validacion_es date, fecha_validacion_uk date);




 
  