CREATE OR REPLACE FORCE VIEW UJI_GUIASDOCENTES.GDO_VW_ASIGNATURAS_PER (ASIGNATURA_ID, PERSONA_ID, CURSO_ACA,
                                                                       TIPO_ACCESO) AS
   select a."ID", a."PERSONA_ID", a."CURSO_ACA", 'AS' tipo
   from   gdo_asignaturas a
   where  persona_id is not null
   union all
/* asignaturas delegadas */
   select a."ID", ap."PERSONA_ID", a."CURSO_ACA", 'DE' tipo
   from   gdo_asi_permisos ap,
          gdo_asignaturas a
   where  a.id = ap.asignatura_id
   union all
/* directores de titulacion */
   select a."ID", tp."PERSONA_ID", a."CURSO_ACA", 'DIR' tipo
   from   gdo_asignaturas a,
          gdo_ext_titulaciones_asi ta,
          gdo_ext_titulaciones_per tp
   where  a.cod_asignatura = ta.cod_asignatura
   and    tp.estudio_id = ta.estudio_id
   and    cargo_id in ('DIR', 'D+', 'ADM')
   union all
/* coordinadores de curso */
   select a."ID", tp."PERSONA_ID", a."CURSO_ACA", 'COR' tipo
   from   gdo_asignaturas a,
          gdo_ext_titulaciones_asi ta,
          gdo_ext_titulaciones_per tp
   where  a.cod_asignatura = ta.cod_asignatura
   and    tp.estudio_id = ta.estudio_id
   and    tp.cargo_id = 'COR'
   and    tp.curso_id = decode (tp.curso_id, 0, tp.curso_id, ta.curso_id)
   union all
/* pas de departamento */
   select a."ID", p2.id "PERSONA_ID", a."CURSO_ACA", 'PAS' tipo
   from   gdo_asignaturas a,
          gdo_ext_personas p,
          gdo_ext_personas p2
   where  a.persona_id is not null
   and    a.persona_id = p.id
   and    p.ubicacion_id = p2.ubicacion_id
   and    p2.actividad_id = 'PAS'
   and    p2.es_auxiliar = 'S'
   and    p2.login is not null
   and    p2.ubicacion_id <> 0;